﻿-- 
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 6.3.323.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 15.01.2015 16:31:45
-- Версия сервера базы данных: 5.6.22
-- Выполните скрипт в базу arttime, чтобы синхронизировать ее с базой arttime_new
-- Пожалуйста, сохраните резервную копию вашей базы получателя перед запуском этого скрипта
-- 

--
-- Отключение внешних ключей
--
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

SET NAMES 'utf8';




--
-- Удалить таблицу "Task"
--
DROP TABLE IF EXISTS Task;

--
-- Изменить таблицу "Employee"
--
ALTER TABLE Employee
  CHANGE COLUMN department department VARCHAR(255) NOT NULL AFTER userName,
  CHANGE COLUMN email email VARCHAR(255) NOT NULL AFTER department,
  CHANGE COLUMN firstName firstName VARCHAR(255) NOT NULL AFTER email,
  CHANGE COLUMN lastName lastName VARCHAR(255) NOT NULL AFTER firstName,
  CHANGE COLUMN workLoad workLoad INT(11) NOT NULL AFTER lastName;

--
-- Создать таблицу "Filter"
--
CREATE TABLE Filter (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) DEFAULT NULL,
  owner VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX unique_filter_name_for_owner (owner, name)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Изменить таблицу "HourType"
--
ALTER TABLE HourType
  DROP INDEX type,
  CHANGE COLUMN actualTime actualTime BIT(1) DEFAULT NULL AFTER id,
  CHANGE COLUMN type type VARCHAR(255) NOT NULL AFTER actualTime;

ALTER TABLE HourType
  ADD UNIQUE INDEX constraint_unique_hourtype_name (type);

--
-- Изменить таблицу "ProjectStatus"
--
ALTER TABLE ProjectStatus
  CHANGE COLUMN startAt startAt DATE NOT NULL AFTER id,
  CHANGE COLUMN status status VARCHAR(255) NOT NULL AFTER startAt;

--
-- Создать таблицу "Settings"
--
DROP TABLE IF EXISTS Settings;

CREATE TABLE Settings (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  applicationBaseUrl VARCHAR(255) DEFAULT NULL,
  employeesSynchronizationEnabled BIT(1) DEFAULT NULL,
  execRoleMemberOf VARCHAR(255) DEFAULT NULL,
  integrationClientGroups VARCHAR(255) DEFAULT NULL,
  ldapBindCredentials VARCHAR(255) DEFAULT NULL,
  ldapBindDN VARCHAR(255) DEFAULT NULL,
  ldapCommonNameAttribute VARCHAR(255) DEFAULT NULL,
  ldapDepartmentAttribute VARCHAR(255) DEFAULT NULL,
  ldapEmployeeFilter VARCHAR(255) DEFAULT NULL,
  ldapFirstNameAttribute VARCHAR(255) DEFAULT NULL,
  ldapGroupMemberFilter VARCHAR(255) DEFAULT NULL,
  ldapLastNameAttribute VARCHAR(255) DEFAULT NULL,
  ldapMailAttribute VARCHAR(255) DEFAULT NULL,
  ldapPrincipalSuffix VARCHAR(255) DEFAULT NULL,
  ldapServerAddress VARCHAR(255) DEFAULT NULL,
  ldapServerPort VARCHAR(255) DEFAULT NULL,
  ldapUserContextDN VARCHAR(255) DEFAULT NULL,
  ldapUserFilter VARCHAR(255) DEFAULT NULL,
  ldapUserNameAttribute VARCHAR(255) DEFAULT NULL,
  smptHostName VARCHAR(255) DEFAULT NULL,
  smtpPassword VARCHAR(255) DEFAULT NULL,
  smtpPortNumber VARCHAR(255) DEFAULT NULL,
  smtpUsername VARCHAR(255) DEFAULT NULL,
  teamSynchronizationEnabled BIT(1) DEFAULT NULL,
  timerHoursInterval INT(11) NOT NULL,
  timerMinutesInterval INT(11) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci;

INSERT INTO Settings (id, employeesSynchronizationEnabled, execRoleMemberOf, integrationClientGroups, ldapBindCredentials, ldapBindDN, ldapCommonNameAttribute, ldapDepartmentAttribute, ldapEmployeeFilter, ldapFirstNameAttribute, ldapGroupMemberFilter, ldapLastNameAttribute, ldapMailAttribute, ldapPrincipalSuffix, ldapServerAddress, ldapServerPort, ldapUserContextDN, ldapUserFilter, ldapUserNameAttribute, smptHostName, smtpPassword, smtpPortNumber, smtpUsername, teamSynchronizationEnabled, timerHoursInterval, timerMinutesInterval)
  VALUES (1, FALSE, '_ART-PRJ-ART-TIME-EXEC,_ART-PRJ-ART-TIME-PG', '_time-access', 'qwertyu_777', 'dev-hrms', 'CN', 'physicalDeliveryOfficeName', '(&(objectClass=user)(memberof=CN=_wiki-users,CN=Users,DC=ARTGROUP,DC=local)(!(physicalDeliveryOfficeName=DND))(!(userAccountControl:1.2.840.113556.1.4.803:=2)))', 'givenName', '(&(objectClass=user)(memberof=CN={0},CN=Users,DC=ARTGROUP,DC=local)(memberof=CN=_wiki-users,CN=Users,DC=ARTGROUP,DC=local)(!(physicalDeliveryOfficeName=DND))(!(userAccountControl:1.2.840.113556.1.4.803:=2)))', 'sn', 'mail', '@ARTGROUP.local', 'dc1-m.ARTGROUP.local', '389', 'CN=Users,DC=ARTGROUP,DC=local', '(&(objectclass=*)(sAMAccountName={0}))', 'sAMAccountName', 'exch1-m.ARTGROUP.local', NULL, '25', 'jira-art-time', FALSE, 1, 0);

--
-- Изменить таблицу "WorkdaysCalendar"
--
ALTER TABLE WorkdaysCalendar
  DROP COLUMN isDefault,
  DROP INDEX name;

ALTER TABLE WorkdaysCalendar
  ADD UNIQUE INDEX constraint_unique_calendar_name (name);

--
-- Изменить таблицу "Day"
--
ALTER TABLE Day
  DROP FOREIGN KEY FK10B7CB363BFF5,
  DROP INDEX FK10B7CB363BFF5,
  CHANGE COLUMN date date DATE NOT NULL AFTER id,
  CHANGE COLUMN isWorking isWorking BIT(1) DEFAULT NULL AFTER date;

ALTER TABLE Day
  ADD CONSTRAINT FK_rx2nn1m75yv8ciaaopc1vvt13 FOREIGN KEY (workdaysCalendar_id)
    REFERENCES WorkdaysCalendar(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Создать таблицу "Filter_departments"
--
CREATE TABLE Filter_departments (
  Filter_id BIGINT(20) NOT NULL,
  departments VARCHAR(255) DEFAULT NULL,
  CONSTRAINT FK_scurb6t9fhimvhb95yo586iwk FOREIGN KEY (Filter_id)
    REFERENCES Filter(id) ON DELETE RESTRICT ON UPDATE RESTRICT
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Создать таблицу "Filter_Employee"
--
CREATE TABLE Filter_Employee (
  Filter_id BIGINT(20) NOT NULL,
  employees_userName VARCHAR(255) NOT NULL,
  CONSTRAINT FK_epo9w7amwg7nqx1nr3e8590hb FOREIGN KEY (Filter_id)
    REFERENCES Filter(id) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT FK_pruvwwhgb6orrk1j593sflj3b FOREIGN KEY (employees_userName)
    REFERENCES Employee(userName) ON DELETE RESTRICT ON UPDATE RESTRICT
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Создать таблицу "Filter_HourType"
--
CREATE TABLE Filter_HourType (
  Filter_id BIGINT(20) NOT NULL,
  hourTypes_id BIGINT(20) NOT NULL,
  CONSTRAINT FK_djxvnmjhu6xy9nu96x328h1nc FOREIGN KEY (Filter_id)
    REFERENCES Filter(id) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT FK_ix3pqk4ni0cnhh9p8v45w8ps9 FOREIGN KEY (hourTypes_id)
    REFERENCES HourType(id) ON DELETE RESTRICT ON UPDATE RESTRICT
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Изменить таблицу "Participation"
--
ALTER TABLE Participation
  DROP FOREIGN KEY FKE5A0BD21B363BFF5,
  DROP INDEX FKE5A0BD21B363BFF5,
  DROP FOREIGN KEY FKE5A0BD21D02C4CD0,
  DROP INDEX FKE5A0BD21D02C4CD0,
  CHANGE COLUMN _from _from DATE NOT NULL,
  CHANGE COLUMN employee_userName employee_userName VARCHAR(255) NOT NULL;

ALTER TABLE Participation
  ADD CONSTRAINT FK_8tlx1fkwae1264vwv2bsyrcv2 FOREIGN KEY (employee_userName)
    REFERENCES Employee(userName) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE Participation
  ADD CONSTRAINT FK_93tkkd47uwengw7v393rhali FOREIGN KEY (workdaysCalendar_id)
    REFERENCES WorkdaysCalendar(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE Participation
  CHANGE COLUMN till till DATETIME DEFAULT NULL AFTER id,
  CHANGE COLUMN _from _from DATETIME DEFAULT NULL AFTER till;

--
-- Изменить таблицу "Project"
--
DELETE FROM Project_HourType WHERE Project_id IN(
  SELECT t.id FROM(
    SELECT * FROM Project p WHERE
      p.code IS NULL OR p.manager_userName IS NULL OR p.defaultWorkdaysCalendar_id IS NULL
  ) AS t
);

DELETE FROM Project_Participation WHERE Project_id IN(
  SELECT t.id FROM(
    SELECT * FROM Project p WHERE
      p.code IS NULL OR p.manager_userName IS NULL OR p.defaultWorkdaysCalendar_id IS NULL
  ) AS t
);

DELETE FROM Project_ProjectStatus WHERE Project_id IN(
  SELECT t.id FROM(
    SELECT * FROM Project p WHERE
      p.code IS NULL OR p.manager_userName IS NULL OR p.defaultWorkdaysCalendar_id IS NULL
  ) AS t
);

DELETE FROM Project WHERE id IN(
  SELECT t.id FROM(
    SELECT * FROM Project p WHERE
      p.code IS NULL OR p.manager_userName IS NULL OR p.defaultWorkdaysCalendar_id IS NULL
  ) AS t
);

ALTER TABLE Project
  DROP COLUMN _key,
  DROP FOREIGN KEY FK50C8E2F94F1AD2B1,
  DROP INDEX FK50C8E2F94F1AD2B1,
  DROP FOREIGN KEY FK50C8E2F9D9BB9814,
  DROP INDEX FK50C8E2F9D9BB9814,
  DROP INDEX code,
  CHANGE COLUMN allowEmployeeReportTime allowEmployeeReportTime BIT(1) DEFAULT NULL AFTER id,
  CHANGE COLUMN code code VARCHAR(255) NOT NULL AFTER allowEmployeeReportTime,
  CHANGE COLUMN description description VARCHAR(1000) DEFAULT NULL AFTER code,
  CHANGE COLUMN imported imported BIT(1) DEFAULT NULL AFTER description,
  CHANGE COLUMN value value VARCHAR(255) DEFAULT NULL AFTER filterType,
  CHANGE COLUMN defaultWorkdaysCalendar_id defaultWorkdaysCalendar_id BIGINT(20) NOT NULL AFTER value,
  CHANGE COLUMN manager_userName manager_userName VARCHAR(255) NOT NULL;

ALTER TABLE Project
  ADD UNIQUE INDEX constraint_unique_project_code (code);

ALTER TABLE Project
  ADD CONSTRAINT FK_1stqtqiabxm1mon4x113f9qn6 FOREIGN KEY (defaultWorkdaysCalendar_id)
    REFERENCES WorkdaysCalendar(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE Project
  ADD CONSTRAINT FK_5wdlfsygdga5jikt2jd5f2aeo FOREIGN KEY (manager_userName)
    REFERENCES Employee(userName) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Создать таблицу "Filter_Project"
--
CREATE TABLE Filter_Project (
  Filter_id BIGINT(20) NOT NULL,
  projects_id BIGINT(20) NOT NULL,
  CONSTRAINT FK_296k2q9hwlc8bxe4o8po776wl FOREIGN KEY (Filter_id)
    REFERENCES Filter(id) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT FK_mui2glf7mo25fe1rh7pr6ntld FOREIGN KEY (projects_id)
    REFERENCES Project(id) ON DELETE RESTRICT ON UPDATE RESTRICT
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Изменить таблицу "Hours"
--
DELETE FROM Hours WHERE id IN(
  9387,
  9411,
  9364,
  22308,
  12225,
  12223,
  12224,
  12222,
  12226,
  51670,
  51673,
  51672,
  51671,
  66290,
  71151,
  71148,
  71150,
  71149,
  71147,
  78940,
  78787,
  78882,
  78810,
  78862,
  95701,
  92276,
  92284,
  165624
);

ALTER TABLE Hours
  DROP COLUMN task_id,
  DROP FOREIGN KEY FK42AD6EF147E38DF,
  DROP INDEX FK42AD6EF147E38DF,
  DROP FOREIGN KEY FK42AD6EF1C4D5BF5,
  DROP INDEX FK42AD6EF1C4D5BF5,
  DROP FOREIGN KEY FK42AD6EFAD5A3659,
  DROP INDEX FK42AD6EFAD5A3659,
  DROP FOREIGN KEY FK42AD6EFD02C4CD0,
  DROP INDEX FK42AD6EFD02C4CD0,
  DROP INDEX project_date,
  CHANGE COLUMN approved approved BIT(1) DEFAULT NULL AFTER id,
  ADD COLUMN comment VARCHAR(255) DEFAULT NULL AFTER approved,
  CHANGE COLUMN date date DATE NOT NULL AFTER comment,
  CHANGE COLUMN employee_userName employee_userName VARCHAR(255) NOT NULL,
  CHANGE COLUMN project_id project_id BIGINT(20) NOT NULL AFTER employee_userName,
  CHANGE COLUMN type_id type_id BIGINT(20) NOT NULL;

ALTER TABLE Hours
  ADD UNIQUE INDEX constraint_unique_hours (date, employee_userName, project_id, type_id);

ALTER TABLE Hours
  ADD CONSTRAINT FK_990bxibm42vkpmnhw5nd3mqwx FOREIGN KEY (employee_userName)
    REFERENCES Employee(userName) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE Hours
  ADD CONSTRAINT FK_kokkwxa2ruugrvt9mud77f3et FOREIGN KEY (project_id)
    REFERENCES Project(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE Hours
  ADD CONSTRAINT FK_tp9l5k62tamm2xfrcql5qpg8y FOREIGN KEY (type_id)
    REFERENCES HourType(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Изменить таблицу "Project_HourType"
--
ALTER TABLE Project_HourType
  DROP FOREIGN KEY FK7C2761E4147E38DF,
  DROP INDEX FK7C2761E4147E38DF,
  DROP FOREIGN KEY FK7C2761E46D58BC6B,
  DROP INDEX FK7C2761E46D58BC6B;

ALTER TABLE Project_HourType
  ADD CONSTRAINT FK_66dbug73v360eso6rtxe366jt FOREIGN KEY (Project_id)
    REFERENCES Project(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE Project_HourType
  ADD CONSTRAINT FK_oke11fms5sa6goy7dw5gcgtso FOREIGN KEY (accountableHours_id)
    REFERENCES HourType(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Изменить таблицу "Project_Participation"
--
ALTER TABLE Project_Participation
  DROP FOREIGN KEY FK23EE4A5B147E38DF,
  DROP INDEX FK23EE4A5B147E38DF,
  DROP FOREIGN KEY FK23EE4A5BF1E1E583,
  DROP INDEX FK23EE4A5BF1E1E583,
  DROP INDEX team_id;

ALTER TABLE Project_Participation
  ADD UNIQUE INDEX UK_a1yk77jbvbcaqhe7pfht50bhn (team_id);

ALTER TABLE Project_Participation
  ADD CONSTRAINT FK_a1yk77jbvbcaqhe7pfht50bhn FOREIGN KEY (team_id)
    REFERENCES Participation(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE Project_Participation
  ADD CONSTRAINT FK_br53fsc7dsee6oam9jle038ws FOREIGN KEY (Project_id)
    REFERENCES Project(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Изменить таблицу "Project_ProjectStatus"
--
ALTER TABLE Project_ProjectStatus
  DROP FOREIGN KEY FK21874D85107A080A,
  DROP INDEX FK21874D85107A080A,
  DROP FOREIGN KEY FK21874D85147E38DF,
  DROP INDEX FK21874D85147E38DF,
  DROP INDEX statuses_id;

ALTER TABLE Project_ProjectStatus
  ADD CONSTRAINT FK_lrkm00l94rifirsfpydfe5oyf FOREIGN KEY (Project_id)
    REFERENCES Project(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE Project_ProjectStatus
  ADD UNIQUE INDEX UK_tpehjdxg9upaljbymgsoc5n5c (statuses_id);

ALTER TABLE Project_ProjectStatus
  ADD CONSTRAINT FK_tpehjdxg9upaljbymgsoc5n5c FOREIGN KEY (statuses_id)
    REFERENCES ProjectStatus(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Включение внешних ключей
--
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;