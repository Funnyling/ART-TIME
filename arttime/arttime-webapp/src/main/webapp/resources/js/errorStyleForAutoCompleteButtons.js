function addErrorStyleForAutoCompleteButtons() {
	var autocompleteItems = $(".ui-autocomplete");
	$.each( autocompleteItems, function( key, value ) {
		if ($(value).find("input.ui-state-error").length > 0) {
			$(value).find("button").addClass("ui-state-error");
		} 							
	});

}

$(document).ready(function() {	
	addErrorStyleForAutoCompleteButtons();				
});			 