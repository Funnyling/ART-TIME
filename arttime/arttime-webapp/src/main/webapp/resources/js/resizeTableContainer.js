document.ready = function() {
	resizeTableContainers();
}

window.onresize = function() {
	resizeTableContainers();
}

window.onload = function() {
	resizeTableContainers();
}

function resizeTableContainers() {				
	var height = window.innerHeight
			|| document.documentElement.clientHeight
			|| document.body.clientHeight; 
	var toolbars = document.getElementsByClassName('toolBar');
	height = height - toolbars[0].offsetTop - toolbars[0].offsetHeight;
	height = height - document.getElementById('copyright').offsetHeight;
	$("div[id$='fullPage']").css("height", height);
}