<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
${project.code} team updated after synchronization with LDAP
<p>
	<#if newMembers?has_content>
		Added new members to the project
		<table border="1">
			<thead>
				<tr>
					<th scope="col">Employee</th>			
				</tr>
			</thead>
			<tbody>
				<#list newMembers as newMember>
					<tr>
						<td>${newMember.fullName}</td>
					</tr>
				</#list>
			</tbody>
		</table>
	</#if>
	<#if alreadyNotMembers?has_content>
		Closed participation for
		<table border="1">
			<thead>
				<tr>
					<th scope="col">Employee</th>			
				</tr>
			</thead>
			<tbody>
				<#list alreadyNotMembers as alreadyNotMember>
					<tr>
						<td>${alreadyNotMember.fullName}</td>
					</tr>
				</#list>
			</tbody>
		</table>
	</#if>
</p>
<p>
	<i>If synchronization is not worked as you expected, go to ART-TIME and change Import settings for the project</i>
</p>
<p>Time Management System</p>