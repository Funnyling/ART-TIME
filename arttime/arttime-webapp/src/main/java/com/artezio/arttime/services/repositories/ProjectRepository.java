package com.artezio.arttime.services.repositories;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.hibernate.Hibernate;

import com.artezio.arttime.admin_tool.cache.WebCached;
import com.artezio.arttime.admin_tool.cache.WebCached.Scope;
import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.Period;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.datamodel.ProjectStatus;
import com.artezio.arttime.datamodel.ProjectStatus.Status;
import com.artezio.arttime.services.ProjectsSelectionCriteria;
import com.artezio.arttime.web.interceptors.FacesMessage;

@Named
@Stateless
@WebCached(scope = Scope.REQUEST_SCOPED)
public class ProjectRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@FacesMessage(onCompleteMessageKey = "message.projectIsSaved")
	public Project create(Project project) {
		saveEmployees(project);
		entityManager.persist(project);
		return project;
	}

	protected void saveEmployees(Project project) {
		Set<Employee> employees = new HashSet<Employee>(project.getTeamMembers());
		employees.add(project.getManager());
		for (Employee employee : employees) {
			employee.castDepartmentToNameCase();
			entityManager.merge(employee);
		}

	}

	public Project fetchComplete(Project project) {
		project = entityManager.find(Project.class, project.getId());
		Hibernate.initialize(project.getAccountableHours());
		Hibernate.initialize(project.getStatuses());
		Hibernate.initialize(project.getTeam());
		return project;
	}

	public List<Project> fetchComplete(List<Project> projects) {
		return projects
				.parallelStream()
				.map(project -> fetchComplete(project))
				.collect(Collectors.toList());
	}

	public List<Project> getActiveProjects(Employee employee, Period period) {
		List<Project> projects = entityManager
				.createQuery(
						"SELECT DISTINCT p FROM Project p "
								+ "LEFT JOIN p.team t WHERE (:employee IS NULL OR t.employee = :employee) "
								+ "AND ((t.period.finish IS NOT NULL AND t.period.start<=:finish AND t.period.finish>=:start) "
								+ "OR (t.period.finish IS NULL AND t.period.start<=:finish))", Project.class)
				.setParameter("employee", employee).setParameter("start", period.getStart(), TemporalType.DATE)
				.setParameter("finish", period.getFinish(), TemporalType.DATE).getResultList();
		List<Project> result = new ArrayList<Project>();
		for (Project project : projects) {
			if (project.isActiveIn(period))
				result.add(project);
		}
		return result;
	}

	public Project findProject(String projectCode) {
		try {
			Project project = entityManager.createQuery("SELECT p FROM Project p WHERE p.code = :code", Project.class)
					.setParameter("code", projectCode).getSingleResult();
			fetchComplete(project);
			return project;
		} catch (NoResultException e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Project> getAll() {
		List<Project> projects = entityManager.createQuery("SELECT p FROM Project p").getResultList();
		return projects;
	}

	@SuppressWarnings("unchecked")
	public List<Project> getProjectsByManager(String manager) {
		List<Project> projects = entityManager
				.createQuery("SELECT p FROM Project p WHERE p.manager.userName = :manager")
				.setParameter("manager", manager).getResultList();
		return projects;
	}

	public List<Project> getProjects(ProjectsSelectionCriteria criteria) {
		List<Project> projects = searchProjects(criteria);
		for (Project project : projects) {
			Hibernate.initialize(project.getStatuses());
		}
		return criteria.getProjectStatus() == null ? projects : filterProjects(projects, criteria.getProjectStatus());
	}

	@SuppressWarnings("unchecked")
	public List<Project> getProjectsByManager(Employee manager) {
		List<Project> projects = entityManager.createQuery("SELECT p FROM Project p WHERE p.manager = :manager")
				.setParameter("manager", manager).getResultList();
		return projects;
	}

	@SuppressWarnings("unchecked")
	protected List<Project> searchProjects(ProjectsSelectionCriteria criteria) {
		Employee searchManager = criteria.getManager();
		String searchQuery = criteria.getSearchQuery();
		if (searchQuery != null && !searchQuery.trim().isEmpty()) {
			searchQuery = "%" + searchQuery.trim() + "%";
		} else {
			searchQuery = null;
		}

		String queryString = getSearchProjectQuery(searchManager, searchQuery);

		Query query = entityManager.createQuery(queryString);

		setParameters(query, searchQuery, "code", "description", "managerFirstName", "managerLastName");
		setParameters(query, searchManager, "manager");

		return query.getResultList();
	}

	private String getSearchProjectQuery(Object searchManager, Object searchQuery) {
		boolean hasSearchManager = searchManager != null;
		boolean hasSearchQuery = searchQuery != null;

		StringBuilder queryString = new StringBuilder("SELECT p FROM Project p ");

		if (hasSearchQuery || hasSearchManager) {
			queryString.append("INNER JOIN p.manager m WHERE ");

			if (hasSearchQuery)
				queryString.append("(p.code LIKE :code ").append("OR p.description LIKE :description ")
						.append("OR m.firstName LIKE :managerFirstName ")
						.append("OR m.lastName LIKE :managerLastName) ");

			if (hasSearchQuery && hasSearchManager)
				queryString.append("AND ");

			if (hasSearchManager)
				queryString.append("m = :manager ");
		}
		return queryString.toString();
	}

	private void setParameters(Query query, Object value, String... parameterNames) {
		if (value != null) {
			for (String parameterName : parameterNames) {
				query.setParameter(parameterName, value);
			}
		}
	}

	private List<Project> filterProjects(List<Project> projects, ProjectStatus.Status status) {
		List<Project> result = new ArrayList<Project>();
		for (Project project : projects) {
			if (status.equals(project.getCurrentStatus())) {
				result.add(project);
			}
		}
		return result;
	}

	@FacesMessage(onCompleteMessageKey = "message.projectIsDeleted")
	public void remove(Project project) {
		Project managedProject = entityManager.find(Project.class, project.getId());
		entityManager.remove(managedProject);
		entityManager.flush();
	}

	@FacesMessage(onCompleteMessageKey = "message.projectIsSaved")
	public Project update(Project project) {
		saveEmployees(project);
		entityManager.merge(project);
		entityManager.flush();
		return project;
	}

	public Map<Employee, List<Project>> getActiveProjectsByEmployees(List<Employee> employees, Period period) {
		Map<Employee, List<Project>> result = new HashMap<>();
		if (!employees.isEmpty()) {
			List<Project> projects = getActiveProjects(null, period);
			Function<Employee, List<Project>> projectsFilter = employee -> projects.stream()
					.filter(project -> project.isTeamMember(employee, period)).collect(Collectors.toList());
			result = employees.stream().collect(Collectors.toMap(employee -> employee, projectsFilter));
		}
		return result;
	}

}
