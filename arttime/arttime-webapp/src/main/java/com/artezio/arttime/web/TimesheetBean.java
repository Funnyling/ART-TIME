package com.artezio.arttime.web;

import java.util.Date;
import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.apache.commons.beanutils.BeanUtils;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.Hours;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.exceptions.SaveApprovedHoursException;
import com.artezio.arttime.filter.Filter;
import com.artezio.arttime.filter.FilterRepository;
import com.artezio.arttime.filter.FilterService;
import com.artezio.arttime.qualifiers.LoggedIn;
import com.artezio.arttime.services.HoursService;
import com.artezio.arttime.services.repositories.HoursRepository;
import com.artezio.arttime.web.spread_sheet.PersonalEffortsSpreadSheet;
import com.artezio.arttime.web.spread_sheet.SpreadSheet;

@ManagedBean
@ViewScoped
public class TimesheetBean extends EffortsBean {
    private static final long serialVersionUID = -1012227760029456067L;
     
    @Inject
    private FilterService filterService;
    @Inject
    @LoggedIn
    private Employee loggedEmployee;
    @Inject
    private FilterRepository filterRepository;
    @Inject
    private HoursRepository hoursRepository;
    @Inject
    private HoursService hoursService;

    protected SpreadSheet initSpreadSheet() {
    	Filter filter = getPersonalTimesheetFilter();
    	filterRepository.fetchProjects(filter);
   		return new PersonalEffortsSpreadSheet(hoursRepository, filter);
    }

    public void setFilterBean(FilterBean filterBean) {
    	this.filterBean = filterBean;
    }

    private Filter getPersonalTimesheetFilter() {
    	return filterService.createPersonalTimesheetFilter(filterBean.getCurrentFilter(), loggedEmployee);
    }

    public void saveHours() throws SaveApprovedHoursException, ReflectiveOperationException {
		Set<Hours> hours = getSpreadSheet().getUpdatedHours(); 
		hoursService.saveReportTime(hours);	
		resetData();
    }
    
    public String getStyleClass(Hours hours) {
    	if (hours != null && !hours.getProject().isAllowEmployeeReportTime() && !hours.isApproved()) {
    		return "blockedHours";    		
    	}    	
    	return super.getStyleClass(hours);
    }
    
    public boolean isReadOnlyCell(Hours hours){
    	if (hours != null && !hours.getProject().getActualTeamMembers(hours.getDate()).contains(hours.getEmployee())){
    		return true;
    	}    	
    	return false;
    }
}
