package com.artezio.arttime.web;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.inject.Inject;

import org.richfaces.component.SortOrder;

import com.artezio.arttime.datamodel.HourType;
import com.artezio.arttime.exceptions.ActualTimeRemovalException;
import com.artezio.arttime.services.repositories.HourTypeRepository;

@ManagedBean
@ViewScoped
public class HourTypesBean implements Serializable {
    private static final long serialVersionUID = 6305759075233416778L;

    private HourType hourType;
    @Inject
    private ExternalContext externalContext;
    @Inject
    private HourTypeRepository hourTypeRepository;
    private SortOrder typeNameOrder = SortOrder.ascending;

    @PostConstruct
    public void init() {
	Map<String, String> requestParams = externalContext.getRequestParameterMap();
	String hourTypeId = requestParams.get("hourType");
	this.hourType = (hourTypeId == null) ? new HourType() : hourTypeRepository.find(Long.parseLong(hourTypeId));
    }

    public void update(HourType hourType) {
	hourTypeRepository.update(hourType);
    }

    public void create(HourType hourType) {
	hourTypeRepository.create(hourType);
    }

    public void remove(HourType hourType) throws ActualTimeRemovalException {
	hourTypeRepository.remove(hourType);
    }

    public SortOrder getTypeNameOrder() {
	return typeNameOrder;
    }

    public void setTypeNameOrder(SortOrder typeNameOrder) {
	this.typeNameOrder = typeNameOrder;
    }

    public void sortByName() {
	if (typeNameOrder.equals(SortOrder.ascending)) {
	    setTypeNameOrder(SortOrder.descending);
	} else {
	    setTypeNameOrder(SortOrder.ascending);
	}
    }

    public void addNew() {
	hourType = new HourType();
    }

    public HourType getHourType() {
	return hourType;
    }

    public void setHourType(HourType hourType) {
	this.hourType = hourType;
    }
}
