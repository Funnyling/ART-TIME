package com.artezio.arttime.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import com.artezio.arttime.admin_tool.cache.WebCached;
import com.artezio.arttime.admin_tool.cache.WebCached.Scope;
import com.artezio.arttime.datamodel.Day;
import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.Hours;
import com.artezio.arttime.datamodel.Participation;
import com.artezio.arttime.datamodel.Period;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.datamodel.WorkdaysCalendar;
import com.artezio.arttime.filter.Filter;
import com.artezio.arttime.services.repositories.EmployeeRepository;
import com.artezio.arttime.services.repositories.HoursRepository;
import com.artezio.arttime.services.repositories.ProjectRepository;
import com.artezio.arttime.services.repositories.WorkdaysCalendarRepository;

@Named
@Stateless
@WebCached(scope = Scope.REQUEST_SCOPED)
public class WorkTimeService {

    @Inject
    private HoursRepository hoursRepository;
    @Inject
    private ProjectRepository projectRepository;
    @Inject
    private WorkdaysCalendarRepository workdaysCalendarRepository;
    @Inject
    private EmployeeRepository employeeRepository;

    public Map<Employee, BigDecimal> getApprovedWorkTimeProblems(Filter filter) {
        Map<Employee, BigDecimal> result = new TreeMap<>(Employee.NAME_COMPARATOR);
        List<Employee> employees = employeeRepository.getEmployees(filter);
        Map<Employee, BigDecimal> requiredWorkTime = getRequiredWorkTime(employees, filter.getPeriod());
        Map<Employee, BigDecimal> approvedWorkTime = hoursRepository.getApprovedActualHoursSum(employees, filter.getPeriod());
        for (Employee employee : employees) {
            if (employee.getWorkLoad() != null) {
                BigDecimal requiredHours = requiredWorkTime.get(employee);
                BigDecimal approvedHours = approvedWorkTime.get(employee);
                if (hasWorkTimeProblems(requiredHours, approvedHours)) {
                    BigDecimal deviation = approvedHours.subtract(requiredHours);
                    result.put(employee, deviation);
                }
            }
        }
        return result;
    }

    public Map<Employee, Map<Date, BigDecimal>> getApprovedWorkTimeProblems(Period period, List<Employee> employees) {
        Map<Employee, Map<Date, BigDecimal>> result = new HashMap<>();
        Map<Employee, List<Hours>> hoursLists = hoursRepository.getApprovedActualHoursLists(employees, period);
        Map<Employee, List<Project>> activeProjects = projectRepository.getActiveProjectsByEmployees(employees, period);
        for (Employee employee : employees) {
            Map<Date, Boolean> workSchedule = getWorkSchedule(activeProjects.get(employee), employee, period);
            Map<Date, BigDecimal> efforts = getWorkTimeProblems(employee, hoursLists.get(employee), workSchedule);
            result.put(employee, efforts);
        }
        return result;
    }

    public BigDecimal getRequiredWorkTime(Employee employee, Period period) {
        Map<Date, Boolean> workSchedule = getWorkSchedule(employee, period);
        return getRequiredWorkTime(workSchedule, employee);
    }

    protected Map<Employee, BigDecimal> getRequiredWorkTime(List<Employee> employees, Period period) {
        Map<Employee, List<Project>> projectsByEmployee = projectRepository.getActiveProjectsByEmployees(employees, period);
        Map<Employee, BigDecimal> result = new HashMap<>();
        for (Employee employee : employees) {
            Map<Date, Boolean> workSchedule = getWorkSchedule(projectsByEmployee.get(employee), employee, period);
            BigDecimal required = getRequiredWorkTime(workSchedule, employee);
            result.put(employee, required);
        }
        return result;
    }

    protected Map<Date, Boolean> getWorkSchedule(List<Project> activeProjects, Employee employee, Period period) {
        Map<Date, Boolean> workSchedule = generateWorkScheduleTemplate(period);
        for (WorkdaysCalendar calendar : getWorkdaysCalendars(activeProjects, employee, period)) {
            for (Day day : workdaysCalendarRepository.getDays(calendar, period)) {
                Date date = day.getDate();
                if (!workSchedule.get(date) && isParticipationDate(activeProjects, employee, calendar, date)) {
                    workSchedule.put(date, day.isWorking());
                }
            }
        }
        return workSchedule;
    }

    protected Map<Date, Boolean> getWorkSchedule(Employee employee, Period period) {
        List<Project> projects = projectRepository.getActiveProjects(employee, period);
        projects = projectRepository.fetchComplete(projects);
        return getWorkSchedule(projects, employee, period);
    }

    protected Set<WorkdaysCalendar> getWorkdaysCalendars(List<Project> projects, Employee employee, Period period) {
        return projects.stream().flatMap(project -> project.getActualParticipations(employee, period).stream())
                .map(Participation::getWorkdaysCalendar).collect(Collectors.toSet());
    }

    protected BigDecimal getRequiredWorkTime(Map<Date, Boolean> workSchedule, Employee employee) {
        BigDecimal workDaysNumber = new BigDecimal(getWorkDaysNumber(workSchedule));
        BigDecimal workLoadHours = employee.getWorkLoadHours() != null ? employee.getWorkLoadHours() : BigDecimal.ZERO;
        return workDaysNumber.multiply(workLoadHours);
    }

    protected int getWorkDaysNumber(Map<Date, Boolean> workSchedule) {
        List<Boolean> workingDays = new ArrayList<>(workSchedule.values());
        workingDays.removeAll(Arrays.asList(Boolean.FALSE));
        return workingDays.size();
    }

    protected Map<Date, BigDecimal> getWorkTimeProblems(Employee employee, List<Hours> hours, Map<Date, Boolean> workSchedule) {
        Map<Date, BigDecimal> result = new TreeMap<Date, BigDecimal>();
        BigDecimal workLoadHours = employee.getWorkLoadHours();
        if (employee.getWorkLoad() != null) {
            for (Date date : workSchedule.keySet()) {
                BigDecimal dailyApprovedHoursSum = hoursRepository.getDailyApprovedHoursSum(hours, date);
                BigDecimal requiredWorkTime = (workSchedule.get(date)) ? workLoadHours : BigDecimal.ZERO;
                if (hasWorkTimeProblems(requiredWorkTime, dailyApprovedHoursSum)) {
                    BigDecimal deviation = dailyApprovedHoursSum.subtract(requiredWorkTime);
                    result.put(date, deviation);
                }
            }
        }
        return result;
    }

    protected Map<Date, Boolean> generateWorkScheduleTemplate(Period period) {
        Map<Date, Boolean> result = new HashMap<Date, Boolean>();
        for (Date periodDate : period.getDays()) {
            result.put(periodDate, Boolean.FALSE);
        }
        return result;
    }

    protected boolean isParticipationDate(List<Project> projects, Employee employee, WorkdaysCalendar calendar, Date date) {
        return projects.stream().anyMatch(project -> project.isActiveOn(date)
                && project.isTeamMember(employee, date, calendar));
    }

    protected boolean hasWorkTimeProblems(BigDecimal requiredWorkTime, BigDecimal submittedHours) {
        return requiredWorkTime.compareTo(submittedHours) != 0;
    }

}
