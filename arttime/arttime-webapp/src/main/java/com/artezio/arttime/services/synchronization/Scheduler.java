package com.artezio.arttime.services.synchronization;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.inject.Named;

import com.artezio.arttime.config.ApplicationSettings;
import com.artezio.arttime.config.Settings;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.services.EmployeeSynchronizerLocal;
import com.artezio.arttime.services.SettingsService;
import com.artezio.arttime.services.TeamSynchronizer;
import com.artezio.arttime.services.repositories.ProjectRepository;

@Named
@Startup
@Singleton
public class Scheduler {

	@Resource
	private TimerService timerService;	
	@Inject 
	@ApplicationSettings
	private Settings settings;
	@Inject 	
	private SettingsService settingsService;
	@Inject
	private EmployeeSynchronizerLocal employeeSynchronizer;
	@Inject
	private TeamSynchronizer teamSynchronizer;
	@Inject
	private ProjectRepository projectRepository;
	
	private final static String TIMER_INFO = "com.artezio.arttime.synchronization";
	
	@PostConstruct
	public void initialize() throws Exception {
		createIntervalTimer();		
	}		
		
	@Timeout
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void timeout(Timer timer) {
		startSynchronization();
	}
		
	@Asynchronous
	public void startSynchronization() {
		actualizeSettings();
		if (settings != null) {
			trySynchronizeTeam();
			trySynchronizeEmployees();
		}
	}

    public Duration getTimeRemaining() {
       return findActiveTimers().stream().findFirst()
               .map(timer -> Duration.ofMillis(timer.getTimeRemaining()))
               .orElseGet(() -> null);
    }

	private void actualizeSettings() {
		Settings currentSettings = settings;
		Settings actualSettings = settingsService.getSettings();
		settings = actualSettings;
		if (!currentSettings.hasSameTimerSettings(actualSettings)) {
			createIntervalTimer();
		}
		
	}

	void trySynchronizeEmployees() {
		if (settings.isEmployeesSynchronizationEnabled()) {
			employeeSynchronizer.synchronizeEmployees();
		}
	}
	
	void trySynchronizeTeam() {
		if (settings.isTeamSynchronizationEnabled()) {
			for (Project project : projectRepository.getAll()) {
				if (project.isTeamSynchronizationEnabled()) {
					teamSynchronizer.importTeam(project);
				}
			}
		}
	}	

	private void createIntervalTimer() {
		cancelExistedTimers();
		Integer hours = (settings == null) ? 1 : settings.getTimerHoursInterval();
		Integer minutes = (settings == null) ? 0 : settings.getTimerMinutesInterval();
		Integer millis = 1000 * 60 * (hours * 60 + minutes);		
		TimerConfig config = new TimerConfig();
		config.setInfo(TIMER_INFO);
		config.setPersistent(false);
		timerService.createIntervalTimer(millis, millis, config);				
	}
	
	private void cancelExistedTimers() {
		findActiveTimers().forEach(Timer::cancel);
	}

    private List<Timer> findActiveTimers() {
        return timerService.getTimers().stream()
                .filter(timer -> TIMER_INFO.equals(timer.getInfo()))
                .collect(Collectors.toList());
    }

}
