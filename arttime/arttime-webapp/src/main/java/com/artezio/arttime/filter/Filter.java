package com.artezio.arttime.filter;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.HourType;
import com.artezio.arttime.datamodel.Period;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.web.criteria.RangePeriodSelector;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(name = "unique_filter_name_for_owner", columnNames = {"owner", "name"}))
public class Filter implements Serializable {

    private static final long serialVersionUID = 1L;
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String owner;
    @ManyToMany
    private List<Project> projects;
    @ManyToMany
    private List<Employee> employees;
    @ElementCollection
    private List<String> departments;
    @ManyToMany
    private List<HourType> hourTypes;
    @Valid
    @Transient
    private RangePeriodSelector rangePeriodSelector = new RangePeriodSelector();
    @Transient
    private Boolean approved;
    @Transient
    private Boolean predefined=false; 

    public Filter() {
	}

	public Filter(String name, String owner, Boolean predefined) {
		this.name = name;
		this.owner = owner;
		this.predefined = predefined;
	}

	public Filter(Filter filter) {
		this.approved = filter.approved;
		this.departments = filter.departments;
		this.employees = filter.employees;
		this.hourTypes = filter.hourTypes;
		this.name = filter.name;
		this.owner = filter.owner;
		this.projects = filter.projects;
		this.rangePeriodSelector = filter.rangePeriodSelector;
	}

	public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getOwner() {
	return owner;
    }

    public void setOwner(String owner) {
	this.owner = owner;
    }

    public List<Project> getProjects() {
	return projects;
    }

    public void setProjects(List<Project> projects) {
	this.projects = projects;
    }

    public List<Employee> getEmployees() {
	return employees;
    }

    public void setEmployees(List<Employee> employees) {
	this.employees = employees;
    }

    public List<String> getDepartments() {
	return departments;
    }

    public void setDepartments(List<String> departments) {
	this.departments = departments;
    }

    public List<HourType> getHourTypes() {
	return hourTypes;
    }

    public void setHourTypes(List<HourType> hourTypes) {
	this.hourTypes = hourTypes;
    }

    public RangePeriodSelector getRangePeriodSelector() {
	return rangePeriodSelector;
    }

    public void setRangePeriodSelector(RangePeriodSelector rangePeriodSelector) {
	this.rangePeriodSelector = rangePeriodSelector;
    }

    @Override
    public String toString() {
	return "Filter [name=" + name + ", userName=" + owner + "]";
    }

    public boolean isProjectSetted() {
	return projects != null;
    }

    public boolean isEmployeesSetted() {
	return employees != null;
    }

    public boolean isDepartmentsSetted() {
	return departments != null;
    }

    public Boolean isApproved() {
	return approved;
    }

    public void setApproved(Boolean approved) {
	this.approved = approved;
    }

    public Period getPeriod() {
	return rangePeriodSelector.getPeriod();
    }
    
    public Boolean isPredefined() {
		return predefined;
	}

	public void setPredefined(Boolean predefined) {
		this.predefined = predefined;
	}

	public List<Employee> getEmployeesInViewOfDepartments() {
    	return employees.stream()
    			.filter(employee -> departments.contains(employee.getDepartment()))
    			.collect(Collectors.toList());
    }
    
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((name == null) ? 0 : name.hashCode());
	result = prime * result + ((owner == null) ? 0 : owner.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Filter other = (Filter) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (name == null) {
	    if (other.name != null)
		return false;
	} else if (!name.equals(other.name))
	    return false;
	if (owner == null) {
	    if (other.owner != null)
		return false;
	} else if (!owner.equals(other.owner))
	    return false;
	return true;
    }

	public void setCustomPeriod(Period period) {
		rangePeriodSelector.setCustomPeriod();
		rangePeriodSelector.getPeriod().setStart(period.getStart());
		rangePeriodSelector.getPeriod().setFinish(period.getFinish());
	}

}
