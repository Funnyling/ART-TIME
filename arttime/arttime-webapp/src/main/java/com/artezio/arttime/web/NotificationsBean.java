package com.artezio.arttime.web;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.Period;
import com.artezio.arttime.filter.Filter;
import com.artezio.arttime.services.NotificationManagerLocal;
import com.artezio.arttime.services.WorkTimeService;

@ManagedBean
@ViewScoped
public class NotificationsBean implements Serializable {
    private static final long serialVersionUID = -2955420544322390401L;

    @Inject
    private NotificationManagerLocal notificationManager;
    @Inject
    private WorkTimeService workTimeService;
    private List<Employee> selectedEmployees;
    private Map<Employee, BigDecimal> employeesProblemTime = new HashMap<Employee, BigDecimal>();
    private String comment;

    public List<Employee> getSelectedEmployees() {
	return selectedEmployees;
    }

    public void setSelectedEmployees(List<Employee> selectedEmployees) {
	this.selectedEmployees = selectedEmployees;
    }

    public Map<Employee, BigDecimal> getEmployeesProblemTime() {
	return employeesProblemTime;
    }

    public void setEmployeesProblemTime(Map<Employee, BigDecimal> employeesProblemTime) {
	this.employeesProblemTime = employeesProblemTime;
    }

    public String getComment() {
	return comment;
    }

    public void setComment(String comment) {
	this.comment = comment;
    }

    public void sendNotifications(Period period) {
	notificationManager.notificateAboutWorkTimeProblem(selectedEmployees, period, comment);
    }

    public List<Employee> getEmployees(Filter filter) {
        employeesProblemTime = workTimeService.getApprovedWorkTimeProblems(filter);
        return new ArrayList<>(employeesProblemTime.keySet());
    }
}
