package com.artezio.arttime.report;

public class ReportUtils {
	
	public static final String REPORT_TYPE_PDF = "REPORT_TYPE_PDF";
	public static final String CONTENT_TYPE_PDF = "application/pdf";
	public static final String REPORT_TYPE_EXTENTION_PDF = ".pdf";
	
	public static final String REPORT_TYPE_EXCEL = "REPORT_TYPE_EXCEL";
	public static final String CONTENT_TYPE_EXCEL = "application/vnd.ms-excel";
	public static final String REPORT_TYPE_EXTENTION_EXCEL = ".xlsx";

}
