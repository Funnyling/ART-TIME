package com.artezio.arttime.web.spread_sheet;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.HourType;
import com.artezio.arttime.datamodel.Hours;
import com.artezio.arttime.datamodel.Period;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.filter.Filter;
import com.artezio.arttime.services.repositories.HoursRepository;
import com.lassitercg.faces.components.event.SheetUpdate;
import com.lassitercg.faces.components.sheet.Sheet;
import org.apache.commons.lang3.SerializationUtils;

public abstract class SpreadSheet implements Serializable {
	private static final long serialVersionUID = 7406047486153333282L;
	
	private Sheet sheet;
	protected Filter filter;	
	protected List<SpreadSheetRow<?>> rows;
	protected HoursRepository hoursRepository;

	protected abstract void buildSkeleton();
	protected abstract int calculateTotalKeyFor(SpreadSheetRow<?> updatedRow);

	public SpreadSheet(HoursRepository hoursRepository, Filter filter) {
        this.filter = SerializationUtils.clone(filter);
        this.hoursRepository = hoursRepository;
	}				

	public List<SpreadSheetRow<?>> getRows() {
		if (rows == null) {
			buildSkeleton();
		}
		return rows;
	}
	
	protected List<Employee> getSortedTeamMembers(Project project) {
		Period period = filter.getPeriod();
		List<Employee> selectedEmployees = filter.getEmployeesInViewOfDepartments();
		List<Employee> employees = new ArrayList<Employee>(project.getTeamMembers(period));
		employees.retainAll(selectedEmployees);
		employees.sort(Employee.NAME_COMPARATOR);
		return employees;
	}

	protected List<HourType> getSortedHourTypes(Project project) {
		List<HourType> hourTypes = project.getAccountableHours();
		hourTypes.retainAll(filter.getHourTypes());
		hourTypes.sort(HourType.ACTUALTIME_TYPE_COMPARATOR);
		return hourTypes;
	}
	
	protected Set<HourType> getSortedHourTypes(List<Project> projects) {
		Set<HourType> hourTypes = new TreeSet<HourType>(HourType.ACTUALTIME_TYPE_COMPARATOR);
		for (Project project : projects) {
			hourTypes.addAll(project.getAccountableHours());
		}		
		hourTypes.retainAll(filter.getHourTypes());
		return hourTypes;
	}

	protected List<Project> getSortedProjects() {
		List<Project> projects = filter.getProjects();		
		projects.sort(Project.CODE_COMPARATOR);
		return projects;
	}	
	
	protected List<Project> getSortedProjects(Employee employee) {		
		Period period = filter.getPeriod();				
		return filter.getProjects()
				.parallelStream()
				.filter(project -> project.isTeamMember(employee, period))
				.sorted(Project.CODE_COMPARATOR)
				.collect(Collectors.toList());
	}
	
	protected List<Employee> getSortedEmployees() {
		List<Employee> employees = filter.getEmployeesInViewOfDepartments();		
		employees.sort(Employee.NAME_COMPARATOR);
		return employees;
	}
	
	public void cellEditEvent() {
		Set<Object> rowKeys = new HashSet<Object>();							
		sheet.getUpdates().parallelStream()
			.map(update -> update.getRowIndex())
			.distinct()
			.forEach(update -> {
					SpreadSheetRow<?> updatedRow = rows.get(update);
					int totalRowKey = calculateTotalKeyFor(updatedRow);
					rowKeys.add(totalRowKey);
			});
		sheet.updateDirtyRows(rowKeys);
	}

	public Sheet getSheet() {
		return sheet;
	}

	public void setSheet(Sheet sheet) {
		this.sheet = sheet;
	}

	public List<Hours> getSelectedHours() {
		List<Date> daysInPeriod = filter.getPeriod().getDays();
		String selection = sheet.getSelection();
		if (selection != null) {
			List<Hours> result = new ArrayList<Hours>();
			Range range = new Range(selection);
			for (int i = range.getFromRow(); i <= range.getToRow(); i++) {
				for (int j = range.getFromCol(); j <= range.getToCol(); j++) {
					int dynamicCol = j - sheet.getFixedCols();
					if (dynamicCol >= 0) {
						Date date = daysInPeriod.get(dynamicCol);
						SpreadSheetRow<?> sheetRow = rows.get(i);
						if (sheetRow instanceof HoursSpreadSheetRow) {
							Hours hour = (Hours)sheetRow.getValuesMap().get(date);
							if (hour != null) {
								result.add(hour);
							}
						}
					}
				}
			}
			return result;
		}
		return Collections.emptyList();
	}	

	public Set<Hours> getUpdatedHours() {
		List<SheetUpdate> updates = sheet.getUpdates();
		List<Date> days = filter.getPeriod().getDays();
		Set<Hours> result = new HashSet<Hours>();
		for (SheetUpdate update : updates) {
			int dayNumber = update.getColIndex() - sheet.getFixedCols();
			if (dayNumber >= 0) {
				Date date = days.get(dayNumber);
				if (update.getRowData() instanceof HoursSpreadSheetRow) {
					HoursSpreadSheetRow hoursRow = (HoursSpreadSheetRow) update.getRowData();
					Hours hours = hoursRow.get(date);
					result.add(hours);
				}
			}
		}
		return result;
	}
	
	class Range {
		private int fromRow;
		private int fromCol;
		private int toRow;
		private int toCol;
		
		public Range(String selectedArea) {			
			String[] coordinates = selectedArea.substring(1, selectedArea.length() - 1).split(",");			
			fromRow = Math.min(Integer.parseInt(coordinates[0]), Integer.parseInt(coordinates[2]));
			fromCol = Math.min(Integer.parseInt(coordinates[1]), Integer.parseInt(coordinates[3]));
			toRow = Math.max(Integer.parseInt(coordinates[0]), Integer.parseInt(coordinates[2]));
			toCol = Math.max(Integer.parseInt(coordinates[1]), Integer.parseInt(coordinates[3]));
		}
		
		public int getFromRow() {
			return fromRow;
		}
		public void setFromRow(int fromRow) {
			this.fromRow = fromRow;
		}
		public int getFromCol() {
			return fromCol;
		}
		public void setFromCol(int fromCol) {
			this.fromCol = fromCol;
		}
		public int getToRow() {
			return toRow;
		}
		public void setToRow(int toRow) {
			this.toRow = toRow;
		}
		public int getToCol() {
			return toCol;
		}
		public void setToCol(int toCol) {
			this.toCol = toCol;
		}				
	}

	public List<Hours> getHours() {
		return rows.stream()
				.filter(row -> row instanceof HoursSpreadSheetRow)				
				.map(row -> ((HoursSpreadSheetRow)row).getHours())
				.collect(ArrayList::new, ArrayList::addAll, ArrayList::addAll);			
	}

	public void updateSelectedRows() {
		Set<Object> selectedRowKeys = getSelectedRowKeys();
		sheet.updateDirtyRows(selectedRowKeys);
	}
	
	public void updateAllRows() {		
		Set<Object> selectedRowKeys = getRowKeys();
		sheet.updateDirtyRows(selectedRowKeys);
	}

	private Set<Object> getSelectedRowKeys() {
		Set<Object> selectedRowKeys = new HashSet<Object>();
		String selection = sheet.getSelection();
		if (selection != null) {
			Range range = new Range(selection);
			for (int i = range.getFromRow(); i <= range.getToRow(); i++) {
				selectedRowKeys.add(rows.get(i).getKey());				
			}
		}
		return selectedRowKeys;
	}
	
	private Set<Object> getRowKeys() {
		Set<Object> rowKeys = new HashSet<Object>();		
		for (SpreadSheetRow<?> row : rows) {
			rowKeys.add(row.getKey());				
		}
		return rowKeys;
	}

    public Filter getFilter() {
        return filter;
    }

}
