package com.artezio.arttime.services;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.exception.ExceptionUtils;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.services.integration.TeamTrackingSystem;
import com.artezio.arttime.services.mailing.Mail;
import com.artezio.arttime.services.mailing.MailTemplate;
import com.artezio.arttime.services.mailing.MailTemplateManager;
import com.artezio.arttime.services.mailing.MailingEngine;
import com.artezio.arttime.services.repositories.ProjectRepository;

@Named
@Stateless
public class TeamSynchronizer {
    @Inject
    private TeamTrackingSystem teamTrackingSystem;
    private Logger log = Logger.getLogger(this.getClass().getName());
    @Inject
    private MailingEngine mailingEngine;
    @Inject
    private MailTemplateManager mailTemplateManager;
    @Inject
    private ProjectRepository projectRepository;

    protected void notifyPM(Project project, List<Employee> alreadyNotMembers, List<Employee> newMembers) {
        try {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("project", project);
            params.put("newMembers", newMembers);
            params.put("alreadyNotMembers", alreadyNotMembers);
            String body = mailTemplateManager.getTemplateText(MailTemplate.TEAM_SYNCHRONIZATION_BODY.getFileName(), params);
            String subject = mailTemplateManager.getTemplateText(MailTemplate.TEAM_SYNCHRONIZATION_SUBJECT.getFileName(), params);
            mailingEngine.send(project.getManager().getEmail(), new Mail(subject, body));
        } catch (Exception e) {
            log.log(Level.SEVERE, "{0}: Notification for employee failed - {1}", new Object[]{project.getCode(), ExceptionUtils.getStackTrace(e)});
        }
    }

    public void importTeam(Project project) {
        log.log(Level.INFO, "{0} : team synchronization started with params = {1}", new Object[]{project.getCode(), project.getTeamFilter()});
        try {
            Date currentDate = new Date();
            project = projectRepository.fetchComplete(project);
            List<Employee> actualTeamMembers = teamTrackingSystem.getTeam(project);
            List<Employee> currentTeamMembers = project.getActualTeamMembers(currentDate);
            List<Employee> notTeamMembersAnymore = (List<Employee>) CollectionUtils.subtract(currentTeamMembers, actualTeamMembers);
            List<Employee> newTeamMembers = (List<Employee>) CollectionUtils.subtract(actualTeamMembers, currentTeamMembers);
            project.closeParticipations(notTeamMembersAnymore);
            project.addTeamMembers(newTeamMembers);
            projectRepository.update(project);
            if (!notTeamMembersAnymore.isEmpty() || !newTeamMembers.isEmpty()) {
                notifyPM(project, notTeamMembersAnymore, newTeamMembers);
            }
            log.log(Level.INFO, "{0} : team synchronization has been finished. "
                    + "{1} team members added. Participation closed for {2} members.", new Object[]{project.getCode(),
                    newTeamMembers.size(), notTeamMembersAnymore.size()});
        } catch (Exception e) {
            String errorDescription = ExceptionUtils.getFullStackTrace(e);
            log.log(Level.SEVERE, "{0}: Error while synchronize project team - {1}", new Object[]{project.getCode(), ExceptionUtils.getStackTrace(e)});
        }
    }

}
