package com.artezio.arttime.services.mailing;

import java.io.Serializable;

/**
 * The Class Mail.
 */
public class Mail implements Serializable {

    private static final long serialVersionUID = 3697539661593550779L;

    private String subject;
    private String text;

    /**
     * Instantiates a new mail.
     */
    public Mail() {
    }

    /**
     * Instantiates a new mail.
     *
     * @param subject the subject
     * @param text the text
     */
    public Mail(String subject, String text) {
	super();
	this.subject = subject;
	this.text = text;
    }

    /**
     * Gets the subject.
     *
     * @return the subject
     */
    public String getSubject() {
	return subject;
    }

    /**
     * Sets the subject.
     *
     * @param subject the new subject
     */
    public void setSubject(String subject) {
	this.subject = subject;
    }

    /**
     * Gets the text.
     *
     * @return the text
     */
    public String getText() {
	return text;
    }

    /**
     * Sets the text.
     *
     * @param text the new text
     */
    public void setText(String text) {
	this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((subject == null) ? 0 : subject.hashCode());
	result = prime * result + ((text == null) ? 0 : text.hashCode());
	return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Mail other = (Mail) obj;
	if (subject == null) {
	    if (other.subject != null)
		return false;
	} else if (!subject.equals(other.subject))
	    return false;
	if (text == null) {
	    if (other.text != null)
		return false;
	} else if (!text.equals(other.text))
	    return false;
	return true;
    }
}
