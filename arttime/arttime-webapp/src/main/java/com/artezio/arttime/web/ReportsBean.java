package com.artezio.arttime.web;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.naming.NamingException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.HourType;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.filter.Filter;
import com.artezio.arttime.report.ReportEngine;
import com.artezio.arttime.report.ReportTemplate;
import com.artezio.arttime.report.ReportUtils;

@ManagedBean
@ViewScoped
public class ReportsBean implements Serializable {

	private static final long serialVersionUID = 2537452262275935640L;

	private static final String UTF_8 = "UTF-8";

	private static final String ID_WRAPPER = "#";

	@Inject
	private ReportEngine reportEngine;
	@ManagedProperty("#{filterBean}")
	private FilterBean filterBean;

	private StreamedContent file;

	private String reportName;
	private String reportType;

	public StreamedContent getFile() throws SQLException, NamingException, IOException {
		this.file = new DefaultStreamedContent(new ByteArrayInputStream(makeReport()), getContentType(),
				getOutputFileName());
		return file;
	}

	protected String getReportTemplate(String fileName) throws IOException {
		try (InputStream in = this.getClass().getClassLoader().getResourceAsStream(fileName)) {
			return IOUtils.toString(in, UTF_8);
		}
	}

	protected byte[] makeReport() throws SQLException, NamingException, IOException {
		Map<String, Object> params = new HashMap<String, Object>();
		java.sql.Date startDate = new java.sql.Date(getFilter().getPeriod().getStart().getTime());
		java.sql.Date endDate = new java.sql.Date(getFilter().getPeriod().getFinish().getTime());
		params.put("startDate", startDate);
		params.put("endDate", endDate);
		params.put("employeeUserNames", getSelectedEmployeeUserNames());
		params.put("departments", getSelectedDepartmens());
		params.put("projectIds", getSelectedProjectIds());
		params.put("hourTypeIds", getSelectedHourTypeIds());
		String reportTemplate = getReportTemplate(ReportTemplate.valueOf(getReportName()).getFileName());
		return reportEngine.getReport(reportTemplate, getReportType(), params);
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getReportType() {
		return StringUtils.isBlank(this.reportType) ? getReportTypePdf() : this.reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public String getReportTypePdf() {
		return ReportUtils.REPORT_TYPE_PDF;
	}

	public String getReportTypeExcel() {
		return ReportUtils.REPORT_TYPE_EXCEL;
	}

	public String getContentType() {
		String reportType = getReportType();
		if (ReportUtils.REPORT_TYPE_PDF.equals(reportType)) {
			return ReportUtils.CONTENT_TYPE_PDF;
		} else {
			return ReportUtils.CONTENT_TYPE_EXCEL;
		}
	}

	public String getOutputFileName() {
		String result = getReportName();
		String reportType = getReportType();
		if (ReportUtils.REPORT_TYPE_PDF.equals(reportType))
			result += ReportUtils.REPORT_TYPE_EXTENTION_PDF;
		else {
			result += ReportUtils.REPORT_TYPE_EXTENTION_EXCEL;
		}
		return result;
	}

	protected String getSelectedEmployeeUserNames() {
		StringBuilder result = new StringBuilder("");
		List<Employee> employees = getFilter().getEmployees();
		for (Employee employee : employees) {
			result.append(ID_WRAPPER);
			result.append(employee.getUserName());
			result.append(ID_WRAPPER);
		}
		return result.toString();
	}

	protected String getSelectedDepartmens() {
		StringBuilder result = new StringBuilder("");
		List<String> departments = getFilter().getDepartments();
		for (String department : departments) {
			result.append(ID_WRAPPER);
			result.append(department);
			result.append(ID_WRAPPER);
		}
		return result.toString();
	}

	protected String getSelectedProjectIds() {
		StringBuilder result = new StringBuilder("");
		List<Project> projects = getFilter().getProjects();
		for (Project project : projects) {
			result.append(ID_WRAPPER);
			result.append(project.getId().toString());
			result.append(ID_WRAPPER);
		}
		return result.toString();
	}

	protected String getSelectedHourTypeIds() {
		StringBuilder result = new StringBuilder("");
		List<HourType> hourTypes = getFilter().getHourTypes();
		for (HourType hourType : hourTypes) {
			result.append(ID_WRAPPER);
			result.append(hourType.getId().toString());
			result.append(ID_WRAPPER);
		}
		return result.toString();
	}

	public void setFilterBean(FilterBean filterBean) {
		this.filterBean = filterBean;
	}

	private Filter getFilter() {
		return filterBean.getCurrentFilter();
	}
}
