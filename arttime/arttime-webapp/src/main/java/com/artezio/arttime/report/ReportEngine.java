package com.artezio.arttime.report;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

@Stateless
public class ReportEngine {
	
	public static final String DS_JNDI_NAME = "java:jboss/datasources/com.artezio.arttime";
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public byte[] getReport(String reportFile, String reportType, Map<String, Object> params) throws SQLException, NamingException{
		ReportBuilder reportBuilder = new ReportBuilder();
		Connection connection = null;
		byte[] result = null;
		try {
			connection = getDataSource().getConnection();
			result = reportBuilder.getReport(reportFile, reportType, params, connection);
		} finally {
			if (connection!=null){
				connection.close();
			}
		}		
		return result;
	}
	
	private Object getService(String jndiName) throws NamingException{
		return new InitialContext().lookup(jndiName);
	}
	
	private DataSource getDataSource() throws NamingException, SQLException {
        return (DataSource) getService(DS_JNDI_NAME);
	}

}
