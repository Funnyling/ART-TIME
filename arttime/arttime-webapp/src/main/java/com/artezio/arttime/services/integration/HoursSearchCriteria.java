package com.artezio.arttime.services.integration;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
public class HoursSearchCriteria implements Serializable {
	private static final long serialVersionUID = 7805097657419399172L;
	
	@XmlSchemaType(name="date", type=XMLGregorianCalendar.class)
	private Date dateFrom;
	@XmlSchemaType(name="date", type=XMLGregorianCalendar.class)
	private Date dateTo;
	private boolean approvedOnly;

	public HoursSearchCriteria() {
	}

	public HoursSearchCriteria(Date dateFrom, Date dateTo, boolean approvedOnly) {
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.approvedOnly = approvedOnly;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public boolean isApprovedOnly() {
		return approvedOnly;
	}

	public void setApprovedOnly(boolean approvedOnly) {
		this.approvedOnly = approvedOnly;
	}

}
