package com.artezio.arttime.web.criteria;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.faces.context.ExternalContext;
import javax.inject.Inject;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.qualifiers.LoggedIn;
import com.artezio.arttime.qualifiers.ManagedProjectsCriteria;
import com.artezio.arttime.services.ProjectsSelectionCriteria;
import com.artezio.arttime.services.ProjectsSelectionCriteria.Status;

public class ProjectsSelectionCriteriaProducer {
	@Inject
	@LoggedIn
	private Employee loggedEmployee;
	@Inject
	private ExternalContext externalContext;
	
	@Produces @ManagedProjectsCriteria ProjectsSelectionCriteria getProjectSelectionCriteria(InjectionPoint ip) {
		boolean onlyOwnProjects = ip.getAnnotated().getAnnotation(ManagedProjectsCriteria.class).onlyOwnProjects();		
		ProjectsSelectionCriteria criteria = (!onlyOwnProjects && externalContext.isUserInRole("exec"))
				? new ProjectsSelectionCriteria()
				: new ProjectsSelectionCriteria(loggedEmployee);
		Status status = ip.getAnnotated().getAnnotation(ManagedProjectsCriteria.class).status();
		criteria.setStatus(status);
		return criteria;
	}			
}
