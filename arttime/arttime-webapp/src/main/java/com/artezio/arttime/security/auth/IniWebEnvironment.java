package com.artezio.arttime.security.auth;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.config.Ini;
import org.apache.shiro.config.IniSecurityManagerFactory;

import com.artezio.arttime.config.Settings;
import com.artezio.arttime.services.SettingsService;

public class IniWebEnvironment extends org.apache.shiro.web.env.IniWebEnvironment {	
	private Ini ini;
	private SettingsService settingsService;
	
	public void init() {
		Ini ini = getIni();		
		super.setIni(ini);		
		super.init();
	}
	
	public Ini getIni() {
		if (ini == null) {			
			ini = getDefaultIni();
			addLdapProperties();
		}
        return this.ini;
    }

	protected void addLdapProperties() {
		try {			
			Settings settings = getSettingsService().getSettings();
			if (settings != null) {
				String ldapUrl = String.format("ldap://%s:%s", settings.getLdapServerAddress(), settings.getLdapServerPort());
				ini.setSectionProperty(IniSecurityManagerFactory.MAIN_SECTION_NAME, ActiveDirectoryRealm.REALM_NAME, "com.artezio.arttime.security.auth.ActiveDirectoryRealm");			
				ini.setSectionProperty(IniSecurityManagerFactory.MAIN_SECTION_NAME, ActiveDirectoryRealm.REALM_NAME+".url", ldapUrl);			
				ini.setSectionProperty(IniSecurityManagerFactory.MAIN_SECTION_NAME, ActiveDirectoryRealm.REALM_NAME+".systemUsername", settings.getLdapBindDN());
				ini.setSectionProperty(IniSecurityManagerFactory.MAIN_SECTION_NAME, ActiveDirectoryRealm.REALM_NAME+".systemPassword", settings.getLdapBindCredentials());
				ini.setSectionProperty(IniSecurityManagerFactory.MAIN_SECTION_NAME, ActiveDirectoryRealm.REALM_NAME+".searchBase", settings.getLdapUserContextDN());
				ini.setSectionProperty(IniSecurityManagerFactory.MAIN_SECTION_NAME, ActiveDirectoryRealm.REALM_NAME+".principalSuffix", settings.getLdapPrincipalSuffix());
				ini.setSectionProperty(IniSecurityManagerFactory.MAIN_SECTION_NAME, ActiveDirectoryRealm.REALM_NAME+".groupRolesMap", getGroupRolesMapProperty(settings));
			}
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	protected String getGroupRolesMapProperty(Settings settings) {
		Map<String, String> groupRolesMap = getGroupRolesMap(settings);
		
		StringBuilder result = new StringBuilder();
		for (String group : groupRolesMap.keySet()) {
			if (result.length() != 0) {
				result.append(",");
			}			
			String groupRole = MessageFormat.format("\"{0}={1},{2}\":\"{3}\"", 
					settings.getLdapCommonNameAttribute(), group.trim(), 
					settings.getLdapUserContextDN(), groupRolesMap.get(group));
			result.append(groupRole);
		}
		return result.toString();
	}

	private Map<String, String> getGroupRolesMap(Settings settings) {
		String[] execGroups = (settings.getExecRoleMemberOf() != null) 
				? settings.getExecRoleMemberOf().split(",")
				: new String[]{};
		String[] integrationClientGroups = (settings.getIntegrationClientGroups() != null)
				? settings.getIntegrationClientGroups().split(",")
				: new String[]{};
		Map<String, String> groupRolesMap = new HashMap<String, String>();
		addGroupRoles(groupRolesMap, execGroups, UserRoles.EXEC_ROLE);
		addGroupRoles(groupRolesMap, integrationClientGroups, UserRoles.INTEGRATION_CLIENT_ROLE);
		return groupRolesMap;
	}

	protected void addGroupRoles(Map<String, String> groupRolesMap, String[] groups, String role) {
		for (String group : groups) {
			String roles = (groupRolesMap.containsKey(group)) 
				? groupRolesMap.get(group).concat("," + role)
				: role;
			groupRolesMap.put(group, roles);
		}
	}

	private SettingsService getSettingsService() throws NamingException {
		if (settingsService == null) {
		    settingsService = (SettingsService) new InitialContext().lookup("java:global/arttime-webapp/SettingsService");
		}
		return settingsService;
    }
	
	protected Ini getDefaultIni() {
		return super.getDefaultIni();
	}
	
}
