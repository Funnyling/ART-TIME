package com.artezio.arttime.services.mailing;

import java.util.Date;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.artezio.arttime.config.ApplicationSettings;
import com.artezio.arttime.config.Settings;
import com.artezio.arttime.services.integration.EmployeeService;

/**
 * The Class MailingEngine.
 */
@Stateless
public class MailingEngine {

	@Resource(mappedName="java:jboss/mail/Default")
	private Session session;
	@Inject @ApplicationSettings
	private Settings settings;
	@Inject
	private EmployeeService employeeService;
	
	@PostConstruct
	private void initSession() {
		session.getProperties().put("mail.smtp.host", settings.getSmptHostName());
		session.getProperties().put("mail.smtp.port", settings.getSmtpPortNumber());
	}

	/**
	 * Send.
	 *
	 * @param recipientId the recipient id
	 * @param mail the mail
	 */
	@Asynchronous
	public void send(String recipientId, Mail mail) {
		try {
			MimeMessage mimeMessage = new MimeMessage(session);
			mimeMessage.setSubject(mail.getSubject());
			mimeMessage.setText(mail.getText(), "UTF-8", "html");
			mimeMessage.setSentDate(new Date());

			InternetAddress[] addressesTo = InternetAddress.parse(recipientId, false);
			mimeMessage.setRecipients(javax.mail.Message.RecipientType.TO, addressesTo);
			String from = employeeService.findEmployee(settings.getSmtpUsername()).getEmail();
			mimeMessage.setFrom(new InternetAddress(from));

			Transport.send(mimeMessage);
		} catch (MessagingException e) {
			throw new RuntimeException("Cannot send a message !", e);
		}
	}
}
