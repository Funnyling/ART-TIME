package com.artezio.arttime.web.filters;

import net.sf.uadetector.*;
import net.sf.uadetector.service.UADetectorServiceFactory;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

@WebFilter(filterName = "checkSupportedBrowsersFilter", urlPatterns = {"*.xhtml", "*.jsf", "*.faces"})
public class CheckSupportedBrowsersFilter implements Filter {

    private final static String ERROR_PAGE_ID = "/unsupportedBrowser.xhtml";
    private final static Map<UserAgentFamily, Integer> MINIMUM_BROWSER_VERSIONS = new HashMap<>();
    static {
        MINIMUM_BROWSER_VERSIONS.put(UserAgentFamily.IE, 10);
        MINIMUM_BROWSER_VERSIONS.put(UserAgentFamily.FIREFOX, 30);
        MINIMUM_BROWSER_VERSIONS.put(UserAgentFamily.OPERA, 20);
        MINIMUM_BROWSER_VERSIONS.put(UserAgentFamily.CHROME, 37);
        MINIMUM_BROWSER_VERSIONS.put(UserAgentFamily.SAFARI, 7);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (checkBrowser(request)) {
            ReadableUserAgent userAgent = parseUserAgent(request);
            Integer minimumVersion = MINIMUM_BROWSER_VERSIONS.get(userAgent.getFamily());
            Integer version = parseMajorVersion(userAgent.getVersionNumber());
            if (version < minimumVersion) {
                request.getRequestDispatcher(ERROR_PAGE_ID).forward(request, response);
                return;
            }
        }
        chain.doFilter(request, response);
    }

    private boolean checkBrowser(HttpServletRequest request) {
        ReadableUserAgent userAgent = parseUserAgent(request);
        return userAgent != null
                && !request.getRequestURI().contains(ERROR_PAGE_ID)
                && !excludeFromFilter(request.getServletPath())
                && userAgent.getType() == UserAgentType.BROWSER
                && MINIMUM_BROWSER_VERSIONS.containsKey(userAgent.getFamily());
    }

    private ReadableUserAgent parseUserAgent(HttpServletRequest request) {
        String userAgentHeader = request.getHeader("User-Agent");
        if (StringUtils.isNotBlank(userAgentHeader)) {
            UserAgentStringParser parser = UADetectorServiceFactory.getResourceModuleParser();
            return parser.parse(userAgentHeader);
        }
        return null;
    }

    private boolean excludeFromFilter(String path) {
        return path.startsWith("/javax.faces.resource");
    }

    private Integer parseMajorVersion(VersionNumber versionNumber) {
        String major = versionNumber.getMajor();
        return Integer.valueOf(major);
    }

    public static Map<UserAgentFamily, Integer> getMinimumBrowserVersions() {
        return new TreeMap<>(MINIMUM_BROWSER_VERSIONS);
    }

    @Override
    public void destroy() {
    }

}
