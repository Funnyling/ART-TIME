package com.artezio.arttime.web;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.inject.Inject;

import com.artezio.arttime.datamodel.Participation;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.services.repositories.ProjectRepository;

@ManagedBean
@ViewScoped
public class ProjectBean implements Serializable {
    private static final long serialVersionUID = -5685248912502538750L;

    private Date startFrom;
    private String selectedTab;
    private Project project;
    @Inject
    private ExternalContext externalContext;
    private Participation participation;
    @Inject
    private ProjectRepository projectRepository;

    @PostConstruct
    public void init() {
	Map<String, String> requestParams = externalContext.getRequestParameterMap();
	String projectCode = requestParams.get("project");
	this.selectedTab = requestParams.get("selectedTab");
	this.project = (projectCode == null) ? new Project() : projectRepository.findProject(projectCode);
	participation = new Participation(project.getDefaultWorkdaysCalendar());
    }

    public void addNew() {
	project = new Project();
    }

    public Project getProject() {
	return project;
    }

    public void setProject(Project project) {
	this.project = project;
    }

    public Date getStartFrom() {
	return startFrom;
    }

    public void setStartFrom(Date startFrom) {
	this.startFrom = startFrom;
    }

    public String getSelectedTab() {
	return selectedTab;
    }

    public void setSelectedTab(String selectedTab) {
	this.selectedTab = selectedTab;
    }

    public void update(Project project) {
	projectRepository.update(project);
    }

    public void create(Project project) {
	projectRepository.create(project);
    }

    public void remove(Project project) {
	projectRepository.remove(project);
    }

    public void delete(Participation participation) {
	project.getTeam().remove(participation);
    }

    public Participation getParticipation() {
	return participation;
    }

    public void setParticipation(Participation participation) {
	this.participation = participation;
    }

    public void addNewParticipation() {
	project.addParticipation(participation);
	participation = new Participation(project.getDefaultWorkdaysCalendar());
    }
}
