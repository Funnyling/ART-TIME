package com.artezio.arttime.services.integration;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.apache.shiro.authz.annotation.RequiresRoles;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.HourType;
import com.artezio.arttime.datamodel.Hours;
import com.artezio.arttime.datamodel.Period;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.filter.Filter;
import com.artezio.arttime.services.repositories.HourTypeRepository;
import com.artezio.arttime.services.repositories.HoursRepository;
import com.artezio.arttime.services.repositories.ProjectRepository;
import com.artezio.arttime.web.criteria.RangePeriodSelector;

@ShiroSecured
@RequiresRoles("ArtTimeIntegrationClient")
@WebService(serviceName = "arttime-facade-ws", name = "arttime-facade-ws")
public class IntegrationFacade {

	@Inject
	private ProjectRepository projectRepository;
	@Inject
	private HourTypeRepository hourTypeRepository;
	@Inject
	private HoursRepository hoursRepository;

	@WebMethod
	@WebResult(name = "project")
	public List<Project> getProjects() {
		List<Project> projects = projectRepository.getAll();
		return projectRepository.fetchComplete(projects);
	}

	@WebMethod
	@WebResult(name = "hoursType")
	public List<HourType> getHourTypes() {
		return hourTypeRepository.getAll();
	}

	@WebMethod
	@WebResult(name = "employee")
	public List<Employee> getProjectTeam(
			@WebParam(name = "projectCode") String projectCode) {
		Project project = projectRepository.findProject(projectCode);
		return project.getTeamMembers();
	}

	@WebMethod
	@WebResult(name = "hours")
	public List<Hours> getHours(
			@WebParam(name = "projectCode") String projectCode,
			@WebParam(name = "hoursSearchCriteria") com.artezio.arttime.services.integration.HoursSearchCriteria criteria) {
		Project project = projectRepository.findProject(projectCode);
		Period period = new Period(criteria.getDateFrom(), criteria.getDateTo());

		Filter filter = new Filter();
		filter.setProjects(Arrays.asList(project));
		RangePeriodSelector periodSelector = new RangePeriodSelector(period);
		filter.setRangePeriodSelector(periodSelector);

		return hoursRepository.getHours(filter);
	}

}
