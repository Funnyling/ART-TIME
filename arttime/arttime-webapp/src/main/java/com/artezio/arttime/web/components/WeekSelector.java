package com.artezio.arttime.web.components;

import java.util.Date;

import javax.faces.component.FacesComponent;

import org.apache.commons.lang.time.DateUtils;

@FacesComponent("weekSelector")
public class WeekSelector extends PeriodSelector {

	@Override
	public void setPreviousPeriod() {
		Date start = DateUtils.addDays(getPeriod().getStart(), -7);
		Date finish = DateUtils.addDays(getPeriod().getFinish(), -7);
		updatePeriod(start, finish);		
	}

	@Override
	public void setNextPeriod() {
		Date start = DateUtils.addDays(getPeriod().getStart(), 7);
		Date finish = DateUtils.addDays(getPeriod().getFinish(), 7);
		updatePeriod(start, finish);
	}

}
