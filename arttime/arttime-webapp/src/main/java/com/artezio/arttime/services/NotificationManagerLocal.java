package com.artezio.arttime.services;

import java.util.List;

import javax.ejb.Asynchronous;
import javax.ejb.Local;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.Period;


@Local
public interface NotificationManagerLocal {		
	@Asynchronous
	void notificateAboutWorkTimeProblem(List<Employee> employees, Period period, String notificationComment);
}
