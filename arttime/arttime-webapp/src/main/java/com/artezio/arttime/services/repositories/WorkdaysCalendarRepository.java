package com.artezio.arttime.services.repositories;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import com.artezio.arttime.admin_tool.cache.WebCached;
import com.artezio.arttime.admin_tool.cache.WebCached.Scope;
import com.artezio.arttime.datamodel.Day;
import com.artezio.arttime.datamodel.Period;
import com.artezio.arttime.datamodel.WorkdaysCalendar;
import com.artezio.arttime.exceptions.WorkdaysCalendarRemoveException;
import com.artezio.arttime.web.interceptors.FacesMessage;

@Named
@Stateless
@WebCached(scope = Scope.REQUEST_SCOPED)
public class WorkdaysCalendarRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public WorkdaysCalendar findWorkdaysCalendar(Long id) {
	return entityManager.find(WorkdaysCalendar.class, id);
    }

    @FacesMessage(onCompleteMessageKey = "message.calendarIsSaved")
    public WorkdaysCalendar create(WorkdaysCalendar workdaysCalendar) {
	entityManager.persist(workdaysCalendar);
	return workdaysCalendar;
    }

    @SuppressWarnings("unchecked")
    public List<WorkdaysCalendar> getWorkdaysCalendars() {
	List<WorkdaysCalendar> result = entityManager.createQuery("SELECT w FROM WorkdaysCalendar w").getResultList();
	return result;
    }

    @FacesMessage(onCompleteMessageKey = "message.calendarIsDeleted")
    public void remove(WorkdaysCalendar workdaysCalendar) throws WorkdaysCalendarRemoveException {
	entityManager.createQuery("DELETE FROM Day d WHERE d.workdaysCalendar = :calendar")
		.setParameter("calendar", workdaysCalendar).executeUpdate();
	entityManager.createQuery("DELETE FROM WorkdaysCalendar w WHERE w = :calendar")
		.setParameter("calendar", workdaysCalendar).executeUpdate();
    }

    public void update(List<Day> days) {
	for (Day day : days) {
	    if (day.getId() == null) {
		entityManager.persist(day);
	    } else {
		entityManager.merge(day);
	    }
	}
    }

    public List<Day> getDays(WorkdaysCalendar workdaysCalendar, Period period) {
		List<Day> result = getPersistedDays(workdaysCalendar, period);
		List<Date> existedDates = getDates(result);
		List<Date> newDates = new ArrayList<Date>(period.getDays());
		newDates.removeAll(existedDates);
		List<Day> createdDays = createDays(newDates, workdaysCalendar);
		result.addAll(createdDays);
		return result;
    }

    @SuppressWarnings("unchecked")
    public List<Day> getPersistedDays(WorkdaysCalendar workdaysCalendar, Period period) {
		String query = "SELECT d FROM Day d " + 
				"WHERE d.workdaysCalendar = :calendar " +
				"AND d.date >= :start AND d.date <= :finish";
		List<Day> result = entityManager.createQuery(query)
				.setParameter("calendar", workdaysCalendar)
				.setParameter("start", period.getStart(), TemporalType.DATE)
				.setParameter("finish", period.getFinish(), TemporalType.DATE).getResultList();
		return result;
    }

    private List<Date> getDates(List<Day> days) {
	List<Date> result = new ArrayList<Date>();
	for (Day day : days) {
	    result.add(day.getDate());
	}
	return result;
    }

    protected List<Day> createDays(List<Date> dates, WorkdaysCalendar calendar) {
	List<Day> result = new ArrayList<Day>();
	for (Date date : dates) {
	    result.add(new Day(date, calendar));
	}
	return result;
    }

    @FacesMessage(onCompleteMessageKey = "message.calendarIsSaved")
    public void update(WorkdaysCalendar workdaysCalendar, List<Day> days) {
	entityManager.merge(workdaysCalendar);
	update(days);
    }
}
