package com.artezio.arttime.security.auth;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.web.filter.authc.BasicHttpAuthenticationFilter;

public class HttpPostAuthenticationFilter extends BasicHttpAuthenticationFilter{
	
	@Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {		
		String method = ((HttpServletRequest)request).getMethod();
		return ("POST".equals(method))
				? super.isAccessAllowed(request, response, mappedValue)
				: true;
    }	
	
}
