package com.artezio.arttime.services.integration;

import java.util.List;

import com.artezio.arttime.datamodel.Employee;

public interface EmployeeService {

	String getName();
	List<Employee> getEmployees();
	
	Employee findEmployee(String userName);
}
