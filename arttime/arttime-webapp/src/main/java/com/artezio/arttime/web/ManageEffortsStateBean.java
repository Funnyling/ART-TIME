package com.artezio.arttime.web;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class ManageEffortsStateBean implements Serializable {

	private static final long serialVersionUID = 7856599847470669412L;
	
	private EffortsGrouping grouping = EffortsGrouping.BY_PROJECTS;

	public EffortsGrouping getGrouping() {
		return grouping;
	}

	public void setGrouping(EffortsGrouping grouping) {
		this.grouping = grouping;
	}
	
}
