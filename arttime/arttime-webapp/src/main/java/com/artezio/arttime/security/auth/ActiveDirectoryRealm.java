package com.artezio.arttime.security.auth;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.ldap.LdapContextFactory;
import org.apache.shiro.subject.PrincipalCollection;

import com.artezio.arttime.services.repositories.ProjectRepository;

public class ActiveDirectoryRealm extends org.apache.shiro.realm.activedirectory.ActiveDirectoryRealm {
	public final static String REALM_NAME = "ldapRealm";
	private ProjectRepository projectRepository;
	
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		AuthorizationInfo info = super.doGetAuthorizationInfo(principals); 
		if (!principals.isEmpty() && principals.getRealmNames().contains(REALM_NAME)) {
			info.getRoles().add(UserRoles.EMPLOYEE_ROLE);
			String principalName = (String) principals.getPrimaryPrincipal();
			if (isProjectManager(principalName)) {
				info.getRoles().add(UserRoles.PM_ROLE);
			}
		}		
		return info;
	}
	
	protected boolean isProjectManager(String username) {
		try {
			return !getProjectRepository().getProjectsByManager(username).isEmpty();
		} catch (NamingException e) {
			e.printStackTrace();
			return false;
		}
    }

    private ProjectRepository getProjectRepository() throws NamingException {
		if (projectRepository == null) {
		    projectRepository = (ProjectRepository) new InitialContext().lookup("java:global/arttime-webapp/ProjectRepository");
		}
		return projectRepository;
    }
    
    protected AuthorizationInfo queryForAuthorizationInfo(PrincipalCollection principals, LdapContextFactory ldapContextFactory) throws NamingException {
    	return super.queryForAuthorizationInfo(principals, ldapContextFactory);
    }

}
