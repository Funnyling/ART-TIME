package com.artezio.arttime.datamodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.NotEmpty;

import com.artezio.arttime.datamodel.ProjectStatus.Status;
import com.artezio.arttime.datamodel.TeamFilter.FilterType;
import com.google.common.collect.Iterables;

@Entity
@XmlAccessorType(XmlAccessType.FIELD)
@Table(uniqueConstraints = {
		@UniqueConstraint(name = "constraint_unique_project_code", columnNames = {"code"})
	})
public class Project implements Serializable {

	private static final long serialVersionUID = 227312916678442090L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;	
	@Column(length=1000)
    @Size(max = 1000, message = "{constraints.string.size.message}")
	private String description;
	@XmlTransient
	@Column(columnDefinition = "BIT", length = 1)
	private boolean imported;
	@Valid
	@NotNull
	@XmlTransient	
	private TeamFilter teamFilter;
	@NotNull
	private String code;
	@NotNull	
	@ManyToOne
	private Employee manager;
	@NotEmpty
	@ManyToMany(fetch = FetchType.LAZY)
	@org.hibernate.annotations.Fetch(FetchMode.SUBSELECT)
	private List<HourType> accountableHours = new ArrayList<HourType>();
	@Valid
	@XmlTransient
	@org.hibernate.annotations.Fetch(FetchMode.SUBSELECT)
	@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, orphanRemoval = true)
	@Size(min=1, message="{project.statuses.notEmpty}")
	private List<ProjectStatus> statuses = new ArrayList<ProjectStatus>();
	@Valid
	@XmlTransient
	@org.hibernate.annotations.Fetch(FetchMode.SUBSELECT)
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<Participation> team = new ArrayList<Participation>();
	@NotNull
	@ManyToOne
	private WorkdaysCalendar defaultWorkdaysCalendar;
	@Column(columnDefinition = "BIT", length = 1)
	private boolean allowEmployeeReportTime = true;

	public final static Comparator<Project> CODE_COMPARATOR = (p1, p2) -> p1.getCode().compareToIgnoreCase(p2.getCode());
	
	
	
	public Project() {
		statuses.add(new ProjectStatus());
	}

	public Long getId() {
		return id;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setImported(boolean imported) {
		this.imported = imported;
	}

	public boolean isImported() {
		return imported;
	}

	public Employee getManager() {
		return manager;
	}

	public void setManager(Employee manager) {
		this.manager = manager;
	}

	public List<HourType> getAccountableHours() {
		return accountableHours;
	}

	public void setAccountableHours(List<HourType> accountableHours) {
		this.accountableHours = accountableHours;
	}

	public List<ProjectStatus> getStatuses() {
		return statuses;
	}

	public void setStatuses(List<ProjectStatus> statuses) {
		this.statuses = statuses;
	}

	public List<Participation> getTeam() {
		return team;
	}

	public void setTeam(List<Participation> team) {
		this.team = team;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

    public WorkdaysCalendar getDefaultWorkdaysCalendar() {
		return defaultWorkdaysCalendar;
	}

	public void setDefaultWorkdaysCalendar(
			WorkdaysCalendar defaultWorkdaysCalendar) {
		this.defaultWorkdaysCalendar = defaultWorkdaysCalendar;
	}

	@XmlElement(name = "currentStatus")
	public ProjectStatus.Status getCurrentStatus() {
		ProjectStatus.Status result = null;
		Date now = new Date();
		statuses.sort(ProjectStatus.STARTAT_COMPARATOR);
		for (int i = statuses.size() - 1; i >= 0; i--) {
			if (!statuses.get(i).isEmpty() && 
					!(statuses.get(i).getStartAt().after(now))) {
				result = statuses.get(i).getStatus();
				break;
			}
		}
		return result;
	}

	public boolean isActiveIn(Period searchedPeriod) {
		Map<ProjectStatus, Period> statusesPeriod = getStatusesPeriod(statuses);
		for (ProjectStatus status : statuses) {
			Period statusPeriod = statusesPeriod.get(status);
			if (status.getStatus() == Status.ACTIVE
					&& searchedPeriod.isIntersect(statusPeriod)) {
				return true;
			}
		}
		return false;
	}

	private Map<ProjectStatus, Period> getStatusesPeriod(
			List<ProjectStatus> statuses) {
		Map<ProjectStatus, Period> result = new HashMap<ProjectStatus, Period>();
		statuses.sort(ProjectStatus.STARTAT_COMPARATOR);
		for (int i = 0; i < statuses.size(); i++) {
			ProjectStatus status = statuses.get(i);
			ProjectStatus nextStatus = i == statuses.size() - 1 ? null
					: statuses.get(i + 1);
			Period period = nextStatus != null ? new Period(
					status.getStartAt(), nextStatus.getStartAt()) : new Period(
					status.getStartAt(), null);
			result.put(status, period);
		}
		return result;
	}

	public boolean isActiveOn(Date date) {
		Map<ProjectStatus, Period> statusesPeriod = getStatusesPeriod(statuses);
		for (ProjectStatus status : statuses) {
			Period statusPeriod = statusesPeriod.get(status);
			if (status.getStatus() == Status.ACTIVE
					&& statusPeriod.contains(date)) {
				return true;
			}
		}
		return false;
	}

	public void addParticipation(Participation participation) {
		team.add(participation);
	}

	public void removeParticipation(Participation part) {
		team.remove(part);
	}

	public void addStatus(ProjectStatus status) {
		if (statuses.isEmpty() || !getFirstProjectStatus().isEmpty()){
			statuses.add(status);
		} else {
			setFirstProjectStatus(status);
		}
	}

	public void addAccountableHours(HourType type) {
		accountableHours.add(type);
	}
	
	public TeamFilter getTeamFilter() {
		if (teamFilter == null) {
			teamFilter = new TeamFilter();
		}
		return teamFilter;
	}

	public void setTeamFilter(TeamFilter teamFilter) {
		this.teamFilter = teamFilter;
	}

	public boolean isTeamMember(Employee employee) {
		boolean result = false;
		for (Participation participation : team) {
			if (participation.getEmployee().equals(employee)) {
				result = true;
				break;
			}
		}
		return result;
	}

	public boolean isTeamMember(Employee employee, Period period) {
		Set<Employee> members = getTeamMembers(period);
		return members.contains(employee);
	}
	
	public boolean isTeamMember(Employee employee, Date date) {
		return team.stream()
				.filter(participation -> employee.equals(participation.getEmployee()))
				.filter(participation -> participation.getPeriod().contains(date))
				.findFirst().isPresent();		
	}

	public boolean isTeamMember(Employee employee, Date date, WorkdaysCalendar calendar) {
		return team.stream().anyMatch(participation -> participation.getWorkdaysCalendar().equals(calendar)
				&& participation.getEmployee().equals(employee)
				&& participation.getPeriod().contains(date));
	}

	public Set<Employee> getTeamMembers(Period period) {
		Set<Employee> teamMembers = new HashSet<Employee>();
        for (Participation participation : getActualParticipations(period)) {
            teamMembers.add(participation.getEmployee());
        }
        return teamMembers;
	}
	
	public List<Participation> getActualParticipation(Date date){		
		List<Participation> result = new ArrayList<Participation>();
		for (Participation participation : team){
			Period period = participation.getPeriod();
			if (period.contains(date)){
				result.add(participation);
			}
		}
		return result;
	}

    public List<Participation> getActualParticipations(Employee employee, Period period) {
        List<Participation> result = new ArrayList<Participation>();
        for (Participation participation : getActualParticipations(period)) {
            if (employee.equals(participation.getEmployee())) {
                result.add(participation);
            }
        }
        return result;
    }

    public List<Participation> getActualParticipations(Period period) {
        List<Participation> result = new ArrayList<Participation>();
        for (Participation participation : team) {
            Period participationPeriod = participation.getPeriod();
            if (period.isIntersect(participationPeriod)) {
                result.add(participation);
            }
        }
        return result;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Project other = (Project) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "projectId: " + id + " code: " + code;
	}

	public List<Employee> getTeamMembers() {
		Set<Employee> teamMambers = new HashSet<Employee>();
		for (Participation participation : team) {
			teamMambers.add(participation.getEmployee());
		}
		return new ArrayList<Employee>(teamMambers);
	}

	public boolean isAllowEmployeeReportTime() {
		return allowEmployeeReportTime;
	}

	public void setAllowEmployeeReportTime(boolean allowEmployeeReportTime) {
		this.allowEmployeeReportTime = allowEmployeeReportTime;
	}

	public boolean isTeamSynchronizationEnabled() {
		return teamFilter.getFilterType() != FilterType.DISABLED
				&& StringUtils.isNotBlank(teamFilter.getValue());
	}
	
	public List<Employee> getActualTeamMembers(Date date) {
		List<Participation> team = getActualParticipation(date);
		List<Employee> teamMembers = new ArrayList<Employee>();
		for (Participation participation : team) {
			teamMembers.add(participation.getEmployee());			
		}
		return teamMembers;
	}

	public void closeParticipations(List<Employee> notTeamMembersAnymore) {
		Date currentDate = new Date();
		List<Participation> team = getActualParticipation(currentDate);
		for (Participation participation : team) {
			Employee employee = participation.getEmployee();
			if (notTeamMembersAnymore.contains(employee)) {
				participation.getPeriod().setFinish(currentDate);
			}
		}
		
	}

	public void addTeamMembers(List<Employee> newTeamMembers) {
		Date currentDate = new Date();
		for (Employee employee : newTeamMembers) {
			Participation participation = new Participation(employee, currentDate);
			participation.setWorkdaysCalendar(defaultWorkdaysCalendar);
			team.add(participation);
		}
		
	}

	public Map<Employee, List<Period>> getParticipationPeriods(Period period) {
		Map<Employee, List<Period>> result = new HashMap<Employee, List<Period>>();
		for (Participation participation : team) {
			Employee employee = participation.getEmployee();
			Period intersection = period.getIntersection(participation.getPeriod());
			if (intersection != null) {
				if (!result.containsKey(employee)) {
					result.put(employee, new ArrayList<Period>());
				}				
				result.get(employee).add(intersection);				
			}
		}
		return result;
	}
	
	public WorkdaysCalendar findWorkdaysCalendar(Employee employee, Date date) {
		 Optional<Participation> optional = team.stream()
				.filter(p -> employee.equals(p.getEmployee()) && p.getPeriod().contains(date))
				.findFirst();
		return (optional.isPresent())
				? optional.get().getWorkdaysCalendar()
				: null;
	}
	
	public Participation getFirstParticipation(Employee employee) {				
		 List<Participation> participations = getParticipations(employee);
		 participations.sort((p1, p2) -> p1.getPeriod().getStart().compareTo(p2.getPeriod().getStart()));
		 return Iterables.getFirst(participations, null);
	}
	
	public Participation getLastParticipation(Employee employee) {				
		 List<Participation> participations = getParticipations(employee);
		 participations.sort((p1, p2) -> p1.getPeriod().getStart().compareTo(p2.getPeriod().getStart()));
		 return Iterables.getLast(participations, null);
	}
	
	public List<Participation> getParticipations(Employee employee) {				
		 return team.stream()
			.filter(p -> employee.equals(p.getEmployee()))
			.collect(Collectors.toList());		 
	}
	
	public void setFirstProjectStatus(ProjectStatus projectStatus){
		statuses.set(0, projectStatus);
	}
	
	public ProjectStatus getFirstProjectStatus(){
		return statuses.isEmpty()? null : statuses.get(0);
	}
}
