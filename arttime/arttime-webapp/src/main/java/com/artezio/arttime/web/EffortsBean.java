package com.artezio.arttime.web;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.faces.application.Application;
import javax.faces.application.ViewHandler;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.commons.lang.time.DateUtils;

import com.artezio.arttime.datamodel.Day;
import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.Hours;
import com.artezio.arttime.datamodel.Period;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.datamodel.WorkdaysCalendar;
import com.artezio.arttime.filter.Filter;
import com.artezio.arttime.services.repositories.WorkdaysCalendarRepository;
import com.artezio.arttime.utils.CalendarUtils;
import com.artezio.arttime.web.spread_sheet.SpreadSheet;

public abstract class EffortsBean implements Serializable {
	private static final long serialVersionUID = 6796778823308301114L;
	
	private SpreadSheet spreadSheet;
	@ManagedProperty("#{filterBean}")
    protected FilterBean filterBean;
    @Inject
    private FacesContext facesContext;
    @Inject
    private WorkdaysCalendarRepository workdaysCalendarRepository;
    private Map<WorkdaysCalendar, List<Day>> calendarDays = new HashMap<WorkdaysCalendar, List<Day>>();
    
    private List<Employee> employees;
    private Period period = new Period();
    
	protected abstract SpreadSheet initSpreadSheet();
    
    public SpreadSheet getSpreadSheet() {
    	if (spreadSheet == null) {
    		spreadSheet = initSpreadSheet();    		
    	}
    	return spreadSheet;
    }           
    
    public void resetData() {
		spreadSheet = null;
		resetComponentsTree();
    }

    protected void resetComponentsTree() {
		Application application = facesContext.getApplication();
		ViewHandler viewHandler = application.getViewHandler();
		UIViewRoot viewRoot = viewHandler.createView(facesContext, facesContext.getViewRoot().getViewId());
		facesContext.setViewRoot(viewRoot);
    }    

    public String getStyleClass(Hours hours) {    	
    	if (hours == null || !hours.getProject().isActiveOn(hours.getDate())
    			|| !hours.getProject().getActualTeamMembers(hours.getDate()).contains(hours.getEmployee())) {
    		return "emptyCell";
    	}
    	Date date = hours.getDate();
		Project project = hours.getProject();
		Employee employee = hours.getEmployee();
		WorkdaysCalendar workdaysCalendar = project.findWorkdaysCalendar(employee, date);
		boolean weekend = isWeekend(workdaysCalendar, date);
		if (hours.isApproved() && !weekend) {
			return "approvedHours";
		} else if (hours.isApproved() && weekend) {
			return "approvedWeekendHours";
		} else if (weekend) {
			return "weekendCell";
		}
    	return "";
    }

	private boolean isWeekend(WorkdaysCalendar workdaysCalendar, Date date) {
		Day day = getDay(workdaysCalendar, date);
		return !day.isWorking();
	}
	
	private Day getDay(WorkdaysCalendar workdaysCalendar, Date date) {
		if (!calendarDays.containsKey(workdaysCalendar)) {
			List<Day> days = workdaysCalendarRepository.getDays(workdaysCalendar, filterBean.getCurrentFilter().getPeriod());
			calendarDays.put(workdaysCalendar, days);
		}
		return findDay(calendarDays.get(workdaysCalendar), date);		
	}

    private Day findDay(List<Day> days, Date date) {
		return days.stream()
				.filter(day -> date.equals(day.getDate()))
				.findFirst().orElse(null);
	}
	
	public String getHeaderStyleClass(Date date){		
		String style = CalendarUtils.isWeekend(date) ? "weekendHeader" : "dayHeader";
		Date currentDay = new Date();
		style += DateUtils.isSameDay(currentDay, date) ? " currentDay" : "";		
		return style;
	}
	
	public void setPeriod(Period period) {
		this.period = period;
	}
	
	public Period getPeriod() {
		return period;
	}
	
	public List<Employee> getEmployees() {
		return employees;
	}
	
	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	
	public void initByGetRequest() {
		if (!period.hasNullDates()) {
			filterBean.setDefaultFilter();
			Filter currentFilter = filterBean.getCurrentFilter(); 
			currentFilter.setCustomPeriod(period);
			if (employees != null && !employees.isEmpty()) {
				currentFilter.setEmployees(employees);
			}
		}
	}
}
