package com.artezio.arttime.web;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.inject.Inject;
import javax.validation.constraints.AssertTrue;

import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.datamodel.ProjectStatus;
import com.artezio.arttime.datamodel.ProjectStatus.Status;
import com.artezio.arttime.services.repositories.ProjectRepository;

@ManagedBean
@ViewScoped
public class ProjectStatusBean implements Serializable {
	private static final long serialVersionUID = -2232557902247779429L;
	private Project project;
	@Inject
	private ExternalContext externalContext;
	private ProjectStatus projectStatus = new ProjectStatus();
	@Inject
	private ProjectRepository projectRepository;

	@PostConstruct
	public void init() {
		Map<String, String> requestParams = externalContext.getRequestParameterMap();
		String projectCode = requestParams.get("project");
		this.project = projectRepository.findProject(projectCode);
	}

	public Project getProject() {
		return project;
	}

	public ProjectStatus getProjectStatus() {
		return projectStatus;
	}

	public void setProjectStatus(ProjectStatus projectStatus) {
		this.projectStatus = projectStatus;
	}

	public void addStatus() {
		project.addStatus(projectStatus);
		projectStatus = new ProjectStatus();
	}

	public Status[] getStatuses() {
		return Status.values();
	}

	public void delete(ProjectStatus projectStatus) {
		project.getStatuses().remove(projectStatus);
	}

}
