package com.artezio.arttime.report;

public enum ReportTemplate {
	PROJECT_REPORT("report_templates/projectReport.rptdesign"),
	DAYS_REPORT("report_templates/daysReport.rptdesign"),	
    EMPLOYEES_REPORT("report_templates/employeesReport.rptdesign"),
    TIME_BALANCE_REPORT("report_templates/timeBalanceReport.rptdesign");
	
	private String fileName;

	private ReportTemplate(String fileName) {
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;
	}
}
