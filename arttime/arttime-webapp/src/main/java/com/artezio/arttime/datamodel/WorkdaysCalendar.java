package com.artezio.arttime.datamodel;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@Entity
@XmlAccessorType(XmlAccessType.FIELD)
@Table(uniqueConstraints = {
 @UniqueConstraint(name = "constraint_unique_calendar_name", columnNames = { "name" }) })
public class WorkdaysCalendar implements Serializable {

    private static final long serialVersionUID = 945234047123854630L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private String name;

    public WorkdaysCalendar() {
    }

    public WorkdaysCalendar(String name) {
	this.name = name;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public Long getId() {
	return id;
    }

    @Override
    public boolean equals(Object o) {
	if (this == o)
	    return true;
	if (!(o instanceof WorkdaysCalendar))
	    return false;

	WorkdaysCalendar that = (WorkdaysCalendar) o;

	if (id != null ? !id.equals(that.id) : that.id != null)
	    return false;

	return true;
    }

    @Override
    public int hashCode() {
	return id != null ? id.hashCode() : 0;
    }

}
