package com.artezio.arttime.web.criteria;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named
@SessionScoped
public class EmployeeSearchCriteriaBean implements Serializable {
	private static final long serialVersionUID = 2460272481816428164L;
	private String searchQuery;
	
	public String getSearchQuery() {
		return searchQuery;
	}

	public void setSearchQuery(String searchQuery) {
		this.searchQuery = searchQuery;
	}		
}
