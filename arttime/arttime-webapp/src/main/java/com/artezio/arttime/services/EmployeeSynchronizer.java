package com.artezio.arttime.services;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.services.integration.EmployeeService;
import com.artezio.arttime.services.repositories.EmployeeRepository;

@Named
@Stateless
public class EmployeeSynchronizer implements EmployeeSynchronizerLocal {

    @Inject
    private EmployeeService employeeService;
    @Inject
    private EmployeeRepository employeeRepository;
    private Logger log = Logger.getLogger(this.getClass().getName());

    public void synchronizeEmployees() {
    log.info("Employees synchronization started: ");
	List<Employee> employees = employeeRepository.getAll();
	for (Employee employee : employees) {
	    String userName = employee.getUserName();
	    Employee externalEmployee = employeeService.findEmployee(userName);
	    if (externalEmployee != null) {
		employee.setFirstName(externalEmployee.getFirstName());
		employee.setLastName(externalEmployee.getLastName());
		employee.setEmail(externalEmployee.getEmail());
		employee.setDepartment(externalEmployee.getDepartment());
		employeeRepository.update(employee);
	    }
	}
	log.info("Employees synchronization completed.");
    }

}
