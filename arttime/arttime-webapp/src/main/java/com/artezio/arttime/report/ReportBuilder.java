package com.artezio.arttime.report;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Map;

import org.eclipse.birt.core.exception.BirtException;
import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.engine.api.EXCELRenderOption;
import org.eclipse.birt.report.engine.api.EngineConfig;
import org.eclipse.birt.report.engine.api.EngineConstants;
import org.eclipse.birt.report.engine.api.EngineException;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportEngineFactory;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.eclipse.birt.report.engine.api.PDFRenderOption;
import org.eclipse.birt.report.engine.api.RenderOption;

import uk.co.spudsoft.birt.emitters.excel.ExcelEmitter;

public class ReportBuilder {
	
	private static final String UTF_8 = "UTF-8";
	private static final String DOC_OUTPUT_FORMAT = "doc";
	private static final String EXCEL_OUTPUT_FORMAT = "xlsx";
	private static final String EXCEL_EMITTER_ID = "uk.co.spudsoft.birt.emitters.excel.XlsxEmitter";
	
	public byte[] getReport(String reportFile, String reportType, Map<String, Object> params, Connection connection){
		byte[] bytes = null;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			EngineConfig config = new EngineConfig();
			Platform.startup(config);
			IReportEngineFactory factory = (IReportEngineFactory)Platform.createFactoryObject(IReportEngineFactory.EXTENSION_REPORT_ENGINE_FACTORY);
			IReportEngine engine = factory.createReportEngine(config);
			IReportRunnable design = getReportDesign(engine, reportFile);
			IRunAndRenderTask task = engine.createRunAndRenderTask(design);
			task.getAppContext().put(EngineConstants.APPCONTEXT_CLASSLOADER_KEY, this.getClass().getClassLoader());
			setTaskParams(task, params);
			setTaskRenderOptions(task, reportType, baos);
			
			task.run();
			task.close();
			engine.destroy();
			bytes = baos.toByteArray();						
		} catch (EngineException e) {
			throw new RuntimeException("Can not create Birt report design",e);
		} catch (IOException e) {
			throw new RuntimeException("Can not load report file!", e);
		} catch (BirtException e) {
			throw new RuntimeException("Can not start Birt platform", e);
		} finally {
			try {
				baos.close();
			} catch (IOException e){
				throw new RuntimeException("Can not close Birt report output stream", e);
			}
		}
		return bytes;
	}
	
	private IReportRunnable getReportDesign(IReportEngine engine, String reportFile) 
			throws EngineException, IOException {
		InputStream is = new ByteArrayInputStream(reportFile.getBytes(UTF_8));
		IReportRunnable design = engine.openReportDesign(is); 
		is.close();
		return design;
	}
	
	private void setTaskParams(IRunAndRenderTask task, Map<String, Object> params) {
		for(String key : params.keySet()) {
			Object value = params.get(key);
			task.setParameterValue(key, value);
		}
		task.validateParameters();
	}
	
	private void setTaskRenderOptions(IRunAndRenderTask task, String reportType, ByteArrayOutputStream baos) {		
		RenderOption options = null;
		if (ReportUtils.REPORT_TYPE_PDF.equalsIgnoreCase(reportType)) {
			options = new PDFRenderOption();
			options.setOutputFormat(PDFRenderOption.OUTPUT_FORMAT_PDF);		
		} else if (ReportUtils.REPORT_TYPE_EXCEL.equalsIgnoreCase(reportType)) {
			options = new EXCELRenderOption();
			options.setEmitterID(EXCEL_EMITTER_ID);
			options.setOutputFormat(EXCEL_OUTPUT_FORMAT);		
			options.setOption(ExcelEmitter.REMOVE_BLANK_ROWS, false);		 
		} else {
			options = new RenderOption();
			options.setOutputFormat(DOC_OUTPUT_FORMAT);
		}
			
		options.setOutputStream(baos);
		task.setRenderOption(options);
	}

}
