package com.artezio.arttime.web.spread_sheet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.HourType;
import com.artezio.arttime.datamodel.Hours;
import com.artezio.arttime.datamodel.Project;

public class HoursIndexedBundle {
	private Map<Object, Map<Object, Map<Object, List<Hours>>>> hours;
	
	public HoursIndexedBundle(List<Hours> hours) {		
		this.hours = hours.parallelStream()
				.collect(
						Collectors.groupingBy(hour -> hour.getProject(), 
								Collectors.groupingBy(hour -> hour.getEmployee(), 
										Collectors.groupingBy(hour -> hour.getType(), Collectors.toList()))));		
	}

	public List<Hours> findHours(Project project, Employee employee, HourType hourType) {		
		return (hours.containsKey(project))
				? findHours(hours.get(project), employee, hourType)
				: new ArrayList<Hours>();
	}

	private List<Hours> findHours(Map<Object, Map<Object, List<Hours>>> map, Employee employee, HourType hourType) {		
		return (map.containsKey(employee))
				? findHours(map.get(employee), hourType)
				: new ArrayList<Hours>();
	}

	private List<Hours> findHours(Map<Object, List<Hours>> map, HourType hourType) {
		return (map.containsKey(hourType))
				? map.get(hourType)
				: new ArrayList<Hours>();
	}
}
