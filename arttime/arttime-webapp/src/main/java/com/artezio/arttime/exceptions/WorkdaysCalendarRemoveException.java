package com.artezio.arttime.exceptions;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class WorkdaysCalendarRemoveException extends Exception {

    private static final long serialVersionUID = 5015920220174612609L;

    public WorkdaysCalendarRemoveException(String message) {
        super(message);
    }
}
