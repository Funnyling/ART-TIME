package com.artezio.arttime.services;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import com.artezio.arttime.admin_tool.cache.WebCached;
import com.artezio.arttime.admin_tool.cache.WebCached.Scope;
import com.artezio.arttime.config.ApplicationSettings;
import com.artezio.arttime.config.Settings;
import com.artezio.arttime.web.interceptors.FacesMessage;

@Named
@Stateless
public class SettingsService {

    @PersistenceContext
    private EntityManager entityManager;

    @Produces
    @ApplicationSettings
    public Settings getSettings() {
	try {
	    return (Settings) entityManager.createQuery("SELECT s FROM Settings s").getSingleResult();
	} catch (NoResultException e) {
	    return null;
	}
    }
    
    @FacesMessage(onCompleteMessageKey = "message.settingsAreSaved")
    public Settings update(Settings settings) {
      Long settingsId = settings.getId();
      if (settingsId == null) {
          entityManager.createQuery("DELETE FROM Settings").executeUpdate();
          entityManager.persist(settings);
      } else {
          entityManager.merge(settings);
      }
      return settings;
  }

}
