package com.artezio.arttime.web;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import com.artezio.arttime.config.ApplicationSettings;
import com.artezio.arttime.config.Settings;

@ManagedBean
@ViewScoped
public class SettingsBean implements Serializable {
    private static final long serialVersionUID = -4663864281011566866L;

    @Inject
    @ApplicationSettings
    private Settings settings;

    public Settings getSettings() {
    	if (settings == null) {
    		settings = new Settings();
    	}
    	return settings;
    }   
}
