package com.artezio.arttime.security.auth;

public class UserRoles {
	public static final String PM_ROLE = "pm";	
	public static final String EXEC_ROLE = "exec";
	public static final String ADMIN_ROLE = "admin";
	public static final String EMPLOYEE_ROLE = "employee";
	public static final String INTEGRATION_CLIENT_ROLE = "ArtTimeIntegrationClient";
}
