package com.artezio.arttime.services.repositories;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import com.artezio.arttime.admin_tool.cache.WebCached;
import com.artezio.arttime.admin_tool.cache.WebCached.Scope;
import com.artezio.arttime.datamodel.HourType;
import com.artezio.arttime.exceptions.ActualTimeRemovalException;
import com.artezio.arttime.web.interceptors.FacesMessage;

@Named
@Stateless
@WebCached(scope = Scope.REQUEST_SCOPED)
public class HourTypeRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @FacesMessage(onCompleteMessageKey = "message.hourTypeIsSaved")
    public HourType create(HourType hourType) {
	entityManager.persist(hourType);
	if (!isActualTimeExist()) {
	    return setAsActualTime(hourType);
	}
	return hourType;
    }

    public boolean isActualTimeExist() {
	List<HourType> hourTypes = entityManager.createQuery("SELECT h FROM HourType h WHERE h.actualTime = true",
		HourType.class).getResultList();
	return !hourTypes.isEmpty();
    }

    public HourType setAsActualTime(HourType hourType) {
	entityManager.createQuery("UPDATE HourType h SET h.actualTime = false ").executeUpdate();
	hourType.setActualTime(true);
	return entityManager.merge(hourType);
    }

    public HourType findActual() {
	try {
	    return (HourType) entityManager.createQuery("SELECT h FROM HourType h WHERE h.actualTime = true")
		    .getSingleResult();
	} catch (NoResultException e) {
	    return null;
	}
    }

    @SuppressWarnings("unchecked")
    public List<HourType> getAll() {
    	List<HourType> result = entityManager.createQuery("SELECT h FROM HourType h").getResultList();
    	result.sort(HourType.ACTUALTIME_TYPE_COMPARATOR);
    	return result;
    }

    @FacesMessage(onCompleteMessageKey = "message.hourTypeIsDeleted")
    public void remove(HourType hourType) throws ActualTimeRemovalException {
	if (hourType.isActualTime()) {
	    throw new ActualTimeRemovalException("Attempt to delete an actual time.");
	}
	HourType type = entityManager.find(HourType.class, hourType.getId());
	entityManager.remove(type);
	entityManager.flush();
    }

    @FacesMessage(onCompleteMessageKey = "message.hourTypeIsSaved")
    public HourType update(HourType hourType) {
	return entityManager.merge(hourType);
    }

    public HourType find(Long id) {
	return entityManager.find(HourType.class, id);
    }
}
