package com.artezio.arttime.filter;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.hibernate.Hibernate;

import com.artezio.arttime.admin_tool.cache.WebCached;
import com.artezio.arttime.admin_tool.cache.WebCached.Scope;
import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.services.repositories.ProjectRepository;
import com.artezio.arttime.web.interceptors.FacesMessage;

@Named
@Stateless
public class FilterRepository {
	@PersistenceContext
	private EntityManager entityManager;
	@Inject
	private ProjectRepository projectRepository;
	
	@WebCached(scope = Scope.REQUEST_SCOPED)
	public List<Filter> getFilters(Employee owner) {
		return entityManager
				.createQuery("SELECT f FROM Filter f WHERE f.owner = :owner", Filter.class)
				.setParameter("owner", owner.getUserName()).getResultList();
	}
	
	@WebCached(resetCache = true)
	public Filter create(Filter filter) {
		filter.setId(null);
		entityManager.persist(filter);
		return filter;
	}
	
	@WebCached(resetCache = true)
	public Filter update(Filter filter) {
		entityManager.merge(filter);
		entityManager.flush();
		return filter;
	}

	public Filter fetchDetails(Filter filter) {
		if (filter.getId() != null) {
			filter = entityManager.find(Filter.class, filter.getId());
			Hibernate.initialize(filter.getDepartments());
			Hibernate.initialize(filter.getEmployees());
			Hibernate.initialize(filter.getProjects());
			Hibernate.initialize(filter.getHourTypes());
		}
		fetchProjects(filter);
		return filter;
	}
	
	public void fetchProjects(Filter filter){
		filter.setProjects(projectRepository.fetchComplete(filter.getProjects()));
	}
	
	@FacesMessage(onCompleteMessageKey = "message.filterIsDeleted")
	@WebCached(resetCache = true)
	public void remove(Filter filter) {
		filter = find(filter.getId());
		entityManager.remove(filter);
		entityManager.flush();
	}		
	
	public Filter find(String owner, String name) {
		try {
			return entityManager
				.createQuery("SELECT f FROM Filter f "
						+ "WHERE f.name = :name AND f.owner = :owner", Filter.class)
				.setParameter("name", name)
				.setParameter("owner", owner)					
				.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public Filter find(Long id) {
		return entityManager.find(Filter.class, id);
	}
	
}
