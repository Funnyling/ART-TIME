package com.artezio.arttime.web;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import com.artezio.arttime.filter.Filter;
import com.artezio.arttime.services.repositories.HoursRepository;
import com.artezio.arttime.services.repositories.WorkdaysCalendarRepository;
import com.artezio.arttime.web.spread_sheet.EmployeeEffortsSpreadSheet;
import com.artezio.arttime.web.spread_sheet.ProjectEffortsSpreadSheet;
import com.artezio.arttime.web.spread_sheet.SpreadSheet;

@Named
public class ManageEffortsSpreadSheetFactory {
	@Inject
    private HoursRepository hoursRepository;	
	
	private static final Map<EffortsGrouping, Class<? extends SpreadSheet>> spreadSheetClasses;
	
	static {
		spreadSheetClasses = new HashMap<EffortsGrouping, Class<? extends SpreadSheet>>();
		spreadSheetClasses.put(EffortsGrouping.BY_EMPLOYEES, EmployeeEffortsSpreadSheet.class);
		spreadSheetClasses.put(EffortsGrouping.BY_PROJECTS, ProjectEffortsSpreadSheet.class);
	}
	
	public SpreadSheet buildSpreadSheet(EffortsGrouping grouping, Filter filter) throws ReflectiveOperationException {
		Class<? extends SpreadSheet> spreadSheetClass = spreadSheetClasses.get(grouping);
		return spreadSheetClass
				.getConstructor(HoursRepository.class, Filter.class)				
				.newInstance(hoursRepository, filter);
	}
}
