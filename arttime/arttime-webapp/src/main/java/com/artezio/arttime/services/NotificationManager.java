package com.artezio.arttime.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.inject.Named;

import com.artezio.arttime.config.ApplicationSettings;
import com.artezio.arttime.config.Settings;
import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.HourType;
import com.artezio.arttime.datamodel.Period;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.services.mailing.Mail;
import com.artezio.arttime.services.mailing.MailTemplate;
import com.artezio.arttime.services.mailing.MailTemplateManager;
import com.artezio.arttime.services.mailing.MailingEngine;
import com.artezio.arttime.services.repositories.HourTypeRepository;
import com.artezio.arttime.services.repositories.ProjectRepository;
import com.artezio.arttime.web.interceptors.FacesMessage;

@Named
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class NotificationManager implements NotificationManagerLocal {

    @Inject
    private MailingEngine mailingEngine;
    @Inject
    private MailTemplateManager mailTemplateManager;
    @Inject
    private WorkTimeService workTimeService;
    @Inject
    private HourTypeRepository hourTypeRepository;
    @Inject
    private ProjectRepository projectRepository;
    @Inject
    @ApplicationSettings
    private Settings settings;

    @Override
    @FacesMessage(onCompleteMessageKey = "message.notificationsAreSent")
    public void notificateAboutWorkTimeProblem(List<Employee> employees, Period period, String notificationComment) {
        notificateAboutOwnTimeProblems(employees, period, notificationComment);
        notificatePmAboutTimeProblems(employees, period, notificationComment);
    }

    protected void notificateAboutOwnTimeProblems(List<Employee> employees, Period period, String comment) {
        Map<Employee, Map<Date, BigDecimal>> timeProblems = workTimeService.getApprovedWorkTimeProblems(period, employees);
        HourType actualTime = hourTypeRepository.findActual();
        for (Employee employee : employees) {
            Map<Date, BigDecimal> approvedWorkTimeProblems = timeProblems.get(employee);
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("recipient", employee);
            params.put("hourType", actualTime);
            params.put("deviation", sumDeviation(approvedWorkTimeProblems.values()));
            params.put("period", period);
            params.put("comment", comment);
            params.put("deviationDetails", approvedWorkTimeProblems.entrySet().toArray());
            params.put("appHost", settings.getApplicationBaseUrl());

            String body = mailTemplateManager.getTemplateText(MailTemplate.TIME_PROBLEM_BODY.getFileName(), params);
            String subject = mailTemplateManager.getTemplateText(MailTemplate.TIME_PROBLEM_SUBJECT.getFileName());
            mailingEngine.send(employee.getEmail(), new Mail(subject, body));
        }
    }

    protected void notificatePmAboutTimeProblems(List<Employee> employees, Period period, String comment) {
        Map<Project, Set<Employee>> projectTeams = groupProjectTeams(employees, period);
        List<Employee> projectManagers = getProjectManagers(projectTeams.keySet());
        Map<Employee, Map<Date, BigDecimal>> timeProblems = workTimeService.getApprovedWorkTimeProblems(period, employees);
        HourType actualTime = hourTypeRepository.findActual();
        for (Employee projectManager : projectManagers) {
            Map<String, Object> params = new HashMap<>();
            params.put("recipient", projectManager);
            params.put("hourType", actualTime);
            params.put("period", period);
            params.put("comment", comment);
            List<Subordinate> subordinates = getSubordinates(projectManager, projectTeams, timeProblems);
            params.put("subordinates", subordinates);
            params.put("appHost", settings.getApplicationBaseUrl());
            params.put("userNames", getSubordinatesAsString(subordinates));

            String body = mailTemplateManager.getTemplateText(MailTemplate.TIME_PROBLEM_BODY_FOR_PM.getFileName(), params);
            String subject = mailTemplateManager.getTemplateText(MailTemplate.TIME_PROBLEM_SUBJECT.getFileName());
            mailingEngine.send(projectManager.getEmail(), new Mail(subject, body));
        }
    }

    protected Map<Project, Set<Employee>> groupProjectTeams(List<Employee> employees, Period period) {
        Map<Project, Set<Employee>> projectTeams = new HashMap<Project, Set<Employee>>();
        List<Project> projects = projectRepository.getAll();
        for (Project project : projects) {
        	project = projectRepository.fetchComplete(project);
            for (Employee employee : employees) {
                if (project.isTeamMember(employee, period)) {
                    if (!projectTeams.containsKey(project)) {
                        projectTeams.put(project, new TreeSet<Employee>(Employee.NAME_COMPARATOR));
                    }
                    projectTeams.get(project).add(employee);
                }
            }
        }
        return projectTeams;
    }

    protected List<Employee> getProjectManagers(Collection<Project> projects) {
        Set<Employee> managers = new HashSet<Employee>();
        for (Project project : projects) {
            managers.add(project.getManager());
        }
        return new ArrayList<Employee>(managers);
    }

    protected List<Subordinate> getSubordinates(Employee manager, Map<Project, Set<Employee>> projectTeams, Map<Employee, Map<Date, BigDecimal>> timeProblems) {
        Set<Employee> employees = new HashSet<Employee>();
        for (Project project : projectTeams.keySet()) {
            if (manager.equals(project.getManager())) {
                employees.addAll(projectTeams.get(project));
            }
        }
        List<Subordinate> subordinates = new ArrayList<Subordinate>();
        for (Employee employee : employees) {
            subordinates.add(new Subordinate(employee, timeProblems.get(employee)));
        }
        return subordinates;
    }

    protected String getSubordinatesAsString(List<Subordinate> subordinates) {
        StringBuilder result = new StringBuilder();
        for (Subordinate subordinate : subordinates) {
            result.append(subordinate.getUserName()).append(",");
        }
        return result.toString();
    }

    private BigDecimal sumDeviation(Collection<BigDecimal> deviations) {
        BigDecimal result = BigDecimal.ZERO;
        for (BigDecimal deviation : deviations) {
            result = result.add(deviation);
        }
        return result;
    }

    public class Subordinate {
        private Employee employee;
        private Map<Date, BigDecimal> approvedWorkTimeProblems;

        public Subordinate(Employee employee, Map<Date, BigDecimal> approvedWorkTimeProblems) {
            super();
            this.employee = employee;
            this.approvedWorkTimeProblems = approvedWorkTimeProblems;
        }

        public String getLastName() {
            return employee.getLastName();
        }

        public String getFirstName() {
            return employee.getFirstName();
        }

        public BigDecimal getDeviation() {
            return sumDeviation(approvedWorkTimeProblems.values());
        }

        public Object[] getDeviationDetails() {
            return approvedWorkTimeProblems.entrySet().toArray();
        }

        public String getUserName() {
            return employee.getUserName();
        }
    }
}
