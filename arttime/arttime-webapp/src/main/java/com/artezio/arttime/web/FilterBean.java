package com.artezio.arttime.web;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.filter.Filter;
import com.artezio.arttime.filter.FilterRepository;
import com.artezio.arttime.filter.FilterService;
import com.artezio.arttime.qualifiers.LoggedIn;
import com.artezio.arttime.qualifiers.ManagedProjectsCriteria;
import com.artezio.arttime.services.ProjectsSelectionCriteria;
import com.artezio.arttime.services.ProjectsSelectionCriteria.Status;
import com.artezio.arttime.web.criteria.RangePeriodSelector;

@ManagedBean
@SessionScoped
public class FilterBean implements Serializable {

	private static final long serialVersionUID = -5784546097312069056L;

	private Filter currentFilter;
	@Inject
	private FilterService filterService;
	@Inject
	private FilterRepository filterRepository;
	@Inject
	@LoggedIn
	private Employee loggedEmployee;
	@Inject
	@ManagedProjectsCriteria(onlyOwnProjects = false, status = Status.ANY)
	private ProjectsSelectionCriteria projectsSelectionCriteria;
    private Map<String, Boolean> panelsCollapsedState = new HashMap<String, Boolean>();

	public Filter getCurrentFilter() {
		if (currentFilter == null) {
			setDefaultFilter();
		}
		return currentFilter;
	}

	public void setDefaultFilter() {		
		Filter defaultFilter = filterService.getActiveProjectsFilter(loggedEmployee);		
		setCurrentFilter(defaultFilter);
	}

	public void setCurrentFilter(Filter filter) {
		filter = filterRepository.fetchDetails(filter);
		if (currentFilter != null) {
			RangePeriodSelector currentPeriodSelector = currentFilter.getRangePeriodSelector();
			filter.setRangePeriodSelector(currentPeriodSelector);
		}		
		currentFilter = filter;	
	}
	
	public void save() {
		setCurrentFilter(filterService.save(currentFilter));
	}
	
	public void remove(Filter filter) {
		filterRepository.remove(filter);
		if (currentFilter.equals(filter)) currentFilter.setId(null);
	}
	
	public List<Project> getAvailableProjects() {
		List<Project> projects = filterService.getAvailableProjects(projectsSelectionCriteria);
		projects.sort(Project.CODE_COMPARATOR);
		return projects;
	}

	public Map<String, Boolean> getPanelsCollapsedState() {
		return panelsCollapsedState;
	}

	public void setPanelsCollapsedState(Map<String, Boolean> panelsCollapsedState) {
		this.panelsCollapsedState = panelsCollapsedState;
	}
}
