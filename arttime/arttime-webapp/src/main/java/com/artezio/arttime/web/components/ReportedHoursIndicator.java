package com.artezio.arttime.web.components;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.HourType;
import com.artezio.arttime.filter.Filter;
import com.artezio.arttime.qualifiers.LoggedIn;
import com.artezio.arttime.services.WorkTimeService;
import com.artezio.arttime.services.repositories.EmployeeRepository;
import com.artezio.arttime.services.repositories.HourTypeRepository;
import com.artezio.arttime.services.repositories.HoursRepository;
import com.artezio.arttime.web.FilterBean;
import com.artezio.arttime.utils.MessagesUtil;

@Named
@ViewScoped
public class ReportedHoursIndicator implements Serializable {
    private static final long serialVersionUID = -5792607307504434029L;

    @Inject
    private HoursRepository hoursRepository;
    @Inject
    private WorkTimeService workTimeService;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    @LoggedIn
    private Employee employee;
    @Inject
    private HourTypeRepository hourTypeRepository;

    @PostConstruct
    protected void refreshEmployee() {
        Employee refreshedEmployee = employeeRepository.find(employee.getUserName());
        if (refreshedEmployee != null) {
            employee = refreshedEmployee;
        }
    }

    public BigDecimal getRequiredTime() {
	return workTimeService.getRequiredWorkTime(employee, getFilter().getPeriod());
    }

    public BigDecimal getReportedTime() {
	return hoursRepository.getActualHoursSum(employee, getFilter().getPeriod());
    }

    public int getReportedTimePercents() {
        BigDecimal required = getRequiredTime();
        BigDecimal reported = getReportedTime();
        BigDecimal result = percent(reported, required);
        return result.intValue();
    }

    public int getOverTimePercents() {
        BigDecimal required = getRequiredTime();
        BigDecimal reported = getReportedTime();
        if (reported.compareTo(required) > 0) {
            return new BigDecimal("100").subtract(percent(required, reported)).intValue();
        }
        return 0;
    }

    private BigDecimal percent(BigDecimal value1, BigDecimal value2) {
        BigDecimal hundred = new BigDecimal("100");
        BigDecimal result = new BigDecimal("100");
        if (value2.compareTo(BigDecimal.ZERO)!=0){
        	result = value1.multiply(hundred).divide(value2, BigDecimal.ROUND_HALF_DOWN);
        }
        return result.min(hundred);
    }

    public boolean isTimeReportedIncorrectly() {
        return getReportedTime().compareTo(getRequiredTime()) != 0;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @SuppressWarnings("unchecked")
    private <T> T findBean(String beanName) {
	FacesContext context = FacesContext.getCurrentInstance();
	return (T) context.getApplication().evaluateExpressionGet(context, "#{" + beanName + "}", Object.class);
    }

    protected Filter getFilter() {
	FilterBean filterBean = findBean("filterBean");
	return filterBean.getCurrentFilter();
    }
    
    public String getWarningMessage(){
    	String title = "";
    	HourType actualHourType = hourTypeRepository.findActual(); 
    	if (actualHourType!=null){
    		title = MessagesUtil.getLocalizedString("hoursIndicator.tooltip", "'"+actualHourType.getType()+"'"); 
    	}
    	return title;
    }

}
