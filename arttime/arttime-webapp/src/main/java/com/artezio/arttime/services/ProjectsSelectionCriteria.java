package com.artezio.arttime.services;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.ProjectStatus;

public class ProjectsSelectionCriteria implements java.io.Serializable {

	private static final long serialVersionUID = -208897061933515460L;

	public static enum Status {ACTIVE, FROZEN, COMPLETED, ANY};
		
	private Status status = Status.ACTIVE;
	private Employee manager;
	private String searchQuery;
	
	public ProjectsSelectionCriteria() {
	}
	
	public ProjectsSelectionCriteria(Employee manager) {
		this.manager = manager;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Status getStatus() {
		return status;
	}
		
	public ProjectStatus.Status getProjectStatus() {
		switch (status) {
			case ACTIVE:	return ProjectStatus.Status.ACTIVE;
			case FROZEN:	return ProjectStatus.Status.FROZEN;
			case COMPLETED:	return ProjectStatus.Status.COMPLETED;
			default:		return null;
		}
	}

	public void setManager(Employee manager) {
		this.manager = manager;
	}

	public Employee getManager() {
		return manager;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((manager == null) ? 0 : manager.hashCode());
		result = prime * result
				+ ((searchQuery == null) ? 0 : searchQuery.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProjectsSelectionCriteria other = (ProjectsSelectionCriteria) obj;
		if (manager == null) {
			if (other.manager != null)
				return false;
		} else if (!manager.equals(other.manager))
			return false;
		if (searchQuery == null) {
			if (other.searchQuery != null)
				return false;
		} else if (!searchQuery.equals(other.searchQuery))
			return false;
		if (status != other.status)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + ": ProjectsSelectionCriteria [manager=" + manager + ", status=" + getStatus() + ", search query =" + getSearchQuery() + "]";
	}

	public String getSearchQuery() {
		return searchQuery;
	}

	public void setSearchQuery(String searchQuery) {
		this.searchQuery = searchQuery;
	}
}
