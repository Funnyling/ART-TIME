package com.artezio.arttime.web.criteria;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.artezio.arttime.qualifiers.ManagedProjectsCriteria;
import com.artezio.arttime.services.ProjectsSelectionCriteria;
import com.artezio.arttime.services.ProjectsSelectionCriteria.Status;


@Named
@SessionScoped
public class ProjectsSelectionCriteriaBean implements Serializable {
	private static final long serialVersionUID = -7880170256928796663L;
	
	@Inject
	@ManagedProjectsCriteria(status = Status.ACTIVE)
	private ProjectsSelectionCriteria criteria;
	
	public ProjectsSelectionCriteria getCriteria() {
		return criteria;
	}
}
