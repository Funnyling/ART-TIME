package com.artezio.arttime.filter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import com.artezio.arttime.admin_tool.cache.WebCached;
import com.artezio.arttime.admin_tool.cache.WebCached.Scope;
import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.exceptions.NotAllowedNameException;
import com.artezio.arttime.qualifiers.ManagedProjectsCriteria;
import com.artezio.arttime.services.ProjectsSelectionCriteria;
import com.artezio.arttime.services.repositories.EmployeeRepository;
import com.artezio.arttime.services.repositories.HourTypeRepository;
import com.artezio.arttime.services.repositories.ProjectRepository;
import com.artezio.arttime.web.interceptors.FacesMessage;

@Named
@Stateless
public class FilterService {
	
	public static final String DEFAULT_FILTER_NAME = "Default";
	public static final String MY_ACTIVE_PROJECTS_FILTER_NAME = "My active projects";

	@Inject
	private FilterRepository filterRepository;
	@Inject
	private EmployeeRepository employeeRepository;
	@Inject
	private HourTypeRepository hourTypeRepository;
	@Inject
	private ProjectRepository projectRepository;
    @Inject
    @ManagedProjectsCriteria(onlyOwnProjects = true, status = ProjectsSelectionCriteria.Status.ACTIVE)
    private ProjectsSelectionCriteria ownProjectsCriteria;

    @WebCached(scope = Scope.REQUEST_SCOPED)
	public Filter getDefaultFilter(Employee employee) {
		Filter filter = new Filter(DEFAULT_FILTER_NAME, employee.getUserName(), true);
		filter.setDepartments(employeeRepository.getDepartments());
		filter.setHourTypes(hourTypeRepository.getAll());
        setOnlyOwnActiveProjects(filter);
		filter.setEmployees(employeeRepository.getAll());
		return filter;
	}
    
    private Filter createPredefinedFilter(String name, String owner, List<Project> projects){
    	Filter result = new Filter(name, owner, true);
    	result.setDepartments(employeeRepository.getDepartments());
		result.setHourTypes(hourTypeRepository.getAll());
		result.setEmployees(employeeRepository.getAll());
		result.setProjects(projects);
    	return result;
    }
    
    @WebCached(scope = Scope.REQUEST_SCOPED)
    public Filter getActiveProjectsFilter(Employee owner){
    	return createPredefinedFilter(
				MY_ACTIVE_PROJECTS_FILTER_NAME, owner.getUserName(), getAvailableProjects(ownProjectsCriteria));
    	
    }
    
	public Filter createPersonalTimesheetFilter(Filter currentFilter, Employee employee) {
        Filter result = new Filter();
		result.setRangePeriodSelector(currentFilter.getRangePeriodSelector());
		result.setDepartments(employeeRepository.getDepartments());
		result.setHourTypes(hourTypeRepository.getAll());
        List<Employee> employees = new ArrayList<Employee>();
        employees.add(employee);
		result.setEmployees(employees);
		result.setProjects(projectRepository.getActiveProjects(employee, result.getPeriod()));
		return result;
	}	

	@WebCached(scope = Scope.REQUEST_SCOPED)
	public List<Project> getAvailableProjects(ProjectsSelectionCriteria criteria) {
		return projectRepository.getProjects(criteria);
	}

	@WebCached(resetCache = true)
	@FacesMessage(onCompleteMessageKey = "message.filterIsSaved")
	public Filter save(Filter filter) {
		Filter equivalentFilter = filterRepository.find(filter.getOwner(), filter.getName());
		if (!filter.isPredefined() && equivalentFilter!=null) {
			filter.setId(equivalentFilter.getId());
			return filterRepository.update(filter);
		} else {
			return filterRepository.create(filter);
		}
	}

    public void setOnlyOwnActiveProjects(Filter filter) {
        List<Project> projects = getAvailableProjects(ownProjectsCriteria);
        filter.setProjects(projects);
    }
}
