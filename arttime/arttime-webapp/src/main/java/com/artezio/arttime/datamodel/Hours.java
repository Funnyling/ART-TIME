package com.artezio.arttime.datamodel;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.time.DateUtils;

@Entity
@XmlAccessorType(XmlAccessType.FIELD)
@Table(uniqueConstraints = {
		@UniqueConstraint(name = "constraint_unique_hours", columnNames = {"date", "employee_userName", "project_id", "type_id"})
})
public class Hours implements Serializable {

	private static final long serialVersionUID = -2547281134254029051L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlTransient
	private Long id;
	@NotNull
	@Temporal(TemporalType.DATE)
	@XmlSchemaType(name="date", type=XMLGregorianCalendar.class)
	private Date date;
	@NotNull
	@ManyToOne
	private Employee employee;
	private BigDecimal quantity;
	@NotNull
	@ManyToOne
	private HourType type;
	@NotNull
	@XmlTransient
	@ManyToOne(optional = false)
	private Project project;
	@Column(columnDefinition = "BIT", length = 1)
	private boolean approved;
	@Size(max = 255)
	private String comment;
	
	public Hours() {}

	public Hours(Date date, HourType type) {
		this.date = date;
		this.type = type;
	}

	
	public Hours(Project project, Date date, Employee employee, HourType type) {
		this.date = date;
		this.employee = employee;
		this.type = type;
		this.project = project;		
	}		

	public Long getId() {
		return id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = (BigDecimal.ZERO.equals(quantity))
				? null
				: quantity;
	}

	public HourType getType() {
		return type;
	}

	public void setType(HourType type) {
		this.type = type;
	}
	
	public Project getProject() {
		return project;
	}
	
	public void setProject(Project project) {
		this.project = project;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : DateUtils.truncate(date, Calendar.DAY_OF_MONTH).hashCode());
		result = prime * result
				+ ((employee == null) ? 0 : employee.hashCode());
		result = prime * result + ((project == null) ? 0 : project.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Hours other = (Hours) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!DateUtils.isSameDay(date, other.date))
			return false;
		if (employee == null) {
			if (other.employee != null)
				return false;
		} else if (!employee.equals(other.employee))
			return false;
		if (project == null) {
			if (other.project != null)
				return false;
		} else if (!project.equals(other.project))
			return false;		
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Hours : Project  " + (project == null ? null : project.getCode()) +
				"\n\t\temployee  " + (employee == null ? null : employee.getUserName()) + 
				"\n\t\tdate  " + date +
				"\n\t\ttype" + type +
				"\n\t\tapproved  " + approved +
				"\n\t\tquantity  " + quantity + "\n";
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}		
}
