package com.artezio.arttime.exceptions;

public class NotAllowedNameException extends Exception {
	private static final long serialVersionUID = -8120719402353821075L;

	public NotAllowedNameException(String message) {
		super(message);
	}

	
}
