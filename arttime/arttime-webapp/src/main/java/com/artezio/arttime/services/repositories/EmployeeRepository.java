package com.artezio.arttime.services.repositories;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.artezio.arttime.admin_tool.cache.WebCached;
import com.artezio.arttime.admin_tool.cache.WebCached.Scope;
import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.Participation;
import com.artezio.arttime.datamodel.Period;
import com.artezio.arttime.filter.Filter;

@Named
@Stateless
@WebCached(scope = Scope.REQUEST_SCOPED)
public class EmployeeRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public Employee find(String userName) {
	try {
	    return (Employee) entityManager.createQuery("SELECT e FROM Employee e WHERE e.userName = :userName")
		    .setParameter("userName", userName).getSingleResult();
	} catch (NoResultException e) {
	    return null;
	}
    }

    @SuppressWarnings("unchecked")
    public List<String> getDepartments() {
    	List<String> result = entityManager.createQuery(
    			"SELECT DISTINCT e.department FROM Employee e WHERE e.department IS NOT NULL")
    			.getResultList();
    	result.sort((d1, d2) -> d1.compareToIgnoreCase(d2));
    	return result;
    }

    @SuppressWarnings("unchecked")
    public List<Employee> getAll() {
    	List<Employee> result = entityManager.createQuery(
    			"SELECT e FROM Employee e")
    			.getResultList();
    	result.sort(Employee.NAME_COMPARATOR);
    	return result;
    }

    @SuppressWarnings("unchecked")
    public List<Employee> getEmployees(String searchName) {
	if (searchName != null && !searchName.trim().isEmpty()) {
	    searchName = "%" + searchName.trim() + "%";
	} else {
	    return getAll();
	}

	String queryString = "SELECT e FROM Employee e "
		+ "WHERE e.firstName LIKE :firstName OR e.lastName LIKE :lastName ";

	Query query = entityManager.createQuery(queryString);
	query.setParameter("firstName", searchName);
	query.setParameter("lastName", searchName);
	
	List<Employee> result = query.getResultList();
	result.sort(Employee.NAME_COMPARATOR);

	return result;
    }

    public Employee update(Employee employee) {
	entityManager.merge(employee);
	return employee;
    }

    @SuppressWarnings("unchecked")
    public List<Employee> getEmployees(Filter filter) {
	StringBuilder queryString = new StringBuilder("SELECT DISTINCT e FROM Project p ")
		.append("INNER JOIN p.team t INNER JOIN t.employee e ")
		.append("WHERE ((t.period.finish IS NOT NULL AND t.period.start<=:finish AND t.period.finish>=:start) ")
		.append("OR (t.period.finish IS NULL AND t.period.start<=:finish)) AND ");

	if (filter.isProjectSetted()) {
	    queryString.append((filter.getProjects().isEmpty() ? " FALSE = TRUE " : " p IN (:projects)") + " AND ");
	}
	if (filter.isEmployeesSetted()) {
	    queryString.append((filter.getEmployees().isEmpty() ? " FALSE = TRUE " : " e IN (:employees)") + " AND ");
	}
	if (filter.isDepartmentsSetted()) {
	    queryString.append((filter.getDepartments().isEmpty() ? " FALSE = TRUE "
		    : " e.department IN (:departments)"));
	}

	Query query = entityManager.createQuery(queryString.toString());
	query.setParameter("start", filter.getPeriod().getStart());
	query.setParameter("finish", filter.getPeriod().getFinish());
	if (filter.isProjectSetted() && !filter.getProjects().isEmpty()) {
	    query.setParameter("projects", filter.getProjects());
	}
	if (filter.isEmployeesSetted() && !filter.getEmployees().isEmpty()) {
	    query.setParameter("employees", filter.getEmployees());
	}
	if (filter.isDepartmentsSetted() && !filter.getDepartments().isEmpty()) {
	    query.setParameter("departments", filter.getDepartments());
	}

	return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Participation> getParticipations(Employee employee, Period period) {
	return entityManager
		.createQuery(
			"SELECT t FROM Project p " + "INNER JOIN p.team t " + "WHERE t.employee = :employee "
				+ "AND ((t.period.finish IS NOT NULL AND t.period.finish>=:start AND t.period.start<=:finish) "
				+ "OR (t.period.finish IS NULL AND t.period.start<=:finish))").setParameter("start", period.getStart())
		.setParameter("finish", period.getFinish()).setParameter("employee", employee).getResultList();
    }

    public List<Employee> getEmployees(List<String> userNames) {
	if (userNames != null && !userNames.isEmpty()) {
	    Query query = entityManager
		    .createQuery("SELECT DISTINCT e FROM Employee e WHERE e.userName IN (:userNames)");
	    query.setParameter("userNames", userNames);
	    return query.getResultList();
	} else {
	    return new ArrayList<Employee>();
	}

    }

}
