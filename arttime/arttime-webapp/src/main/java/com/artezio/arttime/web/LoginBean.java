package com.artezio.arttime.web;

import java.io.Serializable;
import java.security.Principal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.faces.context.ExternalContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.qualifiers.LoggedIn;
import com.artezio.arttime.security.auth.UserRoles;
import com.artezio.arttime.services.repositories.EmployeeRepository;

@Named
@SessionScoped
public class LoginBean implements Serializable {
	private static final long serialVersionUID = -7055525369625427016L;

	private String username;
	private String password;
	private boolean rememberMe;
	private Employee loggedEmployee;
	@Inject
	private EmployeeRepository employeeRepository;
	@Inject
	private ExternalContext externalContext;
	private static Map<String, String> outcomes;

	static {
		outcomes = new LinkedHashMap<String, String>();
		outcomes.put(UserRoles.EXEC_ROLE, "manageEfforts");
		outcomes.put(UserRoles.PM_ROLE, "manageEfforts");
		outcomes.put(UserRoles.EMPLOYEE_ROLE, "timesheet");
		outcomes.put(UserRoles.ADMIN_ROLE, "settings");
	}

	public String logout() {
		SecurityUtils.getSubject().logout();
		externalContext.invalidateSession();
		return "logout";
	}

	public String login() {
		if (loggedEmployee == null) {
			UsernamePasswordToken token = new UsernamePasswordToken(username,
					password, rememberMe);
			SecurityUtils.getSubject().login(token);
		}
		return calculateOutcom();
	}

	private String calculateOutcom() {
		for (String role : outcomes.keySet()) {
			if (externalContext.isUserInRole(role)) {
				return outcomes.get(role);
			}
		}
		return null;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Produces
	@LoggedIn
	public Employee getLoggedEmployee() {
		if (loggedEmployee == null) {
			initLoggedEmployee();
		}
		return loggedEmployee;
	}

	private void initLoggedEmployee() {
		Principal principal = externalContext.getUserPrincipal();
		String username = principal.getName();
		loggedEmployee = employeeRepository.find(username);
		if (loggedEmployee == null) {
			loggedEmployee = new Employee(username);
		}
	}

	public boolean isRememberMe() {
		return rememberMe;
	}

	public void setRememberMe(boolean rememberMe) {
		this.rememberMe = rememberMe;
	}
}
