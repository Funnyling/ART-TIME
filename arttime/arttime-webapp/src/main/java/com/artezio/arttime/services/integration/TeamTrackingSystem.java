package com.artezio.arttime.services.integration;

import java.util.List;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.Project;

public interface TeamTrackingSystem {
	String getName();
	List<Employee> getTeam(Project project);
}
