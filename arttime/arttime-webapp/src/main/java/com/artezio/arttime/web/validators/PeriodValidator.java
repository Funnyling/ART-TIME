package com.artezio.arttime.web.validators;

import com.artezio.arttime.web.criteria.RangePeriodSelector;
import org.omnifaces.util.Components;
import org.omnifaces.util.Messages;

import com.artezio.arttime.datamodel.Period;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.*;
import javax.faces.validator.Validator;
import javax.inject.Inject;
import javax.validation.*;

import java.util.*;
import java.util.stream.Collectors;

@FacesValidator
public class PeriodValidator implements Validator {

    @Inject
    private ValidatorFactory validatorFactory;

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        UIInput startComponent = getStartComponent(component);
        Period period = new Period((Date) startComponent.getValue(), (Date) value);

        Set<ConstraintViolation<Object>> violations = createBeanValidator(context).validate(period);
        violations.addAll(validatePeriodSelector(period, component, context));

        if (!violations.isEmpty()) {
            List<FacesMessage> messages = violations.stream().map(ConstraintViolation::getMessage)
                    .map(Messages::createError).collect(Collectors.toList());
            startComponent.setValid(false);
            throw new ValidatorException(messages);
        }
    }

    private Set<ConstraintViolation<Object>> validatePeriodSelector(Period period, UIComponent component, FacesContext context) {
        Object validate = component.getAttributes().get("validatePeriodSelector");
        if (validate != null && Boolean.parseBoolean(validate.toString())) {
            RangePeriodSelector selector = new RangePeriodSelector(period);
            return createBeanValidator(context).validate(selector);
        }
        return Collections.emptySet();
    }

    private javax.validation.Validator createBeanValidator(FacesContext context) {
        ValidatorContext validatorContext = validatorFactory.usingContext();
        MessageInterpolator messageInterpolator = validatorFactory.getMessageInterpolator();
        validatorContext.messageInterpolator(new JsfAwareMessageInterpolator(context, messageInterpolator));
        return validatorContext.getValidator();
    }

    private UIInput getStartComponent(UIComponent finishComponent) {
        String startId = (String) finishComponent.getAttributes().get("start");
        return Components.findComponentRelatively(finishComponent, startId);
    }

    private static class JsfAwareMessageInterpolator implements MessageInterpolator {
        private FacesContext context;
        private MessageInterpolator delegate;

        public JsfAwareMessageInterpolator(FacesContext context, MessageInterpolator delegate) {
            this.context = context;
            this.delegate = delegate;
        }

        public String interpolate(String message, MessageInterpolator.Context context) {
            Locale locale = this.context.getViewRoot().getLocale();
            if (locale == null) {
                locale = Locale.getDefault();
            }
            return delegate.interpolate(message, context, locale);
        }

        public String interpolate(String message, MessageInterpolator.Context context, Locale locale) {
            return delegate.interpolate(message, context, locale);
        }
    }
}
