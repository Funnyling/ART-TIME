package com.artezio.arttime.config;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;


@Entity
public class Settings {		
	public enum Locale {
		ENGLISH("en"), 
		RUSSIAN("ru");
		
		private String language;
		private Locale(String language) {
			this.language = language;
		}
		public String getLanguage() {
			return language;
		}
		public void setLanguage(String language) {
			this.language = language;
		}
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Size(max=255)
	private String ldapServerAddress;
	private String ldapServerPort;
	@Size(max=255)
	private String ldapBindDN;
	@Size(max=255)
	private String ldapBindCredentials;
	@Size(max=255)
	private String ldapPrincipalSuffix;
	@Size(max=255)
	private String ldapUserContextDN;
	@Size(max=255)
	private String ldapUserNameAttribute;
	@Size(max=255)
	private String ldapCommonNameAttribute;
	@Size(max=255)
	private String ldapMailAttribute;
	@Size(max=255)
	private String ldapFirstNameAttribute;
	@Size(max=255)
	private String ldapLastNameAttribute;
	@Size(max=255)
	private String ldapDepartmentAttribute;
	private String ldapEmployeeFilter;
	private String ldapGroupMemberFilter;
	private String ldapUserFilter;

	private String smptHostName;
	private String smtpPortNumber;
	@Size(max=255)
	private String smtpUsername;
	private String smtpPassword;

	private int timerHoursInterval;
	private int timerMinutesInterval;
	
	@Size(max=255)
	private String execRoleMemberOf;
	@Size(max=255)
	private String integrationClientGroups;
	
	@Column(columnDefinition = "BIT", length = 1)
	private boolean employeesSynchronizationEnabled;
	@Column(columnDefinition = "BIT", length = 1)
    private boolean teamSynchronizationEnabled;
    private String applicationBaseUrl;

	public String getLdapServerAddress() {
		return ldapServerAddress;
	}

	public void setLdapServerAddress(String ldapServerAddress) {
		this.ldapServerAddress = ldapServerAddress;
	}

	public String getLdapServerPort() {
		return ldapServerPort;
	}

	public void setLdapServerPort(String ldapServerPort) {
		this.ldapServerPort = ldapServerPort;
	}

	public String getLdapBindDN() {
		return ldapBindDN;
	}

	public void setLdapBindDN(String ldapBindDN) {
		this.ldapBindDN = ldapBindDN;
	}

	public String getLdapBindCredentials() {
		return ldapBindCredentials;
	}

	public void setLdapBindCredentials(String ldapBindCredentials) {
		this.ldapBindCredentials = ldapBindCredentials;
	}

	public String getLdapUserContextDN() {
		return ldapUserContextDN;
	}

	public void setLdapUserContextDN(String ldapUserContextDN) {
		this.ldapUserContextDN = ldapUserContextDN;
	}

	public String getLdapUserNameAttribute() {
		return ldapUserNameAttribute;
	}

	public void setLdapUserNameAttribute(String ldapUserNameAttribute) {
		this.ldapUserNameAttribute = ldapUserNameAttribute;
	}

	public String getLdapMailAttribute() {
		return ldapMailAttribute;
	}

	public void setLdapMailAttribute(String ldapMailAttribute) {
		this.ldapMailAttribute = ldapMailAttribute;
	}

    public Long getId() {
		return id;
	}

	public void setLdapLastNameAttribute(String ldapLastNameAttribute) {
		this.ldapLastNameAttribute = ldapLastNameAttribute;
	}

	public String getLdapLastNameAttribute() {
		return ldapLastNameAttribute;
	}

	public void setLdapFirstNameAttribute(String ldapFirstNameAttribute) {
		this.ldapFirstNameAttribute = ldapFirstNameAttribute;
	}

	public void setLdapDepartmentAttribute(String ldapDepartmentAttribute) {
		this.ldapDepartmentAttribute = ldapDepartmentAttribute;
	}

	public String getLdapDepartmentAttribute() {
		return ldapDepartmentAttribute;
	}

	public String getLdapFirstNameAttribute() {
		return ldapFirstNameAttribute;
	}

	public void setTimerHoursInterval(int timerHoursInterval) {
		this.timerHoursInterval = timerHoursInterval;
	}

	public int getTimerHoursInterval() {
		return timerHoursInterval;
	}

	public void setTimerMinutesInterval(int timerMinutesInterval) {
		this.timerMinutesInterval = timerMinutesInterval;
	}

	public int getTimerMinutesInterval() {
		return timerMinutesInterval;
	}

	public String getExecRoleMemberOf() {
		return execRoleMemberOf;
	}

	public void setExecRoleMemberOf(String execRoleMemberOf) {
		this.execRoleMemberOf = execRoleMemberOf;
	}

	public String getSmptHostName() {
		return smptHostName;
	}

	public void setSmptHostName(String smptHostName) {
		this.smptHostName = smptHostName;
	}

	public String getSmtpPortNumber() {
		return smtpPortNumber;
	}

	public void setSmtpPortNumber(String smtpPortNumber) {
		this.smtpPortNumber = smtpPortNumber;
	}

	public String getSmtpUsername() {
		return smtpUsername;
	}

	public void setSmtpUsername(String smtpUsername) {
		this.smtpUsername = smtpUsername;
	}

	public String getSmtpPassword() {
		return smtpPassword;
	}

	public void setSmtpPassword(String smtpPassword) {
		this.smtpPassword = smtpPassword;
	}

	public boolean isEmployeesSynchronizationEnabled() {
		return employeesSynchronizationEnabled;
	}

	public void setEmployeesSynchronizationEnabled(boolean employeesSynchronizationEnabled) {
		this.employeesSynchronizationEnabled = employeesSynchronizationEnabled;
	}

	public boolean isTeamSynchronizationEnabled() {
		return teamSynchronizationEnabled;
	}

	public void setTeamSynchronizationEnabled(boolean teamSynchronizationEnabled) {
		this.teamSynchronizationEnabled = teamSynchronizationEnabled;
	}

	public String getLdapEmployeeFilter() {
		return ldapEmployeeFilter;
	}

	public void setLdapEmployeeFilter(String ldapEmployeeFilter) {
		this.ldapEmployeeFilter = ldapEmployeeFilter;
	}	

	public String getLdapGroupMemberFilter() {
		return ldapGroupMemberFilter;
	}

	public void setLdapGroupMemberFilter(String ldapGroupMemberFilter) {
		this.ldapGroupMemberFilter = ldapGroupMemberFilter;
	}

	public String getLdapUserFilter() {
		return ldapUserFilter;
	}

	public void setLdapUserFilter(String ldapUserFilter) {
		this.ldapUserFilter = ldapUserFilter;
	}
	
	public String getLdapPrincipalSuffix() {
		return ldapPrincipalSuffix;
	}

	public void setLdapPrincipalSuffix(String ldapPrincipalSuffix) {
		this.ldapPrincipalSuffix = ldapPrincipalSuffix;
	}

	public String getIntegrationClientGroups() {
		return integrationClientGroups;
	}

	public void setIntegrationClientGroups(String integrationClientGroups) {
		this.integrationClientGroups = integrationClientGroups;
	}

	public String getLdapCommonNameAttribute() {
		return ldapCommonNameAttribute;
	}

	public void setLdapCommonNameAttribute(String ldapCommonNameAttribute) {
		this.ldapCommonNameAttribute = ldapCommonNameAttribute;
	}

    public String getApplicationBaseUrl() {
	return applicationBaseUrl;
    }

    public void setApplicationBaseUrl(String applicationBaseUrl) {
	this.applicationBaseUrl = applicationBaseUrl;
    }

	public Map<String, String> getEmployeeMappingParams() {
		Map<String, String> result = new HashMap<String, String>();
		if (ldapUserNameAttribute != null) {
			result.put(ldapUserNameAttribute, "userName");
		}
		if (ldapFirstNameAttribute != null) {
			result.put(ldapFirstNameAttribute, "firstName");
		}
		if (ldapLastNameAttribute != null) {
			result.put(ldapLastNameAttribute, "lastName");
		}
		if (ldapMailAttribute != null) {
			result.put(ldapMailAttribute, "email");
		}
		if (ldapDepartmentAttribute != null) {
			result.put(ldapDepartmentAttribute, "department");
		}
		return result;
	}
	
	public boolean hasSameTimerSettings(Settings another) {
		return this.timerHoursInterval == another.timerHoursInterval
				&& this.timerMinutesInterval == another.timerMinutesInterval;
	}
}
