package com.artezio.arttime.web.spread_sheet;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.HourType;
import com.artezio.arttime.datamodel.Hours;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.web.EffortsGrouping;

public class TotalsSpreadSheetRow extends SpreadSheetRow<BigDecimal> {
	private static final long serialVersionUID = 639156120656864989L;
	
	private List<HoursSpreadSheetRow> hoursRows;
	
	public TotalsSpreadSheetRow(Project project, HourType hourType, List<HoursSpreadSheetRow> hoursRows) {
		super(project, hourType);
		this.hoursRows = hoursRows;
	}
	
	public TotalsSpreadSheetRow(Employee employee, HourType hourType, List<HoursSpreadSheetRow> hoursRows) {
		super(employee, hourType);
		this.hoursRows = hoursRows;
	}
	
	@Override
    public BigDecimal get(Date date) {
		BigDecimal result = BigDecimal.ZERO;
		for (HoursSpreadSheetRow row : hoursRows) {
			Hours hour = row.get(date);
			if (hour != null && hour.getQuantity() != null) {
				result = result.add(hour.getQuantity());
			}
		}
		return result;			
	}

	public List<HoursSpreadSheetRow> getHoursRows() {
		return hoursRows;
	}
	
	public void setHoursRows(List<HoursSpreadSheetRow> hoursRows) {
		this.hoursRows = hoursRows;
	}

	@Override
	public BigDecimal getRowTotal() {
		BigDecimal result = BigDecimal.ZERO;
		for (HoursSpreadSheetRow row : hoursRows) {
			for (Hours hour : row.getHours()) {
				if (hour != null && hour.getQuantity() != null) {
					result = result.add(hour.getQuantity());
				}
			}
		}
		return result;
	}

	@Override
	public String getFirstColValue(EffortsGrouping grouping) {
		return null;
	}
}
