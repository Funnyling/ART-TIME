package com.artezio.arttime.web.components;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Iterator;

import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.el.ValueReference;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.WordUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.primefaces.component.outputlabel.OutputLabelRenderer;
import org.primefaces.el.ValueExpressionAnalyzer;

public class CustomOutputLabelRenderer extends OutputLabelRenderer {
	@Inject
	private ELContext elContext;
	@Inject
	private FacesContext facesContext;	
	
	@Override
    public void encodeEnd(FacesContext context, UIComponent component) throws IOException {
		super.encodeEnd(context, component);
		if (markAsRequired(component)) {
			ResponseWriter writer = context.getResponseWriter();
			writer.writeText(" *", "value");
		}		
	}
	
	private boolean markAsRequired(UIComponent outputLabel) {		
		UIComponent targetComponent = findTargetComponent(outputLabel);
		if (targetComponent != null) {			
			return (boolean) (targetComponent.getAttributes().get("required"))
					? false
					: determineRequireness(targetComponent);			
		}
		return false;						
	}
	
	private boolean determineRequireness(UIComponent input) {
		ValueExpression valueExpression = input.getValueExpression("value");
		if (valueExpression != null) {			
			ValueReference valueReference = ValueExpressionAnalyzer.getReference(elContext, valueExpression);
			if (valueReference != null) {
				Class<? extends Object> baseClass = valueReference.getBase().getClass();				
		        Object property = valueReference.getProperty();
		        Field field = findField(baseClass, property); 
		        Method getter = findGetter(baseClass, property);		
		        return (field != null && isRequired(field))
		        		|| (getter != null && isRequired(getter));
			}
		}
		return false;
	}

	private boolean isRequired(AnnotatedElement annotatedElement) {
		return hasNotNullRestriction(annotatedElement) || hasNotEmptyRestriction(annotatedElement);
	}
	
	private boolean hasNotEmptyRestriction(AnnotatedElement annotatedElement) {
		for (Annotation annotation : annotatedElement.getAnnotations()) {
        	if ((annotation instanceof NotEmpty)
        	 || (annotation instanceof NotBlank)) {
        		return true;
        	}                	
        }
		return false;
	}

	private boolean hasNotNullRestriction(AnnotatedElement annotatedElement) {
		for (Annotation annotation : annotatedElement.getAnnotations()) {
        	if (annotation instanceof NotNull) {
        		return true;
        	}                	
        }
		return false;
	}

	private Method findGetter(Class<? extends Object> baseClass, Object property) {
		String capitalizeProperty = WordUtils.capitalize((String)property);
		Method[] methods = baseClass.getDeclaredMethods();
		for (Method method : methods) {
			if (isGetter(method)) {
				String methodName = method.getName();			   
				if (methodName.matches("^get" + capitalizeProperty + "$")
						|| methodName.matches("^is" + capitalizeProperty + "$")) {
					return method;
				}
			}
		}
		return null;
	}
	
	private boolean isGetter(Method method) {
		if (Modifier.isPublic(method.getModifiers()) && method.getParameterTypes().length == 0) {
			if (method.getName().matches("^get[A-Z].*") && !method.getReturnType().equals(void.class))
				return true;
			if (method.getName().matches("^is[A-Z].*") && method.getReturnType().equals(boolean.class))
				return true;
		}
		return false;
	}

	private Field findField(Class<? extends Object> baseClass, Object property) {
		try {
			return baseClass.getDeclaredField((String) property);
		} catch (NoSuchFieldException | SecurityException e) {
			return null;
		}
	}

	private UIInput findTargetComponent(UIComponent outputLabel) {		
		String inputId = (String) outputLabel.getAttributes().get("for");
		return (inputId == null)
				? null
				: (UIInput)findComponentInRoot(inputId);
	}	
	
	private UIComponent findComponentInRoot(String id) {
		UIComponent component = null;	    
		UIComponent root = facesContext.getViewRoot();
		component = findComponent(root, id);	    
	    return component;
	}
	
	private UIComponent findComponent(UIComponent base, String id) {
		if (id.equals(base.getId())) return base;	  
	    UIComponent child = null;
	    UIComponent result = null;
	    Iterator<UIComponent> childs = base.getFacetsAndChildren();
	    while (childs.hasNext() && result == null) {
	    	child = (UIComponent) childs.next();
	    	result = (id.equals(child.getId())) 
	    		? child
	    		: findComponent(child, id);	    	
	    }
	    return result;
	}			
}
