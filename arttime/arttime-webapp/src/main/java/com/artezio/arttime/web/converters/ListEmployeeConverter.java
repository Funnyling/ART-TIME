package com.artezio.arttime.web.converters;

import java.util.Arrays;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.services.repositories.EmployeeRepository;

@FacesConverter(value = "listEmployeeConverter")
public class ListEmployeeConverter implements Converter {

    @Inject
    private EmployeeRepository employeeRepository;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
	List<String> userNames = Arrays.asList(value.split(","));
	return employeeRepository.getEmployees(userNames);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
	if (value == null)
	    return null;
	return getAsString(value);
    }

    static public String getAsString(Object value) {
	StringBuilder result = new StringBuilder();
	for (Employee employee : (List<Employee>) value) {
	    result.append(employee.getUserName() + ",");
	}
	return result.toString();
    }

}
