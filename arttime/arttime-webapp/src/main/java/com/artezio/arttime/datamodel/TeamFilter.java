package com.artezio.arttime.datamodel;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.AssertTrue;

import org.apache.commons.lang.StringUtils;


@Embeddable
public class TeamFilter implements java.io.Serializable {
	private static final long serialVersionUID = 3571041891195400830L;

	public enum FilterType {
		PROJECT_CODES,
		NATIVE,
		DISABLED		
	}	
	private String value;
	@Enumerated(EnumType.STRING)
	private FilterType filterType = FilterType.PROJECT_CODES;
	
	public TeamFilter() {}
	
	public TeamFilter(FilterType filterType, String value) {
		this.filterType = filterType;
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public FilterType getFilterType() {
		return filterType;
	}
	
	public void setFilterType(FilterType filterType) {
		this.filterType = filterType;
	}

	@Override
	public String toString() {
		return "TeamFilter [filterType=" + filterType + ", value=" + value + "]";
	}
		
	@AssertTrue(message = "Not valid team filter")
	public boolean isValid() {
		return filterType == FilterType.DISABLED || (!StringUtils.isBlank(value));
	}
}
