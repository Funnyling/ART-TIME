package com.artezio.arttime.datamodel;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;


@Entity
public class Participation implements Serializable {

	private static final long serialVersionUID = -8574424915040355272L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotNull(message = "{valueIsRequired}")
	@ManyToOne
	private Employee employee;
	@Valid
	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name="start", column=@Column(name="_from")),
		@AttributeOverride(name="finish", column=@Column(name="till"))})
	private Period period = new Period();
    @NotNull(message = "{valueIsRequired}")
    @ManyToOne(optional = false)
    private WorkdaysCalendar workdaysCalendar;

	public Participation() {
		super();
	}
	
	public Participation(Employee employee, Date from) {
		super();
		this.employee = employee;
		period.setStart(from);
	}

    public Participation(WorkdaysCalendar workdaysCalendar) {
	super();
	this.workdaysCalendar = workdaysCalendar;
    }

	public Long getId() {
		return id;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

    public WorkdaysCalendar getWorkdaysCalendar() {
        return workdaysCalendar;
    }

    public void setWorkdaysCalendar(WorkdaysCalendar workdaysCalendar) {
        this.workdaysCalendar = workdaysCalendar;
    }
    
    public Period getPeriod() {
    	return period;
    }
    
	public void setPeriod(Period period) {
		this.period = period;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((employee == null) ? 0 : employee.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((period == null) ? 0 : period.hashCode());
		result = prime
				* result
				+ ((workdaysCalendar == null) ? 0 : workdaysCalendar.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Participation other = (Participation) obj;
		if (employee == null) {
			if (other.employee != null)
				return false;
		} else if (!employee.equals(other.employee))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (period == null) {
			if (other.period != null)
				return false;
		} else if (!period.equals(other.period))
			return false;
		if (workdaysCalendar == null) {
			if (other.workdaysCalendar != null)
				return false;
		} else if (!workdaysCalendar.equals(other.workdaysCalendar))
			return false;
		return true;
	}

}
