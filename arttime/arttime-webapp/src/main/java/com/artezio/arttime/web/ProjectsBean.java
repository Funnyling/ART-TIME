package com.artezio.arttime.web;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.richfaces.component.SortOrder;

@ManagedBean
@ViewScoped
public class ProjectsBean implements Serializable {
	private static final long serialVersionUID = 6385561178974142829L;
			
	private SortOrder managerOrder = SortOrder.unsorted;
	private SortOrder projectCodeOrder = SortOrder.ascending;		
	
	public SortOrder getProjectCodeOrder() {
		return projectCodeOrder;
	}
	
	public void setProjectCodeOrder(SortOrder projectCodeOrder) {
		this.projectCodeOrder = projectCodeOrder;
	}
	
	public SortOrder getManagerOrder() {
		return managerOrder;
	}
	
	public void setManagerOrder(SortOrder managerOrder) {
		this.managerOrder = managerOrder;
	}
	
	public void sortByCode() {
        managerOrder = SortOrder.unsorted;        
        if (projectCodeOrder.equals(SortOrder.ascending)) {
        	setProjectCodeOrder(SortOrder.descending);
        } else {
        	setProjectCodeOrder(SortOrder.ascending);
        }
    }
	
	public void sortByManager() {
		projectCodeOrder = SortOrder.unsorted;        
        if (managerOrder.equals(SortOrder.ascending)) {
        	setManagerOrder(SortOrder.descending);
        } else {
        	setManagerOrder(SortOrder.ascending);
        }
    }					
}
