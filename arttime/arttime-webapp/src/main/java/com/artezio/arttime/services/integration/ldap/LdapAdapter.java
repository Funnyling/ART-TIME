package com.artezio.arttime.services.integration.ldap;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;

import com.artezio.arttime.admin_tool.cache.WebCached;
import com.artezio.arttime.admin_tool.cache.WebCached.Scope;
import com.artezio.arttime.config.ApplicationSettings;
import com.artezio.arttime.config.Settings;
import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.datamodel.TeamFilter;
import com.artezio.arttime.services.integration.EmployeeService;
import com.artezio.arttime.services.integration.TeamTrackingSystem;

@RequestScoped
@Named("employeeService")
@WebCached(scope = Scope.REQUEST_SCOPED)
public class LdapAdapter implements EmployeeService, TeamTrackingSystem {

	private String serverAddress;
	private int serverPort = 389;
	private String bindDN;
	private String bindCredentials = "secret";
	private String principalSuffix;
	private int searchTimeLimit = 10000;
	private int searchScope = SearchControls.SUBTREE_SCOPE;
	private String userContextDN = "ou=Person,dc=acme,dc=com";
	
	private String employeeFilter;
	private String groupMemberFilter;
	private String userFilter;
	private Map<String, String> attributeMapping = new TreeMap<String, String>();
	private Logger log = Logger.getLogger(this.getClass().getName());
	
	public LdapAdapter() {		
	}
	
	@Inject 
	public LdapAdapter(@ApplicationSettings Settings settings) {
		if (settings != null) {
			this.serverAddress = settings.getLdapServerAddress();
			if (settings.getLdapServerPort() != null) {
				this.serverPort = Integer.parseInt(settings.getLdapServerPort());
			}
			this.bindDN = settings.getLdapBindDN();
			this.bindCredentials = settings.getLdapBindCredentials();
			this.principalSuffix = settings.getLdapPrincipalSuffix();
			this.userContextDN = settings.getLdapUserContextDN();
			this.employeeFilter = settings.getLdapEmployeeFilter();
			this.groupMemberFilter = settings.getLdapGroupMemberFilter();
			this.userFilter = settings.getLdapUserFilter();
			this.attributeMapping = settings.getEmployeeMappingParams();
		}
	}
		
	
	private List<Employee> listEmployees(Filter filter){
		List<Employee> employees = new ArrayList<Employee>();
		InitialLdapContext ctx = null;
		
		try {
			ctx = initializeContext();
			SearchControls controls = makeSearchControls();
			NamingEnumeration<SearchResult> answer = ctx.search(
					getUserContextDN(), filter.getExpression(), filter.getArgs(),
					controls);
			
			while (answer.hasMore()) {
				SearchResult sr = answer.next();
				Attributes attrs = sr.getAttributes();
				Employee employee;
				employee = createEmployee(attrs);
				employees.add(employee);
			}
			answer.close();
		} catch (NamingException ex) {
			throw new RuntimeException("Error getting Employees ", ex);
		} catch (IllegalAccessException ex) {
			throw new RuntimeException("Cannot create Employee object ", ex);
		} catch (InvocationTargetException ex) {
			throw new RuntimeException("Cannot create Employee object ", ex);
		} finally {
			closeContext(ctx);
		}
		return employees;
	}
	
	public List<Employee> getEmployees() {
		Filter filter = new Filter(employeeFilter, new Object[]{});
		List<Employee> employees = listEmployees(filter);
		employees.sort(Employee.NAME_COMPARATOR);
		return employees;
	}

	public Employee findEmployee(String userName) {
		if (StringUtils.isNotBlank(userFilter)) {			
			Filter filter = new Filter(userFilter, new Object[]{userName});
			List<Employee> employees = listEmployees(filter);			
			return (employees.isEmpty())
					? null
					: employees.get(0);
		}
		return null;
	}
	
	public List<Employee> findEmployeesByFullName(String fullName) {
		List<Employee> employees = getEmployees();
		Iterator<Employee> iterator = employees.iterator();
		fullName = fullName.trim().toLowerCase();
		
		if (!fullName.isEmpty()) {
			while (iterator.hasNext()) {
				Employee employee = iterator.next();
				if (!employee.getFullName().toLowerCase().startsWith(fullName)) {
					iterator.remove();
				}
			}
		}
		
		return employees;
	}

	private void closeContext(InitialLdapContext ctx) {
		if (ctx != null) {
			try {
				ctx.close();
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}
	}		
	
	private SearchControls makeSearchControls() {
		SearchControls controls = new SearchControls();
		controls.setSearchScope(searchScope);
		controls.setReturningAttributes(makeReturningAttr());
		controls.setTimeLimit(getSearchTimeLimit());
		return controls;
	}

	private String[] makeReturningAttr() {
        Set<String> keySet = attributeMapping.keySet();
        return keySet.toArray(new String[keySet.size()]);
	}
	
	protected Employee createEmployee(Attributes attrs) throws NamingException,
			IllegalAccessException, InvocationTargetException {
		Employee employee = new Employee();
		for (Entry<String, String> item : attributeMapping.entrySet()) {
			Attribute attr = attrs.get(item.getKey());
			String value = parseAttribute(attr);
			BeanUtils.setProperty(employee, item.getValue(), value);
		}
		employee.castDepartmentToNameCase();
		return employee;
	}

	protected String parseAttribute(Attribute attribute) throws NamingException {
		StringBuilder attrValue = new StringBuilder();
		if (attribute != null) {
			for (int i = 0; i < attribute.size(); i++) {
				attrValue.append(attribute.get(i).toString());
			}
		}
		return attrValue.toString();
	}
	
	protected InitialLdapContext initializeContext()
			throws NamingException {
		return initializeContext(bindDN + principalSuffix, bindCredentials);
	}

	protected InitialLdapContext initializeContext(String principal,
			String credentials) throws NamingException {
		Properties env = new Properties();

		env.setProperty(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		env.setProperty(Context.SECURITY_AUTHENTICATION, "simple");

		String providerUrl = String.format("ldap://%s:%d", getServerAddress(),
				getServerPort());
		env.setProperty(Context.PROVIDER_URL, providerUrl);

		env.setProperty(Context.SECURITY_PRINCIPAL, principal);
		env.setProperty(Context.SECURITY_CREDENTIALS, credentials);

		InitialLdapContext ctx = new InitialLdapContext(env, null);
		return ctx;
	}

	public String getServerAddress() {
		return serverAddress;
	}

	public void setServerAddress(String serverAddress) {
		this.serverAddress = serverAddress;
	}

	public int getServerPort() {
		return serverPort;
	}

	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	public String getBindDN() {
		return bindDN;
	}

	public void setBindDN(String bindDN) {
		this.bindDN = bindDN;
	}

	public String getBindCredentials() {
		return bindCredentials;
	}

	public void setBindCredentials(String bindCredentials) {
		this.bindCredentials = bindCredentials;
	}

	public int getSearchTimeLimit() {
		return searchTimeLimit;
	}

	public void setSearchTimeLimit(int searchTimeLimit) {
		this.searchTimeLimit = searchTimeLimit;
	}

	public int getSearchScope() {
		return searchScope;
	}

	public void setSearchScope(int searchScope) {
		this.searchScope = searchScope;
	}

	public String getUserContextDN() {
		return userContextDN;
	}

	public void setUserContextDN(String userContextDN) {
		this.userContextDN = userContextDN;
	}

	public Map<String, String> getAttributeMapping() {
		return attributeMapping;
	}

	public void setAttributeMapping(Map<String, String> attributeMapping) {
		this.attributeMapping = attributeMapping;
	}

    class Filter {
		private String expression;
		private Object[] args;

		private Filter() {
			super();
		}

		private Filter(String expression, Object[] filterArgs) {
			super();
			this.expression = expression;
			this.args = filterArgs;
		}

		public String getExpression() {
			return expression;
		}

		public void setExpression(String expression) {
			this.expression = expression;
		}

		public Object[] getArgs() {
			return args;
		}

		public void setArgs(Object[] filterArgs) {
			this.args = filterArgs;
		}
	}
    
    public List<Employee> getTeam(Project project) {
		List<Employee> team = new ArrayList<Employee>();
		TeamFilter teamFilter = project.getTeamFilter();
		if (! TeamFilter.FilterType.DISABLED.equals(teamFilter.getFilterType())) {
			switch (teamFilter.getFilterType()) {
				case PROJECT_CODES:
					team = listEmployeesByTeamCodes(teamFilter.getValue());
					break;
				case NATIVE:
					Filter filter = new Filter(teamFilter.getValue(), null);
					team = listEmployees(filter);
					break;
				default: throw new IllegalStateException("No matching TeamFilter.FilterType for '" + teamFilter.getFilterType() + "' found.");
			}
		}
		log.log(Level.FINE, "{0} : {1} team member have been got from LDAP. Filter = {2}.", 
				new Object[]{project.getCode(), team.size(), teamFilter});
		return new ArrayList<Employee>(team);
	}
	
	List<Employee> listEmployeesByTeamCodes(String teamCodes) {
		Set<Employee> team = new HashSet<Employee>();
		List<String> codes = (!StringUtils.isEmpty(teamCodes))
				? splitTeamCodeString(teamCodes)
				: Collections.<String>emptyList();
		for (String code : codes) {			
			Filter filter = new Filter(groupMemberFilter, new Object[]{code});
			team.addAll(listEmployees(filter));
		}
		return new ArrayList<Employee>(team);
	}

	protected List<String> splitTeamCodeString(String codeInTeamTrackingSystem) {
		List<String> result = new ArrayList<String>();
		for (String code : codeInTeamTrackingSystem.split(",")) {
			result.add(code.trim());
		}
		return result;
	}

	@Override
	public String getName() {
		return "LDAP";
	}

	public void setEmployeeFilter(String employeeFilter) {
		this.employeeFilter = employeeFilter;
	}

	public void setGroupMemberFilter(String groupMemberFilter) {
		this.groupMemberFilter = groupMemberFilter;
	}

	public String getUserFilter() {
		return userFilter;
	}

	public void setUserFilter(String userFilter) {
		this.userFilter = userFilter;
	}

	public String getPrincipalSuffix() {
		return principalSuffix;
	}

	public void setPrincipalSuffix(String principalSuffix) {
		this.principalSuffix = principalSuffix;
	}
}
