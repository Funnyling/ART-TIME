package com.artezio.arttime.web;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import com.artezio.arttime.datamodel.Hours;
import com.artezio.arttime.filter.Filter;
import com.artezio.arttime.filter.FilterRepository;
import com.artezio.arttime.services.HoursService;
import com.artezio.arttime.services.repositories.HoursRepository;
import com.artezio.arttime.web.spread_sheet.EmployeeEffortsSpreadSheet;
import com.artezio.arttime.web.spread_sheet.ProjectEffortsSpreadSheet;
import com.artezio.arttime.web.spread_sheet.SpreadSheet;

@ManagedBean
@ViewScoped
public class ManageEffortsBean extends EffortsBean {
    private static final long serialVersionUID = -2955420544322390401L;
    
    @ManagedProperty("#{manageEffortsStateBean}")
    private ManageEffortsStateBean manageEffortsStateBean;
    @Inject
    private FilterRepository filterRepository;
    @Inject
    private HoursRepository hoursRepository;
    @Inject
    private HoursService hoursService;
        
    private Set<Hours> updatedStatus = new HashSet<Hours>();
    
    private static final Map<EffortsGrouping, Class<? extends SpreadSheet>> spreadSheetClasses;
	
	static {
		spreadSheetClasses = new HashMap<EffortsGrouping, Class<? extends SpreadSheet>>();
		spreadSheetClasses.put(EffortsGrouping.BY_EMPLOYEES, EmployeeEffortsSpreadSheet.class);
		spreadSheetClasses.put(EffortsGrouping.BY_PROJECTS, ProjectEffortsSpreadSheet.class);
	}
    
    protected SpreadSheet initSpreadSheet() {
    	try {
    		Filter currentFilter = filterBean.getCurrentFilter();
    		filterRepository.fetchProjects(currentFilter);
    		return buildSpreadSheet(manageEffortsStateBean.getGrouping(), currentFilter);
    	} catch (ReflectiveOperationException e) {
    		e.printStackTrace();
    		return null;
    	}		
    }
    
	public SpreadSheet buildSpreadSheet(EffortsGrouping grouping, Filter filter) throws ReflectiveOperationException {
		Class<? extends SpreadSheet> spreadSheetClass = spreadSheetClasses.get(grouping);
		return spreadSheetClass
				.getConstructor(HoursRepository.class, Filter.class)				
				.newInstance(hoursRepository, filter);
	}

    public void approveSelectedHours() {
    	List<Hours> selectedHours = getSpreadSheet().getSelectedHours();
    	updatedStatus.addAll(selectedHours);
    	selectedHours
				.parallelStream()
				.forEach(hour -> hour.setApproved(true));
    	getSpreadSheet().updateSelectedRows();
    }

    public void approveAllHours() {
    	List<Hours> hours = getSpreadSheet().getHours();
    	updatedStatus.addAll(hours);
    	hours
			.parallelStream()
			.forEach(hour -> hour.setApproved(true));
    	getSpreadSheet().updateAllRows();
    }

    public void disapproveAllHours() {
    	List<Hours> hours = getSpreadSheet().getHours();
    	updatedStatus.addAll(hours);
    	hours
			.parallelStream()
			.forEach(hour -> hour.setApproved(false));
    	getSpreadSheet().updateAllRows();
    }

    public void disapproveSelectedHours() {
    	List<Hours> selectedHours = getSpreadSheet().getSelectedHours();
    	updatedStatus.addAll(selectedHours);
    	selectedHours
				.parallelStream()
				.forEach(hour -> hour.setApproved(false));
    	getSpreadSheet().updateSelectedRows();
    }

    public void setFilterBean(FilterBean filterBean) {
    	this.filterBean = filterBean;
    }

    public void saveHours() throws ReflectiveOperationException {
		Set<Hours> hours = getSpreadSheet().getUpdatedHours();
		hours.addAll(updatedStatus);
		hoursService.saveManagedHours(hours);		
		resetData();		
    }
    
    @Override
    public void resetData() {
    	super.resetData();
    	updatedStatus = new HashSet<Hours>();
    }
       
	public void setManageEffortsStateBean(ManageEffortsStateBean manageEffortsStateBean) {
		this.manageEffortsStateBean = manageEffortsStateBean;
	}
    
	public void setCurrentFilterAndResetData(Filter currentFilter) {
		filterBean.setCurrentFilter(currentFilter);
		resetData();
	}
	
	
    
}
