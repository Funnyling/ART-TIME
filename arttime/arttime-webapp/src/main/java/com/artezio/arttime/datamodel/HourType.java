package com.artezio.arttime.datamodel;

import java.io.Serializable;
import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.UniqueConstraint;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@XmlAccessorType(XmlAccessType.FIELD)
@Table(uniqueConstraints = {
	@UniqueConstraint(name = "constraint_unique_hourtype_name", columnNames = {"type"})
})
public class HourType implements Serializable {

	private static final long serialVersionUID = -7365565539438274143L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotNull
	private String type;
	@Column(columnDefinition = "BIT", length = 1)
	private boolean actualTime;

	@XmlTransient
	public final static Comparator<HourType> ACTUALTIME_TYPE_COMPARATOR = (ht1, ht2) -> {
		if (ht1.isActualTime() != ht2.isActualTime()) return (ht2.actualTime) ? 1 : -1;
		return ht1.type.compareToIgnoreCase(ht2.type);
	};
	
	public Long getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public HourType() {
	}

	public HourType(String type) {
		this.type = type;
	}

	public void setActualTime(boolean isActualTime) {
		this.actualTime = isActualTime;
	}

	public boolean isActualTime() {
		return actualTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (actualTime ? 1231 : 1237);
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HourType other = (HourType) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (actualTime != other.actualTime)
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

}
