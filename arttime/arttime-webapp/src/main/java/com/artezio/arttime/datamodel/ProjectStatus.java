package com.artezio.arttime.datamodel;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
public class ProjectStatus implements Serializable {
	private static final long serialVersionUID = -4889517601918620656L;

	public enum Status {

		ACTIVE("com.artezio.arttime.status.active"), 
		FROZEN("com.artezio.arttime.status.onHold"), 
		COMPLETED("com.artezio.arttime.status.closed");

		private String key;

		@Override
		public String toString() {
			return key;
		}

		private Status(String key) {
			this.key = key;
		}
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotNull
	@Enumerated(EnumType.STRING)
	private Status status;
	@NotNull
	@Temporal(TemporalType.DATE)
	private Date startAt;

	public final static Comparator<ProjectStatus> STARTAT_COMPARATOR = (ps1, ps2) -> ps1.getStartAt().compareTo(ps2.getStartAt());  

	public ProjectStatus() {
	}

	public ProjectStatus(Status status) {
		this.status = status;
	}

	public ProjectStatus(Status status, Date startAt) {
		this.status = status;
		this.startAt = startAt;
	}

	public Long getId() {
		return id;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getStartAt() {
		return startAt;
	}

	public void setStartAt(Date startAt) {
		this.startAt = startAt;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((startAt == null) ? 0 : startAt.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProjectStatus other = (ProjectStatus) obj;
		if (startAt == null) {
			if (other.startAt != null)
				return false;
		} else if (!startAt.equals(other.startAt))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
	
	public boolean isEmpty(){
		return status==null;
	}

}
