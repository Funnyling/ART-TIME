package com.artezio.arttime.web.spread_sheet;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.HourType;
import com.artezio.arttime.datamodel.Hours;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.filter.Filter;
import com.artezio.arttime.services.repositories.HoursRepository;

public class ProjectEffortsSpreadSheet extends SpreadSheet {
	private static final long serialVersionUID = -1046744816130524270L;

	public ProjectEffortsSpreadSheet(HoursRepository hoursRepository, Filter filter) {
		super(hoursRepository, filter);
	}

	@Override
	protected void buildSkeleton() {
		rows = new ArrayList<SpreadSheetRow<?>>();
		List<Hours> existedHours = hoursRepository.getHours(filter);		
		HoursIndexedBundle bundle = new HoursIndexedBundle(existedHours);		
		
		for (Project project : getSortedProjects()) {						
			List<HourType> hourTypes = getSortedHourTypes(project);
			List<Employee> employees = getSortedTeamMembers(project);
			List<HoursSpreadSheetRow> hoursRows = new ArrayList<HoursSpreadSheetRow>();
			if (employees.isEmpty() || hourTypes.isEmpty()) continue;
			SpreadSheetRow<?> headRow = new HeadSpreadSheetRow(project);
			rows.add(headRow);
			for (Employee employee : employees) {				
				for (HourType hourType : hourTypes) {
					List<Hours> hours = bundle.findHours(project, employee, hourType);
					HoursSpreadSheetRow hoursRow = new HoursSpreadSheetRow(project, employee, hourType, hours);
					hoursRows.add(hoursRow);
					rows.add(hoursRow);
				}				
			}
			Map<HourType, List<HoursSpreadSheetRow>> hoursRowsMap = hoursRows.stream()
					.collect(Collectors.groupingBy(row -> row.getHourType()));
			for (HourType hourType : hourTypes) {				
				SpreadSheetRow<BigDecimal> totalRow = new TotalsSpreadSheetRow(project, hourType, hoursRowsMap.get(hourType));
				rows.add(totalRow);
			}
		}		
	}

	@Override
	protected int calculateTotalKeyFor(SpreadSheetRow<?> row) {
		return SpreadSheetRow.calculateKey(row.getProject(), null, row.getHourType());
	}
}
