package com.artezio.arttime.web.spread_sheet;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.HourType;
import com.artezio.arttime.datamodel.Hours;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.filter.Filter;
import com.artezio.arttime.services.repositories.HoursRepository;

public class EmployeeEffortsSpreadSheet extends SpreadSheet {	
	private static final long serialVersionUID = 8766325612366887278L;

	public EmployeeEffortsSpreadSheet(HoursRepository hoursRepository, Filter filter) {
		super(hoursRepository, filter);
	}

	
	@Override
	protected void buildSkeleton() {
		rows = new ArrayList<SpreadSheetRow<?>>();
		List<Hours> existedHours = hoursRepository.getHours(filter);		
		HoursIndexedBundle bundle = new HoursIndexedBundle(existedHours);		
		
		for (Employee employee : getSortedEmployees()) {
			List<Project> projects = getSortedProjects(employee);
			Set<HourType> employeeHourTypes = getSortedHourTypes(projects);
			List<HoursSpreadSheetRow> hoursRows = new ArrayList<HoursSpreadSheetRow>();
			if (projects.isEmpty() || employeeHourTypes.isEmpty()) continue;
			SpreadSheetRow<?> headRow = new HeadSpreadSheetRow(employee);
			rows.add(headRow);			
			for (Project project : projects) {
				List<HourType> hourTypes = getSortedHourTypes(project);				
				for (HourType hourType : hourTypes) {
					List<Hours> hours = bundle.findHours(project, employee, hourType);
					HoursSpreadSheetRow hoursRow = new HoursSpreadSheetRow(project, employee, hourType, hours);
					hoursRows.add(hoursRow);
					rows.add(hoursRow);
				}
			}																		
			Map<HourType, List<HoursSpreadSheetRow>> hoursRowsMap = hoursRows.stream()
					.collect(Collectors.groupingBy(row -> row.getHourType()));
			for (HourType hourType : employeeHourTypes) {				
				SpreadSheetRow<BigDecimal> totalRow = new TotalsSpreadSheetRow(employee, hourType, hoursRowsMap.get(hourType));
				rows.add(totalRow);
			}
		}
	}
	
	@Override
	protected int calculateTotalKeyFor(SpreadSheetRow<?> row) {
		return SpreadSheetRow.calculateKey(null, row.getEmployee(), row.getHourType());
	}
}
