package com.artezio.arttime.web.exception_handler;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.faces.FacesException;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;
import javax.servlet.ServletContext;

import org.hibernate.exception.ConstraintViolationException;

import com.artezio.arttime.utils.MessagesUtil;
import com.artezio.arttime.utils.OrderedProperties;

public class ExceptionHandler extends ExceptionHandlerWrapper {

    private final static String ERROR_MESSAGES_PROPERTIES_PATH = "/WEB-INF/errorMessages.xml";

    private javax.faces.context.ExceptionHandler wrappedExceptionHandler;
    private static OrderedProperties errorMessages;

    static {
	loadErrorMessages();
    }

    protected static void loadErrorMessages() {
	errorMessages = new OrderedProperties();
	try {
	    ServletContext servletContext = getServletContext();
	    InputStream input = servletContext.getResourceAsStream(ERROR_MESSAGES_PROPERTIES_PATH);
	    errorMessages.loadFromXML(input);
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    protected static ServletContext getServletContext() {
	return (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
    }

    ExceptionHandler(javax.faces.context.ExceptionHandler exceptionHandler) {
	this.wrappedExceptionHandler = exceptionHandler;
    }

    @Override
    public void handle() throws FacesException {
	final Iterator<ExceptionQueuedEvent> i = getUnhandledExceptionQueuedEvents().iterator();
	while (i.hasNext()) {
	    ExceptionQueuedEvent exceptionEvent = i.next();
	    ExceptionQueuedEventContext exceptionEventContext = (ExceptionQueuedEventContext) exceptionEvent.getSource();
	    Throwable incomingException = exceptionEventContext.getException();
	    String errorMessage = findErrorMessage(incomingException);
	    if (errorMessage != null) {
		MessagesUtil.addError(null, errorMessage);
		i.remove();
	    }
	}
	getWrapped().handle();
    }

    @Override
    public javax.faces.context.ExceptionHandler getWrapped() {
	return wrappedExceptionHandler;
    }

    protected String findErrorMessage(Throwable exceptionForSearch) {
    	List<Throwable> exceptionPath = getCauses(exceptionForSearch);
    	for (String exceptionClassName : errorMessages.stringPropertyNames()) {
    		try {
    			Class exceptionClass = Class.forName(exceptionClassName);
    			for (Throwable exception : exceptionPath) {
    				if (exception instanceof ConstraintViolationException) {
    					return getHibernateConstraintViolationMessage(exception);
    				} else if (exceptionClass.isInstance(exception)){
    					return errorMessages.get(exceptionClassName);
    				}
    			}
    		} catch (ClassNotFoundException e) {
    			// e.printStackTrace();
    		}
    	}
    	return null;
    }
    
    private String getHibernateConstraintViolationMessage(Throwable throwable) {
    	String constraintName = ((ConstraintViolationException) throwable).getConstraintName();	    		
    	return (constraintName != null)
    			? "message.constraint." + constraintName 
    			: errorMessages.get(throwable.getClass().getName());
    }

    protected List<Throwable> getCauses(Throwable throwable) {
	List<Throwable> result = new ArrayList<Throwable>();
	while (throwable != null) {
	    result.add(throwable);
	    throwable = throwable.getCause();
	}
	return result;
    }
}