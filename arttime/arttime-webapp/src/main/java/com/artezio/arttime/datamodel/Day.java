package com.artezio.arttime.datamodel;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;

@Entity
public class Day implements Serializable {

    private static final long serialVersionUID = 902134612376410L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(columnDefinition = "BIT", length = 1)
    private Boolean isWorking;
    @NotNull
    @Temporal(value = TemporalType.DATE)
    private Date date;    
    @OneToOne
    private WorkdaysCalendar workdaysCalendar;

    public static final Comparator<Day> DATE_COMPARATOR = (d1, d2) -> d1.getDate().compareTo(d2.getDate());  

    public Day() {    	
    }
    
    public Day(Date date, WorkdaysCalendar workdaysCalendar) {
        this.date = date;
        this.workdaysCalendar = workdaysCalendar;
        this.isWorking = !isWeekend();
    }
    
    public Day(Date date, WorkdaysCalendar workdaysCalendar, Boolean isWorking) {
        this.date = date;
        this.workdaysCalendar = workdaysCalendar;
        this.isWorking = isWorking;
    }

    public void switchDayType() {
        isWorking = !isWorking;
    }

    protected Boolean isWeekend() {
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(date);
    	int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
    	if (dayOfWeek == Calendar.SATURDAY || 
			dayOfWeek == Calendar.SUNDAY) {
    		return true;
    	}
    	return false;
    }

    public Long getId() {
        return id;
    }

    public Boolean isWorking() {
        return isWorking;
    }

    public void setWorking(Boolean working) {
        isWorking = working;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public WorkdaysCalendar getWorkdaysCalendar() {
        return workdaysCalendar;
    }

    public void setWorkdaysCalendar(WorkdaysCalendar workdaysCalendar) {
        this.workdaysCalendar = workdaysCalendar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Day)) return false;

        Day day = (Day) o;

        if (id != null ? !id.equals(day.id) : day.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

}
