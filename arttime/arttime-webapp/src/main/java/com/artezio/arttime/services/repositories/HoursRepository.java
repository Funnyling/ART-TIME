package com.artezio.arttime.services.repositories;

import java.math.BigDecimal;
import java.util.*;
import java.util.Date;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.*;

import org.hibernate.Hibernate;

import com.artezio.arttime.admin_tool.cache.WebCached;
import com.artezio.arttime.admin_tool.cache.WebCached.Scope;
import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.HourType;
import com.artezio.arttime.datamodel.Hours;
import com.artezio.arttime.datamodel.Period;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.filter.Filter;
import com.artezio.arttime.web.criteria.RangePeriodSelector;

@Named
@Stateless
@WebCached(scope = Scope.REQUEST_SCOPED)
public class HoursRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    public List<Hours> getActualHours(Employee employee, Period period) {
	return entityManager
		.createQuery(
			"SELECT h FROM Hours h " + "INNER JOIN h.type t "
				+ "WHERE (:dateFrom <= h.date AND h.date <= :dateTo) " + "AND h.employee = :employee "
				+ "AND t.actualTime = true ")
		.setParameter("dateFrom", period.getStart(), TemporalType.DATE)
		.setParameter("dateTo", period.getFinish(), TemporalType.DATE).setParameter("employee", employee)
		.getResultList();
    }

    void predeleteEqualHours(List<Hours> newHours) {

	Filter filter = new Filter();
	filter.setProjects(getProjects(newHours));
	filter.setEmployees(getEmployees(newHours));
	RangePeriodSelector periodSelector = new RangePeriodSelector(getPeriod(newHours));
	filter.setRangePeriodSelector(periodSelector);
	List<Hours> existingHours = getHours(filter);

	existingHours.retainAll(newHours);
	for (Hours hours : existingHours) {
	    entityManager.remove(hours);
	}
    }

    public List<Hours> getApprovedActualHours(Employee employee, Period period) {
        return getApprovedActualHours(Arrays.asList(employee), period);
    }

    public List<Hours> getApprovedActualHours(List<Employee> employees, Period period) {
        if (!employees.isEmpty()) {
            return entityManager.createQuery("SELECT h FROM Hours h INNER JOIN h.type t "
                    + "WHERE (:dateFrom <= h.date AND h.date <= :dateTo) "
                    + "AND (h.employee IN (:employees)) "
                    + "AND t.actualTime = true AND h.approved = true", Hours.class)
                    .setParameter("dateFrom", period.getStart(), TemporalType.DATE)
                    .setParameter("dateTo", period.getFinish(), TemporalType.DATE)
                    .setParameter("employees", employees)
                    .getResultList();
        }
        return new ArrayList<>();
    }

    public BigDecimal getApprovedActualHoursSum(Employee employee, Period period) {
		return getActualHoursSum(employee, period, true);
    }

	protected BigDecimal getActualHoursSum(Employee employee, Period period, boolean onlyApproved) {
		BigDecimal result = entityManager.createQuery("SELECT SUM(h.quantity) FROM Hours h " +
				"INNER JOIN h.type t " +
				"WHERE (:dateFrom <= h.date AND h.date <= :dateTo) " +
				"AND h.employee = :employee " +
				"AND t.actualTime = true " +
				(onlyApproved ? "AND h.approved = true " : ""), BigDecimal.class)
				.setParameter("dateFrom", period.getStart(), TemporalType.DATE)
				.setParameter("dateTo", period.getFinish(), TemporalType.DATE)
				.setParameter("employee", employee)
				.getSingleResult();
		return result == null
				? BigDecimal.ZERO
				: result;
	}

	public BigDecimal getActualHoursSum(Employee employee, Period period) {
		return getActualHoursSum(employee, period, false);
	}

    public BigDecimal getDailyApprovedHoursSum(List<Hours> approvedHours, Date date) {
	BigDecimal result = BigDecimal.ZERO;
	for (Hours hours : approvedHours) {
	    if (date.equals(hours.getDate()) && hours.getQuantity() != null) {
		result = result.add(hours.getQuantity());
	    }
	}
	return result;
    }

    @SuppressWarnings("unchecked")
    public List<Hours> getHours(Filter filter) {
		String queryString = "SELECT DISTINCT h FROM Hours h "+
				"WHERE (:approved IS NULL OR h.approved = :approved) AND ";
		if (filter.isProjectSetted()) {
		    queryString += (filter.getProjects().isEmpty() ? " FALSE = TRUE " : " h.project IN (:projects)") + " AND ";
		}
		if (filter.isEmployeesSetted()) {
		    queryString += (filter.getEmployees().isEmpty() ? " FALSE = TRUE " : " (h.employee IN (:employees))")
			    + " AND ";
		}
		if (filter.isDepartmentsSetted()) {
		    queryString += (filter.getDepartments().isEmpty() ? " FALSE = TRUE "
			    : " (h.employee.department IN (:departments))") + " AND ";
		}
		queryString = queryString + " (h.date >= :dateFrom AND :dateTo >= h.date) ";
		Query query = entityManager.createQuery(queryString);
		if (filter.isProjectSetted() && !filter.getProjects().isEmpty()) {
		    query.setParameter("projects", filter.getProjects());
		}
		if (filter.isEmployeesSetted() && !filter.getEmployees().isEmpty()) {
		    query.setParameter("employees", filter.getEmployees());
		}
		if (filter.isDepartmentsSetted() && !filter.getDepartments().isEmpty()) {
		    query.setParameter("departments", filter.getDepartments());
		}
		query.setParameter("dateFrom", filter.getPeriod().getStart());
		query.setParameter("dateTo", filter.getPeriod().getFinish());
		query.setParameter("approved", filter.isApproved());
		List<Hours> hours =  query.getResultList();
		for (Hours hour : hours) {
			Hibernate.initialize(hour.getProject().getTeam());
			Hibernate.initialize(hour.getProject().getStatuses());
			Hibernate.initialize(hour.getProject().getAccountableHours());			
		}								
		return hours;
    }

    List<Employee> getEmployees(Collection<Hours> hoursCollection) {
	Set<Employee> result = new HashSet<Employee>();
	for (Hours hours : hoursCollection) {
	    result.add(hours.getEmployee());
	}
	return new ArrayList<Employee>(result);
    }

    Period getPeriod(Collection<Hours> hoursCollection) {
	long minimum = Long.MAX_VALUE;
	long maximum = 0;
	for (Hours hours : hoursCollection) {
	    minimum = Math.min(hours.getDate().getTime(), minimum);
	    maximum = Math.max(hours.getDate().getTime(), maximum);
	}
	return new Period(new Date(minimum), new Date(maximum));
    }

    List<Project> getProjects(Collection<Hours> hoursCollection) {
	Set<Project> result = new HashSet<Project>();
	for (Hours hours : hoursCollection) {
	    result.add(hours.getProject());
	}
	return new ArrayList<Project>(result);
    }

	public Map<Employee, List<Hours>> getApprovedActualHoursLists(List<Employee> employees, Period period) {
        List<Hours> hoursList = getApprovedActualHours(employees, period);
        Map<Employee, List<Hours>> result = hoursList.stream().collect(Collectors.groupingBy(Hours::getEmployee));
        employees.forEach(e -> result.putIfAbsent(e, new ArrayList<>()));
		return result;
	}

    public Map<Employee, BigDecimal> getApprovedActualHoursSum(List<Employee> employees, Period period) {
        Map<Employee, List<Hours>> hoursLists = getApprovedActualHoursLists(employees, period);
        return hoursLists.entrySet().stream().collect(Collectors.
                toMap(Map.Entry::getKey, hoursList -> sumHours(hoursList.getValue())));
    }

	private BigDecimal sumHours(List<Hours> hoursList) {
        return hoursList.stream().filter(hours -> hours.getQuantity() != null)
                .collect(Collectors.reducing(BigDecimal.ZERO, Hours::getQuantity, BigDecimal::add));
    }

	public void lock(Employee employee) {
		entityManager.find(Employee.class, employee.getUserName(), LockModeType.PESSIMISTIC_WRITE);		
	}
	
	public Hours findHours(Date date, Employee employee, Project project, HourType hourType) {
		try {
			return entityManager
					.createQuery("SELECT h FROM Hours h "
							+ "WHERE h.date = :date AND h.employee = :employee "
							+ "AND h.project = :project AND h.type = :hourType ", Hours.class)
					.setParameter("date", date)
					.setParameter("employee", employee)
					.setParameter("project", project)
					.setParameter("hourType", hourType)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Hours create(Hours hours) {
		entityManager.persist(hours);
		return hours;
	}
	
	public Hours update(Hours hours) {
		return entityManager.merge(hours);
	}
}
