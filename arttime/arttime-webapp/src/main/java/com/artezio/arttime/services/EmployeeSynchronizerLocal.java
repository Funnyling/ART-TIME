package com.artezio.arttime.services;

import javax.ejb.Local;

@Local
public interface EmployeeSynchronizerLocal {
	void synchronizeEmployees();
}
