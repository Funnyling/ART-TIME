package com.artezio.arttime.web;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;

import javax.inject.Inject;

import org.apache.commons.lang.time.DateUtils;
import org.primefaces.event.SelectEvent;

import com.artezio.arttime.datamodel.Day;
import com.artezio.arttime.datamodel.Period;
import com.artezio.arttime.datamodel.WorkdaysCalendar;
import com.artezio.arttime.exceptions.WorkdaysCalendarRemoveException;
import com.artezio.arttime.services.repositories.WorkdaysCalendarRepository;
import com.artezio.arttime.utils.CalendarUtils;

@ManagedBean
@ViewScoped
public class WorkdaysCalendarBean implements Serializable {
    private static final long serialVersionUID = 7254105710714398312L;
    @Inject
    private ExternalContext externalContext;
    private WorkdaysCalendar workdaysCalendar;
    @Inject
    private WorkdaysCalendarRepository calendarRepository;
    private Date selectedDate;
    private Period period = new Period();
    private Period extendedPeriod = new Period();
    private Map<Date, Day> days = new HashMap<Date, Day>();
    private String daysOff;


    @PostConstruct
    public void init() {
		Map<String, String> requestParams = externalContext.getRequestParameterMap();
		String calendarId = requestParams.get("calendar");
		if (calendarId == null){
			this.workdaysCalendar = new WorkdaysCalendar();
		} else {
			this.workdaysCalendar = calendarRepository.findWorkdaysCalendar(Long.parseLong(calendarId));
			Date start = CalendarUtils.firstDayOfMonth(new Date());
			Date finish = CalendarUtils.lastDayOfMonth(new Date());
			updatePeriod(start, finish);
		}
    }

    public WorkdaysCalendar getWorkdaysCalendar() {
    	return workdaysCalendar;
    }

    public void remove(WorkdaysCalendar workdaysCalendar) throws WorkdaysCalendarRemoveException {
    	calendarRepository.remove(workdaysCalendar);
    }

    public void create() {
    	calendarRepository.create(workdaysCalendar);
    }

    public void updateWorkdaysCalendar(List<Day> days) {    	
    	calendarRepository.update(workdaysCalendar, days);
    }

	public Date getSelectedDate() {
		return selectedDate;
	}

	public void setSelectedDate(Date selectedDate) {
		this.selectedDate = selectedDate;
	}
    
    public void onDateSelect(SelectEvent event){    	
    	Date selected = (Date)event.getObject();
    	Day day = days.get(selected);
		day.switchDayType();
		updateDaysOff();
    }
    
    public void nextMonth(){
    	Date selectedDate = period.getStart();
		selectedDate = DateUtils.addMonths(selectedDate, 1);
		Date start = CalendarUtils.firstDayOfMonth(selectedDate);
		Date finish = CalendarUtils.lastDayOfMonth(selectedDate);
		updatePeriod(start, finish);
    }
    
    public void prevMonth(){
    	Date selectedDate = period.getStart();
		selectedDate = DateUtils.addMonths(selectedDate, -1);
		Date start = CalendarUtils.firstDayOfMonth(selectedDate);
		Date finish = CalendarUtils.lastDayOfMonth(selectedDate);
		updatePeriod(start, finish);		
    }
    
    protected void updatePeriod(Date start, Date finish) {
		period.setStart(start);
		period.setFinish(finish);
		extendedPeriod.setStart(CalendarUtils.firstDayOfMonth(DateUtils.addMonths(start, -1)));
		extendedPeriod.setFinish(CalendarUtils.lastDayOfMonth(DateUtils.addMonths(start, 1)));
		populateDays();
		updateDaysOff();
	}
    
    protected void populateDays() {    	
		List<Day> daysInPeriod = calendarRepository.getDays(workdaysCalendar, extendedPeriod);
		for (Day day : daysInPeriod){
			Date date = day.getDate();
		    if (!days.containsKey(date)) {		    	
		    	days.put(day.getDate(), day);
		    }			
		}				
    }
	
	protected void updateDaysOff(){
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat dateFormat = new SimpleDateFormat("d-M-yyyy");
		for (Day day: days.values()){
			if (!day.isWorking() && extendedPeriod.contains(day.getDate())){
				sb.append(dateFormat.format(day.getDate())+",");				
			}
		}
		daysOff = sb.toString();		
	}
	
	public String getDaysOff() {				
		return daysOff;
	}
	
	public void setDaysOff(String daysOff){
		this.daysOff = daysOff;
	}
	
	public List<Day> getDays(){
		return new ArrayList<Day>(days.values());
	}
		
}

