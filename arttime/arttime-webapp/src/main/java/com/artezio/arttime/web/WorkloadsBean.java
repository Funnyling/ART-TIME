package com.artezio.arttime.web;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.inject.Inject;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.services.repositories.EmployeeRepository;
import com.artezio.arttime.web.criteria.EmployeeSearchCriteriaBean;

@ManagedBean
@ViewScoped
public class WorkloadsBean implements Serializable {
    private static final long serialVersionUID = 1050474595811573237L;
    private Employee employee;
    @Inject
    private ExternalContext externalContext;
    @Inject
    private EmployeeSearchCriteriaBean employeeSearchCriteriaBean;
    @Inject
    private EmployeeRepository employeeRepository;

    @PostConstruct
    public void init() {
	Map<String, String> requestParams = externalContext.getRequestParameterMap();
	String username = requestParams.get("employee");
	this.employee = employeeRepository.find(username);
    }

    public void editWorkload(Employee employee) {
	this.employee = employee;
    }

    public void save() {
	employeeRepository.update(employee);
    }

    public Employee getEmployee() {
	return employee;
    }

    public List<Employee> getEmployees() {
	String searchString = employeeSearchCriteriaBean.getSearchQuery();
	return employeeRepository.getEmployees(searchString);
    }
}
