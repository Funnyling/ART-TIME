package com.artezio.arttime.services.integration.ldap;

import static junitx.util.PrivateAccessor.setField;
import static org.easymock.EasyMock.createMock;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import com.artezio.arttime.config.Settings;
import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.datamodel.TeamFilter;
import com.artezio.arttime.datamodel.TeamFilter.FilterType;
import com.artezio.arttime.web.converters.ListEmployeeConverter;


public class LdapAdapterTest {
	
	private LdapAdapter ldapImpl = new LdapAdapter(new Settings());	

	@Before 
	public void setUp(){
		ldapImpl.setBindCredentials("qwertyu_777");
		ldapImpl.setBindDN("dev-hrms");
		ldapImpl.setPrincipalSuffix("@ARTGROUP.local");
		ldapImpl.setServerAddress("dc-m.ARTGROUP.local");
		ldapImpl.setServerPort(389);
		
		ldapImpl.getAttributeMapping().put("sn", "lastName");
		ldapImpl.getAttributeMapping().put("givenName", "firstName");
		ldapImpl.getAttributeMapping().put("sAMAccountName", "userName");
		ldapImpl.getAttributeMapping().put("physicalDeliveryOfficeName", "department");
		
		ldapImpl.setUserContextDN("CN=Users,DC=ARTGROUP,DC=local");
		ldapImpl.setEmployeeFilter("(&(objectClass=user)(sn=*)(givenName=*))");
		ldapImpl.setGroupMemberFilter("(&(objectClass=user)(sn=*)(givenName=*))");
		ldapImpl.setUserFilter("(&(objectclass=*)(sAMAccountName={0}))");
	}
	
	
	@Test
	public void testSearchAllAttrs() throws Exception {
		List<Employee> employees = ldapImpl.getEmployees();
		assertTrue(employees.size() > 0);		
	}
	
	@Test
	public void testFindEmployee() throws Exception {
		Employee employee = ldapImpl.findEmployee("sdegtyarev");
		assertTrue(employee.getUserName().equals("sdegtyarev"));
	}
	
	@Test
	public void testFindEmployee_SearchMustBeCaseInsensitive() {
		Employee employee = ldapImpl.findEmployee("Sdegtyarev");
		
		assertNotNull(employee);
		assertEquals("sdegtyarev", employee.getUserName());
	}
	
	@Test
	public void testGetTeam() throws Exception {
		Logger log = createMock(Logger.class);
		setField(ldapImpl, "log", log);
		Project project = new Project();
		TeamFilter teamFilter = new TeamFilter(FilterType.PROJECT_CODES, "_ART-PRJ-ART-TIME-PG");
		project.setTeamFilter(teamFilter);
				
		List<Employee> team = ldapImpl.getTeam(project);		
		
		assertTrue(team.size() > 0);			
	}
	
	@Test
	public void testGetTeam_TwoTeamCodes() throws Exception {
		Logger log = createMock(Logger.class);
		setField(ldapImpl, "log", log);
		Project project = new Project();
		TeamFilter teamFilter = new TeamFilter(FilterType.PROJECT_CODES, "_ART-PRJ-ART-TIME-PG,_ART-PRJ-ART-TIME-PM");
		project.setTeamFilter(teamFilter);
				
		List<Employee> team = ldapImpl.getTeam(project);		
		
		assertTrue(team.size() > 0);			
	}
	
	@Test
	public void testGetTeam_ByNativeFilter() throws Exception {
		Logger log = createMock(Logger.class);
		setField(ldapImpl, "log", log);
		Project project = new Project();
		TeamFilter teamFilter = new TeamFilter(FilterType.NATIVE, "(&(objectClass=user)(physicalDeliveryOfficeName=Minsk))");
		project.setTeamFilter(teamFilter);
				
		List<Employee> team = ldapImpl.getTeam(project);		
		
		assertTrue(team.size() > 0);
	}
	
	
	@Test
	public void testGetTeam_DisabledFilter() throws Exception {
		Logger log = createMock(Logger.class);
		setField(ldapImpl, "log", log);
		Project project = new Project();
		TeamFilter teamFilter = new TeamFilter(FilterType.DISABLED, "");
		project.setTeamFilter(teamFilter);
				
		List<Employee> team = ldapImpl.getTeam(project);		
		
		assertEquals(0, team.size());
	}
	
	@Test
	public void testListEmployeesByTeamCodes() throws Exception {
		Logger log = createMock(Logger.class);
		setField(ldapImpl, "log", log);
		Project project = new Project();
		TeamFilter teamFilter = new TeamFilter(FilterType.PROJECT_CODES, "_ART-PRJ-ART-TIME-PG");
		project.setTeamFilter(teamFilter);
				
		List<Employee> team = ldapImpl.getTeam(project);		
		
		assertTrue(team.size() > 0);			
	}
	
	@Test
	public void testListEmployeesByTeamCodes_TwoTeamCodes() throws Exception {
		Logger log = createMock(Logger.class);
		setField(ldapImpl, "log", log);
		Project project = new Project();
		TeamFilter teamFilter = new TeamFilter(FilterType.PROJECT_CODES, "_ART-PRJ-ART-TIME-PG,_ART-PRJ-ART-TIME-PM");
		project.setTeamFilter(teamFilter);
				
		List<Employee> team = ldapImpl.getTeam(project);		
		
		assertTrue(team.size() > 0);			
	}
	
	@Test
	public void testSplitTeamCodeString() throws Exception{
		String code1 = "code1";
		String code2 = "code2";
		String codeToSplit = code1 + "," + code2;
		
		List<String> actual = ldapImpl.splitTeamCodeString(codeToSplit);
		
		assertEquals(2, actual.size());
		assertTrue(actual.contains(code1));
		assertTrue(actual.contains(code2));
	}
	
	@Test
	public void testSplitTeamCodeString_therIsSpace() throws Exception{
		String code1 = "code1";
		String code2 = "code2";
		String codeToSplit = code1 + "  ,  " + code2;
		
		List<String> actual = ldapImpl.splitTeamCodeString(codeToSplit);
		
		assertEquals(2, actual.size());
		assertTrue(actual.contains(code1));
		assertTrue(actual.contains(code2));
	}
	
	@Test
	public void testFindEmployeesByFullName(){
		
		Employee employee1 = new Employee("jsmith", "John", "Smith", "j_smith@mail.com");
		Employee employee2 = new Employee("bgray", "Bob", "Gray", "b_gray@mail.com");
		List<Employee> employees = new ArrayList<Employee>(Arrays.asList(employee1,employee2));
		
		ldapImpl = EasyMock.createMockBuilder(LdapAdapter.class).addMockedMethod("getEmployees").createMock();
		EasyMock.expect(ldapImpl.getEmployees()).andReturn(employees);
		EasyMock.replay(ldapImpl);
		
		List<Employee> actual = ldapImpl.findEmployeesByFullName("smi");
		
		assertEquals(1, actual.size());
		assertEquals(employee1, actual.get(0));
	}
	
	@Test
	public void testFindEmployeesByFullName_SearchNameIsEmpty(){
		
		Employee employee1 = new Employee("jsmith", "John", "Smith", "j_smith@mail.com");
		Employee employee2 = new Employee("bgray", "Bob", "Gray", "b_gray@mail.com");
		List<Employee> expected = new ArrayList<Employee>(Arrays.asList(employee1,employee2));
		
		ldapImpl = EasyMock.createMockBuilder(LdapAdapter.class).addMockedMethod("getEmployees").createMock();
		EasyMock.expect(ldapImpl.getEmployees()).andReturn(expected);
		EasyMock.replay(ldapImpl);
		
		List<Employee> actual = ldapImpl.findEmployeesByFullName("");
		
		assertThat(actual, is(expected));
	}
	
}
