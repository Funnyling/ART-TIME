package com.artezio.arttime.services.repositories;

import static junitx.util.PrivateAccessor.setField;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.artezio.arttime.datamodel.HourType;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.exceptions.ActualTimeRemovalException;

public class HourTypeRepositoryTest {

    private EntityManager entityManager;
    private EntityManagerFactory entityManagerFactory;
    private HourTypeRepository hourTypeRepository;

    @Before
    public void setUp() throws Exception {
	hourTypeRepository = new HourTypeRepository();
	Map<String, String> properties = new HashMap<String, String>();
	properties.put("javax.persistence.validation.mode", "none");
	entityManagerFactory = Persistence.createEntityManagerFactory("test", properties);
	entityManager = entityManagerFactory.createEntityManager();
	setField(hourTypeRepository, "entityManager", entityManager);
	entityManager.getTransaction().begin();
    }

    @After
    public void tearDown() throws Exception {
	if (entityManager.getTransaction().isActive()) {
	    if (entityManager.getTransaction().getRollbackOnly()) {
		entityManager.getTransaction().rollback();
	    } else {
		entityManager.getTransaction().commit();
	    }
	    entityManagerFactory.close();
	}
    }

    @Test
    public void testCreateHourType() {
	HourType hourType = new HourType();
	hourTypeRepository.create(hourType);

	Long id = hourType.getId();
	assertNotNull(id);
	HourType actual = entityManager.find(HourType.class, id);
	assertEquals(hourType, actual);
    }

    @Test
    public void testCreateHourType_notExistActualTime() {
	HourType notActualTime = new HourType("not actual");
	notActualTime.setActualTime(false);
	entityManager.persist(notActualTime);
	HourType hourType = new HourType("hourType");
	hourType.setActualTime(false);

	HourType actual = hourTypeRepository.create(hourType);

	assertEquals(hourType, actual);
	assertTrue(actual.isActualTime());
    }

    @Test
    public void testFindActual() {
	HourType expectedType = new HourType("actual type");
	expectedType.setActualTime(true);
	HourType unexpectedType = new HourType("unexpected type");
	unexpectedType.setActualTime(false);
	entityManager.persist(expectedType);
	entityManager.persist(unexpectedType);

	HourType actual = hourTypeRepository.findActual();

	assertNotNull(actual);
	assertEquals(expectedType, actual);
    }

    @Test
    public void testFindActual_ifNotSetActualTime() {
	HourType unexpectedType1 = new HourType("unexpected type 1");
	unexpectedType1.setActualTime(false);
	HourType unexpectedType2 = new HourType("unexpected type 2");
	unexpectedType2.setActualTime(false);
	entityManager.persist(unexpectedType1);
	entityManager.persist(unexpectedType2);

	HourType actual = hourTypeRepository.findActual();

	assertNull(actual);
    }

    @Test
    public void testGetAll() {
	HourType hourType1 = new HourType();
	HourType hourType2 = new HourType();
	hourType1.setType("day off");
	hourType2.setType("over time");
	entityManager.persist(hourType1);
	entityManager.persist(hourType2);

	List<HourType> actuals = hourTypeRepository.getAll();
	assertEquals(2, actuals.size());
	assertTrue(actuals.contains(hourType1));
	assertTrue(actuals.contains(hourType2));

    }

    @Test
    public void testIsActualTimeExist() {
	HourType actualTime = new HourType("actual");
	actualTime.setActualTime(true);
	HourType hourType = new HourType("hourType");
	entityManager.persist(actualTime);
	entityManager.persist(hourType);

	boolean actual = hourTypeRepository.isActualTimeExist();

	assertTrue(actual);
    }

    @Test
    public void testIsActualTimeExist_notExist() {
	HourType notActualTime = new HourType("not actual");
	notActualTime.setActualTime(false);
	entityManager.persist(notActualTime);

	boolean actual = hourTypeRepository.isActualTimeExist();

	assertFalse(actual);
    }

    @Test
    public void testRemoveHourType() throws ActualTimeRemovalException {
	HourType hourType = new HourType();
	entityManager.persist(hourType);
	Long id = hourType.getId();

	hourTypeRepository.remove(hourType);

	HourType actual = entityManager.find(HourType.class, id);
	assertNull(actual);
    }

    @Test(expected = com.artezio.arttime.exceptions.ActualTimeRemovalException.class)
    public void testRemoveHourType_ifActualTime() throws ActualTimeRemovalException {
	HourType actualTime = new HourType("actual time");
	actualTime.setActualTime(true);
	entityManager.persist(actualTime);

	hourTypeRepository.remove(actualTime);
    }

    @Test(expected = PersistenceException.class)
    public void testRemoveHourType_RemoveForbidden() throws ActualTimeRemovalException {
	HourType hourType = new HourType();
	Project project = new Project();
	project.addAccountableHours(hourType);
	entityManager.persist(hourType);
	entityManager.persist(project);

	hourTypeRepository.remove(hourType);
    }

    @Test
    public void testSetAsActualTime() {
	HourType expectedType = new HourType("type");
	entityManager.persist(expectedType);

	HourType actual = hourTypeRepository.setAsActualTime(expectedType);

	assertTrue(actual.isActualTime());
    }

    @Test
    public void testSetAsActualTime_OnlyOneActual() {
	HourType expectedType1 = new HourType("type 1");
	HourType expectedType2 = new HourType("type 2");
	expectedType1.setActualTime(false);
	expectedType2.setActualTime(true);
	entityManager.persist(expectedType1);
	entityManager.persist(expectedType2);

	HourType actual = hourTypeRepository.setAsActualTime(expectedType1);

	entityManager.flush();
	entityManager.clear();

	List<HourType> actualList = entityManager.createQuery("SELECT h FROM HourType h ORDER BY h.type",
		HourType.class).getResultList();

	assertTrue(actual.isActualTime());
	assertTrue(actualList.get(0).isActualTime());
	assertFalse(actualList.get(1).isActualTime());
    }

    @Test
    public void testUpdateHourType() {
	HourType hourType = new HourType();
	hourType.setType("day off");
	entityManager.persist(hourType);
	Long id = hourType.getId();
	String newType = "over time";
	hourType.setType(newType);

	hourTypeRepository.update(hourType);
	entityManager.flush();

	HourType actual = entityManager.find(HourType.class, id);
	assertEquals(newType, actual.getType());
    }

}
