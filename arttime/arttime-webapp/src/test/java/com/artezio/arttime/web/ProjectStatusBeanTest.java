package com.artezio.arttime.web;

import static junitx.util.PrivateAccessor.setField;
import static org.easymock.EasyMock.createMock;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import junitx.framework.ListAssert;

import org.junit.Before;
import org.junit.Test;

import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.datamodel.ProjectStatus;
import com.artezio.arttime.datamodel.ProjectStatus.Status;
import com.artezio.arttime.services.repositories.ProjectRepository;

public class ProjectStatusBeanTest {
    private ProjectStatusBean bean;
    private ProjectRepository projectRepository;

    @Before
    public void setUp() throws NoSuchFieldException {
	bean = new ProjectStatusBean();
	projectRepository = createMock(ProjectRepository.class);
	setField(bean, "projectRepository", projectRepository);
    }

    @Test
    public void testAddStatus() throws NoSuchFieldException {
	Project project = new Project();
	ProjectStatus projectStatus = new ProjectStatus(Status.FROZEN);
	setField(bean, "project", project);
	setField(bean, "projectStatus", projectStatus);

	bean.addStatus();

	List<ProjectStatus> expected = Arrays.asList(projectStatus);

	ListAssert.assertEquals(expected, project.getStatuses());
	assertSame(projectStatus, project.getStatuses().get(0));
	assertNotSame(projectStatus, bean.getProjectStatus());
    }

    @Test
    public void testDeleteProjectStatus() throws NoSuchFieldException {
	Project project = new Project();
	ProjectStatus projectStatus = new ProjectStatus(Status.FROZEN);
	project.addStatus(projectStatus);
	setField(bean, "project", project);

	bean.delete(projectStatus);

	assertTrue(project.getStatuses().isEmpty());
    }
}
