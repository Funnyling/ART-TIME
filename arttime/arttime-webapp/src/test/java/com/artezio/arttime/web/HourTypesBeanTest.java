package com.artezio.arttime.web;

import static junitx.util.PrivateAccessor.getField;
import static junitx.util.PrivateAccessor.setField;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.richfaces.component.SortOrder;

import com.artezio.arttime.datamodel.HourType;
import com.artezio.arttime.exceptions.ActualTimeRemovalException;
import com.artezio.arttime.services.repositories.HourTypeRepository;

public class HourTypesBeanTest {
    private HourTypesBean bean;
    private HourTypeRepository hourTypeRepository;

    @Before
    public void setUp() throws NoSuchFieldException {
	bean = new HourTypesBean();
	hourTypeRepository = createMock(HourTypeRepository.class);
	setField(bean, "hourTypeRepository", hourTypeRepository);
    }

    @Test
    public void testCreate() {
	HourType hourType = new HourType();
	expect(hourTypeRepository.create(hourType)).andReturn(hourType);
	replay(hourTypeRepository);

	bean.create(hourType);

	verify(hourTypeRepository);
    }

    @Test
    public void testUpdate() {
	HourType hourType = new HourType();
	expect(hourTypeRepository.update(hourType)).andReturn(hourType);
	replay(hourTypeRepository);

	bean.update(hourType);

	verify(hourTypeRepository);
    }

    @Test
    public void testRemove() throws ActualTimeRemovalException {
	HourType hourType = new HourType();
	hourTypeRepository.remove(hourType);
	replay(hourTypeRepository);

	bean.remove(hourType);

	verify(hourTypeRepository);
    }

    @Test
    public void testSortByName() {
	bean.setTypeNameOrder(SortOrder.ascending);

	bean.sortByName();

	assertEquals(bean.getTypeNameOrder(), SortOrder.descending);
    }

    @Test
    public void testAddNew() throws NoSuchFieldException {
	setField(bean, "hourType", null);

	bean.addNew();

	HourType actual = (HourType) getField(bean, "hourType");
	assertNotNull(actual);
    }
}
