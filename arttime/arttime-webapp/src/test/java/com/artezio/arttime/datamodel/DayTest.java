package com.artezio.arttime.datamodel;


import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

public class DayTest {

    @SuppressWarnings("deprecation")
	@Test
    public void testIsWeekend_SundayDate() {
        WorkdaysCalendar workdaysCalendar = new WorkdaysCalendar("calendar");        
        Date sundayDate = new Date(2011, 0, Calendar.SUNDAY);
        Day day = new Day(sundayDate, workdaysCalendar);

        assertTrue(day.isWeekend());
    }

    @SuppressWarnings("deprecation")
	@Test
    public void testIsWeekend_SaturdayDate() {
        WorkdaysCalendar workdaysCalendar = new WorkdaysCalendar("calendar");
        Date saturdayDate = new Date(2011, 0, Calendar.SATURDAY);
        Day day = new Day(saturdayDate, workdaysCalendar);

        assertTrue(day.isWeekend());
    }

    @SuppressWarnings("deprecation")
	@Test
    public void testSwitchDayType() {
        Day day = new Day(new Date(), new WorkdaysCalendar());
        boolean unexpected = day.isWorking();

        day.switchDayType();

        boolean actual = day.isWorking();
        assertEquals(unexpected, !actual);
    }
}
