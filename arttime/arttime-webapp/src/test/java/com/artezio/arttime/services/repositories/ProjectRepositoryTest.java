package com.artezio.arttime.services.repositories;

import static com.artezio.arttime.test.utils.CalendarUtils.createPeriod;
import static com.artezio.arttime.test.utils.CalendarUtils.getOffsetDate;
import static junitx.util.PrivateAccessor.setField;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;

import junitx.framework.ListAssert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.HourType;
import com.artezio.arttime.datamodel.Hours;
import com.artezio.arttime.datamodel.Participation;
import com.artezio.arttime.datamodel.Period;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.datamodel.ProjectStatus;
import com.artezio.arttime.datamodel.WorkdaysCalendar;
import com.artezio.arttime.services.ProjectsSelectionCriteria;
import com.artezio.arttime.services.ProjectsSelectionCriteria.Status;

public class ProjectRepositoryTest {

    private EntityManager entityManager;
    private EntityManagerFactory entityManagerFactory;
    private ProjectRepository projectRepository;

    @Before
    public void setUp() throws Exception {
	projectRepository = new ProjectRepository();
	Map<String, String> properties = new HashMap<String, String>();
	properties.put("javax.persistence.validation.mode", "none");
	entityManagerFactory = Persistence.createEntityManagerFactory("test", properties);
	entityManager = entityManagerFactory.createEntityManager();
	setField(projectRepository, "entityManager", entityManager);
	entityManager.getTransaction().begin();
    }

    @After
    public void tearDown() throws Exception {
	if (entityManager.getTransaction().isActive()) {
	    if (entityManager.getTransaction().getRollbackOnly()) {
		entityManager.getTransaction().rollback();
	    } else {
		entityManager.getTransaction().commit();
	    }
	    entityManagerFactory.close();
	}
    }

    @Test
    public void testCreateProject() {
	Date startFrom = new Date();
	HourType hourType = new HourType("type");
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	entityManager.persist(hourType);
	entityManager.persist(calendar);
	entityManager.flush();
	entityManager.clear();

	Employee manager = new Employee("manager");
	Participation participation = new Participation();
	participation.setWorkdaysCalendar(calendar);
	Employee resource = new Employee("slave");
	participation.setEmployee(resource);
	Project project = new Project();
	project.setManager(manager);
	project.getAccountableHours().add(hourType);
	project.addParticipation(participation);
	assertNull(project.getId());

	Project actual = projectRepository.create(project);
	entityManager.flush();

	assertNotNull(actual.getId());
	assertEquals(1, project.getStatuses().size());
	ProjectStatus actualStatus = project.getStatuses().get(0);
    }

    @Test
    public void testCreateProjectWithExistsManager() {
	Employee manager = new Employee("manager");
	entityManager.persist(manager);
	entityManager.flush();
	entityManager.clear();

	Project project = new Project();
	project.setManager(manager);

	project = projectRepository.create(project);

	entityManager.flush();
    }

    @Test
    public void testCreateProjectWithSameEmployee() {
	Employee employee = new Employee("slave");
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	entityManager.merge(employee);
	entityManager.persist(calendar);
	entityManager.flush();
	entityManager.clear();

	Employee manager = new Employee("manager");
	Participation participation = new Participation();
	participation.setWorkdaysCalendar(calendar);
	participation.setEmployee(employee);
	Project project = new Project();
	project.setManager(manager);
	project.addParticipation(participation);

	projectRepository.create(project);
    }

    @Test
    public void testGetActiveProjects_ByEmployeeAndPeriod() {
	Date start = new Date();
	Date finish = getOffsetDate(5);
	Period period = new Period(start, finish);
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	Employee employee = new Employee("employee");
	Participation participation = new Participation(employee, start);
	participation.getPeriod().setFinish(getOffsetDate(10));
	participation.setWorkdaysCalendar(calendar);
	ProjectStatus status = createProjectStatus(start, ProjectStatus.Status.ACTIVE);
	Project project = new Project();
	project.setDefaultWorkdaysCalendar(calendar);
	project.addParticipation(participation);
	project.addStatus(status);
	entityManager.persist(calendar);
	entityManager.persist(employee);
	entityManager.persist(status);
	entityManager.persist(project);

	List<Project> actual = projectRepository.getActiveProjects(employee, period);

	assertFalse(actual.isEmpty());
    }

    @Test
    public void testGetActiveProjects_ByEmployeeAndPeriod_projectIsActiveSinceFinishPeriod() {
	Date start = new Date();
	Date finish = getOffsetDate(5);
	Period period = new Period(start, finish);
	Date date = getOffsetDate(-2);
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	Employee employee = new Employee("employee");
	Participation participation = new Participation(employee, date);
	participation.setWorkdaysCalendar(calendar);
	ProjectStatus status1 = createProjectStatus(date, ProjectStatus.Status.COMPLETED);
	ProjectStatus status2 = createProjectStatus(finish, ProjectStatus.Status.ACTIVE);
	Project expected = new Project();
	expected.setDefaultWorkdaysCalendar(calendar);
	expected.addParticipation(participation);
	expected.addStatus(status1);
	expected.addStatus(status2);
	entityManager.persist(calendar);
	entityManager.persist(employee);
	entityManager.persist(status1);
	entityManager.persist(status2);
	entityManager.persist(expected);

	List<Project> actuals = projectRepository.getActiveProjects(employee, period);

	assertEquals(1, actuals.size());
	assertEquals(expected, actuals.get(0));
    }

    @Test
    public void testGetActiveProjects_ByEmployeePeriod() {
	Period period = new Period(getTwoDaysAgo(), new Date());
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	Employee employee1 = new Employee("employee 1");
	Employee employee2 = new Employee("employee 2");
	Participation participation1 = new Participation(employee1, getTwoDaysAgo());
	participation1.setWorkdaysCalendar(calendar);
	Participation participation2 = new Participation(employee2, getTwoDaysAgo());
	participation2.setWorkdaysCalendar(calendar);
	ProjectStatus status1 = createProjectStatus(getTwoDaysAgo(), ProjectStatus.Status.ACTIVE);
	ProjectStatus status2 = createProjectStatus(new Date(), ProjectStatus.Status.FROZEN);
	Project project1 = new Project();
	project1.setDefaultWorkdaysCalendar(calendar);
	project1.addParticipation(participation1);
	project1.addStatus(status1);
	Project project2 = new Project();
	project2.setDefaultWorkdaysCalendar(calendar);
	project2.addParticipation(participation2);
	project2.addStatus(status2);
	entityManager.persist(calendar);
	entityManager.persist(employee1);
	entityManager.persist(employee2);
	entityManager.persist(status1);
	entityManager.persist(status2);
	entityManager.persist(project1);
	entityManager.persist(project2);
	entityManager.flush();
	entityManager.clear();

	List<Project> actual = projectRepository.getActiveProjects(employee1, period);
	Project actualProject = actual.get(0);
	Participation actualParticipation = actualProject.getTeam().get(0);
	Employee actualEmployee = actualParticipation.getEmployee();

	assertNotNull(actual);
	assertTrue(actual.size() == 1);
	assertEquals(actualEmployee, employee1);
    }

    @Test
    public void testGetActiveProjects_ByEmployeePeriod_employeeInNotTeamMember() {
	Date start = new Date();
	Date finish = getOffsetDate(5);
	Period period = new Period(start, finish);
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	Employee expectedEmployee = new Employee("expected employee");
	Employee unexpectedEmployee = new Employee("unexpected employee");
	Date date = getOffsetDate(-2);
	Participation participation = new Participation(unexpectedEmployee, date);
	participation.setWorkdaysCalendar(calendar);
	ProjectStatus status = createProjectStatus(date, ProjectStatus.Status.ACTIVE);
	Project project = new Project();
	project.addStatus(status);
	project.addParticipation(participation);
	project.setDefaultWorkdaysCalendar(calendar);
	entityManager.persist(calendar);
	entityManager.persist(expectedEmployee);
	entityManager.persist(unexpectedEmployee);
	entityManager.persist(status);
	entityManager.persist(project);
	entityManager.flush();
	entityManager.clear();

	List<Project> actual = projectRepository.getActiveProjects(expectedEmployee, period);

	assertTrue(actual.isEmpty());
    }

    @Test
    public void testGetActiveProjects_ByEmployeePeriod_employeeIsNotTeamMemberDuringPeriod() {
	Date start = new Date();
	Date finish = getOffsetDate(5);
	Period period = new Period(start, finish);
	Date date = getOffsetDate(-2);
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	Employee employee = new Employee("employee");
	Participation participation = new Participation(employee, getOffsetDate(10));
	participation.setWorkdaysCalendar(calendar);
	ProjectStatus status = createProjectStatus(date, ProjectStatus.Status.ACTIVE);
	Project project = new Project();
	project.setDefaultWorkdaysCalendar(calendar);
	project.addParticipation(participation);
	project.addStatus(status);
	entityManager.persist(calendar);
	entityManager.persist(employee);
	entityManager.persist(status);
	entityManager.persist(project);
	entityManager.flush();
	entityManager.clear();

	List<Project> actual = projectRepository.getActiveProjects(employee, period);

	assertTrue(actual.isEmpty());
    }

    @Test
    public void testGetActiveProjects_ByEmployeePeriod_employeeIsNotTeamMemberSinceStartPeriod() {
	Date start = new Date();
	Date finish = getOffsetDate(5);
	Period period = new Period(start, finish);
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	Employee employee = new Employee("employee");
	Participation participation = new Participation(employee, getOffsetDate(-2));
	participation.getPeriod().setFinish(getOffsetDate(-1));
	participation.setWorkdaysCalendar(calendar);
	ProjectStatus status = createProjectStatus(getOffsetDate(-2), ProjectStatus.Status.ACTIVE);
	Project project = new Project();
	project.setDefaultWorkdaysCalendar(calendar);
	project.addParticipation(participation);
	project.addStatus(status);
	entityManager.persist(calendar);
	entityManager.persist(employee);
	entityManager.persist(status);
	entityManager.persist(project);
	entityManager.flush();
	entityManager.clear();

	List<Project> actual = projectRepository.getActiveProjects(employee, period);

	assertTrue(actual.isEmpty());
    }

    @Test
    public void testGetActiveProjects_ByEmployeePeriod_employeeIsTeamMemberSinceFinishPeriod() {
	Date start = new Date();
	Date finish = getOffsetDate(5);
	Period period = new Period(start, finish);
	Date date = getOffsetDate(-2);
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	Employee employee = new Employee("employee");
	Participation participation = new Participation(employee, finish);
	participation.setWorkdaysCalendar(calendar);
	ProjectStatus status = createProjectStatus(date, ProjectStatus.Status.ACTIVE);
	Project expected = new Project();
	expected.setDefaultWorkdaysCalendar(calendar);
	expected.addParticipation(participation);
	expected.addStatus(status);
	entityManager.persist(calendar);
	entityManager.persist(employee);
	entityManager.persist(status);
	entityManager.persist(expected);
	entityManager.flush();
	entityManager.clear();

	List<Project> actuals = projectRepository.getActiveProjects(employee, period);

	assertEquals(1, actuals.size());
	assertEquals(expected, actuals.get(0));
    }

    @Test
    public void testGetActiveProjects_ByEmployeePeriod_projectIsNotActive() {
	Date start = new Date();
	Date finish = getOffsetDate(5);
	Period period = new Period(start, finish);
	Date date = getOffsetDate(-2);
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	Employee employee = new Employee("employee");
	Participation participation = new Participation(employee, date);
	participation.setWorkdaysCalendar(calendar);
	ProjectStatus status = createProjectStatus(date, ProjectStatus.Status.COMPLETED);
	Project project = new Project();
	project.addStatus(status);
	project.addParticipation(participation);
	project.setDefaultWorkdaysCalendar(calendar);
	entityManager.persist(calendar);
	entityManager.persist(employee);
	entityManager.persist(status);
	entityManager.persist(project);
	entityManager.flush();
	entityManager.clear();

	List<Project> actual = projectRepository.getActiveProjects(employee, period);

	assertTrue(actual.isEmpty());
    }

    @Test
    public void testGetActiveProjects_ByEmployeePeriod_projectIsNotActiveSinceStartPeriod() {
	Date start = new Date();
	Date finish = getOffsetDate(5);
	Period period = new Period(start, finish);
	Date date = getOffsetDate(-2);
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	Employee employee = new Employee("employee");
	Participation participation = new Participation(employee, date);
	participation.setWorkdaysCalendar(calendar);
	ProjectStatus status1 = createProjectStatus(date, ProjectStatus.Status.ACTIVE);
	ProjectStatus status2 = createProjectStatus(start, ProjectStatus.Status.COMPLETED);
	Project project = new Project();
	project.setDefaultWorkdaysCalendar(calendar);
	project.addParticipation(participation);
	project.addStatus(status1);
	project.addStatus(status2);
	entityManager.persist(calendar);
	entityManager.persist(employee);
	entityManager.persist(status1);
	entityManager.persist(status2);
	entityManager.persist(project);
	entityManager.flush();
	entityManager.clear();

	List<Project> actual = projectRepository.getActiveProjects(employee, period);

	assertTrue(actual.isEmpty());
    }

    @Test
    public void testGetProjects_ByConcreteStatus() {
	ProjectStatus statuse1 = createProjectStatus(new Date(), ProjectStatus.Status.ACTIVE);
	ProjectStatus statuse2 = createProjectStatus(new Date(), ProjectStatus.Status.FROZEN);
	Project unexpectedProject = new Project();
	unexpectedProject.setCode("unexpected");
	unexpectedProject.addStatus(statuse1);
	Project expectedProject = new Project();
	expectedProject.setCode("expected");
	expectedProject.addStatus(statuse2);
	entityManager.persist(statuse1);
	entityManager.persist(statuse2);
	entityManager.persist(unexpectedProject);
	entityManager.persist(expectedProject);
	List<Project> expected = Arrays.asList(expectedProject);

	ProjectsSelectionCriteria criteria = new ProjectsSelectionCriteria();
	criteria.setStatus(Status.FROZEN);

	List<Project> actual = projectRepository.getProjects(criteria);

	ListAssert.assertEquals(expected, actual);
    }

    @Test
    public void testGetProjects_byManager() {
	Employee manager1 = new Employee("manager1");
	Employee manager2 = new Employee("manager2");
	Project project1 = new Project();
	project1.setCode("project1");
	project1.setManager(manager1);
	project1.addStatus(createProjectStatus(new Date(), ProjectStatus.Status.ACTIVE));
	Project project2 = new Project();
	project2.setCode("project2");
	project2.setManager(manager2);
	project2.addStatus(createProjectStatus(new Date(), ProjectStatus.Status.ACTIVE));
	entityManager.persist(manager1);
	entityManager.persist(manager2);
	entityManager.persist(project1.getStatuses().get(0));
	entityManager.persist(project2.getStatuses().get(0));
	entityManager.persist(project1);
	entityManager.persist(project2);
	ProjectsSelectionCriteria criteria = new ProjectsSelectionCriteria();
	criteria.setManager(manager1);

	List<Project> actual = projectRepository.getProjects(criteria);

	assertEquals(1, actual.size());
    }

    @Test
    public void testGetProjects_ByStatusAny() throws InterruptedException {
	Project project = new Project();
	project.setCode("project");
	ArrayList<ProjectStatus> statuses = new ArrayList<ProjectStatus>();
	statuses.add(createProjectStatus(new Date(), ProjectStatus.Status.ACTIVE));
	project.setStatuses(statuses);
	entityManager.persist(statuses.get(0));
	entityManager.persist(project);
	entityManager.flush();
	entityManager.clear();

	ProjectsSelectionCriteria criteria = new ProjectsSelectionCriteria();
	criteria.setStatus(Status.ANY);

	List<Project> actual = projectRepository.getProjects(criteria);
	ProjectStatus.Status actualStatus = actual.get(0).getCurrentStatus();
	assertEquals(1, actual.size());
	assertEquals(ProjectStatus.Status.ACTIVE, actualStatus);
    }

    @Test
    public void testRemoveProject() {
	Project project = new Project();
	entityManager.persist(project);
	Long id = project.getId();

	projectRepository.remove(project);
	Project actual = entityManager.find(Project.class, id);
	assertNull(actual);
    }

    @Test(expected = PersistenceException.class)
    public void testRemoveProject_RemoveForbidden() {
	Project project = new Project();
	Hours hours = new Hours();
	hours.setProject(project);
	entityManager.persist(project);
	entityManager.persist(hours);

	projectRepository.remove(project);
    }

    @Test
    public void testSaveEmployees() {
	Employee manager = new Employee("manager");
	manager.setDepartment("department");
	Employee employee = new Employee("employee");
	employee.setDepartment("DEPARTMENT");
	Participation participation = new Participation(employee, new Date());
	Project project = new Project();
	project.setManager(manager);
	project.addParticipation(participation);

	projectRepository.saveEmployees(project);

	Employee actual1 = entityManager.find(Employee.class, manager.getUserName());
	Employee actual2 = entityManager.find(Employee.class, employee.getUserName());

	assertNotNull(actual1);
	assertNotNull(actual2);
	assertEquals("Department", actual1.getDepartment());
	assertEquals("Department", actual2.getDepartment());
    }

    @Test
    public void testUpdateProject() {
	Employee manager = new Employee("manager");
	Project project = new Project();
	project.setManager(manager);
	project = projectRepository.create(project);
	ProjectStatus status = createProjectStatus();
	project.getStatuses().add(status);

	project = projectRepository.update(project);

	assertNotNull(project.getStatuses().get(0).getId());
    }

    private ProjectStatus createProjectStatus(Date startAt, ProjectStatus.Status status) {
	ProjectStatus projectStatus = new ProjectStatus();
	projectStatus.setStartAt(startAt);
	projectStatus.setStatus(status);
	return projectStatus;
    }

    private Date getTwoDaysAgo() {
	Calendar calendar = Calendar.getInstance();
	calendar.add(Calendar.DATE, -2);
	return calendar.getTime();
    }

    private ProjectStatus createProjectStatus() {
	ProjectStatus projectStatus = new ProjectStatus();
	projectStatus.setStartAt(new Date(0L));
	projectStatus.setStatus(ProjectStatus.Status.ACTIVE);
	return projectStatus;
    }

	@Test
	public void testGetActiveProjectsByEmployees_filterEmployees() {
		Period period = createPeriod(-2, 2);
		Employee employee1 = new Employee("employee1");
		Employee employee2 = new Employee("employee2");
		WorkdaysCalendar calendar = new WorkdaysCalendar();
		Project project = createProject(createProjectStatus(), calendar);
		project.addTeamMembers(Arrays.asList(employee1, employee2));
		entityManager.persist(employee1);
		entityManager.persist(employee2);
		entityManager.persist(calendar);
		entityManager.persist(project.getStatuses().get(0));
		entityManager.persist(project);

		Map<Employee, List<Project>> actuals = projectRepository.getActiveProjectsByEmployees(Arrays.asList(employee2), period);

		assertEquals(Collections.singleton(employee2), actuals.keySet());
		assertEquals(Arrays.asList(project), actuals.get(employee2));
	}

	@Test
	public void testGetActiveProjectsByEmployees_noActiveProjectsInPeriod() {
		Period period = createPeriod(2, 3);
		Employee employee = new Employee("employee");
		WorkdaysCalendar calendar = new WorkdaysCalendar();
		Project project = createProject(createProjectStatus(), calendar);
		project.addTeamMembers(Arrays.asList(employee));
		project.getTeam().get(0).getPeriod().setFinish(getOffsetDate(1));
		entityManager.persist(employee);
		entityManager.persist(calendar);
		entityManager.persist(project.getStatuses().get(0));
		entityManager.persist(project);

		Map<Employee, List<Project>> actuals = projectRepository.getActiveProjectsByEmployees(Arrays.asList(employee), period);

		assertEquals(Collections.singleton(employee), actuals.keySet());
		assertTrue(actuals.get(employee).isEmpty());
	}

	@Test
	public void testGetActiveProjectsByEmployees() {
		Period period = createPeriod(-2, 2);
		WorkdaysCalendar calendar = new WorkdaysCalendar();
		Employee employee1 = new Employee("employee1");
		Employee employee2 = new Employee("employee2");
		Employee employee3 = new Employee("employee3");
		List<Employee> employees = Arrays.asList(employee1, employee2, employee3);
		ProjectStatus inactiveStatus = createProjectStatus(getOffsetDate(0), ProjectStatus.Status.FROZEN);
		Project project1 = createProject(createProjectStatus(), calendar);
		Project project2 = createProject(createProjectStatus(), calendar);
		Project project3 = createProject(createProjectStatus(), calendar);
		Project inactiveProject = createProject(inactiveStatus, calendar);
		project1.addTeamMembers(Arrays.asList(employee1));
		project2.addTeamMembers(Arrays.asList(employee2));
		project3.addTeamMembers(Arrays.asList(employee2, employee3));
		inactiveProject.addTeamMembers(employees);
		entityManager.persist(calendar);
		entityManager.persist(employee1);
		entityManager.persist(employee2);
		entityManager.persist(employee3);
		entityManager.persist(project1.getStatuses().get(0));
		entityManager.persist(project2.getStatuses().get(0));
		entityManager.persist(project3.getStatuses().get(0));
		entityManager.persist(inactiveStatus);
		entityManager.persist(project1);
		entityManager.persist(project2);
		entityManager.persist(project3);

		Map<Employee, List<Project>> actuals = projectRepository.getActiveProjectsByEmployees(employees, period);

		assertEquals(new HashSet<>(employees), actuals.keySet());
		ListAssert.assertEquals(Arrays.asList(project1), actuals.get(employee1));
		ListAssert.assertEquals(Arrays.asList(project2, project3), actuals.get(employee2));
		ListAssert.assertEquals(Arrays.asList(project3), actuals.get(employee3));
	}

	private Project createProject(ProjectStatus status, WorkdaysCalendar calendar) {
		Project project = new Project();
		project.addStatus(status);
		project.setDefaultWorkdaysCalendar(calendar);
		return project;
	}
	
	@Test
	public void testFetchComplete() throws NoSuchFieldException{
		WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
		ProjectStatus status1 = createProjectStatus(new Date(), ProjectStatus.Status.ACTIVE);
		ProjectStatus status2 = createProjectStatus(new Date(), ProjectStatus.Status.COMPLETED);
		List<ProjectStatus> expectedStatuses1 = Arrays.asList(status1);
		List<ProjectStatus> expectedStatuses2 = Arrays.asList(status2);
		
		Employee employee = new Employee("userName");
		Participation participation1 = new Participation(employee, new Date()); 
		participation1.setWorkdaysCalendar(calendar);
		Participation participation2 = new Participation(employee, new Date());
		participation2.setWorkdaysCalendar(calendar);
		List<Participation> expectedTeam1 = Arrays.asList(participation1); 
		List<Participation> expectedTeam2 = Arrays.asList(participation2);
		
		HourType hourType1 = new HourType("type1");
		HourType hourType2 = new HourType("type2");
		List<HourType> expectedAccountableHours1 = Arrays.asList(hourType1);
		List<HourType> expectedAccountableHours2 = Arrays.asList(hourType2);
		
		
		Project project1 = new Project(); project1.setCode("project1"); 
		project1.setStatuses(expectedStatuses1); project1.setTeam(expectedTeam1); project1.setAccountableHours(expectedAccountableHours1);
		
		Project project2 = new Project(); project1.setCode("project2"); 
		project2.setStatuses(expectedStatuses2); project2.setTeam(expectedTeam2); project2.setAccountableHours(expectedAccountableHours2);
		
		entityManager.persist(calendar);
		entityManager.persist(status1);
		entityManager.persist(status2);
		entityManager.persist(employee);
		entityManager.persist(hourType1);
		entityManager.persist(hourType2);
		entityManager.persist(project1);
		entityManager.persist(project2);
		
		Project project1NotLoaded = new Project(); setField(project1NotLoaded, "id", project1.getId());
		Project project2NotLoaded = new Project(); setField(project2NotLoaded, "id", project2.getId());
		
		List<Project> expected = Arrays.asList(project1, project2);
		
		List<Project> actual = projectRepository.fetchComplete(Arrays.asList(project1NotLoaded, project2NotLoaded));
		
		ListAssert.assertEquals(actual, expected);
		
		ListAssert.assertEquals(actual.get(actual.indexOf(project1)).getStatuses(), expectedStatuses1);
		ListAssert.assertEquals(actual.get(actual.indexOf(project1)).getTeam(), expectedTeam1);
		ListAssert.assertEquals(actual.get(actual.indexOf(project1)).getAccountableHours(), expectedAccountableHours1);
		
		ListAssert.assertEquals(actual.get(actual.indexOf(project2)).getStatuses(), expectedStatuses2);
		ListAssert.assertEquals(actual.get(actual.indexOf(project2)).getTeam(), expectedTeam2);
		ListAssert.assertEquals(actual.get(actual.indexOf(project2)).getAccountableHours(), expectedAccountableHours2);
	}

}
