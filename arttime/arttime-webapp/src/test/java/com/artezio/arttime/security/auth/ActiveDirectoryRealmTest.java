package com.artezio.arttime.security.auth;

import static junitx.util.PrivateAccessor.setField;
import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.createMockBuilder;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import javax.naming.NamingException;

import org.apache.shiro.authc.SimpleAccount;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.ldap.LdapContextFactory;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.easymock.EasyMockRunner;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.services.repositories.ProjectRepository;

@RunWith(EasyMockRunner.class)
public class ActiveDirectoryRealmTest {
	@Mock
	private ProjectRepository projectRepository;
	private ActiveDirectoryRealm activeDirectoryRealm;
	
	@Before
	public void setUp() {
		activeDirectoryRealm = new ActiveDirectoryRealm();		
	}
	
	@Test
	public void testIsProjectManager_ifNotExistManagedProjects() throws NoSuchFieldException {
		List<Project> projects = new ArrayList<Project>();
		setField(activeDirectoryRealm, "projectRepository", projectRepository);
		expect(projectRepository.getProjectsByManager("username")).andReturn(projects);
		replay(projectRepository);
		
		boolean actual = activeDirectoryRealm.isProjectManager("username");
		
		verify(projectRepository);
		assertFalse(actual);
	}
	
	@Test
	public void testIsProjectManager_ifExistManagedProjects() throws NoSuchFieldException {
		List<Project> projects = Arrays.asList(new Project());
		setField(activeDirectoryRealm, "projectRepository", projectRepository);
		expect(projectRepository.getProjectsByManager("username")).andReturn(projects);
		replay(projectRepository);
		
		boolean actual = activeDirectoryRealm.isProjectManager("username");
		
		verify(projectRepository);
		assertTrue(actual);
	}
	
	@Test
	public void testDoGetAuthorizationInfo_ifPrincipalsCollectionEmpty() throws NamingException {
		activeDirectoryRealm = createMockBuilder(ActiveDirectoryRealm.class)
				.addMockedMethod("queryForAuthorizationInfo")
				.createMock();
		PrincipalCollection principals = new SimplePrincipalCollection();
		AuthorizationInfo info = new SimpleAccount(principals, "credential", new HashSet<String>());		
		expect(activeDirectoryRealm.queryForAuthorizationInfo(anyObject(PrincipalCollection.class), anyObject(LdapContextFactory.class))).andReturn(info);
		replay(activeDirectoryRealm);
		
		AuthorizationInfo actual = activeDirectoryRealm.doGetAuthorizationInfo(principals);
		
		verify(activeDirectoryRealm);
		assertTrue(actual.getRoles().isEmpty());
	}
	
	@Test
	public void testDoGetAuthorizationInfo_ifPrincipalsCollectionNotEmpty() throws NamingException, NoSuchFieldException {
		activeDirectoryRealm = createMockBuilder(ActiveDirectoryRealm.class)
				.addMockedMethod("queryForAuthorizationInfo")
				.createMock();
		setField(activeDirectoryRealm, "projectRepository", projectRepository);
		List<Project> projects = new ArrayList<Project>();
		PrincipalCollection principals = new SimplePrincipalCollection("principal", "ldapRealm");
		AuthorizationInfo info = new SimpleAccount(principals, "credential", new HashSet<String>());
		expect(activeDirectoryRealm.queryForAuthorizationInfo(anyObject(PrincipalCollection.class), anyObject(LdapContextFactory.class))).andReturn(info);		
		expect(projectRepository.getProjectsByManager("principal")).andReturn(projects);
		replay(projectRepository, activeDirectoryRealm);
		
		AuthorizationInfo actual = activeDirectoryRealm.doGetAuthorizationInfo(principals);
		
		verify(activeDirectoryRealm, projectRepository);
		assertTrue(actual.getRoles().contains(UserRoles.EMPLOYEE_ROLE));
	}
	
	@Test
	public void testDoGetAuthorizationInfo_ifExistManagedProjects() throws NamingException, NoSuchFieldException {
		activeDirectoryRealm = createMockBuilder(ActiveDirectoryRealm.class)
				.addMockedMethod("queryForAuthorizationInfo")
				.createMock();
		setField(activeDirectoryRealm, "projectRepository", projectRepository);
		List<Project> projects = Arrays.asList(new Project());
		PrincipalCollection principals = new SimplePrincipalCollection("principal", "ldapRealm");
		AuthorizationInfo info = new SimpleAccount(principals, "credential", new HashSet<String>());
		expect(activeDirectoryRealm.queryForAuthorizationInfo(anyObject(PrincipalCollection.class), anyObject(LdapContextFactory.class))).andReturn(info);		
		expect(projectRepository.getProjectsByManager("principal")).andReturn(projects);
		replay(projectRepository, activeDirectoryRealm);
		
		AuthorizationInfo actual = activeDirectoryRealm.doGetAuthorizationInfo(principals);
		
		verify(activeDirectoryRealm, projectRepository);
		assertTrue(actual.getRoles().contains(UserRoles.EMPLOYEE_ROLE));
		assertTrue(actual.getRoles().contains(UserRoles.PM_ROLE));
	}
}
