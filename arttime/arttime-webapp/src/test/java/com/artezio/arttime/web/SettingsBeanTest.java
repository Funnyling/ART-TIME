package com.artezio.arttime.web;

import static junitx.util.PrivateAccessor.setField;
import static org.easymock.EasyMock.createMock;

import org.junit.Before;

import com.artezio.arttime.config.Settings;
import com.artezio.arttime.services.SettingsService;
import com.artezio.arttime.services.synchronization.Scheduler;

public class SettingsBeanTest {
    private SettingsBean settingsBean;
    private SettingsService settingsService;
    private Scheduler scheduler;
    private Settings settings;

    @Before
    public void setUp() throws NoSuchFieldException {
	settingsBean = new SettingsBean();
	settingsService = createMock(SettingsService.class);
	scheduler = createMock(Scheduler.class);
	settings = createMock(Settings.class);
	setField(settingsBean, "settingsService", settingsService);
	setField(settingsBean, "scheduler", scheduler);
	setField(settingsBean, "settings", settings);
    }    

}
