package com.artezio.arttime.services.synchronization;

import static junitx.util.PrivateAccessor.setField;

import java.util.Arrays;
import java.util.logging.Logger;

import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

import com.artezio.arttime.config.Settings;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.datamodel.TeamFilter.FilterType;
import com.artezio.arttime.services.EmployeeSynchronizerLocal;
import com.artezio.arttime.services.SettingsService;
import com.artezio.arttime.services.TeamSynchronizer;
import com.artezio.arttime.services.repositories.ProjectRepository;

public class SchedulerTest {

    private Scheduler scheduler;
    private IMocksControl mockControl;
    private EmployeeSynchronizerLocal employeeSynchronizer;
    private TeamSynchronizer teamSynchronizer;
    private SettingsService settingsService;
    private Settings settings;
    private ProjectRepository projectRepository;

    @Before
    public void setUp() throws NoSuchFieldException {
		scheduler = new Scheduler();
		settings = new Settings();
		mockControl = EasyMock.createControl();
		employeeSynchronizer = mockControl.createMock(EmployeeSynchronizerLocal.class);
		teamSynchronizer = mockControl.createMock(TeamSynchronizer.class);
		projectRepository = mockControl.createMock(ProjectRepository.class);
		settingsService = mockControl.createMock(SettingsService.class);
		setField(scheduler, "employeeSynchronizer", employeeSynchronizer);
		setField(scheduler, "teamSynchronizer", teamSynchronizer);
		setField(scheduler, "projectRepository", projectRepository);
		setField(scheduler, "settingsService", settingsService);
		setField(scheduler, "settings", settings);
    }

    @Test
    public void testTrySynchronizeEmployees() {
		settings.setEmployeesSynchronizationEnabled(true);
		employeeSynchronizer.synchronizeEmployees();
		mockControl.replay();
	
		scheduler.trySynchronizeEmployees();
	
		mockControl.verify();
    }

    @Test
    public void testTrySynchronizeEmployees_ImportDisabled() {
		settings.setEmployeesSynchronizationEnabled(false);
		mockControl.replay();
	
		scheduler.trySynchronizeEmployees();
	
		mockControl.verify();
    }

    @Test
    public void testTrySynchronizeTeam() throws NoSuchFieldException {
		settings.setTeamSynchronizationEnabled(true);
		Project project = new Project();
		project.getTeamFilter().setFilterType(FilterType.PROJECT_CODES);
		project.getTeamFilter().setValue("project_code");
		setField(project, "id", 1L);
		EasyMock.expect(projectRepository.getAll()).andReturn(Arrays.asList(project));
		teamSynchronizer.importTeam(project);
		mockControl.replay();
	
		scheduler.trySynchronizeTeam();
	
		mockControl.verify();
    }

    @Test
    public void testTrySynchronizeTeam_ifInProject_ImportDisabled() throws NoSuchFieldException {
		settings.setTeamSynchronizationEnabled(true);
		Project project = new Project();
		project.getTeamFilter().setFilterType(FilterType.DISABLED);
		setField(project, "id", 1L);
		EasyMock.expect(projectRepository.getAll()).andReturn(Arrays.asList(project));
		mockControl.replay();
	
		scheduler.trySynchronizeTeam();
	
		mockControl.verify();
    }

    @Test
    public void testTrySynchronizeTeam_ImportDisabled() {
		settings.setTeamSynchronizationEnabled(false);
		mockControl.replay();
	
		scheduler.trySynchronizeTeam();
	
		mockControl.verify();
    }

    @Test
    public void testStartSynchronization() throws NoSuchFieldException {
		settings.setEmployeesSynchronizationEnabled(true);
		settings.setTeamSynchronizationEnabled(true);
		mockControl.checkOrder(true);
		Project project = new Project();
		project.getTeamFilter().setFilterType(FilterType.PROJECT_CODES);
		project.getTeamFilter().setValue("project_code");
		setField(project, "id", 1L);
		EasyMock.expect(settingsService.getSettings()).andReturn(settings);
		EasyMock.expect(projectRepository.getAll()).andReturn(Arrays.asList(project));
		teamSynchronizer.importTeam(project);
		employeeSynchronizer.synchronizeEmployees();
		mockControl.replay();
	
		scheduler.startSynchronization();
	
		mockControl.verify();
    }
}













