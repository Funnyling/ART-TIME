package com.artezio.arttime.web.criteria;

import com.artezio.arttime.datamodel.Period;
import com.ibm.icu.text.SimpleDateFormat;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.util.Date;

import static com.artezio.arttime.test.utils.CalendarUtils.getOffsetDate;
import static org.junit.Assert.*;

public class RangePeriodSelectorTest {

    @Test
    public void testIsValidDuration_LessThanYear() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date start = sdf.parse("01-01-2014");
        Date finish = sdf.parse("31-12-2014");
        Period period = new Period(start, finish);
        RangePeriodSelector periodSelector = new RangePeriodSelector(period);

        assertTrue(periodSelector.isValidPeriodLength());
    }

    @Test
    public void testIsValidDuration_OneYear() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date start = sdf.parse("01-01-2014");
        Date finish = sdf.parse("01-01-2015");
        Period period = new Period(start, finish);
        RangePeriodSelector periodSelector = new RangePeriodSelector(period);

        assertTrue(periodSelector.isValidPeriodLength());
    }

    @Test
    public void testIsValidDuration_OverThanYear() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date start = sdf.parse("01-01-2014");
        Date finish = sdf.parse("02-01-2015");
        Period period = new Period(start, finish);
        RangePeriodSelector periodSelector = new RangePeriodSelector(period);

        assertFalse(periodSelector.isValidPeriodLength());
    }

    @Test
    public void testIsValidDuration_NullDates() {
        Period period1 = new Period(getOffsetDate(-1), null);
        Period period2 = new Period(null, getOffsetDate(1));
        Period period3 = new Period(null, null);
        RangePeriodSelector periodSelector1 = new RangePeriodSelector(period1);
        RangePeriodSelector periodSelector2 = new RangePeriodSelector(period2);
        RangePeriodSelector periodSelector3 = new RangePeriodSelector(period3);

        assertTrue(periodSelector1.isValidPeriodLength());
        assertTrue(periodSelector2.isValidPeriodLength());
        assertTrue(periodSelector3.isValidPeriodLength());
    }

}