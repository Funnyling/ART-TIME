package com.artezio.arttime.datamodel;

import org.junit.Test;

import static junit.framework.Assert.*;

import java.math.BigDecimal;

public class EmployeeTest {

	@SuppressWarnings("deprecation")
	@Test
	public void testEqualsWithSameUserName() {
		Employee employee = new Employee("user");
		Employee sameEmppl = new Employee("user");
		assertTrue(employee.equals(sameEmppl));
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testEqualsWithDiffrentUserName() {
		Employee employee = new Employee("human");
		Employee sameEmppl = new Employee("programmer");
		assertFalse(employee.equals(sameEmppl));
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testGetWorkLoadHours() {
		Employee employee = new Employee();
		employee.setWorkLoad(15);
		BigDecimal expected = new BigDecimal("1.2");
		BigDecimal actual = employee.getWorkLoadHours();
		assertTrue(expected.compareTo(actual) == 0);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testGetWorkLoadHours_ByNullWorkLoad() {
		Employee employee = new Employee();
		BigDecimal actual = employee.getWorkLoadHours();
		assertNotNull(actual);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testGetWorkLoadHours_ByZeroWorkLoad() {
		Employee employee = new Employee();
		employee.setWorkLoad(0);
		BigDecimal expected = BigDecimal.ZERO;
		BigDecimal actual = employee.getWorkLoadHours();
		assertTrue(expected.compareTo(actual) == 0);
	}

}
