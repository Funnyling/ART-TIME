package com.artezio.arttime.web;

import static junitx.util.PrivateAccessor.getField;
import static junitx.util.PrivateAccessor.setField;
import static org.easymock.EasyMock.createMockBuilder;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.easymock.EasyMockRunner;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.artezio.arttime.datamodel.Hours;
import com.artezio.arttime.services.HoursService;
import com.artezio.arttime.services.ProjectsSelectionCriteria;
import com.artezio.arttime.services.repositories.HoursRepository;
import com.artezio.arttime.services.repositories.ProjectRepository;
import com.artezio.arttime.web.spread_sheet.SpreadSheet;

@RunWith(EasyMockRunner.class)
public class ManageEffortsBeanTest {
    private ManageEffortsBean bean;
    @Mock
    private FilterBean filterBean;
    @Mock
    private SpreadSheet spreadSheet;
    @Mock
    private ProjectsSelectionCriteria projectsSelectionCriteria;
    @Mock
    private HoursRepository hoursRepository;
    @Mock
    private HoursService hoursService;
    @Mock
    private ProjectRepository projectRepository;

    @Before
    public void setUp() {
	bean = new ManageEffortsBean();
    }

    @Test
    public void testGetSpreadSheet_ifNotNull() throws Exception {
	setField(bean, "spreadSheet", spreadSheet);

	SpreadSheet actual = bean.getSpreadSheet();

	assertSame(spreadSheet, actual);
    }

    @Ignore
    @Test
    public void testSaveHours() throws ReflectiveOperationException {
    	bean = createMockBuilder(ManageEffortsBean.class).addMockedMethod("resetComponentsTree").createMock();
		setField(bean, "spreadSheet", spreadSheet);
		setField(bean, "hoursService", hoursService);		
		Set<Hours> hours = new HashSet<Hours>();
		expect(spreadSheet.getUpdatedHours()).andReturn(hours);
		hoursService.saveManagedHours(hours);
		bean.resetComponentsTree();
		replay(bean, spreadSheet, hoursService);
	
		bean.saveHours();
	
		verify(bean, spreadSheet, hoursService);
    }

    @Test
    public void testResetData() throws NoSuchFieldException {
	bean = createMockBuilder(ManageEffortsBean.class).addMockedMethod("resetComponentsTree").createMock();
	setField(bean, "spreadSheet", spreadSheet);
	bean.resetComponentsTree();
	replay(bean);

	bean.resetData();

	verify(bean);
	assertNull(getField(bean, "spreadSheet"));
    }

    @Test
    public void testApproveSelectedHours() throws NoSuchFieldException {
	Hours hours1 = new Hours();
	Hours hours2 = new Hours();
	List<Hours> hours = Arrays.asList(hours1, hours2);
	setField(bean, "spreadSheet", spreadSheet);
	expect(spreadSheet.getSelectedHours()).andReturn(hours);
	spreadSheet.updateSelectedRows();
	replay(spreadSheet);

	bean.approveSelectedHours();

	verify(spreadSheet);
	assertTrue(hours.get(0).isApproved());
	assertTrue(hours.get(1).isApproved());
    }

    @Test
    public void testApproveAllHours() throws NoSuchFieldException {
	Hours hours1 = new Hours();
	Hours hours2 = new Hours();
	List<Hours> hours = Arrays.asList(hours1, hours2);
	expect(spreadSheet.getHours()).andReturn(hours);
	spreadSheet.updateAllRows();
	setField(bean, "spreadSheet", spreadSheet);	
	replay(spreadSheet);

	bean.approveAllHours();

	verify(spreadSheet);
	assertTrue(hours.get(0).isApproved());
	assertTrue(hours.get(1).isApproved());
    }

    @Test
    public void testDisapproveSelectedHours() throws NoSuchFieldException {
	Hours hours1 = new Hours();
	hours1.setApproved(true);
	Hours hours2 = new Hours();
	hours2.setApproved(true);
	List<Hours> hours = Arrays.asList(hours1, hours2);
	setField(bean, "spreadSheet", spreadSheet);
	expect(spreadSheet.getSelectedHours()).andReturn(hours);
	spreadSheet.updateSelectedRows();
	replay(spreadSheet);

	bean.disapproveSelectedHours();

	verify(spreadSheet);
	assertFalse(hours.get(0).isApproved());
	assertFalse(hours.get(1).isApproved());
    }

    @Test
    public void testDisapproveAllHours() throws NoSuchFieldException {
	Hours hours1 = new Hours();
	hours1.setApproved(true);
	Hours hours2 = new Hours();
	hours2.setApproved(true);
	List<Hours> hours = Arrays.asList(hours1, hours2);
	expect(spreadSheet.getHours()).andReturn(hours);
	spreadSheet.updateAllRows();
	setField(bean, "spreadSheet", spreadSheet);
	replay(spreadSheet);

	bean.disapproveAllHours();

	verify(spreadSheet);
	assertFalse(hours.get(0).isApproved());
	assertFalse(hours.get(1).isApproved());
    }    
}
