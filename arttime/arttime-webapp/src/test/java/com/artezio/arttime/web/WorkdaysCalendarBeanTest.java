package com.artezio.arttime.web;

import static junitx.util.PrivateAccessor.setField;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.artezio.arttime.datamodel.Day;
import com.artezio.arttime.datamodel.Period;
import com.artezio.arttime.datamodel.WorkdaysCalendar;
import com.artezio.arttime.exceptions.WorkdaysCalendarRemoveException;
import com.artezio.arttime.services.repositories.WorkdaysCalendarRepository;

public class WorkdaysCalendarBeanTest {
    private WorkdaysCalendarBean bean;
    private WorkdaysCalendarRepository calendarRepository;

    @Before
    public void setUp() throws NoSuchFieldException {
		bean = new WorkdaysCalendarBean();
		calendarRepository = createMock(WorkdaysCalendarRepository.class);
		setField(bean, "calendarRepository", calendarRepository);
    }

    @Test
    public void testRemove() throws WorkdaysCalendarRemoveException {
		WorkdaysCalendar workdaysCalendar = new WorkdaysCalendar();
		calendarRepository.remove(workdaysCalendar);
		replay(calendarRepository);
	
		bean.remove(workdaysCalendar);
	
		verify(calendarRepository);
    }

    @Test
    public void testCreate() throws NoSuchFieldException {
		WorkdaysCalendar workdaysCalendar = new WorkdaysCalendar();
		setField(bean, "workdaysCalendar", workdaysCalendar);
		expect(calendarRepository.create(workdaysCalendar)).andReturn(workdaysCalendar);
		replay(calendarRepository);
	
		bean.create();
	
		verify(calendarRepository);
    }

    @Test
    public void testUpdateWorkdaysCalendar() throws NoSuchFieldException {
		WorkdaysCalendar workdaysCalendar = new WorkdaysCalendar();
		List<Day> days = new ArrayList<Day>();
		setField(bean, "workdaysCalendar", workdaysCalendar);
		calendarRepository.update(workdaysCalendar, days);
		replay(calendarRepository);
	
		bean.updateWorkdaysCalendar(days);
	
		verify(calendarRepository);
    }
    
    @Test
    public void testUpdateDaysOff() throws NoSuchFieldException, ParseException{
    	DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
    	Period extendedPeriod = new Period(df.parse("01-12-2014"),df.parse("28-02-2015"));
    	setField(bean,"extendedPeriod",extendedPeriod);
    	Day day1 = new Day();
    	day1.setDate(df.parse("01-01-2015"));
    	day1.setWorking(false);
    	Day day2 = new Day();
    	day2.setDate(df.parse("02-01-2015"));
    	day2.setWorking(true);
    	Map<Date,Day> days = new HashMap<Date, Day>();
    	days.put(day1.getDate(), day1);
    	days.put(day2.getDate(), day2);
    	setField(bean, "days", days);
    	String daysOff = null;
    	setField(bean, "daysOff", daysOff);
    	bean.updateDaysOff();
    	assertEquals("1-1-2015,", bean.getDaysOff());
    }
    
    @Test
    public void testPopulateDays() throws NoSuchFieldException, ParseException {
    	Map<Date, Day> days = new HashMap<Date, Day>();
    	setField(bean, "days", days);
    	DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
    	Period extendedPeriod = new Period(df.parse("01-12-2014"),df.parse("28-02-2015"));
    	setField(bean,"extendedPeriod",extendedPeriod);
    	WorkdaysCalendar workdaysCalendar = new WorkdaysCalendar();		
		setField(bean, "workdaysCalendar", workdaysCalendar);
		List<Day> result = new ArrayList<Day>();
		for(Date date: extendedPeriod.getDays()){			
			result.add(new Day(date, workdaysCalendar));
		}
		expect(calendarRepository.getDays(workdaysCalendar, extendedPeriod)).andReturn(result);
		replay(calendarRepository);
		
		bean.populateDays();
		
		verify(calendarRepository);		
		assertArrayEquals(result.toArray(), days.values().toArray());		
    }
}
