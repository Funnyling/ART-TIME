package com.artezio.arttime.web;

import static junitx.util.PrivateAccessor.setField;
import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.primefaces.context.RequestContext;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.filter.Filter;
import com.artezio.arttime.filter.FilterRepository;
import com.artezio.arttime.filter.FilterService;
import com.artezio.arttime.services.ProjectsSelectionCriteria;

@RunWith(PowerMockRunner.class)
@PrepareForTest({RequestContext.class})
public class FilterBeanTest {

    private FilterBean filterBean;
    private FilterService filterService;
    private FilterRepository filterRepository;
    private Employee loggedEmployee;

    @Before
    public void setUp() throws Exception {
		filterBean = new FilterBean();
		filterService = createMock(FilterService.class);
		filterRepository = createMock(FilterRepository.class);
		loggedEmployee = createMock(Employee.class);
		setField(filterBean, "filterService", filterService);
		setField(filterBean, "loggedEmployee", loggedEmployee);
		setField(filterBean, "filterRepository", filterRepository);
    }
    
    @Test
    public final void testGetCurrentFilter() throws NoSuchFieldException {
		Filter expected = createMock(Filter.class);
		setField(filterBean, "currentFilter", expected);
	
		Filter actual = filterBean.getCurrentFilter();
	
		assertSame(expected, actual);

    }
    
    @Test
    public final void testGetCurrentFilter_ifNull() throws NoSuchFieldException {		
		setField(filterBean, "currentFilter", null);
		Filter expected = new Filter();
		expect(filterService.getActiveProjectsFilter(loggedEmployee)).andReturn(expected);
		expect(filterRepository.fetchDetails(expected)).andReturn(expected);
		replay(filterService, filterRepository);
	
		Filter actual = filterBean.getCurrentFilter();
	
		verify(filterService, filterRepository);
		assertSame(expected, actual);

    }
      
    @Test
    public final void testSetCurrentFilter() {
    	Filter expected = new Filter();
    	expected.setName("filterName");
    	expect(filterRepository.fetchDetails(expected)).andReturn(expected);
		replay(filterRepository);

    	filterBean.setCurrentFilter(expected);

    	verify(filterRepository);
    	assertSame(expected, filterBean.getCurrentFilter());
    }
    
    @Test
    public final void testSetCurrentFilter_ifCurrentFilterNotNull() throws NoSuchFieldException {
    	Filter currentFilter = new Filter();
    	setField(filterBean, "currentFilter", currentFilter);
    	Filter expected = new Filter();
    	expect(filterRepository.fetchDetails(expected)).andReturn(expected);
		replay(filterRepository);

    	filterBean.setCurrentFilter(expected);

    	verify(filterRepository);
    	assertSame(expected, filterBean.getCurrentFilter());
    	assertSame(currentFilter.getRangePeriodSelector(), expected.getRangePeriodSelector());
    }
    
    
    @Test
    public final void testGetAvailableProjects() {
		List<Project> expected = createMock(List.class);
		expect(filterService.getAvailableProjects(anyObject(ProjectsSelectionCriteria.class))).andReturn(expected);
		replay(filterService);
	
		List<Project> actual = filterBean.getAvailableProjects();
	
		verify(filterService);
		assertThat(actual, is(expected));
    }
    
}
