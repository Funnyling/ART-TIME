package com.artezio.arttime.services;

import static junitx.util.PrivateAccessor.setField;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertSame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import com.artezio.arttime.datamodel.HourType;
import com.artezio.arttime.datamodel.Hours;
import com.artezio.arttime.datamodel.Period;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.filter.Filter;
import com.artezio.arttime.services.integration.IntegrationFacade;
import com.artezio.arttime.services.repositories.HourTypeRepository;
import com.artezio.arttime.services.repositories.HoursRepository;
import com.artezio.arttime.services.repositories.ProjectRepository;
import com.artezio.arttime.web.criteria.RangePeriodSelector;

public class IntegrationFacadeTest {

    private IntegrationFacade facade;
    private ProjectRepository projectRepository;
    private HourTypeRepository hourTypeRepository;
    private HoursRepository hoursRepository;

    @Before
    public void setUp() throws Exception {
	projectRepository = EasyMock.createMock(ProjectRepository.class);
	hourTypeRepository = EasyMock.createMock(HourTypeRepository.class);
	hoursRepository = EasyMock.createMock(HoursRepository.class);
	facade = new IntegrationFacade();
	setField(facade, "projectRepository", projectRepository);
	setField(facade, "hourTypeRepository", hourTypeRepository);
	setField(facade, "hoursRepository", hoursRepository);
    }

    @Test
    public void testGetProjects() {
	List<Project> expecteds = new ArrayList<Project>();
	expect(projectRepository.getAll()).andReturn(expecteds);
	expect(projectRepository.fetchComplete(expecteds)).andReturn(expecteds);
	replay(projectRepository);

	List<Project> actuals = facade.getProjects();

	verify(projectRepository);
	assertSame(expecteds, actuals);
    }

    @Test
    public void testGetHourTypes() {
	List<HourType> expecteds = new ArrayList<HourType>();
	expect(hourTypeRepository.getAll()).andReturn(expecteds);
	replay(hourTypeRepository);

	List<HourType> actuals = facade.getHourTypes();

	verify(hourTypeRepository);
	assertSame(expecteds, actuals);
    }

    @Test
    public void testGetHours_approvedOnly() {
	List<Hours> expecteds = new ArrayList<Hours>();
	Project project = new Project();
	project.setCode("TEST-PROJECT");
	Date from = new GregorianCalendar(2011, 1, 5).getTime();
	Date to = new GregorianCalendar(2011, 1, 15).getTime();
	Period period = new Period(from, to);
	Filter expectedFilter = new Filter();
	expectedFilter.setProjects(Arrays.asList(project));
	expectedFilter.setApproved(Boolean.TRUE);
	RangePeriodSelector rangePeriodSelector = new RangePeriodSelector(period);
	expectedFilter.setRangePeriodSelector(rangePeriodSelector);

	expect(projectRepository.findProject("TEST-PROJECT")).andReturn(project);
	expect(hoursRepository.getHours(eq(expectedFilter))).andReturn(expecteds);
	replay(projectRepository, hoursRepository);

	List<Hours> actuals = facade.getHours("TEST-PROJECT",
		new com.artezio.arttime.services.integration.HoursSearchCriteria(from, to, true));

	verify(projectRepository, hoursRepository);
	assertSame(expecteds, actuals);
    }

    @Test
    public void testGetHours() {
	List<Hours> expecteds = new ArrayList<Hours>();
	Project project = new Project();
	project.setCode("TEST-PROJECT");
	Date from = new GregorianCalendar(2011, 1, 5).getTime();
	Date to = new GregorianCalendar(2011, 1, 15).getTime();
	Period period = new Period(from, to);
	Filter expectedFilter = new Filter();
	expectedFilter.setProjects(Arrays.asList(project));
	expectedFilter.setApproved(null);
	RangePeriodSelector rangePeriodSelector = new RangePeriodSelector(period);
	expectedFilter.setRangePeriodSelector(rangePeriodSelector);

	expect(projectRepository.findProject("TEST-PROJECT")).andReturn(project);
	expect(hoursRepository.getHours(eq(expectedFilter))).andReturn(expecteds);
	replay(projectRepository, hoursRepository);

	List<Hours> actuals = facade.getHours("TEST-PROJECT",
		new com.artezio.arttime.services.integration.HoursSearchCriteria(from, to, false));

	verify(projectRepository, hoursRepository);
	assertSame(expecteds, actuals);
    }
}
