package com.artezio.arttime.datamodel;

import static org.junit.Assert.*;
import static junitx.util.PrivateAccessor.setField;

import java.util.Date;
import org.junit.Test;

import com.artezio.arttime.datamodel.ProjectStatus.Status;

public class ProjectStatusTest {

	@Test
	public void testEquals_WithDifferentIds() throws NoSuchFieldException {
		Date startDate = new Date();
		ProjectStatus status1 = new ProjectStatus(Status.ACTIVE, startDate);
		ProjectStatus status2 = new ProjectStatus(Status.ACTIVE, startDate);
		setField(status1, "id", 1L);
		setField(status2, "id", 2L);
		
		assertTrue(status1.equals(status2));
	}

}
