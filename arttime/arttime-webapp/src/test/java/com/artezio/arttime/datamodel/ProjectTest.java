package com.artezio.arttime.datamodel;

import static com.artezio.arttime.test.utils.CalendarUtils.createPeriod;
import static com.artezio.arttime.test.utils.CalendarUtils.getOffsetDate;
import static junitx.util.PrivateAccessor.setField;
import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import junitx.framework.ListAssert;

import org.junit.Test;

import com.artezio.arttime.datamodel.ProjectStatus.Status;
import com.artezio.arttime.datamodel.TeamFilter.FilterType;
import com.artezio.arttime.test.utils.CalendarUtils;

public class ProjectTest{
	
	@Test
	public void testGetDefaultCurrentStatus(){
		Project project = new Project();
		project.getStatuses().clear();
		assertEquals(project.getCurrentStatus(), null);			
	}
	
	private ProjectStatus createStatus(ProjectStatus.Status status, Date startAt){
		ProjectStatus projectStatus = new ProjectStatus();
		projectStatus.setStatus(status);
		projectStatus.setStartAt(startAt);
		return projectStatus;
	}	
	
	@Test
	public void testGetCurrentStatus() {		
		Project project = new Project();
		ArrayList<ProjectStatus> statuses = new ArrayList<ProjectStatus>();
		statuses.add(createStatus(ProjectStatus.Status.FROZEN, getTwoDaysBefore()));		
		statuses.add(createStatus(ProjectStatus.Status.ACTIVE, getYesterday()));
		statuses.add(createStatus(ProjectStatus.Status.COMPLETED, getTomorrow()));								
		project.setStatuses(statuses);
		
		ProjectStatus.Status actual = project.getCurrentStatus(); 
		assertEquals(actual, ProjectStatus.Status.ACTIVE);
		
	}

	private Date getTomorrow() {
		return getOffsetDate(1);
	}

	private Date getYesterday() {
		return CalendarUtils.getOffsetDate(-1);
	}

	private Date getTwoDaysBefore() {
		return CalendarUtils.getOffsetDate(-2);
	}

	@Test
	public void testAddStatus(){
		Project project = new Project();
		assertEquals(1, project.getStatuses().size());
		
		ProjectStatus status = new ProjectStatus(Status.ACTIVE);
		project.addStatus(status);
		
		assertEquals(1, project.getStatuses().size());
		assertTrue(project.getStatuses().contains(status));
	}
	
	@Test
	public void testAddStatus_ifFirstStatusNotNull(){
		Project project = new Project();
		project.setFirstProjectStatus(new ProjectStatus(Status.ACTIVE));
		
		ProjectStatus status = new ProjectStatus(Status.ACTIVE);
		project.addStatus(status);
		
		assertEquals(2, project.getStatuses().size());
		assertTrue(project.getStatuses().contains(status));
	}
	
	@Test
	public void testIsTeamMember(){
		Project project = new Project();
		Employee employee = new Employee("not team member");
		Employee teamMember = new Employee("team member");
		Participation participation = new Participation();
		participation.setEmployee(teamMember);
		List<Participation> team = Arrays.asList(participation);
		project.setTeam(team);				
		
		
		assertTrue(project.isTeamMember(teamMember));
		assertFalse(project.isTeamMember(employee));
	}

    @Test
    public void testAddAccountableHours(){
    	Project project = new Project();
    	assertEquals(0, project.getAccountableHours().size());
    	HourType expected = new HourType("type");
    	project.addAccountableHours(expected);
    	
    	assertTrue(project.getAccountableHours().contains(expected));
    }
    
    @Test
    public void testIsTeamMemberInPeriod_periodAfterParticipation(){
    	Employee employee = new Employee();
    	Participation participation = new Participation(employee, new Date());
    	Date tomorrow = getOffsetDate(1);
    	participation.getPeriod().setFinish(tomorrow);
    	Project project = new Project();
    	project.addParticipation(participation);
    	Period period = new Period(tomorrow, getOffsetDate(2));

    	boolean actual = project.isTeamMember(employee, period);
    	assertTrue(actual);
    }
    
    @Test
    public void testIsTeamMemberInPeriod_periodBeforeParticipation(){
    	Employee employee = new Employee();
    	Participation participation = new Participation(employee, new Date());
    	participation.getPeriod().setFinish(getOffsetDate(1));
    	Project project = new Project();
    	project.addParticipation(participation);
    	Period period = new Period(getOffsetDate(-2), getOffsetDate(-1));

    	boolean actual = project.isTeamMember(employee, period);
    	assertFalse(actual);
    }
    
    @Test
    public void testIsTeamMemberIn_participationNotEnded(){
    	Employee employee = new Employee();
    	Participation participation = new Participation(employee, new Date());
    	Project project = new Project();
    	project.addParticipation(participation);
    	Period period = new Period(getOffsetDate(-2), getOffsetDate(1));

    	boolean actual = project.isTeamMember(employee, period);
    	assertTrue(actual);
    }
    
    
    @Test
    public void testIsActiveInPeriod_WhenNotActive() {
    	Project project = new Project();
    	project.addStatus(new ProjectStatus(Status.ACTIVE, getOffsetDate(-10)));
    	project.addStatus(new ProjectStatus(Status.FROZEN, getOffsetDate(-5)));
    	Period period = new Period(getOffsetDate(-3), getOffsetDate(2));
    	
    	assertFalse(project.isActiveIn(period));
    }
    
    @Test
    public void testIsActiveInPeriod() {
    	Project project = new Project();
    	project.addStatus(new ProjectStatus(Status.ACTIVE, getOffsetDate(-10)));
    	project.addStatus(new ProjectStatus(Status.FROZEN, getOffsetDate(-3)));
    	Period period = new Period(getOffsetDate(-5), getOffsetDate(2));
    	
    	assertTrue(project.isActiveIn(period));
    }
    
    @Test
    public void testIsActiveInPeriod_OnOpenedActivePeriod() {
    	Project project = new Project();
    	project.addStatus(new ProjectStatus(Status.FROZEN, getOffsetDate(-10)));
    	project.addStatus(new ProjectStatus(Status.ACTIVE, getOffsetDate(-3)));
    	Period period = new Period(getOffsetDate(-5), getOffsetDate(2));
    	
    	assertTrue(project.isActiveIn(period));
    }

    @Test
    public void testGetTeamMembersByPeriod_ifEmployeeParticipate() {
    	Employee expected = new Employee("userName");
    	Participation participation = new Participation(expected, getOffsetDate(-5));
    	Project project = new Project();
    	project.addParticipation(participation);
    	
    	Set<Employee> actual = project.getTeamMembers(createPeriod(2, 3));
    	
    	assertEquals(1, actual.size());
    	assertTrue(actual.contains(expected));
    }
    
    @Test
    public void testGetTeamMembersByPeriod_ifEmployeeNotParticipate() {
    	Employee expected = new Employee("userName");
    	Participation participation = new Participation(expected, getOffsetDate(-5));
    	participation.getPeriod().setFinish(getOffsetDate(0));
    	Project project = new Project();
    	project.addParticipation(participation);
    	
    	Set<Employee> actual = project.getTeamMembers(createPeriod(2, 3));
    	
    	assertTrue(actual.isEmpty());
    	assertFalse(actual.contains(expected));
    }
    
    @Test
    public void testGetTeamMembers() {
    	Project project = new Project();
    	Employee expected1 = new Employee(); expected1.setUserName("user1");
    	Employee expected2 = new Employee(); expected2.setUserName("user2");
    	project.getTeam().add(new Participation(expected1, getTwoDaysBefore()));
    	project.getTeam().add(new Participation(expected1, getYesterday()));
    	project.getTeam().add(new Participation(expected2, getYesterday()));
    	
    	List<Employee> actuals = project.getTeamMembers();
    	
    	assertEquals(2, actuals.size());
    	assertTrue(actuals.contains(expected1));
    	assertTrue(actuals.contains(expected2));
    }

    @Test
    public void testGetActualParticipationsByEmployeeAndPeriod_IfNotActual() {
        Employee employee = new Employee("employee");
        Participation unexpected1 = new Participation(employee, getOffsetDate(-5));
        unexpected1.getPeriod().setFinish(getOffsetDate(-2));
        Participation unexpected2 = new Participation(employee, getOffsetDate(2));
        Project project = new Project();
        project.getTeam().add(unexpected1);
        project.getTeam().add(unexpected2);
        Period searchPeriod = new Period(getOffsetDate(-1), getOffsetDate(1));

        List<Participation> actuals = project.getActualParticipations(employee, searchPeriod);

        assertTrue(actuals.isEmpty());
    }

    @Test
    public void testGetActualParticipationsByEmployeeAndPeriod_IfAnotherEmployee() {
        Employee participatedEmployee = new Employee("participated");
        Employee requestedEmployee = new Employee("requested");
        Participation unexpected = new Participation(participatedEmployee, getOffsetDate(-5));
        Project project = new Project();
        project.getTeam().add(unexpected);
        Period searchPeriod = new Period(getOffsetDate(-1), null);

        List<Participation> actuals = project.getActualParticipations(requestedEmployee, searchPeriod);

        assertTrue(actuals.isEmpty());
    }

    @Test
    public void testGetActualParticipationsByEmployeeAndPeriod() {
        Employee employee = new Employee("employee");
        Participation expected1 = new Participation(employee, getOffsetDate(-1));
        Participation expected2 = new Participation(employee, getOffsetDate(1));
        Project project = new Project();
        project.getTeam().add(expected1);
        project.getTeam().add(expected2);
        Period searchPeriod = new Period(getOffsetDate(0), null);

        List<Participation> actuals = project.getActualParticipations(employee, searchPeriod);

        assertEquals(2, actuals.size());
        assertTrue(actuals.contains(expected1));
        assertTrue(actuals.contains(expected2));
    }
    
    @Test
    public void testIsTeamMember_ByEmployeeAndDate(){
    	Project project = new Project();
    	Employee expectedEmployee = new Employee("expected employee");
    	Date expectedDate = new Date();
    	Participation participation = new Participation(expectedEmployee, expectedDate);
    	project.addParticipation(participation);
    	
    	boolean actual = project.isTeamMember(expectedEmployee, expectedDate);
    	
    	assertTrue(actual);
    }
    
    @Test
    public void testIsTeamMember_ByEmployeeAndDate_UnexpectedEmployee(){
    	Project project = new Project();
    	Employee expectedEmployee = new Employee("expected employee");
    	Employee unexpectedEmployee = new Employee("unexpected employee");
    	Date expectedDate = new Date();
    	Participation participation = new Participation(unexpectedEmployee, expectedDate);
    	project.addParticipation(participation);
    	
    	boolean actual = project.isTeamMember(expectedEmployee, expectedDate);
    	
    	assertFalse(actual);
    }
    
    @Test
    public void testIsTeamMember_ByEmployeeAndDate_UnexpectedDate(){
    	Project project = new Project();
    	Employee expectedEmployee = new Employee("expected employee");    	
    	Date expectedDate = new Date();
    	Date unexpectedDate = getOffsetDate(-2);
    	Participation participation = new Participation(expectedEmployee, expectedDate);
    	project.addParticipation(participation);
    	
    	boolean actual = project.isTeamMember(expectedEmployee, unexpectedDate);
    	
    	assertFalse(actual);
    }
    
    @Test
    public void testIsTeamMember_ByEmployeeAndDate_EndPartisipationIsNull(){
    	Project project = new Project();
    	Employee expectedEmployee = new Employee("expected employee");    	    	    	
    	Participation participation = new Participation(expectedEmployee, new Date());
    	participation.getPeriod().setFinish(null);
    	project.addParticipation(participation);
    	
    	boolean actual = project.isTeamMember(expectedEmployee, getOffsetDate(10));
    	
    	assertTrue(actual);
    }
    
    @Test
    public void testIsTeamMember_ByEmployeeAndDate_EndPartisipationIsNotNull(){
    	Project project = new Project();
    	Employee expectedEmployee = new Employee("expected employee");  
    	Date endParticipation = getOffsetDate(10);
    	Participation participation = new Participation(expectedEmployee, new Date());
    	participation.getPeriod().setFinish(endParticipation);
    	project.addParticipation(participation);
    	
    	boolean actual = project.isTeamMember(expectedEmployee, getOffsetDate(5));
    	
    	assertTrue(actual);
    }
    
    @Test
    public void testIsTeamMember_ByEmployeeAndDate_DataEqualsEndPartisipation(){
    	Project project = new Project();
    	Employee expectedEmployee = new Employee("expected employee");  
    	Date endParticipation = getOffsetDate(10);
    	Participation participation = new Participation(expectedEmployee, new Date());
    	participation.getPeriod().setFinish(endParticipation);
    	project.addParticipation(participation);
    	
    	boolean actual = project.isTeamMember(expectedEmployee, endParticipation);
    	
    	assertTrue(actual);
    }
    
    @Test
    public void testGetActualParticipation_byDate(){
    	Project project = new Project();    	
    	Participation participation1 = new Participation();
    	participation1.getPeriod().setStart(getYesterday());
    	participation1.getPeriod().setFinish(getTomorrow());
    	Participation participation2 = new Participation();
    	participation2.getPeriod().setStart(getTwoDaysBefore());
    	project.addParticipation(participation1);
    	project.addParticipation(participation2);
    	Date today = new Date();
    	
    	List<Participation> actual = project.getActualParticipation(today);
    	
    	assertEquals(2, actual.size());
    	assertTrue(actual.contains(participation1));
    	assertTrue(actual.contains(participation2));
    }
    
    @Test
    public void testGetActualParticipation_byDate_participationBeforeTheDate(){
    	Project project = new Project();    	
    	Participation participation = new Participation();
    	participation.getPeriod().setStart(getTwoDaysBefore());
    	participation.getPeriod().setFinish(getYesterday());    	
    	project.addParticipation(participation);    	
    	Date today = new Date();
    	
    	List<Participation> actual = project.getActualParticipation(today);
    	
    	assertTrue(actual.isEmpty());    	    
    }
    
    @Test
    public void testGetActualParticipation_byDate_dateEqualsStartParticipation(){
    	Project project = new Project();    	
    	Date date = getTwoDaysBefore();
    	Participation participation = new Participation();    	
    	participation.getPeriod().setStart(date);    	    
    	project.addParticipation(participation);    	    	
    	
    	List<Participation> actual = project.getActualParticipation(date);
    	
    	assertEquals(1, actual.size());
    	assertTrue(actual.contains(participation));       	
    }
    
    @Test
    public void testGetActualParticipation_byDate_dateEqualsEndParticipation(){
    	Project project = new Project();    	
    	Participation expected = new Participation();    	
    	expected.getPeriod().setStart(getTwoDaysBefore());
    	expected.getPeriod().setFinish(getYesterday());
    	project.addParticipation(expected);    	    	
    	
    	List<Participation> actuals = project.getActualParticipation(getYesterday());
    	    	
    	assertEquals(1, actuals.size());
    	assertEquals(expected, actuals.get(0));
    }
    
    @Test
    public void testIsTeamSynchronizationEnabled_ProjectTeamFilter_Native() {
    	Project project = new Project();
    	project.getTeamFilter().setFilterType(FilterType.NATIVE);
    	project.getTeamFilter().setValue("native filter");
    	
    	boolean actual = project.isTeamSynchronizationEnabled();
    	
    	assertTrue(actual);
    }
    
    @Test
    public void testIsTeamSynchronizationEnabled_ProjectTeamFilter_ProjectCode() {
    	Project project = new Project();
    	project.getTeamFilter().setFilterType(FilterType.PROJECT_CODES);
    	project.getTeamFilter().setValue("project_code");
    	
    	boolean actual = project.isTeamSynchronizationEnabled();
    	
    	assertTrue(actual);
    }
    
    @Test
    public void testIsTeamSynchronizationEnabled_ProjectTeamFilter_Disabled() {    	
    	Project project = new Project();
    	project.getTeamFilter().setFilterType(FilterType.DISABLED);
    	
    	boolean actual = project.isTeamSynchronizationEnabled();
    	
    	assertFalse(actual);
    }
    
    @Test
    public void testValidation_ifNotValid() {
    	 ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
         Validator validator = factory.getValidator();
         
         WorkdaysCalendar workdaysCalendar = new WorkdaysCalendar("calendar");
         Employee employee = new Employee("employee", "employee_name", "employee_lastName", "employee_email");
         Project project = new Project();
         project.setCode("project code");
         project.setManager(employee);
         project.setTeamFilter(new TeamFilter());
         project.addAccountableHours(new HourType());
         project.addStatus(new ProjectStatus(Status.ACTIVE, new Date()));
         project.setDefaultWorkdaysCalendar(workdaysCalendar);
        
         Set<ConstraintViolation<Project>> actual = validator.validate(project); 
         
         assertEquals(1, actual.size());
         assertEquals("Not valid team filter", actual.iterator().next().getMessage());
    }
    
    @Test
    public void testValidation_ifValid() {
    	ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
         
        Employee employee = new Employee("employee", "employee_name", "employee_lastName", "employee_email");
        WorkdaysCalendar workdaysCalendar = new WorkdaysCalendar("calendar");
        Project project = new Project();
        project.setCode("project code");
        project.setManager(employee);
        project.setTeamFilter(new TeamFilter(FilterType.DISABLED, null));
        project.addAccountableHours(new HourType());
        project.addStatus(new ProjectStatus(Status.ACTIVE, new Date()));
        project.setDefaultWorkdaysCalendar(workdaysCalendar);
        Participation participation = new Participation(employee, new Date());
     	participation.setWorkdaysCalendar(workdaysCalendar);
     	project.addParticipation(participation);
        
        Set<ConstraintViolation<Project>> actual = validator.validate(project); 
         
        assertTrue(actual.isEmpty());
    }
    
    @Test
    public void testGetActualTeamMembers() throws ParseException {
    	Project project = new Project();
    	Employee employee1 = new Employee("employee1");
    	Employee employee2 = new Employee("employee2");
    	Employee employee3 = new Employee("employee3");
    	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    	Participation participation1 = new Participation(employee1, sdf.parse("1-01-2013"));
    	Participation participation2 = new Participation(employee2, sdf.parse("1-03-2013"));
    	participation2.getPeriod().setFinish(sdf.parse("1-05-2013"));
    	Participation participation3 = new Participation(employee3, sdf.parse("1-04-2013"));
    	project.addParticipation(participation1);
    	project.addParticipation(participation2);
    	project.addParticipation(participation3);
    	List<Employee> expected = Arrays.asList(employee1, employee2);
    	
    	List<Employee> actual = project.getActualTeamMembers(sdf.parse("10-03-2013"));
    	
    	ListAssert.assertEquals(expected, actual);
    }
    
    @Test
    public void testCloseParticipations() throws ParseException {
    	Project project = new Project();
    	Employee employee1 = new Employee("employee1");
    	Employee employee2 = new Employee("employee2");
    	Employee employee3 = new Employee("employee3");
    	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    	Participation participation1 = new Participation(employee1, sdf.parse("1-01-2013"));
    	Participation participation2 = new Participation(employee2, sdf.parse("1-03-2013"));
    	Participation participation3 = new Participation(employee3, sdf.parse("1-04-2013"));
    	project.addParticipation(participation1);
    	project.addParticipation(participation2);
    	project.addParticipation(participation3);
    	List<Employee> employees = Arrays.asList(employee1, employee2);
    	
    	project.closeParticipations(employees);
    	
    	Date date = new Date();
    	assertEquals(sdf.format(date), sdf.format(participation1.getPeriod().getFinish()));
    	assertEquals(sdf.format(date), sdf.format(participation2.getPeriod().getFinish()));
    	assertNull(participation3.getPeriod().getFinish());
    }
    
    @Test
    public void testAddTeamMembers() throws ParseException {
    	Project project = new Project();
    	WorkdaysCalendar calendar = new WorkdaysCalendar();
    	project.setDefaultWorkdaysCalendar(calendar);
    	Employee employee1 = new Employee("employee1");
    	Employee employee2 = new Employee("employee2");
    	Employee employee3 = new Employee("employee3");
    	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    	Participation participation1 = new Participation(employee1, sdf.parse("1-01-2013"));    	
    	project.addParticipation(participation1);    	
    	List<Employee> employees = Arrays.asList(employee2, employee3);
    	
    	project.addTeamMembers(employees);
    	
    	Date date = new Date();
    	assertEquals(3, project.getTeam().size());
    	assertSame(participation1, project.getTeam().get(0));    
    	assertEquals(employee2, project.getTeam().get(1).getEmployee());
    	assertEquals(employee3, project.getTeam().get(2).getEmployee());
    	assertEquals(sdf.format(date), sdf.format(project.getTeam().get(1).getPeriod().getStart()));
    	assertEquals(sdf.format(date), sdf.format(project.getTeam().get(2).getPeriod().getStart()));
    	assertSame(calendar, project.getTeam().get(1).getWorkdaysCalendar());
    	assertSame(calendar, project.getTeam().get(2).getWorkdaysCalendar());
    	assertNull(project.getTeam().get(1).getPeriod().getFinish());
    	assertNull(project.getTeam().get(2).getPeriod().getFinish());
    	
    }
    
	@Test
	public void testGetParticipationPeriods() throws NoSuchFieldException, ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Employee employee1 = new Employee("employee1");		 
		Employee employee2 = new Employee("employee2");
		Employee employee3 = new Employee("employee3");		
		Period period = new Period(sdf.parse("1-03-2014"), sdf.parse("15-03-2014"));
		Project project = new Project();
		project.addParticipation(createParticipation(employee1, sdf.parse("1-01-2013"), sdf.parse("15-04-2013")));
		project.addParticipation(createParticipation(employee2, sdf.parse("1-01-2014"), null));
		project.addParticipation(createParticipation(employee3, sdf.parse("1-01-2014"), sdf.parse("1-03-2014")));
		project.addParticipation(createParticipation(employee3, sdf.parse("8-03-2014"), sdf.parse("1-04-2014")));
		Map<Employee, List<Period>> expected = new HashMap<Employee, List<Period>>();
		expected.put(employee2, Arrays.asList(new Period(sdf.parse("1-03-2014"), sdf.parse("15-03-2014"))));
		expected.put(employee3, Arrays.asList(new Period(sdf.parse("1-03-2014"), sdf.parse("1-03-2014")), 
				new Period(sdf.parse("8-03-2014"), sdf.parse("15-03-2014"))));
		
		
		Map<Employee, List<Period>> actual = project.getParticipationPeriods(period);
		
		assertEquals(expected, actual);		 
	 }

	private Participation createParticipation(Employee employee, Date from, Date till) {
		Participation participation = new Participation();
		participation.setEmployee(employee);
		participation.getPeriod().setStart(from);
		participation.getPeriod().setFinish(till);
		return participation;
	}

	@Test
	public void testIsTeamMember_ByEmployeeAndDateAndCalendar_CheckCalendar() throws NoSuchFieldException {
		Project project = new Project();
		Employee employee = new Employee("employee");
		Date date = getOffsetDate(0);
		Participation participation = createParticipation(employee, date, null);
		WorkdaysCalendar expectedCalendar = new WorkdaysCalendar();
		setField(expectedCalendar, "id", 1L);
		WorkdaysCalendar unexpectedCalendar = new WorkdaysCalendar();
		setField(unexpectedCalendar, "id", 2L);
		participation.setWorkdaysCalendar(expectedCalendar);
		project.addParticipation(participation);

		assertTrue(project.isTeamMember(employee, date, expectedCalendar));
		assertFalse(project.isTeamMember(employee, date, unexpectedCalendar));
	}

	@Test
	public void testIsTeamMember_ByEmployeeAndDateAndCalendar_CheckDate() {
		Project project = new Project();
		Employee employee = new Employee("employee");
		Date start = getOffsetDate(0);
		Date finish = getOffsetDate(2);
		WorkdaysCalendar calendar = new WorkdaysCalendar();
		Participation participation = createParticipation(employee, start, finish);
		participation.setWorkdaysCalendar(calendar);
		project.addParticipation(participation);

		assertFalse(project.isTeamMember(employee, getOffsetDate(-1), calendar));
		assertTrue(project.isTeamMember(employee, getOffsetDate(0), calendar));
		assertTrue(project.isTeamMember(employee, getOffsetDate(1), calendar));
		assertTrue(project.isTeamMember(employee, getOffsetDate(2), calendar));
		assertFalse(project.isTeamMember(employee, getOffsetDate(3), calendar));
	}

	@Test
	public void testIsTeamMember_ByEmployeeAndDateAndCalendar_CheckEmployee() {
		Project project = new Project();
		Employee expectedEmployee = new Employee("employee1");
		Employee unexpectedEmployee = new Employee("employee2");
		Date start = getOffsetDate(0);
		Participation participation = createParticipation(expectedEmployee, start, null);
		WorkdaysCalendar calendar = new WorkdaysCalendar();
		participation.setWorkdaysCalendar(calendar);
		project.addParticipation(participation);

		assertTrue(project.isTeamMember(expectedEmployee, start, calendar));
		assertFalse(project.isTeamMember(unexpectedEmployee, start, calendar));
	}
}


