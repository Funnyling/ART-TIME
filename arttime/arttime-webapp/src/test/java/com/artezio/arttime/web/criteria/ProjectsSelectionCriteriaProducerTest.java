package com.artezio.arttime.web.criteria;

import java.lang.annotation.Annotation;

import javax.enterprise.inject.spi.Annotated;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.faces.context.ExternalContext;

import org.easymock.EasyMockRunner;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.qualifiers.ManagedProjectsCriteria;
import com.artezio.arttime.services.ProjectsSelectionCriteria;
import com.artezio.arttime.services.ProjectsSelectionCriteria.Status;

import static junitx.util.PrivateAccessor.setField;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.*;

@RunWith(EasyMockRunner.class)
public class ProjectsSelectionCriteriaProducerTest {
	private ProjectsSelectionCriteriaProducer producer;
	@Mock	
	private InjectionPoint ip;
	@Mock	
	private Annotated annotated;
	@Mock
	private ExternalContext externalContext;
	private Employee loggedEmployee;
	
	@Before
	public void setUp() throws NoSuchFieldException {
		producer = new ProjectsSelectionCriteriaProducer();
		loggedEmployee = new Employee();
		setField(producer, "loggedEmployee", loggedEmployee);
	}
	
	@Test
	public void testGetCriteria_ifPM() throws NoSuchFieldException {			
		ManagedProjectsCriteria criteria = new ManagedProjectsCriteria() {			
			@Override
			public Class<? extends Annotation> annotationType() {
				return null;
			}
			@Override
			public Status status() {				
				return Status.ACTIVE;
			}
			@Override
			public boolean onlyOwnProjects() {
				return false;
			}
		};
		setField(producer, "externalContext", externalContext);
		expect(ip.getAnnotated()).andReturn(annotated).times(2);
		expect(externalContext.isUserInRole("exec")).andReturn(false);
		expect(annotated.getAnnotation(ManagedProjectsCriteria.class)).andReturn(criteria).times(2);
		replay(ip, annotated, externalContext);
		
		ProjectsSelectionCriteria actual = producer.getProjectSelectionCriteria(ip);
		
		verify(ip, annotated, externalContext);
		assertEquals(Status.ACTIVE, actual.getStatus());
		assertSame(loggedEmployee, actual.getManager());
	}
	
	@Test
	public void testGetCriteria_ifExec() throws NoSuchFieldException {			
		ManagedProjectsCriteria criteria = new ManagedProjectsCriteria() {			
			@Override
			public Class<? extends Annotation> annotationType() {
				return null;
			}
			@Override
			public Status status() {				
				return Status.ANY;
			}
			@Override
			public boolean onlyOwnProjects() {
				return false;
			}
		};
		setField(producer, "externalContext", externalContext);
		expect(ip.getAnnotated()).andReturn(annotated).times(2);
		expect(externalContext.isUserInRole("exec")).andReturn(true);
		expect(annotated.getAnnotation(ManagedProjectsCriteria.class)).andReturn(criteria).times(2);
		replay(ip, annotated, externalContext);
		
		ProjectsSelectionCriteria actual = producer.getProjectSelectionCriteria(ip);
		
		verify(ip, annotated, externalContext);
		assertEquals(Status.ANY, actual.getStatus());
		assertNull(actual.getManager());
	}
	
	@Test
	public void testGetCriteria_ifOnlyOwnProjects() throws NoSuchFieldException {			
		ManagedProjectsCriteria criteria = new ManagedProjectsCriteria() {			
			@Override
			public Class<? extends Annotation> annotationType() {
				return null;
			}
			@Override
			public Status status() {				
				return Status.ANY;
			}
			@Override
			public boolean onlyOwnProjects() {
				return true;
			}
		};
		setField(producer, "externalContext", externalContext);
		expect(ip.getAnnotated()).andReturn(annotated).times(2);
		expect(annotated.getAnnotation(ManagedProjectsCriteria.class)).andReturn(criteria).times(2);
		replay(ip, annotated, externalContext);
		
		ProjectsSelectionCriteria actual = producer.getProjectSelectionCriteria(ip);
		
		verify(ip, annotated, externalContext);
		assertEquals(Status.ANY, actual.getStatus());
		assertSame(loggedEmployee, actual.getManager());
	}
	
}
