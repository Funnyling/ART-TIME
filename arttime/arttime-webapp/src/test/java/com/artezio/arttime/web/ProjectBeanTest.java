package com.artezio.arttime.web;

import static junitx.util.PrivateAccessor.getField;
import static junitx.util.PrivateAccessor.setField;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.artezio.arttime.datamodel.Participation;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.services.repositories.ProjectRepository;

public class ProjectBeanTest {
    private ProjectBean bean;
    private ProjectRepository projectRepository;

    @Before
    public void setUp() throws NoSuchFieldException {
	bean = new ProjectBean();
	projectRepository = createMock(ProjectRepository.class);
	setField(bean, "projectRepository", projectRepository);
    }

    @Test
    public void testAddNew() throws NoSuchFieldException {
	setField(bean, "project", null);

	bean.addNew();

	Project actual = (Project) getField(bean, "project");
	assertNotNull(actual);
    }

    @Test
    public void testCreate() throws NoSuchFieldException {
	Date startFrom = new Date();
	setField(bean, "startFrom", startFrom);
	Project project = new Project();
	expect(projectRepository.create(project)).andReturn(project);
	replay(projectRepository);

	bean.create(project);

	verify(projectRepository);
    }

    @Test
    public void testUpdate() {
	Project project = new Project();
	expect(projectRepository.update(project)).andReturn(project);
	replay(projectRepository);

	bean.update(project);

	verify(projectRepository);
    }

    @Test
    public void testRemove() {
	Project project = new Project();
	projectRepository.remove(project);
	replay(projectRepository);

	bean.remove(project);

	verify(projectRepository);
    }

    @Test
    public void testAddNewParticipation() throws NoSuchFieldException {
	Project project = new Project();
	Participation participation = new Participation();
	setField(bean, "project", project);
	setField(bean, "participation", participation);

	bean.addNewParticipation();

	assertFalse(project.getTeam().isEmpty());
	assertNotSame(participation, bean.getParticipation());
    }
}
