package com.artezio.arttime.web.spread_sheet;

import static junitx.util.PrivateAccessor.setField;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.easymock.EasyMockRunner;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.HourType;
import com.artezio.arttime.datamodel.Hours;
import com.artezio.arttime.datamodel.Participation;
import com.artezio.arttime.datamodel.Period;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.filter.Filter;
import com.artezio.arttime.services.repositories.HoursRepository;
import com.artezio.arttime.web.criteria.RangePeriodSelector;

@RunWith(EasyMockRunner.class)
public class ProjectEffortsSpreadSheetTest {
	private ProjectEffortsSpreadSheet spreadSheet;
	private static SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	private Filter filter;
	@Mock 
	private HoursRepository hoursRepository;		
	
	@Before
	public void setUp() throws ParseException, NoSuchFieldException {
		Date date1 = sdf.parse("1-01-2014");
		Date date2 = sdf.parse("2-01-2014");
		Period period = new Period(date1, date2);		
		HourType hourType1 = new HourType("regular time");
		setField(hourType1, "id", 1L);
		hourType1.setActualTime(true);
		HourType hourType2 = new HourType("over time");
		setField(hourType1, "id", 2L);
		List<HourType> hourTypes = Arrays.asList(hourType1, hourType2);
		Employee employee1 = new Employee("iivanov", "ivan", "ivanov", "ivanov@mail.ru");
		employee1.setDepartment("Minsk");
		Employee employee2 = new Employee("ipetrov", "ivan", "petrov", "petrov@mail.ru");
		employee2.setDepartment("Minsk");
		List<Employee> employees = Arrays.asList(employee1, employee2);
		Project project1 = createProject("project1", Arrays.asList(hourType1, hourType2), Arrays.asList(employee1), date1);
		setField(project1, "id", 1L);
		Project project2 = createProject("project2", Arrays.asList(hourType1), Arrays.asList(employee1, employee2), date1);
		setField(project2, "id", 2L);
		List<Project> projects = Arrays.asList(project1, project2);
		filter = new Filter();
		filter.setEmployees(employees);
		filter.setProjects(projects);
		filter.setHourTypes(hourTypes);
		filter.setDepartments(Arrays.asList("Minsk"));
		filter.setRangePeriodSelector(new RangePeriodSelector(period));								
	}
	
	private List<Hours> getHours() {
		List<Hours> hours = new ArrayList<Hours>();		
		for (Project project : filter.getProjects()) {
			for (Participation participation : project.getTeam()) {
				Employee employee = participation.getEmployee();
				for (HourType hourType : project.getAccountableHours()) {
					for (Date date : filter.getRangePeriodSelector().getPeriod().getDays()) {
						Hours hour = new Hours(project, date, employee, hourType);
						hours.add(hour);
					}
				}
			}
		}
		return hours;
	}

	private Project createProject(String code, List<HourType> hourTypes, List<Employee> employees, Date from) {
		Project project = new Project();
		project.setAccountableHours(hourTypes);
		project.setCode(code);
		for (Employee employee : employees) {
			Participation participation = new Participation(employee, from);
			project.addParticipation(participation);
		}
		return project;
	}

	@Test
	public void testBuildSkeleton() throws ParseException, NoSuchFieldException {		
		Employee employee1 = filter.getEmployees().get(0);
		Employee employee2 = filter.getEmployees().get(1);
		Project project1 = filter.getProjects().get(0);
		Project project2 = filter.getProjects().get(1);
		HourType hourType1 = filter.getHourTypes().get(0);
		HourType hourType2 = filter.getHourTypes().get(1);
		spreadSheet = new ProjectEffortsSpreadSheet(hoursRepository, filter);
		List<Hours> hours = getHours();
		expect(hoursRepository.getHours(filter)).andReturn(hours);
		replay(hoursRepository);
		
		spreadSheet.buildSkeleton();
		
		List<SpreadSheetRow<?>> actual = spreadSheet.getRows();
		
		verify(hoursRepository);
		SpreadSheetRow<?> expected1 = new HeadSpreadSheetRow(project1);		
		SpreadSheetRow<?> expected2 = createSpreadSheatRow(project1, employee1, hourType1, findHours(hours, project1, employee1, hourType1));
		SpreadSheetRow<?> expected3 = createSpreadSheatRow(project1, employee1, hourType2, findHours(hours, project1, employee1, hourType2));				
		SpreadSheetRow<?> expected4 = createSpreadSheatRow(project1, hourType1, Arrays.asList((HoursSpreadSheetRow)expected2));
		SpreadSheetRow<?> expected5 = createSpreadSheatRow(project1, hourType2, Arrays.asList((HoursSpreadSheetRow)expected3));
		SpreadSheetRow<?> expected6 = new HeadSpreadSheetRow(project2);
		SpreadSheetRow<?> expected7 = createSpreadSheatRow(project2, employee1, hourType1, findHours(hours, project2, employee1, hourType1));
		SpreadSheetRow<?> expected8 = createSpreadSheatRow(project2, employee2, hourType1, findHours(hours, project2, employee2, hourType1));
		SpreadSheetRow<?> expected9 = createSpreadSheatRow(project2, hourType1, Arrays.asList((HoursSpreadSheetRow)expected7, (HoursSpreadSheetRow)expected8));		
		
		assertEquals(9, actual.size());
		assertEquals(expected1, actual.get(0));
		assertEquals(expected2, actual.get(1));		
		assertEquals(expected3, actual.get(2));
		assertEquals(expected4, actual.get(3));
		assertEquals(expected5, actual.get(4));
		assertEquals(expected6, actual.get(5));
		assertEquals(expected7, actual.get(6));
		assertEquals(expected8, actual.get(7));
		assertEquals(expected9, actual.get(8));
	}

	private List<Hours> findHours(List<Hours> hours, Project project, Employee employee, HourType hourType) {
		List<Hours> result = new ArrayList<Hours>();
		for (Hours hour : hours) {
			if (project.equals(hour.getProject())) {
				if (employee.equals(hour.getEmployee())) {
					if (hourType.equals(hour.getType())) {
						result.add(hour);
					}
				}
			}
		}
		return result;
	}

	private SpreadSheetRow<?> createSpreadSheatRow(Project project, HourType hourType, List<HoursSpreadSheetRow> hoursRows) {		
		return new TotalsSpreadSheetRow(project, hourType, hoursRows);				
	}
	
	private SpreadSheetRow<?> createSpreadSheatRow(Project project, Employee employee, HourType hourType, List<Hours> hours) {		
		return new HoursSpreadSheetRow(project, employee, hourType, hours);				
	}


}
