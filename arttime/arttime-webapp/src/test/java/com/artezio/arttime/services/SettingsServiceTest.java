package com.artezio.arttime.services;

import static junitx.util.PrivateAccessor.setField;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.artezio.arttime.config.Settings;

public class SettingsServiceTest {

    private EntityManager entityManager;
    private EntityManagerFactory entityManagerFactory;
    private SettingsService settingsService;

    @Before
    public void setUp() throws Exception {
	settingsService = new SettingsService();
	Map<String, String> properties = new HashMap<String, String>();
	properties.put("javax.persistence.validation.mode", "none");
	entityManagerFactory = Persistence.createEntityManagerFactory("test", properties);
	entityManager = entityManagerFactory.createEntityManager();
	setField(settingsService, "entityManager", entityManager);
	entityManager.getTransaction().begin();
    }

    @After
    public void tearDown() throws Exception {
	if (entityManager.getTransaction().isActive()) {
	    if (entityManager.getTransaction().getRollbackOnly()) {
		entityManager.getTransaction().rollback();
	    } else {
		entityManager.getTransaction().commit();
	    }
	    entityManagerFactory.close();
	}
    }

    @Test
    public void testGetSettings() {
	Settings settings = new Settings();
	entityManager.persist(settings);

	Settings actual = settingsService.getSettings();

	assertNotNull(actual);
	assertEquals(actual, settings);
    }

    @Test
    public void testUpdateSettings() {
	Settings settings = new Settings();
	settings.setLdapBindCredentials("credentials");
	entityManager.persist(settings);
	Long id = settings.getId();
	String newLdapBindCredentials = "new credentials";
	settings.setLdapBindCredentials(newLdapBindCredentials);

	settingsService.update(settings);
	entityManager.flush();

	Settings actual = entityManager.find(Settings.class, id);
	assertEquals(newLdapBindCredentials, actual.getLdapBindCredentials());
    }

}
