package com.artezio.arttime.web;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.richfaces.component.SortOrder;

public class ProjectsBeanTest {
	private ProjectsBean bean;
	
	@Before
	public void setUp() throws NoSuchFieldException {
		bean = new ProjectsBean();
	}		
	
	@Test
	public void testSortByName() {
		bean.setProjectCodeOrder(SortOrder.ascending);
		bean.setManagerOrder(SortOrder.descending);
		
		bean.sortByCode();
		
		assertEquals(bean.getProjectCodeOrder(), SortOrder.descending);
		assertEquals(bean.getManagerOrder(), SortOrder.unsorted);
	}
	
	@Test
	public void testSortByManager() {
		bean.setProjectCodeOrder(SortOrder.ascending);
		bean.setManagerOrder(SortOrder.descending);
		
		bean.sortByManager();
		
		assertEquals(bean.getProjectCodeOrder(), SortOrder.unsorted);
		assertEquals(bean.getManagerOrder(), SortOrder.ascending);
	}
}
