package com.artezio.arttime.report;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class ReportBuilderTest {
	private static final String LINE_SEPARATOR = "line.separator";
	private static final String TEST_REPORT = "reports/testReport.rptdesign";
	private static final String UTF_8 = "UTF-8";
	
	private ReportBuilder reportBuilder;

	@Before
	public void setUp() {
		reportBuilder = new ReportBuilder();
	}

	@Test
	public void testGetReport_PDF() throws IOException {
		String reportFile = getReportFromFile(TEST_REPORT);
		Map<String, Object> params = new HashMap<String, Object>();
		byte[] expected = reportBuilder.getReport(reportFile, ReportUtils.REPORT_TYPE_PDF, params, null);
		assertTrue(expected.length>0);
	}
	
	@Test
	public void testGetReport_EXCEL() throws IOException {
		String reportFile = getReportFromFile(TEST_REPORT);
		Map<String, Object> params = new HashMap<String, Object>();
		byte[] expected = reportBuilder.getReport(reportFile, ReportUtils.REPORT_TYPE_EXCEL, params, null);
		assertTrue(expected.length>0);
	}
	
	private String getReportFromFile(String fileName) throws IOException {
		InputStream is = this.getClass().getClassLoader().getResourceAsStream(fileName);
		BufferedReader reader = new BufferedReader(new InputStreamReader(is, UTF_8));
		String line = null;
		StringBuilder stringBuilder = new StringBuilder();
		String separator = System.getProperty(LINE_SEPARATOR);
		while ((line=reader.readLine()) != null){
			stringBuilder.append(line);
			stringBuilder.append(separator);
		}
		return stringBuilder.toString();
	}

}
