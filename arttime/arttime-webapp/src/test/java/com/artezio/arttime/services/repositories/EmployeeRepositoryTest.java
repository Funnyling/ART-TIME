package com.artezio.arttime.services.repositories;

import static junitx.util.PrivateAccessor.setField;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.artezio.arttime.datamodel.Employee;

public class EmployeeRepositoryTest {
    private EntityManager entityManager;
    private EntityManagerFactory entityManagerFactory;
    private EmployeeRepository employeeRepository;

    @Before
    public void setUp() throws Exception {
	employeeRepository = new EmployeeRepository();
	Map<String, String> properties = new HashMap<String, String>();
	properties.put("javax.persistence.validation.mode", "none");
	entityManagerFactory = Persistence.createEntityManagerFactory("test", properties);
	entityManager = entityManagerFactory.createEntityManager();
	setField(employeeRepository, "entityManager", entityManager);
	entityManager.getTransaction().begin();
    }

    @After
    public void tearDown() throws Exception {
	if (entityManager.getTransaction().isActive()) {
	    if (entityManager.getTransaction().getRollbackOnly()) {
		entityManager.getTransaction().rollback();
	    } else {
		entityManager.getTransaction().commit();
	    }
	    entityManagerFactory.close();
	}
    }

    @Test
    public void testFind_ByUsername() {
	Employee expectedEmployee = new Employee("expected employee");
	Employee unexpectedEmployee = new Employee("unexpected employee");
	entityManager.persist(expectedEmployee);
	entityManager.persist(unexpectedEmployee);
	
	Employee actual = employeeRepository.find("expected employee");

	assertNotNull(actual);
	assertEquals(expectedEmployee, actual);
    }

    @Test
    public void testFind_ByUsername_EmployeeNotExist() {
	Employee unexpectedEmployee = new Employee("unexpected employee");
	entityManager.persist(unexpectedEmployee);

	Employee actual = employeeRepository.find("expected employee");

	assertNull(actual);
    }

}
