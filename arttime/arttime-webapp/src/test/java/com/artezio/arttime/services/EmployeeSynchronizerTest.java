package com.artezio.arttime.services;

import static junitx.util.PrivateAccessor.setField;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.services.integration.EmployeeService;
import com.artezio.arttime.services.repositories.EmployeeRepository;

public class EmployeeSynchronizerTest {
    private EmployeeService employeeService;
    private EmployeeSynchronizer employeeSynchronizer;
    private Logger log;
    private EmployeeRepository employeeRepository;

    @Before
    public void setUp() throws NoSuchFieldException {
	employeeSynchronizer = new EmployeeSynchronizer();
	employeeService = createMock(EmployeeService.class);
	log = createMock(Logger.class);
	employeeRepository = createMock(EmployeeRepository.class);

	setField(employeeSynchronizer, "employeeService", employeeService);
	setField(employeeSynchronizer, "log", log);
	setField(employeeSynchronizer, "employeeRepository", employeeRepository);
    }

    @Test
    public void testSynchronizeEmployees() throws Exception {
	String userName = "ssmirnoff";
	Employee externalEmployee = new Employee(userName, "Sergei", "Smirnoff", "ssmirnoff@domain.com");
	Employee internalEmployee = new Employee(userName, "Oldfirst", "Oldlast", "Oldmail");
	List<Employee> internalEmployeeList = Arrays.asList(internalEmployee);

	expect(employeeRepository.getAll()).andReturn(internalEmployeeList);
	expect(employeeService.findEmployee(userName)).andReturn(externalEmployee);
	expect(employeeRepository.update(externalEmployee)).andReturn(externalEmployee);
	replay(employeeRepository, employeeService);

	employeeSynchronizer.synchronizeEmployees();

	verify(employeeRepository, employeeService);
    }

    @Test
    public void testSynchronizeEmployees_externalEmployeeNotFound() throws Exception {
	String userName = "ssmirnoff";
	Employee externalEmployee = null;
	Employee internalEmployee = new Employee(userName, "Oldfirst", "Oldlast", "Oldmail");
	List<Employee> internalEmployeeList = Arrays.asList(internalEmployee);

	expect(employeeRepository.getAll()).andReturn(internalEmployeeList);
	expect(employeeService.findEmployee(userName)).andReturn(externalEmployee);
	replay(employeeRepository, employeeService);

	employeeSynchronizer.synchronizeEmployees();

	verify(employeeRepository, employeeService);
    }

}
