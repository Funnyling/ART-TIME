package com.artezio.arttime.services.repositories;

import static com.artezio.arttime.test.utils.CalendarUtils.createPeriod;
import static com.artezio.arttime.test.utils.CalendarUtils.getOffsetDate;
import static com.artezio.arttime.test.utils.CalendarUtils.resetTime;
import static junitx.util.PrivateAccessor.setField;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.HourType;
import com.artezio.arttime.datamodel.Hours;
import com.artezio.arttime.datamodel.Participation;
import com.artezio.arttime.datamodel.Period;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.datamodel.ProjectStatus;
import com.artezio.arttime.datamodel.WorkdaysCalendar;
import com.artezio.arttime.filter.Filter;
import com.artezio.arttime.web.criteria.RangePeriodSelector;

public class HoursRepositoryTest {
    private EntityManager entityManager;
    private EntityManagerFactory entityManagerFactory;
    private HoursRepository hoursRepository;

    @Before
    public void setUp() throws Exception {
	hoursRepository = new HoursRepository();
	Map<String, String> properties = new HashMap<String, String>();
	properties.put("javax.persistence.validation.mode", "none");
	entityManagerFactory = Persistence.createEntityManagerFactory("test", properties);
	entityManager = entityManagerFactory.createEntityManager();
	setField(hoursRepository, "entityManager", entityManager);
	entityManager.getTransaction().begin();
    }

    @After
    public void tearDown() throws Exception {
	if (entityManager.getTransaction().isActive()) {
	    if (entityManager.getTransaction().getRollbackOnly()) {
		entityManager.getTransaction().rollback();
	    } else {
		entityManager.getTransaction().commit();
	    }
	    entityManagerFactory.close();
	}
    }

    @Test
    public void testGetActualHours() throws NoSuchFieldException {
	Employee employee = new Employee("employee");
	Date start = resetTime(new Date());
	Date finish = resetTime(getOffsetDate(1));
	Period period = new Period(start, finish);
	Project project = new Project();
	HourType actualTime = new HourType("actual");
	actualTime.setActualTime(true);
	HourType unexpectedHourType = new HourType("unexpected");
	Hours hours1 = new Hours(project, resetTime(getOffsetDate(-1)), employee, actualTime);
	Hours hours2 = new Hours(project, start, employee, actualTime);
	Hours hours3 = new Hours(project, finish, employee, actualTime);
	Hours hours4 = new Hours(project, finish, employee, unexpectedHourType);
	Hours hours5 = new Hours(project, resetTime(getOffsetDate(2)), employee, actualTime);
	hours1.setApproved(true);
	hours2.setApproved(true);
	hours3.setApproved(false);
	hours4.setApproved(true);
	hours5.setApproved(true);
	entityManager.persist(employee);
	entityManager.persist(project);
	entityManager.persist(actualTime);
	entityManager.persist(unexpectedHourType);
	entityManager.persist(hours1);
	entityManager.persist(hours2);
	entityManager.persist(hours3);
	entityManager.persist(hours4);
	entityManager.persist(hours5);

	List<Hours> actual = hoursRepository.getActualHours(employee, period);

	assertEquals(2, actual.size());
	assertTrue(actual.contains(hours2));
	assertTrue(actual.contains(hours3));
	assertFalse(actual.contains(hours1));
	assertFalse(actual.contains(hours4));
	assertFalse(actual.contains(hours5));
    }

    @Test
    public void testGetApprovedActualHours() throws NoSuchFieldException {
	Employee employee = new Employee("employee");
	Date start = new Date();
	Date finish = getOffsetDate(1);
	Period period = new Period(start, finish);
	Project project = new Project();
	HourType actualTime = new HourType("actual");
	actualTime.setActualTime(true);
	Hours hours1 = new Hours(project, getOffsetDate(-1), employee, actualTime);
	Hours hours2 = new Hours(project, start, employee, actualTime);
	Hours hours3 = new Hours(project, getOffsetDate(1), employee, actualTime);
	Hours hours4 = new Hours(project, getOffsetDate(2), employee, actualTime);
	hours1.setApproved(true);
	hours2.setApproved(true);
	hours3.setApproved(true);
	hours4.setApproved(true);
	entityManager.persist(employee);
	entityManager.persist(project);
	entityManager.persist(actualTime);
	entityManager.persist(hours1);
	entityManager.persist(hours2);
	entityManager.persist(hours3);
	entityManager.persist(hours4);

	List<Hours> actual = hoursRepository.getApprovedActualHours(employee, period);

	assertEquals(2, actual.size());
	assertTrue(actual.contains(hours2));
	assertTrue(actual.contains(hours3));
	assertFalse(actual.contains(hours1));
	assertFalse(actual.contains(hours4));
    }

    @Test
    public void testGetApprovedActualHoursSum() throws NoSuchFieldException {
		Date start = new Date();
		Date finish = getOffsetDate(2);
		Period period = new Period(start, finish);
		Employee employee = new Employee("employee");
		Project project = new Project();
		HourType actualType = new HourType("actual type");
		actualType.setActualTime(true);
		BigDecimal quantity1 = new BigDecimal(8);
		BigDecimal quantity2 = new BigDecimal(8);
		Hours hours1 = createHours(project, employee, getOffsetDate(1), actualType, quantity1);
		Hours hours2 = createHours(project, employee, getOffsetDate(2), actualType, quantity2);
		hours1.setApproved(true);
		hours2.setApproved(true);
		entityManager.persist(employee);
		entityManager.persist(project);
		entityManager.persist(actualType);
		entityManager.persist(hours1);
		entityManager.persist(hours2);
	
		BigDecimal actual = hoursRepository.getApprovedActualHoursSum(employee, period);
	
		assertEquals(new BigDecimal("16.00"), actual);
    }

    @Test
    public void testGetApprovedActualHoursSum_ifHoursForFinishPeriod() throws NoSuchFieldException {
	Date start = new Date();
	Date finish = getOffsetDate(2);
	Period period = new Period(start, finish);
	Employee employee = new Employee("expected employee");
	Project project = new Project();
	HourType actualType = new HourType("actual type");
	actualType.setActualTime(true);
	BigDecimal quantity = new BigDecimal(8);
	Hours hours = createHours(project, employee, finish, actualType, quantity);
	entityManager.persist(employee);
	entityManager.persist(project);
	entityManager.persist(actualType);
	entityManager.persist(hours);

	BigDecimal actual = hoursRepository.getApprovedActualHoursSum(employee, period);

	assertEquals(BigDecimal.ZERO, actual);
    }

    @Test
    public void testGetApprovedActualHoursSum_ifHoursForStartPeriod() throws NoSuchFieldException {
	Date start = new Date();
	Date finish = getOffsetDate(2);
	Period period = new Period(start, finish);
	Employee employee = new Employee("expected employee");
	Project project = new Project();
	HourType actualType = new HourType("actual type");
	actualType.setActualTime(true);
	BigDecimal quantity = new BigDecimal(8);
	Hours hours = createHours(project, employee, start, actualType, quantity);
	hours.setApproved(true);
	entityManager.persist(employee);
	entityManager.persist(project);
	entityManager.persist(actualType);
	entityManager.persist(hours);

	BigDecimal actual = hoursRepository.getApprovedActualHoursSum(employee, period);

	assertEquals(new BigDecimal("8.00"), actual);
    }

    @Test
    public void testGetApprovedActualHoursSum_thereNotHoursInPeriod() throws NoSuchFieldException {
	Date start = new Date();
	Date finish = getOffsetDate(2);
	Period period = new Period(start, finish);
	Employee employee = new Employee("expected employee");
	Project project = new Project();
	HourType actualType = new HourType("actual type");
	actualType.setActualTime(true);
	BigDecimal quantity = new BigDecimal(8);
	Hours hours = createHours(project, employee, getOffsetDate(5), actualType, quantity);
	entityManager.persist(employee);
	entityManager.persist(project);
	entityManager.persist(actualType);
	entityManager.persist(hours);

	BigDecimal actual = hoursRepository.getApprovedActualHoursSum(employee, period);

	assertEquals(BigDecimal.ZERO, actual);
    }

    @Test
    public void testGetApprovedActualHoursSum_unexpectedEmployee() throws NoSuchFieldException {
	Date start = new Date();
	Date finish = getOffsetDate(2);
	Period period = new Period(start, finish);
	Employee expectedEmployee = new Employee("expected employee");
	Employee unexpectedEmployee = new Employee("unexpected employee");
	Project project = new Project();
	HourType actualType = new HourType("actual type");
	actualType.setActualTime(true);
	BigDecimal quantity = new BigDecimal(8);
	Hours hours = createHours(project, unexpectedEmployee, getOffsetDate(1), actualType, quantity);
	entityManager.persist(expectedEmployee);
	entityManager.persist(unexpectedEmployee);
	entityManager.persist(project);
	entityManager.persist(actualType);
	entityManager.persist(hours);

	BigDecimal actual = hoursRepository.getApprovedActualHoursSum(expectedEmployee, period);

	assertEquals(BigDecimal.ZERO, actual);
    }

    @Test
    public void testGetApprovedActualHoursSum_unexpectedHourType() throws NoSuchFieldException {
	Date start = new Date();
	Date finish = getOffsetDate(2);
	Period period = new Period(start, finish);
	Employee employee = new Employee("expected employee");
	Project project = new Project();
	HourType actualType = new HourType("actual type");
	actualType.setActualTime(true);
	HourType unexpectedType = new HourType("type");
	actualType.setActualTime(false);
	BigDecimal quantity = new BigDecimal(8);
	Hours hours = createHours(project, employee, getOffsetDate(1), unexpectedType, quantity);
	entityManager.persist(employee);
	entityManager.persist(project);
	entityManager.persist(actualType);
	entityManager.persist(unexpectedType);
	entityManager.persist(hours);

	BigDecimal actual = hoursRepository.getApprovedActualHoursSum(employee, period);

	assertEquals(BigDecimal.ZERO, actual);
    }

    @Test
    public void testGetDailyApprovedHoursSum() {
	Employee employee = new Employee("employee");
	Project project = new Project();
	HourType actualTime = new HourType("actual");
	actualTime.setActualTime(true);
	Date date = getOffsetDate(-1);
	Hours hours1 = new Hours(project, date, employee, actualTime);
	hours1.setQuantity(new BigDecimal(1));
	Hours hours2 = new Hours(project, getOffsetDate(0), employee, actualTime);
	hours2.setQuantity(new BigDecimal(2));
	Hours hours3 = new Hours(project, getOffsetDate(1), employee, actualTime);
	hours3.setQuantity(new BigDecimal(3));
	Hours hours4 = new Hours(project, getOffsetDate(2), employee, actualTime);
	hours4.setQuantity(new BigDecimal(4));
	List<Hours> hours = Arrays.asList(hours1, hours2, hours3, hours4);

	BigDecimal actual = hoursRepository.getDailyApprovedHoursSum(hours, date);

	assertEquals(new BigDecimal(1), actual);
    }

    @Test
    public void testGetEmployees_FromHours() {
	Employee employee = new Employee("employee");
	Employee anotherEmployee = new Employee("anotherEmployee");
	Hours hours = new Hours();
	hours.setEmployee(employee);
	Hours anotherHours = new Hours();
	anotherHours.setEmployee(anotherEmployee);
	List<Hours> hoursList = Arrays.asList(hours, anotherHours);

	List<Employee> actuals = hoursRepository.getEmployees(hoursList);

	assertEquals(2, actuals.size());
	assertTrue(actuals.contains(employee));
	assertTrue(actuals.contains(anotherEmployee));
    }

    @Test
    public void testGetHours_ByDepartments() {
	Project expectedProject = new Project();
	Employee expectedEmployee = new Employee("user");
	expectedEmployee.setDepartment("expected");
	Hours expected = new Hours();
	expected.setProject(expectedProject);
	expected.setEmployee(expectedEmployee);
	expected.setDate(new Date());
	entityManager.persist(expectedProject);
	entityManager.persist(expectedEmployee);
	entityManager.persist(expected);
	Project unexpectedProject = new Project();
	Employee unexpectedEmployee = new Employee("another user");
	unexpectedEmployee.setDepartment("fail");
	Hours unexpected = new Hours();
	unexpected.setProject(unexpectedProject);
	unexpected.setEmployee(unexpectedEmployee);
	unexpected.setDate(new Date());
	entityManager.persist(unexpectedProject);
	entityManager.persist(unexpectedEmployee);
	entityManager.persist(unexpected);
	Filter filter = new Filter();
	RangePeriodSelector rangePeriodSelector = new RangePeriodSelector(createPeriod(-1, 1));
	filter.setRangePeriodSelector(rangePeriodSelector);
	filter.setDepartments(Arrays.asList("expected"));

	List<Hours> actuals = hoursRepository.getHours(filter);

	assertEquals(1, actuals.size());
	assertTrue(actuals.contains(expected));
    }

    @Test
    public void testGetHours_ByDepartments_DepartmentsIsEmpty() {
	Project unexpectedProject = new Project();
	Employee unexpectedEmployee = new Employee("user");
	unexpectedEmployee.setDepartment("department");
	Hours unexpected = new Hours();
	unexpected.setProject(unexpectedProject);
	unexpected.setEmployee(unexpectedEmployee);
	unexpected.setDate(new Date());
	entityManager.persist(unexpectedProject);
	entityManager.persist(unexpectedEmployee);
	entityManager.persist(unexpected);
	Filter filter = new Filter();
	RangePeriodSelector rangePeriodSelector = new RangePeriodSelector(createPeriod(-1, 1));
	filter.setRangePeriodSelector(rangePeriodSelector);
	filter.setDepartments(Collections.<String> emptyList());

	List<Hours> actuals = hoursRepository.getHours(filter);

	assertTrue(actuals.isEmpty());
    }

    @Test
    public void testGetHours_ByEmployees() {
	Project project = new Project();
	Employee expectedEmployee = new Employee("user");
	Hours expected = new Hours();
	expected.setProject(project);
	expected.setEmployee(expectedEmployee);
	expected.setDate(new Date());
	entityManager.persist(project);
	entityManager.persist(expectedEmployee);
	entityManager.persist(expected);

	Filter filter = new Filter();
	filter.setProjects(null);
	filter.setEmployees(Arrays.asList(expectedEmployee));
	RangePeriodSelector rangePeriodSelector = new RangePeriodSelector(createPeriod(-1, 1));
	filter.setRangePeriodSelector(rangePeriodSelector);

	List<Hours> actual = hoursRepository.getHours(filter);

	assertTrue(actual.contains(expected));
	assertEquals(expectedEmployee, actual.get(0).getEmployee());
    }

    @Test
    public void testGetHours_ByEmployees_EmployeesIsEmpty() {
	Project project = new Project();
	Employee expectedEmployee = new Employee("user");
	Hours expected = new Hours();
	expected.setProject(project);
	expected.setEmployee(expectedEmployee);
	expected.setDate(new Date());
	entityManager.persist(project);
	entityManager.persist(expectedEmployee);
	entityManager.persist(expected);

	Filter filter = new Filter();
	filter.setProjects(null);
	filter.setEmployees(Collections.<Employee> emptyList());
	RangePeriodSelector rangePeriodSelector = new RangePeriodSelector(createPeriod(-1, 1));
	filter.setRangePeriodSelector(rangePeriodSelector);

	List<Hours> actual = hoursRepository.getHours(filter);

	assertTrue(actual.isEmpty());
    }

    @Test
    public void testGetHours_ByHoursSearchCriteria_AnyHours() {
	Date start = new Date(0);
	Date finish = getOffsetDate(2);
	Period period = new Period(start, finish);
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	Project project = new Project();
	project.setDefaultWorkdaysCalendar(calendar);
	Hours hours = new Hours();
	hours.setProject(project);
	hours.setDate(new Date());
	hours.setApproved(true);
	Hours hours2 = new Hours();
	hours2.setDate(new Date());
	hours2.setProject(project);
	entityManager.persist(calendar);
	entityManager.persist(project);
	entityManager.persist(hours);
	entityManager.persist(hours2);
	Filter filter = new Filter();
	RangePeriodSelector rangePeriodSelector = new RangePeriodSelector(period);
	filter.setRangePeriodSelector(rangePeriodSelector);
	filter.setApproved(null);

	List<Hours> actuals = hoursRepository.getHours(filter);

	assertEquals(2, actuals.size());
	assertTrue(actuals.contains(hours));
	assertTrue(actuals.contains(hours2));
    }

    @Test
    public void testGetHours_ByHoursSearchCriteria_ApprovedHoursOnly() {
	Date start = new Date(0);
	Date finish = getOffsetDate(2);
	Period period = new Period(start, finish);
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	Project project = new Project();
	project.setDefaultWorkdaysCalendar(calendar);
	Hours expectedHours = new Hours();
	expectedHours.setProject(project);
	expectedHours.setDate(new Date());
	expectedHours.setApproved(true);
	Hours unexpectedHours = new Hours();
	unexpectedHours.setDate(new Date());
	unexpectedHours.setProject(project);
	entityManager.persist(calendar);
	entityManager.persist(project);
	entityManager.persist(expectedHours);
	entityManager.persist(unexpectedHours);
	Filter filter = new Filter();
	RangePeriodSelector rangePeriodSelector = new RangePeriodSelector(period);
	filter.setRangePeriodSelector(rangePeriodSelector);
	filter.setApproved(Boolean.TRUE);

	List<Hours> actuals = hoursRepository.getHours(filter);

	assertEquals(1, actuals.size());
	assertEquals(expectedHours, actuals.get(0));
    }

    @Test
    public void testGetHours_ByPeriod() {
	HourType type = new HourType();
	Hours expected1 = new Hours();
	Hours expected2 = new Hours();
	Hours unexpected = new Hours();

	Project project = new Project();
	expected1.setProject(project);
	expected2.setProject(project);
	unexpected.setProject(project);
	expected1.setDate(getOffsetDate(-1));
	expected2.setDate(getOffsetDate(-1));
	unexpected.setDate(getOffsetDate(1));
	entityManager.persist(type);
	entityManager.persist(project);
	entityManager.persist(expected1);
	entityManager.persist(expected2);
	entityManager.persist(unexpected);
	Period period = new Period(getOffsetDate(-1), new Date());
	Filter filter = new Filter();
	RangePeriodSelector rangePeriodSelector = new RangePeriodSelector(period);
	filter.setRangePeriodSelector(rangePeriodSelector);
	filter.setProjects(Arrays.asList(project));

	List<Hours> actual = hoursRepository.getHours(filter);

	assertEquals(2, actual.size());
	assertTrue(actual.contains(expected1));
	assertTrue(actual.contains(expected2));
    }

    @Test
    public void testGetHours_ByProjectAndEmployee() {
	Employee expectedEmployee = new Employee("expected");
	Employee unexpectedEmployee = new Employee("unexpected");
	Project project = new Project();
	HourType type = new HourType();
	Hours expected = new Hours();
	Hours unexpected = new Hours();
	expected.setProject(project);
	expected.setEmployee(expectedEmployee);
	expected.setDate(getOffsetDate(-1));
	unexpected.setProject(project);
	unexpected.setEmployee(unexpectedEmployee);
	unexpected.setDate(getOffsetDate(-1));
	entityManager.persist(type);
	entityManager.persist(expectedEmployee);
	entityManager.persist(unexpectedEmployee);
	entityManager.persist(project);
	entityManager.persist(expected);
	entityManager.persist(unexpected);
	List<Project> projects = Arrays.asList(project);
	List<Employee> employees = Arrays.asList(expectedEmployee);
	Period period = new Period(getOffsetDate(-1), new Date());
	Filter filter = new Filter();
	RangePeriodSelector rangePeriodSelector = new RangePeriodSelector(period);
	filter.setRangePeriodSelector(rangePeriodSelector);
	filter.setProjects(projects);
	filter.setEmployees(employees);

	List<Hours> actual = hoursRepository.getHours(filter);

	assertEquals(1, actual.size());
	assertTrue(actual.contains(expected));
    }

    @Test
    public void testGetHours_ByProjects() {
	Project expectedProject = new Project();
	Hours expected = new Hours();
	expected.setProject(expectedProject);
	expected.setDate(new Date());
	entityManager.persist(expectedProject);
	entityManager.persist(expected);
	Filter filter = new Filter();
	RangePeriodSelector rangePeriodSelector = new RangePeriodSelector(createPeriod(-1, 1));
	filter.setRangePeriodSelector(rangePeriodSelector);
	filter.setProjects(Arrays.asList(expectedProject));

	List<Hours> actual = hoursRepository.getHours(filter);

	assertTrue(actual.contains(expected));
	assertEquals(expectedProject, actual.get(0).getProject());
    }

    @Test
    public void testGetHours_ByProjects_ProjectsIsEmpty() {
	Project expectedProject = new Project();
	Hours expected = new Hours();
	expected.setProject(expectedProject);
	expected.setDate(new Date());
	entityManager.persist(expectedProject);
	entityManager.persist(expected);
	Filter filter = new Filter();
	RangePeriodSelector rangePeriodSelector = new RangePeriodSelector(createPeriod(-1, 1));
	filter.setRangePeriodSelector(rangePeriodSelector);
	filter.setProjects(Collections.<Project> emptyList());

	List<Hours> actual = hoursRepository.getHours(filter);

	assertTrue(actual.isEmpty());
    }

    @Test
    public void testGetHours_ByProjectsWithoutTask() {
	HourType type = new HourType();
	Hours expected = new Hours(getOffsetDate(-2), type);
	Hours unexpected = new Hours(getOffsetDate(-5), type);
	Project expectedProject = new Project();
	Project unexpectedProject = new Project();
	expected.setProject(expectedProject);
	unexpected.setProject(unexpectedProject);
	entityManager.persist(type);
	entityManager.persist(expectedProject);
	entityManager.persist(unexpectedProject);
	entityManager.persist(expected);
	entityManager.persist(unexpected);

	List<Project> projects = Arrays.asList(expectedProject);
	Period period = new Period(getOffsetDate(-3), getOffsetDate(-2));
	Filter filter = new Filter();
	RangePeriodSelector rangePeriodSelector = new RangePeriodSelector(period);
	filter.setRangePeriodSelector(rangePeriodSelector);
	filter.setProjects(projects);

	List<Hours> actual = hoursRepository.getHours(filter);

	assertEquals(1, actual.size());
	assertTrue(actual.contains(expected));
    }

    @Test
    public void testGetHours_WithEmptyProjectsInCriteria() {
	Project expectedProject = new Project();
	Hours expectedHours = new Hours();
	expectedHours.setProject(expectedProject);
	expectedHours.setDate(new Date());

	entityManager.persist(expectedProject);
	entityManager.persist(expectedHours);

	Filter filter = new Filter();
	RangePeriodSelector rangePeriodSelector = new RangePeriodSelector(createPeriod(0, 1));
	filter.setRangePeriodSelector(rangePeriodSelector);

	List<Hours> actual = hoursRepository.getHours(filter);

	assertTrue(actual.contains(expectedHours));
    }

    @Test
    public void testGetHours_WithEmptyProjectsAndEmployees() {
	Project project = new Project();
	Hours expected = new Hours();
	expected.setProject(project);
	expected.setDate(new Date());
	entityManager.persist(project);
	entityManager.persist(expected);
	Filter filter = new Filter();
	RangePeriodSelector rangePeriodSelector = new RangePeriodSelector(createPeriod(-1, 1));
	filter.setRangePeriodSelector(rangePeriodSelector);

	List<Hours> actual = hoursRepository.getHours(filter);

	assertTrue(actual.contains(expected));
    }

    @Test
    public void testGetPeriod_FromHours() {
	Date expectedMinimum = new Date(111111L);
	Date expectedMaximum = new Date(333333L);
	Date unexpectedDate = new Date(222222L);
	Hours hours = new Hours();
	hours.setDate(unexpectedDate);
	Hours hours2 = new Hours();
	hours2.setDate(expectedMinimum);
	Hours hours3 = new Hours();
	hours3.setDate(expectedMaximum);
	List<Hours> hoursList = Arrays.asList(hours, hours2, hours3);
	Period expected = new Period(expectedMinimum, expectedMaximum);

	Period actual = hoursRepository.getPeriod(hoursList);

	assertEquals(expected, actual);
    }

    @Test
    public void testPredeleteHours() throws NoSuchFieldException {
	Employee employee = new Employee("user");
	Project project = new Project();
	HourType hourType = new HourType("hourType");
	Hours unexpected = new Hours(project, new Date(), employee, hourType);
	unexpected.setQuantity(BigDecimal.ONE);
	entityManager.persist(employee);
	entityManager.persist(project);
	entityManager.persist(hourType);
	entityManager.persist(unexpected);
	entityManager.flush();
	entityManager.clear();
	Hours newHours = new Hours(project, resetTime(new Date()), employee, hourType);
	newHours.setQuantity(BigDecimal.TEN);
	List<Hours> hoursList = Arrays.asList(newHours);

	hoursRepository.predeleteEqualHours(hoursList);

	entityManager.flush();
	entityManager.clear();
	List<Hours> actuals = entityManager.createQuery("SELECT h FROM Hours h", Hours.class).getResultList();
	assertEquals(0, actuals.size());
    }

    private Hours createHours(Project project, Employee employee, Date date, HourType actualType, BigDecimal quantity) {
	Hours hours = new Hours();
	hours.setEmployee(employee);
	hours.setDate(date);
	hours.setQuantity(quantity);
	hours.setType(actualType);
	hours.setProject(project);
	return hours;
    }

	@Test
	public void testGetActualHoursSum() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Employee employee = new Employee("username");
		Date start = sdf.parse("2014-11-01");
		WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
		Project project = createActiveProjectWithParticipation(employee, start, calendar);
		HourType actualType = new HourType("actual");
		actualType.setActualTime(true);
		HourType notActualType = new HourType("not_actual");
		Hours approved = createHours(project, employee, sdf.parse("2014-11-02"), actualType, new BigDecimal("8"));
		approved.setApproved(true);
		Hours notApproved1 = createHours(project, employee, sdf.parse("2014-11-03"), actualType, new BigDecimal("8"));
		Hours notApproved2 = createHours(project, employee, sdf.parse("2014-11-04"), actualType, new BigDecimal("8"));
		Hours notActual = createHours(project, employee, sdf.parse("2014-11-05"), notActualType, new BigDecimal("8"));
		Hours beforePeriod = createHours(project, employee, sdf.parse("2014-10-31"), actualType, new BigDecimal("8"));
		entityManager.persist(actualType);
		entityManager.persist(notActualType);
		entityManager.persist(approved);
		entityManager.persist(notApproved1);
		entityManager.persist(notApproved2);
		entityManager.persist(notActual);
		entityManager.persist(beforePeriod);

		Period period = new Period(start, sdf.parse("2014-11-30"));
		BigDecimal actual = hoursRepository.getActualHoursSum(employee, period);

		assertEquals(new BigDecimal("24.00"), actual);
	}

	private Project createActiveProjectWithParticipation(Employee employee, Date start, WorkdaysCalendar calendar) {
		Participation participation = new Participation(employee, start);
		participation.setWorkdaysCalendar(calendar);
		Project project = new Project();
		project.setDefaultWorkdaysCalendar(calendar);
		project.addParticipation(participation);
		ProjectStatus activeStatus = new ProjectStatus(ProjectStatus.Status.ACTIVE, start);
		project.addStatus(activeStatus);
		entityManager.persist(calendar);
		entityManager.persist(employee);
		entityManager.persist(participation);
		entityManager.persist(activeStatus);
		entityManager.persist(project);
		return project;
	}

	@Test
	public void testGetApprovedActualHoursSum_ByEmployees() {
		Period period = new Period(getOffsetDate(-2), getOffsetDate(2));
		Project project = new Project();
		HourType actualTime = new HourType("actual");
		actualTime.setActualTime(true);
		Employee employee1 = new Employee("employee1");
		Employee employee2 = new Employee("employee2");
		Employee notRequested = new Employee("employee3");
		Employee withoutHours = new Employee("employee4");
		Hours hours1 = createHours(project, employee1, getOffsetDate(-2), actualTime, new BigDecimal("3"));
		Hours hours2 = createHours(project, employee1, getOffsetDate(-1), actualTime, new BigDecimal("4"));
		Hours hours3 = createHours(project, employee1, getOffsetDate(2), actualTime, new BigDecimal("6"));
		Hours hours4 = createHours(project, employee2, getOffsetDate(-3), actualTime, new BigDecimal("8"));
		Hours hours5 = createHours(project, employee2, getOffsetDate(0), actualTime, new BigDecimal("8"));
		Hours hours6 = createHours(project, employee2, getOffsetDate(3), actualTime, new BigDecimal("8"));
		Hours hours7 = createHours(project, notRequested, getOffsetDate(-1), actualTime, new BigDecimal("8"));
		approveHours(hours1, hours2, hours3, hours4, hours5, hours6, hours7);
		persist(actualTime, project, employee1, employee2, notRequested, withoutHours);
		persist(hours1, hours2, hours3, hours4, hours5, hours6, hours7);

		Map<Employee, BigDecimal> actual = hoursRepository.getApprovedActualHoursSum(Arrays.asList(employee1, employee2, withoutHours), period);

		assertEquals(asSet(employee1, employee2, withoutHours), actual.keySet());
		assertEquals(new BigDecimal("13"), actual.get(employee1));
		assertEquals(new BigDecimal("8"), actual.get(employee2));
		assertEquals(BigDecimal.ZERO, actual.get(withoutHours));
	}

	@Test
	public void testGetApprovedActualHoursSum_ByEmployees_CheckNullQuantity() {
		Period period = new Period(getOffsetDate(-2), getOffsetDate(2));
		Project project = new Project();
		HourType actualTime = new HourType("actual");
		actualTime.setActualTime(true);
		Employee employee = new Employee("employee");
		Hours hours = createHours(project, employee, getOffsetDate(-2), actualTime, null);
		hours.setApproved(true);
		entityManager.persist(project);
		entityManager.persist(actualTime);
		entityManager.persist(employee);
		entityManager.persist(hours);

		Map<Employee, BigDecimal> actual = hoursRepository.getApprovedActualHoursSum(Arrays.asList(employee), period);

		assertEquals(asSet(employee), actual.keySet());
		assertEquals(BigDecimal.ZERO, actual.get(employee));
	}

	private void persist(Object ... entities) {
		for (Object entity : entities) {
			entityManager.persist(entity);
		}
	}

	private void approveHours(Hours ... hoursArray) {
		for (Hours hours : hoursArray) {
			hours.setApproved(true);
		}
	}

	private <T> Set<T> asSet(T ... values) {
		HashSet<T> result = new HashSet<>();
		result.addAll(Arrays.asList(values));
		return result;
	}
}
