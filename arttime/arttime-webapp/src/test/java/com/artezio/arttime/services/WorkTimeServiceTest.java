package com.artezio.arttime.services;

import static com.artezio.arttime.test.utils.CalendarUtils.*;
import static junitx.util.PrivateAccessor.setField;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.artezio.arttime.datamodel.Day;
import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.HourType;
import com.artezio.arttime.datamodel.Hours;
import com.artezio.arttime.datamodel.Participation;
import com.artezio.arttime.datamodel.Period;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.datamodel.ProjectStatus;
import com.artezio.arttime.datamodel.WorkdaysCalendar;
import com.artezio.arttime.filter.Filter;
import com.artezio.arttime.services.repositories.EmployeeRepository;
import com.artezio.arttime.services.repositories.HoursRepository;
import com.artezio.arttime.services.repositories.ProjectRepository;
import com.artezio.arttime.services.repositories.WorkdaysCalendarRepository;
import com.artezio.arttime.web.criteria.RangePeriodSelector;

public class WorkTimeServiceTest {

    private EntityManager entityManager;
    private EntityManagerFactory entityManagerFactory;
    private WorkTimeService workTimeService;

    @Before
    public void setUp() throws Exception {
	workTimeService = new WorkTimeService();
	Map<String, String> properties = new HashMap<String, String>();
	properties.put("javax.persistence.validation.mode", "none");
	entityManagerFactory = Persistence.createEntityManagerFactory("test", properties);
	entityManager = entityManagerFactory.createEntityManager();
	ProjectRepository projectRepository = new ProjectRepository();
	EmployeeRepository employeeRepository = new EmployeeRepository();
	WorkdaysCalendarRepository workdaysCalendarRepository = new WorkdaysCalendarRepository();
	HoursRepository hoursRepository = new HoursRepository();
	setField(projectRepository, "entityManager", entityManager);
	setField(employeeRepository, "entityManager", entityManager);
	setField(workdaysCalendarRepository, "entityManager", entityManager);
	setField(hoursRepository, "entityManager", entityManager);
	setField(workTimeService, "projectRepository", projectRepository);
	setField(workTimeService, "employeeRepository", employeeRepository);
	setField(workTimeService, "workdaysCalendarRepository", workdaysCalendarRepository);
	setField(workTimeService, "hoursRepository", hoursRepository);
	entityManager.getTransaction().begin();
    }

    @After
    public void tearDown() throws Exception {
	if (entityManager.getTransaction().isActive()) {
	    if (entityManager.getTransaction().getRollbackOnly()) {
		entityManager.getTransaction().rollback();
	    } else {
		entityManager.getTransaction().commit();
	    }
	    entityManagerFactory.close();
	}
    }

    @Test
    public void testGenerateWorkScheduleTemplate() {
	Date start = resetTime(new Date());
	Date middle = resetTime(getOffsetDate(1));
	Date finish = resetTime(getOffsetDate(2));
	Period period = new Period(start, finish);

	Map<Date, Boolean> actuals = workTimeService.generateWorkScheduleTemplate(period);

	assertEquals(3, actuals.size());
	assertTrue(actuals.containsKey(start));
	assertTrue(actuals.containsKey(middle));
	assertTrue(actuals.containsKey(finish));
	assertEquals(Boolean.FALSE, actuals.get(start));
	assertEquals(Boolean.FALSE, actuals.get(middle));
    }

    @Test
    public void testGetApprovedWorkTimeProblems() throws NoSuchFieldException {
        Employee employee = new Employee("employee");
        employee.setWorkLoad(100);
        Date start = new Date();
        Date finish = getOffsetDate(1);
        Period period = new Period(start, finish);
        HourType actualTime = new HourType("actual");
        actualTime.setActualTime(true);
        WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
        Participation participation = new Participation(employee, start);
        participation.setWorkdaysCalendar(calendar);
        Project project = new Project();
        project.addParticipation(participation);
        ProjectStatus projectStatus = new ProjectStatus(ProjectStatus.Status.ACTIVE, start);
        project.addStatus(projectStatus);
        Day day1 = new Day(start, calendar, true);
        Day day2 = new Day(getOffsetDate(1), calendar, true);
        Day day3 = new Day(getOffsetDate(2), calendar, true);
        Hours hours1 = new Hours(project, getOffsetDate(-1), employee, actualTime);
        hours1.setQuantity(new BigDecimal(1));
        Hours hours2 = new Hours(project, getOffsetDate(0), employee, actualTime);
        hours2.setQuantity(new BigDecimal(2));
        Hours hours3 = new Hours(project, getOffsetDate(1), employee, actualTime);
        hours3.setQuantity(new BigDecimal(3));
        Hours hours4 = new Hours(project, getOffsetDate(2), employee, actualTime);
        hours4.setQuantity(new BigDecimal(4));
        hours1.setApproved(true);
        hours2.setApproved(true);
        hours3.setApproved(true);
        hours4.setApproved(true);
        entityManager.persist(employee);
        entityManager.persist(calendar);
        entityManager.persist(day1);
        entityManager.persist(day2);
        entityManager.persist(day3);
        entityManager.persist(actualTime);
        entityManager.persist(participation);
        entityManager.persist(projectStatus);
        entityManager.persist(project);
        entityManager.persist(hours1);
        entityManager.persist(hours2);
        entityManager.persist(hours3);
        entityManager.persist(hours4);
        entityManager.flush();
        entityManager.clear();

        Map<Employee, Map<Date, BigDecimal>> actuals = workTimeService.getApprovedWorkTimeProblems(period, Arrays.asList(employee));
        Map<Date, BigDecimal> actual = actuals.get(employee);
        List<Day> actualDays = entityManager.createQuery("SELECT d FROM Day d ORDER BY d.date", Day.class).getResultList();
        Date actualDate1 = actualDays.get(0).getDate();
        Date actualDate2 = actualDays.get(1).getDate();
        Date actualDate3 = actualDays.get(2).getDate();

        assertEquals(2, actual.size());
        assertTrue(actual.containsKey(actualDate1));
        assertTrue(actual.containsKey(actualDate2));
        assertFalse(actual.containsKey(actualDate3));
        assertEquals(-6, actual.get(actualDate1).intValue());
        assertEquals(-5, actual.get(actualDate2).intValue());
    }

    @Test
    public void testGetApprovedWorkTimeProblems_CorrectTime() throws NoSuchFieldException {
        Employee employee = new Employee("employee");
        employee.setWorkLoad(100);
        Date start = getOffsetDate(0);
        Period period = new Period(start, getOffsetDate(1));
        HourType actualTime = new HourType("actual");
        actualTime.setActualTime(true);
        WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
        Participation participation = new Participation(employee, start);
        participation.setWorkdaysCalendar(calendar);
        Project project = new Project();
        project.addParticipation(participation);
        ProjectStatus projectStatus = new ProjectStatus(ProjectStatus.Status.ACTIVE, start);
        project.addStatus(projectStatus);
        Day day1 = new Day(start, calendar, true);
        Day day2 = new Day(getOffsetDate(1), calendar, true);
        Hours hours1 = new Hours(project, getOffsetDate(0), employee, actualTime);
        hours1.setQuantity(new BigDecimal(8));
        Hours hours2 = new Hours(project, getOffsetDate(1), employee, actualTime);
        hours2.setQuantity(new BigDecimal(8));
        hours1.setApproved(true);
        hours2.setApproved(true);
        entityManager.persist(employee);
        entityManager.persist(calendar);
        entityManager.persist(day1);
        entityManager.persist(day2);
        entityManager.persist(actualTime);
        entityManager.persist(participation);
        entityManager.persist(projectStatus);
        entityManager.persist(project);
        entityManager.persist(hours1);
        entityManager.persist(hours2);
        entityManager.flush();
        entityManager.clear();

        Map<Employee, Map<Date, BigDecimal>> actuals = workTimeService.getApprovedWorkTimeProblems(period, Arrays.asList(employee));

        assertTrue(actuals.get(employee).isEmpty());
    }

    @Test
    public void testGetApprovedWorkTimeProblems_existProblems() {
	String department = "department";
	List<String> departments = Arrays.asList(department);
	Date start = new Date();
	Date finish = getOffsetDate(1);
	Period period = new Period(start, finish);
	Employee employee = new Employee("employee", "name", "lastname", "mail");
	employee.setWorkLoad(100);
	employee.setDepartment(department);
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	Day day1 = new Day(new Date(), calendar, true);
	Day day2 = new Day(getOffsetDate(1), calendar, true);
	Participation participation = new Participation();
	participation.setEmployee(employee);
	participation.getPeriod().setStart(new Date());
	participation.setWorkdaysCalendar(calendar);
	Project project = new Project();
	project.addParticipation(participation);
	ProjectStatus projectStatus = new ProjectStatus(ProjectStatus.Status.ACTIVE, start);
	project.addStatus(projectStatus);
	HourType actualType = new HourType("actual type");
	actualType.setActualTime(true);
	BigDecimal quantity1 = new BigDecimal(3);
	BigDecimal quantity2 = new BigDecimal(10);
	Hours hours1 = createHours(project, employee, new Date(), actualType, quantity1);
	Hours hours2 = createHours(project, employee, getOffsetDate(1), actualType, quantity2);
	hours1.setApproved(true);
	hours2.setApproved(true);
	entityManager.persist(employee);
	entityManager.persist(calendar);
	entityManager.persist(participation);
	entityManager.persist(actualType);
	entityManager.persist(projectStatus);
	entityManager.persist(project);
	entityManager.persist(hours1);
	entityManager.persist(hours2);
	entityManager.persist(day1);
	entityManager.persist(day2);
	entityManager.flush();
	entityManager.clear();
	
	Filter filter = new Filter();
	RangePeriodSelector rangePeriodSelector = new RangePeriodSelector(period);
	filter.setRangePeriodSelector(rangePeriodSelector);
	filter.setDepartments(departments);
	filter.setEmployees(Arrays.asList(employee));
	filter.setProjects(Arrays.asList(project));
	
	Map<Employee, BigDecimal> actual = workTimeService.getApprovedWorkTimeProblems(filter);

	BigDecimal expected = new BigDecimal(-3);

	assertNotNull(actual);
	assertEquals(1, actual.size());
	assertTrue(actual.containsKey(employee));
	assertTrue(expected.compareTo(actual.get(employee)) == 0);
    }

    @Test
    public void testGetApprovedWorkTimeProblems_ifEmployeeWorkLoadIsNull() {
	String department = "department";
	List<String> departments = Arrays.asList(department);
	Date start = new Date();
	Date finish = getOffsetDate(2);
	Period period = new Period(start, finish);
	Employee employee = new Employee("employee");
	employee.setWorkLoad(null);
	employee.setDepartment(department);
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	Day day1 = new Day(new Date(), calendar, true);
	Day day2 = new Day(getOffsetDate(1), calendar, true);
	Participation participation = new Participation(employee, new Date());
	Project project = new Project();
	project.addParticipation(participation);
	participation.setWorkdaysCalendar(calendar);
	HourType actualTime = new HourType("actual time");
	actualTime.setActualTime(true);
	Hours hours = new Hours(project, start, employee, actualTime);
	hours.setQuantity(new BigDecimal(8));
	entityManager.persist(employee);
	entityManager.persist(calendar);
	entityManager.persist(day1);
	entityManager.persist(day2);
	entityManager.persist(participation);
	entityManager.persist(actualTime);
	entityManager.persist(project);
	entityManager.persist(hours);

	Filter filter = new Filter();
	RangePeriodSelector rangePeriodSelector = new RangePeriodSelector(period);
	filter.setRangePeriodSelector(rangePeriodSelector);
	filter.setDepartments(departments);
	filter.setEmployees(Arrays.asList(employee));
	filter.setProjects(Arrays.asList(project));

	Map<Employee, BigDecimal> actual = workTimeService.getApprovedWorkTimeProblems(filter);

	assertTrue(actual.isEmpty());
    }

    @Test
    public void testGetApprovedWorkTimeProblems_IfParticipationStartsAfterPeriod() {
	String department = "department";
	List<String> departments = Arrays.asList(department);
	Employee employee = new Employee("username", "name", "lastname", "mail");
	employee.setDepartment(department);
	Date participationStartDate = new GregorianCalendar(2011, 0, 5).getTime();
	Participation participation = new Participation(employee, participationStartDate);
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	participation.setWorkdaysCalendar(calendar);
	Project project = new Project();
	project.addParticipation(participation);
	Date periodStartDate = new GregorianCalendar(2011, 0, 3).getTime();
	Date periodFinishDate = new GregorianCalendar(2011, 0, 8).getTime();
	Period period = new Period(periodStartDate, periodFinishDate);
	ProjectStatus projectStatus = new ProjectStatus(ProjectStatus.Status.ACTIVE, periodStartDate);
	project.addStatus(projectStatus);
	entityManager.persist(employee);
	entityManager.persist(calendar);
	entityManager.persist(participation);
	entityManager.persist(projectStatus);
	entityManager.persist(project);
	entityManager.flush();
	entityManager.clear();

	Filter filter = new Filter();
	RangePeriodSelector rangePeriodSelector = new RangePeriodSelector(period);
	filter.setRangePeriodSelector(rangePeriodSelector);
	filter.setDepartments(departments);
	filter.setEmployees(Arrays.asList(employee));
	filter.setProjects(Arrays.asList(project));

	Map<Employee, BigDecimal> actual = workTimeService.getApprovedWorkTimeProblems(filter);

	assertEquals(new BigDecimal("-24.00"), actual.get(employee));
    }

    @Test
    public void testGetApprovedWorkTimeProblems_NoProblems() {
	String department = "department";
	List<String> departments = Arrays.asList(department);
	Date now = new Date();
	Date start = now;
	Date finish = getOffsetDate(1);
	Period period = new Period(start, finish);
	Employee employee = new Employee("employee");
	employee.setWorkLoad(100);
	employee.setDepartment(department);
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	Day day1 = new Day(now, calendar, true);
	Day day2 = new Day(getOffsetDate(1), calendar, true);
	Participation participation = new Participation();
	participation.setEmployee(employee);
	participation.getPeriod().setStart(now);
	participation.setWorkdaysCalendar(calendar);
	Project project = new Project();
	project.addParticipation(participation);
	ProjectStatus projectStatus = new ProjectStatus(ProjectStatus.Status.ACTIVE, start);
	project.addStatus(projectStatus);
	HourType actualType = new HourType("actual type");
	actualType.setActualTime(true);
	BigDecimal quantity1 = new BigDecimal(8);
	BigDecimal quantity2 = new BigDecimal(8);
	Hours hours1 = createHours(project, employee, now, actualType, quantity1);
	Hours hours2 = createHours(project, employee, getOffsetDate(1), actualType, quantity2);
	hours1.setApproved(true);
	hours2.setApproved(true);
	entityManager.persist(employee);
	entityManager.persist(calendar);
	entityManager.persist(participation);
	entityManager.persist(actualType);
	entityManager.persist(projectStatus);
	entityManager.persist(project);
	entityManager.persist(hours1);
	entityManager.persist(hours2);
	entityManager.persist(day1);
	entityManager.persist(day2);
	entityManager.flush();
	entityManager.clear();

	Filter filter = new Filter();
	RangePeriodSelector rangePeriodSelector = new RangePeriodSelector(period);
	filter.setRangePeriodSelector(rangePeriodSelector);
	filter.setDepartments(departments);
	filter.setEmployees(Arrays.asList(employee));
	filter.setProjects(Arrays.asList(project));

	Map<Employee, BigDecimal> actual = workTimeService.getApprovedWorkTimeProblems(filter);

	System.out.println(actual);
	assertTrue(actual.isEmpty());
    }

    @Test
    public void testGetApprovedWorkTimeProblems_twoProjectsAndTwoCalendars() {
	String department = "department";
	List<String> departments = Arrays.asList(department);
	Date start = new Date();
	Date finish = getOffsetDate(1);
	Period period = new Period(start, finish);
	Employee employee = new Employee("employee");
	employee.setDepartment(department);
	employee.setWorkLoad(100);
	WorkdaysCalendar calendar1 = new WorkdaysCalendar("calendar 1");
	Day day1 = new Day(new Date(), calendar1, false);
	Day day2 = new Day(getOffsetDate(1), calendar1, true);
	WorkdaysCalendar calendar2 = new WorkdaysCalendar("calendar 2");
	Day day3 = new Day(new Date(), calendar2, true);
	Day day4 = new Day(getOffsetDate(1), calendar2, true);
	Participation participation1 = new Participation();
	participation1.setEmployee(employee);
	participation1.getPeriod().setStart(new Date());
	participation1.setWorkdaysCalendar(calendar1);
	Participation participation2 = new Participation();
	participation2.setEmployee(employee);
	participation2.getPeriod().setStart(getOffsetDate(1));
	participation2.setWorkdaysCalendar(calendar2);
	Project project1 = new Project();
	project1.addParticipation(participation1);
	ProjectStatus projectStatus1 = new ProjectStatus(ProjectStatus.Status.ACTIVE, start);
	project1.addStatus(projectStatus1);
	Project project2 = new Project();
	project2.addParticipation(participation2);
	ProjectStatus projectStatus2 = new ProjectStatus(ProjectStatus.Status.ACTIVE, start);
	project2.addStatus(projectStatus2);
	HourType actualType = new HourType("actual type");
	actualType.setActualTime(true);
	Hours hours = createHours(project1, employee, getOffsetDate(1), actualType, new BigDecimal(8));
	hours.setApproved(true);
	entityManager.persist(employee);
	entityManager.persist(calendar1);
	entityManager.persist(calendar2);
	entityManager.persist(participation1);
	entityManager.persist(participation2);
	entityManager.persist(actualType);
	entityManager.persist(projectStatus1);
	entityManager.persist(projectStatus2);
	entityManager.persist(project1);
	entityManager.persist(project2);
	entityManager.persist(hours);
	entityManager.persist(day1);
	entityManager.persist(day2);
	entityManager.persist(day3);
	entityManager.persist(day4);
	entityManager.flush();
	entityManager.clear();

	Filter filter = new Filter();
	RangePeriodSelector rangePeriodSelector = new RangePeriodSelector(period);
	filter.setRangePeriodSelector(rangePeriodSelector);
	filter.setDepartments(departments);
	filter.setEmployees(Arrays.asList(employee));
	filter.setProjects(Arrays.asList(project1, project2));

	Map<Employee, BigDecimal> actual = workTimeService.getApprovedWorkTimeProblems(filter);

	assertTrue(actual.isEmpty());
    }

    @Test
    public void testGetWorkDaysNumber() {
	Map<Date, Boolean> workSchedule = new HashMap<Date, Boolean>();
	workSchedule.put(new Date(), true);
	workSchedule.put(getOffsetDate(1), true);
	workSchedule.put(getOffsetDate(2), false);

	int actual = workTimeService.getWorkDaysNumber(workSchedule);

	assertEquals(2, actual);
    }

    @Test
    public void testGetWorkSchedule() throws NoSuchFieldException {
	Employee employee = new Employee("employee");
	employee.setWorkLoad(100);
	Date start = new Date();
	Date finish = getOffsetDate(1);
	Period period = new Period(start, finish);
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	Day day1 = new Day(start, calendar, true);
	Day day2 = new Day(getOffsetDate(1), calendar, true);
	Day day3 = new Day(getOffsetDate(2), calendar, true);
	Participation participation = new Participation(employee, start);
	participation.setWorkdaysCalendar(calendar);
	Project project = new Project();
	project.addParticipation(participation);
	ProjectStatus projectStatus = new ProjectStatus(ProjectStatus.Status.ACTIVE, start);
	project.addStatus(projectStatus);
	entityManager.persist(employee);
	entityManager.persist(calendar);
	entityManager.persist(day1);
	entityManager.persist(day2);
	entityManager.persist(day3);
	entityManager.persist(participation);
	entityManager.persist(projectStatus);
	entityManager.persist(project);
	entityManager.flush();
	entityManager.clear();

	Map<Date, Boolean> actual = workTimeService.getWorkSchedule(employee, period);
	List<Day> actualDays = entityManager.createQuery("SELECT d FROM Day d ORDER BY d.date", Day.class)
		.getResultList();
	Date actualDate1 = actualDays.get(0).getDate();
	Date actualDate2 = actualDays.get(1).getDate();
	Date actualDate3 = actualDays.get(2).getDate();

	assertEquals(2, actual.size());
	assertTrue(actual.containsKey(actualDate1));
	assertTrue(actual.containsKey(actualDate2));
	assertFalse(actual.containsKey(actualDate3));
	assertTrue(actual.get(actualDate1));
	assertTrue(actual.get(actualDate2));
    }

    @Test
    public void testGetWorkSchedule_MultipleCalendars() throws NoSuchFieldException {
	Employee employee = new Employee("employee");
	employee.setWorkLoad(100);
	Date start = resetTime(new Date());
	Date finish = resetTime(getOffsetDate(1));
	Period period = new Period(start, finish);
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	Day day = new Day(start, calendar, true);
	Day day1 = new Day(finish, calendar, true);
	Participation participation = new Participation(employee, start);
	participation.setWorkdaysCalendar(calendar);
	Project project = new Project();
	project.addParticipation(participation);
	ProjectStatus projectStatus = new ProjectStatus(ProjectStatus.Status.ACTIVE, start);
	project.addStatus(projectStatus);
	WorkdaysCalendar calendar2 = new WorkdaysCalendar("calendar2");
	Day day2 = new Day(start, calendar2, false);
	Participation participation2 = new Participation(employee, finish);
	participation2.setWorkdaysCalendar(calendar2);
	Project project2 = new Project();
	project2.addParticipation(participation2);
	entityManager.persist(employee);
	entityManager.persist(calendar);
	entityManager.persist(day);
	entityManager.persist(day1);
	entityManager.persist(projectStatus);
	entityManager.persist(project);
	entityManager.persist(participation);
	entityManager.persist(calendar2);
	entityManager.persist(day2);
	entityManager.persist(project2);
	entityManager.persist(participation2);
	entityManager.flush();
	entityManager.clear();
	List<Participation> participations = Arrays.asList(participation, participation2);
	List<WorkdaysCalendar> calendars = Arrays.asList(calendar, calendar2);

	Map<Date, Boolean> actuals = workTimeService.getWorkSchedule(employee, period);

	assertEquals(2, actuals.size());
	assertTrue(actuals.containsKey(start));
	assertTrue(actuals.containsKey(finish));
	assertTrue("The day must be working if it's working in any of the employee's calendars", actuals.get(finish));
	assertTrue("The day must be working if it's working in any of the employee's calendars", actuals.get(start));
    }

    @Test
    public void testGetWorkSchedule_MultipleCalendars_SingleProject() throws NoSuchFieldException {
	Employee employee = new Employee("employee");
	employee.setWorkLoad(100);
	Date start = resetTime(new Date());
	Date finish = resetTime(getOffsetDate(1));
	Period period = new Period(start, finish);
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	Day day = new Day(start, calendar, true);
	Day day1 = new Day(finish, calendar, true);
	Participation participation = new Participation(employee, start);
	participation.setWorkdaysCalendar(calendar);
	WorkdaysCalendar calendar2 = new WorkdaysCalendar("calendar2");
	Day day2 = new Day(start, calendar2, false);
	Participation participation2 = new Participation(employee, finish);
	participation2.setWorkdaysCalendar(calendar2);
	Project project = new Project();
	project.addParticipation(participation);
	project.addParticipation(participation2);
	ProjectStatus projectStatus = new ProjectStatus(ProjectStatus.Status.ACTIVE, start);
	project.addStatus(projectStatus);
	entityManager.persist(employee);
	entityManager.persist(calendar);
	entityManager.persist(calendar2);
	entityManager.persist(day);
	entityManager.persist(day1);
	entityManager.persist(day2);
	entityManager.persist(participation2);
	entityManager.persist(participation);
	entityManager.persist(projectStatus);
	entityManager.persist(project);
	entityManager.flush();
	entityManager.clear();
	List<Participation> participations = Arrays.asList(participation2, participation);
	List<WorkdaysCalendar> calendars = Arrays.asList(calendar, calendar2);

	Map<Date, Boolean> actuals = workTimeService.getWorkSchedule(employee, period);

	assertEquals(2, actuals.size());
	assertTrue(actuals.containsKey(start));
	assertTrue(actuals.containsKey(finish));
	assertTrue("The day must be working if it's working in any of the employee's calendars", actuals.get(finish));
	assertTrue("The day must be working if it's working in any of the employee's calendars", actuals.get(start));
    }

    @Test
    public void testGetWorkSchedule_MustBePopulatedWithDatesWithinPeriod() throws NoSuchFieldException {
	Employee employee = new Employee("employee");
	Date start = resetTime(new Date());
	Date middle = resetTime(getOffsetDate(1));
	Date finish = resetTime(getOffsetDate(2));
	Period period = new Period(start, finish);
	entityManager.persist(employee);
	entityManager.flush();
	entityManager.clear();

	Map<Date, Boolean> actuals = workTimeService.getWorkSchedule(employee, period);

	assertEquals(3, actuals.size());
	assertTrue(actuals.containsKey(start));
	assertTrue(actuals.containsKey(middle));
	assertTrue(actuals.containsKey(finish));
    }

    @Test
    public void testGetWorkTimeProblems_ByEmployeeHoursWorkschedule() {
	Employee employee = new Employee("user");
	employee.setWorkLoad(100);
	Date today = new Date();
	Date yesterday = getOffsetDate(-1);
	Hours hours1 = new Hours();
	hours1.setDate(today);
	Hours hours2 = new Hours();
	hours2.setDate(yesterday);
	List<Hours> hoursList = Arrays.asList(hours1, hours2);
	Map<Date, Boolean> workSchedule = new HashMap<Date, Boolean>();
	workSchedule.put(today, Boolean.TRUE);
	workSchedule.put(yesterday, Boolean.TRUE);

	Map<Date, BigDecimal> actuals = workTimeService.getWorkTimeProblems(employee, hoursList, workSchedule);

	assertEquals(2, actuals.size());
	assertTrue(actuals.containsKey(today));
	assertEquals(new BigDecimal("-8.00"), actuals.get(today));
	assertTrue(actuals.containsKey(yesterday));
	assertEquals(new BigDecimal("-8.00"), actuals.get(yesterday));
    }

    @Test
    public void testGetWorkTimeProblems_ByEmployeeHoursWorkschedule_NoWorkLoad() {
	Employee employee = new Employee("user");
	employee.setWorkLoad(null);
	Date today = new Date();
	Date yesterday = getOffsetDate(-1);
	Hours hours1 = new Hours();
	hours1.setDate(today);
	Hours hours2 = new Hours();
	hours2.setDate(yesterday);
	List<Hours> hoursList = Arrays.asList(hours1, hours2);
	Map<Date, Boolean> workSchedule = new HashMap<Date, Boolean>();
	workSchedule.put(today, Boolean.TRUE);
	workSchedule.put(yesterday, Boolean.TRUE);

	Map<Date, BigDecimal> actuals = workTimeService.getWorkTimeProblems(employee, hoursList, workSchedule);

	assertTrue(actuals.isEmpty());
    }

    @Test
    public void testGetWorkTimeProblems_ByEmployeeHoursWorkschedule_WorkScheduleContainsHolidays() {
	Employee employee = new Employee("user");
	employee.setWorkLoad(100);
	Date today = new Date();
	Date yesterday = getOffsetDate(-1);
	Hours hours1 = new Hours();
	hours1.setDate(today);
	Hours hours2 = new Hours();
	hours2.setDate(yesterday);
	List<Hours> hoursList = Arrays.asList(hours1, hours2);
	Map<Date, Boolean> workSchedule = new HashMap<Date, Boolean>();
	workSchedule.put(today, Boolean.TRUE);
	workSchedule.put(yesterday, Boolean.FALSE);

	Map<Date, BigDecimal> actuals = workTimeService.getWorkTimeProblems(employee, hoursList, workSchedule);

	assertEquals(1, actuals.size());
	assertTrue(actuals.containsKey(today));
	assertEquals(new BigDecimal("-8.00"), actuals.get(today));
	assertFalse(actuals.containsKey(yesterday));
    }

    @Test
    public void testHasWorkTimeProblems() {
	BigDecimal value1 = new BigDecimal(10);
	BigDecimal value2 = new BigDecimal(10.00);

	boolean actual = workTimeService.hasWorkTimeProblems(value1, value2);

	assertFalse(actual);
    }

    @Test
    public void testHasWorkTimeProblems_hasProblem() {
	BigDecimal value1 = new BigDecimal(10);
	BigDecimal value2 = new BigDecimal(3);

	boolean actual = workTimeService.hasWorkTimeProblems(value1, value2);

	assertTrue(actual);
    }

    @Test
    public void testIsParticipationDate_ifDateExcluded() throws NoSuchFieldException {
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	Employee employee = new Employee();
	Date participationStart = new Date();
	Date participationFinish = getOffsetDate(2);
	Date notParticipationDate = getOffsetDate(3);
	Participation participation = new Participation();
	participation.setWorkdaysCalendar(calendar);
	participation.getPeriod().setStart(participationStart);
	participation.getPeriod().setFinish(participationFinish);
	participation.setEmployee(employee);
	Project project = new Project();
	project.addParticipation(participation);
	ProjectStatus projectStatus = new ProjectStatus(ProjectStatus.Status.ACTIVE, participationStart);
	project.addStatus(projectStatus);

	boolean actual = workTimeService.isParticipationDate(Arrays.asList(project), employee, calendar,
		notParticipationDate);

	assertFalse(actual);
    }

    @Test
    public void testIsParticipationDate_ifDateIncluded() throws NoSuchFieldException {
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	Employee employee = new Employee();
	Date participationStart = new Date();
	Date participationFinish = getOffsetDate(3);
	Date participationDate = getOffsetDate(2);
	Participation participation = new Participation();
	participation.setWorkdaysCalendar(calendar);
	participation.getPeriod().setStart(participationStart);
	participation.getPeriod().setFinish(participationFinish);
	participation.setEmployee(employee);
	Project project = new Project();
	project.addParticipation(participation);
	ProjectStatus projectStatus = new ProjectStatus(ProjectStatus.Status.ACTIVE, participationStart);
	project.addStatus(projectStatus);

	boolean actual = workTimeService.isParticipationDate(Arrays.asList(project), employee, calendar,
		participationDate);

	assertTrue(actual);
    }

    @Test
    public void testIsParticipationDate_ShouldIncludeLastParticipationDay() throws NoSuchFieldException {
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	Employee employee = new Employee();
	Date participationStart = new Date();
	Date participationFinish = getOffsetDate(3);
	Participation participation = new Participation();
	participation.setWorkdaysCalendar(calendar);
	participation.getPeriod().setStart(participationStart);
	participation.getPeriod().setFinish(participationFinish);
	participation.setEmployee(employee);
	Project project = new Project();
	project.addParticipation(participation);
	ProjectStatus projectStatus = new ProjectStatus(ProjectStatus.Status.ACTIVE, participationStart);
	project.addStatus(projectStatus);

	boolean actual = workTimeService
		.isParticipationDate(Arrays.asList(project), employee, calendar, participationFinish);

	assertTrue(actual);
    }

	@Test
	public void testIsParticipationDate_CheckProjectIsActive() {
		WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
		Employee employee = new Employee();
		Date start = getOffsetDate(-1);
		Date finish = getOffsetDate(1);
		Participation participation = new Participation();
		participation.setWorkdaysCalendar(calendar);
		participation.getPeriod().setStart(start);
		participation.getPeriod().setFinish(finish);
		participation.setEmployee(employee);
		Project project = new Project();
		project.addParticipation(participation);
		project.addStatus(new ProjectStatus(ProjectStatus.Status.ACTIVE, getOffsetDate(-4)));
		project.addStatus(new ProjectStatus(ProjectStatus.Status.FROZEN, getOffsetDate(-2)));

		boolean actual = workTimeService.isParticipationDate(Arrays.asList(project), employee, calendar, getOffsetDate(0));

		assertFalse(actual);
	}

	@Test
	public void testIsParticipationDate_CheckCalendarIsEqual() throws NoSuchFieldException {
		WorkdaysCalendar calendar1 = new WorkdaysCalendar("calendar1");
		setField(calendar1, "id", 1L);
		WorkdaysCalendar calendar2 = new WorkdaysCalendar("calendar2");
		setField(calendar2, "id", 2L);
		Employee employee = new Employee();
		Participation participation1 = new Participation(calendar1);
		participation1.getPeriod().setStart(getOffsetDate(0));
		participation1.setEmployee(employee);
		Participation participation2 = new Participation(calendar2);
		participation2.getPeriod().setStart(getOffsetDate(2));
		participation2.setEmployee(employee);
		Project project = new Project();
		project.addParticipation(participation1);
		project.addParticipation(participation2);
		project.addStatus(new ProjectStatus(ProjectStatus.Status.ACTIVE, getOffsetDate(-4)));

		boolean actual = workTimeService.isParticipationDate(Arrays.asList(project), employee, calendar2, getOffsetDate(0));

		assertFalse(actual);
	}

	@Test
	public void testGetRequiredWorkTime_PartTime() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Employee employee = new Employee("test_employee");
		employee.setWorkLoad(50);
		WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
		Date start = sdf.parse("2014-11-10");
		createActiveProjectWithParticipation(employee, start, calendar);

		Period period = new Period(sdf.parse("2014-11-10"), sdf.parse("2014-11-30"));
		BigDecimal actual = workTimeService.getRequiredWorkTime(employee, period);

		assertEquals(new BigDecimal("60.00"), actual);
	}

	@Test
	public void testGetRequiredWorkTime_CheckProjectInactive() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Employee employee = new Employee("test_employee");
		WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
		Date start = sdf.parse("2014-11-10");
		Date finish = sdf.parse("2014-11-20");
		Project project = createActiveProjectWithParticipation(employee, start, calendar);
		ProjectStatus inactiveStatus = new ProjectStatus(ProjectStatus.Status.FROZEN, finish);
		project.addStatus(inactiveStatus);
		entityManager.persist(inactiveStatus);
		entityManager.merge(project);

		Period period = new Period(sdf.parse("2014-11-01"), sdf.parse("2014-11-30"));
		BigDecimal actual = workTimeService.getRequiredWorkTime(employee, period);

		assertEquals(new BigDecimal("72.00"), actual);
	}

	@Test
	public void testGetRequiredWorkTime_CheckParticipationStart() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Employee employee = new Employee("test_employee");
		WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
		Project project = createActiveProjectWithParticipation(employee, sdf.parse("2014-11-01"), calendar);
		Participation participation = project.getTeam().get(0);
		participation.getPeriod().setStart(sdf.parse("2014-11-10"));
		entityManager.merge(participation);

		Period period = new Period(sdf.parse("2014-11-01"), sdf.parse("2014-11-30"));
		BigDecimal actual = workTimeService.getRequiredWorkTime(employee, period);

		assertEquals(new BigDecimal("120.00"), actual);
	}

	@Test
	public void testGetRequiredWorkTime_ProjectsWithDifferentCalendars() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Employee employee = new Employee("test_employee");
		Date start = sdf.parse("2014-11-10");
		WorkdaysCalendar calendar1 = new WorkdaysCalendar("calendar1");
		WorkdaysCalendar calendar2 = new WorkdaysCalendar("calendar2");
		createActiveProjectWithParticipation(employee, start, calendar1);
		createActiveProjectWithParticipation(employee, start, calendar2);
		Day workingDay = new Day(start, calendar1, true);
		Day nonworkingDay1 = new Day(start, calendar2, false);
		Day nonworkingDay2 = new Day(sdf.parse("2014-11-11"), calendar1, false);
		Day nonworkingDay3 = new Day(sdf.parse("2014-11-11"), calendar2, false);
		entityManager.persist(workingDay);
		entityManager.persist(nonworkingDay1);
		entityManager.persist(nonworkingDay2);
		entityManager.persist(nonworkingDay3);

		Period period = new Period(start, sdf.parse("2014-11-30"));
		BigDecimal actual = workTimeService.getRequiredWorkTime(employee, period);

		assertEquals(new BigDecimal("112.00"), actual);
	}

	private Project createActiveProjectWithParticipation(Employee employee, Date start, WorkdaysCalendar calendar) {
		Participation participation = new Participation(employee, start);
		participation.setWorkdaysCalendar(calendar);
		Project project = new Project();
		project.setDefaultWorkdaysCalendar(calendar);
		project.addParticipation(participation);
		project.getTeamMembers().add(employee);
		ProjectStatus activeStatus = new ProjectStatus(ProjectStatus.Status.ACTIVE, start);
		project.addStatus(activeStatus);
		entityManager.persist(calendar);
		entityManager.persist(employee);
		entityManager.persist(participation);
		entityManager.persist(activeStatus);
		entityManager.persist(project);
		return project;
	}

	@Test
	public void testGetWorkdaysCalendars() throws NoSuchFieldException {
		WorkdaysCalendar expected = new WorkdaysCalendar();
		setField(expected, "id", 1L);
		WorkdaysCalendar unexpected = new WorkdaysCalendar();
		setField(unexpected, "id", 2L);
		Employee employee1 = new Employee("employee1");
		Employee employee2 = new Employee("employee2");
		Period period = createPeriod(-1, 1);
		Participation participation1 = new Participation(employee1, getOffsetDate(0));
		participation1.setWorkdaysCalendar(expected);
		Participation participation2 = new Participation(employee2, getOffsetDate(0));
		participation2.setWorkdaysCalendar(unexpected);
		Project project = new Project();
		project.addParticipation(participation1);
		project.addParticipation(participation2);

		Set<WorkdaysCalendar> actual = workTimeService.getWorkdaysCalendars(Arrays.asList(project), employee1, period);

		assertEquals(Collections.singleton(expected), actual);
	}

	@Test
	public void testGetWorkdaysCalendars_CheckPeriod() throws NoSuchFieldException {
		WorkdaysCalendar expected = new WorkdaysCalendar();
		setField(expected, "id", 1L);
		WorkdaysCalendar unexpected = new WorkdaysCalendar();
		setField(unexpected, "id", 2L);
		Employee employee = new Employee("employee");
		Period period = createPeriod(0, 2);
		Participation participation1 = new Participation(employee, getOffsetDate(0));
		participation1.setWorkdaysCalendar(expected);
		Participation participation2 = new Participation(employee, getOffsetDate(3));
		participation2.setWorkdaysCalendar(unexpected);
		Participation participation3 = new Participation(employee, getOffsetDate(-2));
		participation3.getPeriod().setFinish(getOffsetDate(-1));
		participation3.setWorkdaysCalendar(unexpected);
		Project project = new Project();
		project.addParticipation(participation1);
		project.addParticipation(participation2);
		project.addParticipation(participation3);

		Set<WorkdaysCalendar> actual = workTimeService.getWorkdaysCalendars(Arrays.asList(project), employee, period);

		assertEquals(Collections.singleton(expected), actual);
	}
}
