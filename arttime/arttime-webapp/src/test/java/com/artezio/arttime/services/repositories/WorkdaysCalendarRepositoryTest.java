package com.artezio.arttime.services.repositories;

import static com.artezio.arttime.test.utils.CalendarUtils.getOffsetDate;
import static junitx.util.PrivateAccessor.setField;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.artezio.arttime.datamodel.Day;
import com.artezio.arttime.datamodel.Period;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.datamodel.WorkdaysCalendar;
import com.artezio.arttime.exceptions.WorkdaysCalendarRemoveException;

public class WorkdaysCalendarRepositoryTest {

    private EntityManager entityManager;
    private EntityManagerFactory entityManagerFactory;
    private WorkdaysCalendarRepository workdaysCalendarRepository;

    @Before
    public void setUp() throws Exception {
	workdaysCalendarRepository = new WorkdaysCalendarRepository();
	Map<String, String> properties = new HashMap<String, String>();
	properties.put("javax.persistence.validation.mode", "none");
	entityManagerFactory = Persistence.createEntityManagerFactory("test", properties);
	entityManager = entityManagerFactory.createEntityManager();
	setField(workdaysCalendarRepository, "entityManager", entityManager);
	entityManager.getTransaction().begin();
    }

    @After
    public void tearDown() throws Exception {
	if (entityManager.getTransaction().isActive()) {
	    if (entityManager.getTransaction().getRollbackOnly()) {
		entityManager.getTransaction().rollback();
	    } else {
		entityManager.getTransaction().commit();
	    }
	    entityManagerFactory.close();
	}
    }

    @Test
    public void testCreateWorkdaysCalendar() throws NoSuchFieldException {
	WorkdaysCalendar expected = new WorkdaysCalendar("expected calendar");

	WorkdaysCalendar actual = workdaysCalendarRepository.create(expected);

	assertSame(expected, actual);
    }

    @Test
    public void testCreateDays() {
	Date date1 = new GregorianCalendar(2011, 1, 3).getTime();
	Date date2 = new GregorianCalendar(2011, 1, 0).getTime();
	List<Date> dates = Arrays.asList(date1, date2);
	WorkdaysCalendar workdaysCalendar = new WorkdaysCalendar("calendar");

	List<Day> actuals = workdaysCalendarRepository.createDays(dates, workdaysCalendar);

	assertEquals(2, actuals.size());
	Day actual = actuals.get(0);
	assertEquals(date1, actual.getDate());
	assertEquals(workdaysCalendar.getName(), actual.getWorkdaysCalendar().getName());
    }

    @Test
    public void testCreateOrUpdateDays_Update() {
	WorkdaysCalendar workdaysCalendar = new WorkdaysCalendar("calendar-testCreateOrUpdateDays_Update");
	Day day = new Day(new Date(), workdaysCalendar);
	entityManager.persist(workdaysCalendar);
	entityManager.persist(day);
	WorkdaysCalendar calendar = new WorkdaysCalendar();
	String expected = "expected calendar name";
	calendar.setName(expected);
	entityManager.persist(calendar);

	day.setWorkdaysCalendar(calendar);
	workdaysCalendarRepository.update(Arrays.asList(day));

	assertEquals(expected, day.getWorkdaysCalendar().getName());
    }

    @Test
    public void testCreateOrUpdateDays_Create() {
	WorkdaysCalendar workdaysCalendar = new WorkdaysCalendar("calendarTtestCreateOrUpdateDays_Create");
	entityManager.persist(workdaysCalendar);
	Day expected = new Day(new Date(), workdaysCalendar);

	workdaysCalendarRepository.update(Arrays.asList(expected));

	assertNotNull(expected.getId());
	Day actual = entityManager.find(Day.class, expected.getId());
	assertEquals(expected, actual);
    }

    @Test
    public void testGetDays() {
	Date start = new Date();
	Date finish = getOffsetDate(2);
	Period period = new Period(start, finish);
	WorkdaysCalendar calendar = new WorkdaysCalendar("calendar");
	Day day = new Day(new Date(), calendar);
	entityManager.persist(calendar);
	entityManager.persist(day);
	entityManager.flush();
	entityManager.clear();

	List<Day> actual = workdaysCalendarRepository.getDays(calendar, period);

	assertNotNull(actual);
	assertEquals(3, actual.size());
    }

    @Test
    public void testGetPersistedDays_ShouldFilterByCalendar() {
	WorkdaysCalendar expectedCalendar = new WorkdaysCalendar();
	WorkdaysCalendar unexpectedCalendar = new WorkdaysCalendar();
	String expectedName = "expected calendar name";
	String unexpectedName = "unexpected calendar name";
	expectedCalendar.setName(expectedName);
	unexpectedCalendar.setName(unexpectedName);
	Date start = new Date();
	Date finish = getOffsetDate(1);
	Day expectedDay = new Day(start, expectedCalendar);
	Day unexpectedDay = new Day(start, unexpectedCalendar);
	entityManager.persist(expectedCalendar);
	entityManager.persist(unexpectedCalendar);
	entityManager.persist(expectedDay);
	entityManager.persist(unexpectedDay);

	Period period = new Period(start, finish);
	List<Day> actuals = workdaysCalendarRepository.getPersistedDays(expectedCalendar, period);

	assertEquals(1, actuals.size());
	assertTrue(actuals.contains(expectedDay));
    }

    @Test
    public void testGetPersistedDays_ShouldFilterByPeriod() {
	WorkdaysCalendar workdaysCalendar = new WorkdaysCalendar();
	workdaysCalendar.setName("calendarName");
	Date start = new GregorianCalendar(2011, 0, 2).getTime();
	Date finish = new GregorianCalendar(2011, 0, 29).getTime();
	Date expectedDate = new GregorianCalendar(2011, 0, 15).getTime();
	Date unexpectedDate1 = new GregorianCalendar(2011, 0, 1).getTime();
	Date unexpectedDate2 = new GregorianCalendar(2011, 0, 30).getTime();
	Period period = new Period(start, finish);
	Day expected1 = new Day(start, workdaysCalendar);
	Day expected2 = new Day(expectedDate, workdaysCalendar);
	Day expected3 = new Day(finish, workdaysCalendar);
	Day unexpected1 = new Day(unexpectedDate1, workdaysCalendar);
	Day unexpected2 = new Day(unexpectedDate2, workdaysCalendar);
	entityManager.persist(workdaysCalendar);
	entityManager.persist(expected1);
	entityManager.persist(expected3);
	entityManager.persist(expected2);
	entityManager.persist(unexpected1);
	entityManager.persist(unexpected2);

	List<Day> actuals = workdaysCalendarRepository.getPersistedDays(workdaysCalendar, period);

	assertEquals(3, actuals.size());
	assertFalse(actuals.contains(unexpected1));
	assertFalse(actuals.contains(unexpected2));
    }

    @Test
    public void testGetWorkdaysCalendars() {
	WorkdaysCalendar workdaysCalendar1 = new WorkdaysCalendar("calendar1");
	WorkdaysCalendar workdaysCalendar2 = new WorkdaysCalendar("calendar2");
	entityManager.persist(workdaysCalendar1);
	entityManager.persist(workdaysCalendar2);

	List<WorkdaysCalendar> actuals = workdaysCalendarRepository.getWorkdaysCalendars();

	assertEquals(2, actuals.size());
	assertTrue(actuals.contains(workdaysCalendar1));
	assertTrue(actuals.contains(workdaysCalendar2));
    }

    @Test(expected = PersistenceException.class)
    public void testRemoveWorkdays_RemoveForbidden() throws WorkdaysCalendarRemoveException {
	WorkdaysCalendar workdaysCalendar = new WorkdaysCalendar("calendar");
	entityManager.persist(workdaysCalendar);
	Project project = new Project();
	project.setDefaultWorkdaysCalendar(workdaysCalendar);
	entityManager.persist(project);

	workdaysCalendarRepository.remove(workdaysCalendar);
    }

    @Test
    public void testRemoveWorkdaysCalendar() throws WorkdaysCalendarRemoveException {
	WorkdaysCalendar workdaysCalendar = new WorkdaysCalendar("calendar");
	Day day = new Day(new Date(), workdaysCalendar);
	entityManager.persist(workdaysCalendar);
	entityManager.persist(day);
	Long dayId = day.getId();
	Long workdaysCalendarId = workdaysCalendar.getId();

	workdaysCalendarRepository.remove(workdaysCalendar);

	entityManager.flush();
	entityManager.clear();
	WorkdaysCalendar actualCalendar = entityManager.find(WorkdaysCalendar.class, workdaysCalendarId);
	Day actualDay = entityManager.find(Day.class, dayId);
	assertNull(actualCalendar);
	assertNull(actualDay);
    }

    @Test
    public void testUpdateWorkdaysCalendar() {
	WorkdaysCalendar workdaysCalendar = new WorkdaysCalendar("calendar");
	entityManager.persist(workdaysCalendar);
	String expected = "calendar new name";
	workdaysCalendar.setName(expected);

	workdaysCalendarRepository.update(workdaysCalendar, new ArrayList<Day>());
	WorkdaysCalendar actual = entityManager.find(WorkdaysCalendar.class, workdaysCalendar.getId());

	assertEquals(expected, actual.getName());
    }

}
