package com.artezio.arttime.web;

import static junitx.util.PrivateAccessor.setField;
import static org.easymock.EasyMock.createMockBuilder;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;

import org.easymock.EasyMockRunner;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.primefaces.model.StreamedContent;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.HourType;
import com.artezio.arttime.datamodel.Period;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.filter.Filter;
import com.artezio.arttime.report.ReportEngine;
import com.artezio.arttime.report.ReportTemplate;
import com.artezio.arttime.report.ReportUtils;
import com.artezio.arttime.web.criteria.RangePeriodSelector;

@RunWith(EasyMockRunner.class)
public class ReportsBeanTest {	
	
	private ReportsBean reportsBean;
	@Mock
    private FilterBean filterBean;
    @Mock
	private ReportEngine reportEngine;

	@Before
	public void setUp() {
		reportsBean = new ReportsBean();		
	}

	@Test
	public void testGetFile() throws SQLException, NamingException, IOException {
		reportsBean = createMockBuilder(ReportsBean.class)
				.addMockedMethod("makeReport")
				.addMockedMethod("getContentType")
				.addMockedMethod("getOutputFileName")
				.createMock();
		byte[] makedReport = new byte[1024];
		String contentType = ReportUtils.CONTENT_TYPE_PDF;
		String fileName = "report.pdf";
		expect(reportsBean.makeReport()).andReturn(makedReport);
		expect(reportsBean.getContentType()).andReturn(contentType);
		expect(reportsBean.getOutputFileName()).andReturn(fileName);
		replay(reportsBean);
		StreamedContent file = reportsBean.getFile();
		verify(reportsBean);
		assertNotNull(file);
	}

	@Test
	public void testMakeReport() throws SQLException, NamingException, IOException, NoSuchFieldException, ParseException {
		reportsBean = createMockBuilder(ReportsBean.class)
				.addMockedMethod("getSelectedEmployeeUserNames")
				.addMockedMethod("getSelectedDepartmens")
				.addMockedMethod("getSelectedProjectIds")
				.addMockedMethod("getSelectedHourTypeIds")				
				.addMockedMethod("getReportTemplate")
				.createMock();
		setField(reportsBean, "filterBean", filterBean);
		setField(reportsBean, "reportEngine", reportEngine);
		setField(reportsBean, "reportName", ReportTemplate.PROJECT_REPORT.toString());
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Date start = dateFormat.parse("01-01-2014");
		Date finish = dateFormat.parse("31-01-2014");
		Period period = new Period(start, finish);
		Filter filter = new Filter();
		RangePeriodSelector rangePeriodSelector = new RangePeriodSelector(period);
		filter.setRangePeriodSelector(rangePeriodSelector);
		expect(filterBean.getCurrentFilter()).andReturn(filter).times(2);
		expect(reportsBean.getSelectedEmployeeUserNames()).andReturn("#userName#");
		expect(reportsBean.getSelectedDepartmens()).andReturn("#department#");
		expect(reportsBean.getSelectedProjectIds()).andReturn("#1#");
		expect(reportsBean.getSelectedHourTypeIds()).andReturn("#1#");
		String reportFile = new String();
		expect(reportsBean.getReportTemplate(ReportTemplate.valueOf(reportsBean.getReportName()).getFileName())).andReturn(reportFile);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("startDate", new java.sql.Date(start.getTime()));
		params.put("endDate", new java.sql.Date(finish.getTime()));
		params.put("employeeUserNames", "#userName#");		
		params.put("departments", "#department#");
		params.put("projectIds", "#1#");
		params.put("hourTypeIds", "#1#");
		expect(reportEngine.getReport(reportFile, reportsBean.getReportType(), params)).andReturn(new byte[1024]);
		replay(reportsBean, reportEngine, filterBean);
		
		byte[] report = reportsBean.makeReport();
		
		verify(reportsBean, reportEngine, filterBean);
		assertNotNull(report);
	}

	@Test
	public void testGetReportType() throws NoSuchFieldException {
		setField(reportsBean, "reportType", ReportUtils.REPORT_TYPE_EXCEL);
		String actual = reportsBean.getReportType();
		assertEquals("REPORT_TYPE_EXCEL", actual);
	}
	
	@Test
	public void testGetReportType_defaultReportType() throws NoSuchFieldException {		
		String actual = reportsBean.getReportType();
		assertEquals("REPORT_TYPE_PDF", actual);
	}

	@Test
	public void testGetOutputFileName_pdfReportType() throws NoSuchFieldException {
		setField(reportsBean, "reportName", "reportName");
		setField(reportsBean, "reportType", ReportUtils.REPORT_TYPE_PDF);
		String actual = reportsBean.getOutputFileName();		
		assertEquals("reportName.pdf", actual);
	}
	
	@Test
	public void testGetOutputFileName_excelReportType() throws NoSuchFieldException {
		setField(reportsBean, "reportName", "reportName");
		setField(reportsBean, "reportType", ReportUtils.REPORT_TYPE_EXCEL);
		String actual = reportsBean.getOutputFileName();		
		assertEquals("reportName.xlsx", actual);
	}

	@Test
	public void testGetSelectedEmployeeUserNames() throws NoSuchFieldException {
		setField(reportsBean, "filterBean", filterBean);
		Filter filter = new Filter();
		filter.setEmployees(Arrays.asList(new Employee("userName1"), new Employee("userName2")));
		expect(filterBean.getCurrentFilter()).andReturn(filter);
		filter.getEmployees();
		replay(filterBean);
		
		String actual = reportsBean.getSelectedEmployeeUserNames();
		
		verify(filterBean);
		assertEquals("#userName1##userName2#", actual);
	}

	@Test
	public void testGetSelectedDepartmens() throws NoSuchFieldException {
		setField(reportsBean, "filterBean", filterBean);

		Filter filter = new Filter();
		filter.setDepartments(Arrays.asList("Minsk", "Vitebsk"));
		expect(filterBean.getCurrentFilter()).andReturn(filter);
		filter.getDepartments();
		replay(filterBean);
		
		String actual = reportsBean.getSelectedDepartmens();
		
		verify(filterBean);
		assertEquals("#Minsk##Vitebsk#", actual);
	}

	@Test
	public void testGetSelectedProjectIds() throws NoSuchFieldException {
		setField(reportsBean, "filterBean", filterBean);
		Filter filter = new Filter();
		Project project1 = new Project();
		setField(project1, "id", 1L);
		Project project2 = new Project();
		setField(project2, "id", 2L);
		List<Project> projects = Arrays.asList(project1, project2);
		filter.setProjects(projects);
		expect(filterBean.getCurrentFilter()).andReturn(filter);
		filter.getProjects();
		replay(filterBean);
		
		String actual = reportsBean.getSelectedProjectIds();
		
		verify(filterBean);
		
		assertEquals("#1##2#", actual);
	}

	@Test
	public void testGetSelectedHourTypeIds() throws NoSuchFieldException {		
		setField(reportsBean, "filterBean", filterBean);
		Filter filter = new Filter();
		HourType hourType1 = new HourType();
		setField(hourType1, "id",1L);
		HourType hourType2 = new HourType();
		setField(hourType2, "id",2L);
		List<HourType> hourTypes = Arrays.asList(hourType1, hourType2);
		filter.setHourTypes(hourTypes);
		expect(filterBean.getCurrentFilter()).andReturn(filter);
		filter.getHourTypes();
		replay(filterBean);
		
		String actual = reportsBean.getSelectedHourTypeIds();		
		
		verify(filterBean);
		
		assertEquals("#1##2#", actual);		
	}	
	

}
