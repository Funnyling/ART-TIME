package com.artezio.arttime.web;

import static junitx.util.PrivateAccessor.getField;
import static junitx.util.PrivateAccessor.setField;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.createMockBuilder;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;

import org.easymock.EasyMockRunner;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.Hours;
import com.artezio.arttime.datamodel.Participation;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.exceptions.SaveApprovedHoursException;
import com.artezio.arttime.filter.Filter;
import com.artezio.arttime.filter.FilterRepository;
import com.artezio.arttime.filter.FilterService;
import com.artezio.arttime.services.HoursService;
import com.artezio.arttime.services.repositories.HoursRepository;
import com.artezio.arttime.web.spread_sheet.SpreadSheet;

@RunWith(EasyMockRunner.class)
public class TimesheetBeanTest {
    private TimesheetBean bean;
    @Mock
    private FilterBean filterBean;
    @Mock
    private FilterService filterService;
    @Mock
    private FilterRepository filterRepository;
    @Mock
    private Employee loggedEmployee;
    @Mock
    private SpreadSheet spreadSheet;
    @Mock
    private HoursRepository hoursRepository;
    @Mock
    private HoursService hoursService;

    @Before
    public void setUp() {
	bean = new TimesheetBean();
    }

    @Test
    public void testGetSpreadSheet_ifNull() throws ReflectiveOperationException {
		setField(bean, "spreadSheet", null);
		setField(bean, "filterBean", filterBean);
		setField(bean, "filterService", filterService);
		setField(bean, "filterRepository", filterRepository);
		setField(bean, "loggedEmployee", loggedEmployee);
	
		Filter currentFilter = createMock(Filter.class);
		Filter personalTimesheetFilter = createMock(Filter.class);
	
		expect(filterBean.getCurrentFilter()).andReturn(currentFilter);
		expect(filterService.createPersonalTimesheetFilter(currentFilter, loggedEmployee)).andReturn(personalTimesheetFilter);
		filterRepository.fetchProjects(personalTimesheetFilter);
		replay(filterBean, filterService, filterRepository);
	
		SpreadSheet actual = bean.getSpreadSheet();
	
		verify(filterBean, filterService, filterRepository);
		assertNotNull(actual);
    }

    @Test
    public void testGetSpreadSheet_ifNotNull() throws ReflectiveOperationException {
	setField(bean, "spreadSheet", spreadSheet);

	SpreadSheet actual = bean.getSpreadSheet();

	assertSame(spreadSheet, actual);
    }

    @Test
    public void testSaveHours() throws SaveApprovedHoursException, ReflectiveOperationException {
    	bean = createMockBuilder(TimesheetBean.class).addMockedMethod("resetComponentsTree").createMock();
    	setField(bean, "spreadSheet", spreadSheet);
    	setField(bean, "hoursService", hoursService);
    	Set<Hours> hours = new HashSet<Hours>();
    	expect(spreadSheet.getUpdatedHours()).andReturn(hours);
    	hoursService.saveReportTime(hours);
    	bean.resetComponentsTree();
    	replay(spreadSheet, hoursService, bean);

    	bean.saveHours();

    	verify(spreadSheet, hoursService, bean);
    }

    @Test
    public void testResetData() throws NoSuchFieldException {
	bean = createMockBuilder(TimesheetBean.class).addMockedMethod("resetComponentsTree").createMock();
	setField(bean, "spreadSheet", spreadSheet);
	bean.resetComponentsTree();
	replay(bean);

	bean.resetData();

	verify(bean);
	assertNull(getField(bean, "spreadSheet"));
    }
    
    @Test
    public void testIsReadOnlyCell_readableCell() throws NoSuchFieldException, ParseException{
    	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    	Project project = new Project();
    	Employee employee = new Employee();
    	setField(employee, "userName", "mrFirst");    	
    	Participation teamMember = new Participation(employee, sdf.parse("02-03-2015"));
    	teamMember.getPeriod().setFinish(sdf.parse("31-03-2015"));    	
    	project.addParticipation(teamMember);
    	Hours hours = new Hours();
    	hours.setProject(project);
    	hours.setDate(sdf.parse("01-03-2015"));
    	hours.setEmployee(employee);
    	
    	boolean actual = bean.isReadOnlyCell(hours);
    	
    	assertTrue(actual);    	
    }
    
    @Test
    public void testIsReadOnlyCell_editableCell() throws NoSuchFieldException, ParseException{
    	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    	Project project = new Project();
    	Employee employee = new Employee();
    	setField(employee, "userName", "mrFirst");    	
    	Participation teamMember = new Participation(employee, sdf.parse("02-03-2015"));
    	teamMember.getPeriod().setFinish(sdf.parse("31-03-2015"));    	
    	project.addParticipation(teamMember);
    	Hours hours = new Hours();
    	hours.setProject(project);
    	hours.setDate(sdf.parse("10-03-2015"));
    	hours.setEmployee(employee);
    	
    	boolean actual = bean.isReadOnlyCell(hours);
    	
    	assertFalse(actual);    	
    }
}
