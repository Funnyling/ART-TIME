package com.artezio.arttime.filter;

import static junitx.util.PrivateAccessor.setField;
import static org.junit.Assert.*;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;

import junitx.framework.ListAssert;

import org.easymock.EasyMock;
import org.hibernate.Hibernate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.services.repositories.ProjectRepository;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Hibernate.class})
public class FilterRepositoryTest {
	private EntityManager entityManager;
    private EntityManagerFactory entityManagerFactory;
    private FilterRepository filterRepository;
    private ProjectRepository projectRepository;

    @Before
    public void setUp() throws Exception {
    	filterRepository = new FilterRepository();
    	projectRepository = EasyMock.createMock(ProjectRepository.class);
    	setField(filterRepository, "projectRepository", projectRepository);
    	
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("javax.persistence.validation.mode", "none");
		entityManagerFactory = Persistence.createEntityManagerFactory("test", properties);
		entityManager = entityManagerFactory.createEntityManager();
		setField(filterRepository, "entityManager", entityManager);
		entityManager.getTransaction().begin();
    }

    @After
    public void tearDown() throws Exception {
		if (entityManager.getTransaction().isActive()) {
		    if (entityManager.getTransaction().getRollbackOnly()) {
			entityManager.getTransaction().rollback();
		    } else {
			entityManager.getTransaction().commit();
		    }
		    entityManagerFactory.close();
		}
    }
    
    @Test
    public void testGetFiltersByOwner() {
    	Employee employee = new Employee("user1");
    	Filter filter1 = createFilter("user1");
    	Filter filter2 = createFilter("user2");
    	entityManager.persist(filter1);
    	entityManager.persist(filter2);
    	List<Filter> expected = Arrays.asList(filter1);
    	
    	List<Filter> actual = filterRepository.getFilters(employee);
    	
    	ListAssert.assertEquals(expected, actual);
    }

    private Filter createFilter(String owner) {
    	Filter filter = new Filter();
    	filter.setOwner(owner);
    	return filter;
    }
    
    private Filter createFilter(String owner, String filterName) {
    	Filter filter = createFilter(owner);
    	filter.setName(filterName);
    	return filter;
    }
    
    @Test
    public void testCreate() {
    	Filter filter = new Filter();
    	
    	Filter actual = filterRepository.create(filter);
    	
    	assertSame(filter, actual);
    	assertNotNull(actual.getId());
    }
    
    @Test
    public void testUpdate() {
    	Filter filter = new Filter();
    	entityManager.persist(filter);
    	filter.setName("new filter name");
    	
    	filterRepository.update(filter);
    	
    	Filter actual = entityManager.find(Filter.class, filter.getId());
    	    	
    	assertEquals(filter.getName(), actual.getName());
    }
    
    @Test
    public void testFetchDetails() throws NoSuchMethodException, SecurityException, NoSuchFieldException {
    	Filter filter = new Filter();
    	Project project = new Project(); 
    	List<Project> projects = Arrays.asList(project);
    	filter.setProjects(projects);
    	Project projectLoaded = new Project();
    	List<Project> projectsLoaded = Arrays.asList(projectLoaded);
    	
    	Method fetchCompleteMethod = ProjectRepository.class.getMethod("fetchComplete", List.class);
    	projectRepository = EasyMock.createMockBuilder(ProjectRepository.class).addMockedMethod(fetchCompleteMethod).createMock();
    	setField(filterRepository, "projectRepository", projectRepository);
    	
    	EasyMock.expect(projectRepository.fetchComplete(projects)).andReturn(projectsLoaded);
    	EasyMock.replay(projectRepository);
    	
    	filterRepository.fetchDetails(filter);
    	
    	EasyMock.verify(projectRepository);
    	
    	assertSame(projectLoaded, filter.getProjects().get(0));
    }
    
    @Test
    public void testRemove() {
    	Filter filter = new Filter();
    	entityManager.persist(filter);
    	
    	filterRepository.remove(filter);
    	
    	Filter actual = entityManager.find(Filter.class, filter.getId());
    	assertNull(actual);
    }
    
}
