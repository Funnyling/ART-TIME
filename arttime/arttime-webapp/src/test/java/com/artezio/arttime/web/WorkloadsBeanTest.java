package com.artezio.arttime.web;

import static junitx.util.PrivateAccessor.setField;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.services.repositories.EmployeeRepository;
import com.artezio.arttime.web.criteria.EmployeeSearchCriteriaBean;

public class WorkloadsBeanTest {
    private WorkloadsBean bean;
    private EmployeeRepository employeeRepository;
    private EmployeeSearchCriteriaBean employeeSearchCriteriaBean;

    @Before
    public void setUp() throws NoSuchFieldException {
	bean = new WorkloadsBean();
	employeeRepository = createMock(EmployeeRepository.class);
	employeeSearchCriteriaBean = createMock(EmployeeSearchCriteriaBean.class);
	setField(bean, "employeeRepository", employeeRepository);
	setField(bean, "employeeSearchCriteriaBean", employeeSearchCriteriaBean);
    }

    @Test
    public void testSave() throws NoSuchFieldException {
	Employee employee = new Employee();
	setField(bean, "employee", employee);
	expect(employeeRepository.update(employee)).andReturn(employee);
	replay(employeeRepository);

	bean.save();

	verify(employeeRepository);
    }

    @Test
    public void testGetEmployees() {
	expect(employeeSearchCriteriaBean.getSearchQuery()).andReturn("search");
	expect(employeeRepository.getEmployees("search")).andReturn(new ArrayList<Employee>());
	replay(employeeSearchCriteriaBean, employeeRepository);

	@SuppressWarnings("unused")
	List<Employee> actual = bean.getEmployees();

	verify(employeeSearchCriteriaBean, employeeRepository);
    }
}
