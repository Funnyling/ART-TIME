package com.artezio.arttime.datamodel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Test;

public class ParticipationTest {	
	
	 @Test
	 public void testValidation_ifValid() {
		 ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		 Validator validator = factory.getValidator();	         
		 Participation participation = new Participation();
		 participation.getPeriod().setStart(new Date());
		 participation.setEmployee(new Employee());
		 participation.setWorkdaysCalendar(new WorkdaysCalendar());
	        
	     Set<ConstraintViolation<Participation>> actual = validator.validate(participation); 
	         
	     assertTrue(actual.isEmpty());
	 }
	 
	 @Test
	 public void testValidation_ifNotValid() throws ParseException {
		 SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	   	 ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
	     Validator validator = factory.getValidator();
	     Participation participation = new Participation();
	     participation.getPeriod().setStart(dateFormat.parse("5-05-2014"));
		 participation.getPeriod().setFinish(dateFormat.parse("1-05-2014"));
		 participation.setEmployee(new Employee());
		 participation.setWorkdaysCalendar(new WorkdaysCalendar());
	        
	     Set<ConstraintViolation<Participation>> actual = validator.validate(participation); 
	         
	     assertEquals(1, actual.size());
	     assertEquals("Not valid date range", actual.iterator().next().getMessage());
	 }
	 
	 @Test
	 public void testGetPeriod() throws ParseException {
		 SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		 Participation participation = new Participation();
		 participation.getPeriod().setStart(sdf.parse("1-03-2014"));
		 participation.getPeriod().setFinish(sdf.parse("31-03-2014"));
		 Period expected = new Period(sdf.parse("1-03-2014"), sdf.parse("31-03-2014"));
		 
		 Period actual = participation.getPeriod();
		 
		 assertEquals(expected, actual);
	 }
	 	
}
