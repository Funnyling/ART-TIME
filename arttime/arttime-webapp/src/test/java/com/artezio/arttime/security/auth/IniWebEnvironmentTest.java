package com.artezio.arttime.security.auth;

import static junitx.util.PrivateAccessor.setField;
import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.config.Ini;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.easymock.EasyMockRunner;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.artezio.arttime.config.Settings;
import com.artezio.arttime.services.SettingsService;

import static org.easymock.EasyMock.*;

@RunWith(EasyMockRunner.class)
public class IniWebEnvironmentTest {
	@Mock
	private SettingsService settingsService;
	private IniWebEnvironment iniWebEnvironment;
	
	@Before
	public void setUp() {
		iniWebEnvironment = new IniWebEnvironment();
	}
	
	@Test
	public void testGetIni_ifIniNotNull() throws NoSuchFieldException {
		Ini expected = new Ini();
		setField(iniWebEnvironment, "ini", expected);
		
		Ini actual = iniWebEnvironment.getIni();
		
		assertSame(expected, actual);
	}
	
	@Test
	public void testGetIni_ifIniNull() throws NoSuchFieldException {
		Ini expected = new Ini();
		iniWebEnvironment = createMockBuilder(IniWebEnvironment.class)
				.addMockedMethod("getDefaultIni")
				.addMockedMethod("addLdapProperties")
				.createMock();		
		setField(iniWebEnvironment, "ini", null);
		expect(iniWebEnvironment.getDefaultIni()).andReturn(expected);
		iniWebEnvironment.addLdapProperties();
		replay(iniWebEnvironment);
		
		Ini actual = iniWebEnvironment.getIni();
		
		verify(iniWebEnvironment);
		assertSame(expected, actual);
	}
	
	@Test
	public void testAddLdapProperties() throws NoSuchFieldException {
		Ini ini = new Ini();
		Settings settings = new Settings();
		settings.setLdapBindDN("username");
		settings.setLdapBindCredentials("password");
		settings.setLdapServerAddress("localhost");
		settings.setLdapServerPort("356");
		settings.setLdapPrincipalSuffix("@company.com");
		settings.setExecRoleMemberOf("EXEC_GROUP");
		settings.setLdapUserContextDN("DC=MyCompany,DC=local");
		settings.setLdapCommonNameAttribute("CN");
		setField(iniWebEnvironment, "ini", ini);
		setField(iniWebEnvironment, "settingsService", settingsService);
		expect(settingsService.getSettings()).andReturn(settings);
		replay(settingsService);
		
		iniWebEnvironment.addLdapProperties();
		
		verify(settingsService);
		assertEquals("com.artezio.arttime.security.auth.ActiveDirectoryRealm", ini.getSectionProperty("main", "ldapRealm"));		
		assertEquals("ldap://localhost:356", ini.getSectionProperty("main", "ldapRealm.url"));			
		assertEquals("username", ini.getSectionProperty("main", "ldapRealm.systemUsername"));
		assertEquals("password", ini.getSectionProperty("main", "ldapRealm.systemPassword"));
		assertEquals("DC=MyCompany,DC=local", ini.getSectionProperty("main", "ldapRealm.searchBase"));
		assertEquals("@company.com", ini.getSectionProperty("main", "ldapRealm.principalSuffix"));
		assertEquals("\"CN=EXEC_GROUP,DC=MyCompany,DC=local\":\"exec\"", ini.getSectionProperty("main", "ldapRealm.groupRolesMap"));
	}
	
	@Test
	public void testBuildGroupRolesMap() {
		Settings settings = new Settings();
		settings.setLdapCommonNameAttribute("CN");
		settings.setExecRoleMemberOf("EXEC_GROUP,ONE-MORE-EXEC-GROUP");
		settings.setIntegrationClientGroups("INTEGRATION-CLIENT");
		settings.setLdapUserContextDN("DC=MyCompany,DC=local");
		String expected = "\"CN=INTEGRATION-CLIENT,DC=MyCompany,DC=local\":\"ArtTimeIntegrationClient\",\"CN=ONE-MORE-EXEC-GROUP,DC=MyCompany,DC=local\":\"exec\",\"CN=EXEC_GROUP,DC=MyCompany,DC=local\":\"exec\"";
		
		String actual = iniWebEnvironment.getGroupRolesMapProperty(settings);
		
		assertEquals(expected, actual);
		
	}

	@Test
	public void testAddGroupRoles() {					
		Map<String, String> expected = new HashMap<String, String>();
		expected.put("group1", "admin_role");
		expected.put("group2", "admin_role");
				
		Map<String, String> groupRolesMap = new HashMap<String, String>();
		
		iniWebEnvironment.addGroupRoles(groupRolesMap, new String[]{"group1","group2"},  "admin_role");
		
		assertEquals(groupRolesMap, expected);
		
	}
	
	@Test
	public void testAddGroupRoles_ifGroupAlreadyInMap() {					
		Map<String, String> expected = new HashMap<String, String>();
		expected.put("group", "admin_role,user_role");		
				
		Map<String, String> groupRolesMap = new HashMap<String, String>();
		groupRolesMap.put("group", "admin_role");
		
		iniWebEnvironment.addGroupRoles(groupRolesMap, new String[]{"group"},  "user_role");
		
		assertEquals(groupRolesMap, expected);
		
	}
}
