package com.artezio.arttime.services;

import static junitx.util.PrivateAccessor.setField;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.Participation;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.datamodel.TeamFilter;
import com.artezio.arttime.datamodel.TeamFilter.FilterType;
import com.artezio.arttime.services.integration.TeamTrackingSystem;
import com.artezio.arttime.services.repositories.ProjectRepository;

public class TeamSynchronizerTest {

    private TeamSynchronizer teamSynchronizer;
    private TeamTrackingSystem teamTrackingSystem;
    private Logger log;
    private ProjectRepository projectRepository;

    @Before
    public void setUp() throws NoSuchFieldException {
        teamSynchronizer = new TeamSynchronizer();
        teamTrackingSystem = createMock(TeamTrackingSystem.class);
        log = createMock(Logger.class);
        projectRepository = createMock(ProjectRepository.class);

        setField(teamSynchronizer, "teamTrackingSystem", teamTrackingSystem);
        setField(teamSynchronizer, "log", log);
        setField(teamSynchronizer, "projectRepository", projectRepository);
    }

    @Test
    public void testImportTeamByProject() {
        Project project = new Project();
        project.setTeamFilter(new TeamFilter(FilterType.PROJECT_CODES, "foreign code"));
        Employee employee = new Employee("employee");
        List<Employee> projectTeam = Arrays.asList(employee);
        expect(projectRepository.fetchComplete(project)).andReturn(project);
        expect(teamTrackingSystem.getTeam(project)).andReturn(projectTeam);
        expect(projectRepository.update(project)).andReturn(project);
        replay(teamTrackingSystem, projectRepository);

        teamSynchronizer.importTeam(project);

        verify(teamTrackingSystem, projectRepository);
        assertTrue(project.getTeam().size() > 0);
        Participation actualParticipation = project.getTeam().get(0);
        assertSame(employee, actualParticipation.getEmployee());
    }

    @Test
    public void testImportTeam_ShouldNotSendEmptyNotification() {
        Project project = new Project();
        List<Employee> newMembers = Collections.emptyList();
        expect(projectRepository.fetchComplete(project)).andReturn(project);
        expect(teamTrackingSystem.getTeam(project)).andReturn(newMembers);
        expect(projectRepository.update(project)).andReturn(project);
        replay(projectRepository, teamTrackingSystem);

        teamSynchronizer.importTeam(project);

        verify(projectRepository, teamTrackingSystem);
    }

    @Test
    public void testImportTeamLogging_WithNoNewMembers() {
        Project project = new Project();
        List<Employee> newMembers = Collections.emptyList();
        expect(projectRepository.fetchComplete(project)).andReturn(project);
        expect(teamTrackingSystem.getTeam(project)).andReturn(newMembers);
        expect(projectRepository.update(project)).andReturn(project);
        replay(projectRepository, teamTrackingSystem);

        teamSynchronizer.importTeam(project);

        verify(projectRepository, teamTrackingSystem);
    }

    @Test
    public void testImportTeamLogging_WithNewMembers() {
        Project project = new Project();
        List<Employee> newMembers = new ArrayList<Employee>();
        newMembers.add(new Employee("username", "firstname", "lastname", "email"));
        expect(projectRepository.fetchComplete(project)).andReturn(project);
        expect(teamTrackingSystem.getTeam(project)).andReturn(newMembers);
        expect(projectRepository.update(project)).andReturn(project);
        replay(projectRepository, teamTrackingSystem);

        teamSynchronizer.importTeam(project);

        verify(projectRepository, teamTrackingSystem);
    }

}















