package com.artezio.arttime.datamodel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

import com.artezio.arttime.datamodel.TeamFilter.FilterType;

public class TeamFilterTest {
	private static Validator validator;

    @BeforeClass
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void testValidation() {
        TeamFilter teamFilter1 = new TeamFilter(FilterType.DISABLED, null);
        TeamFilter teamFilter2 = new TeamFilter(FilterType.NATIVE, "");
        TeamFilter teamFilter3 = new TeamFilter(FilterType.PROJECT_CODES, null);
        TeamFilter teamFilter4 = new TeamFilter(FilterType.NATIVE, "value");

        assertEquals(0, validator.validate(teamFilter1).size());
        assertEquals(1, validator.validate(teamFilter2).size());
        assertEquals(1, validator.validate(teamFilter3).size());
        assertEquals(0, validator.validate(teamFilter4).size());
    }

    @Test
    public void testIsValid() {
    	TeamFilter teamFilter1 = new TeamFilter(FilterType.DISABLED, null);
        TeamFilter teamFilter2 = new TeamFilter(FilterType.NATIVE, "");
        TeamFilter teamFilter3 = new TeamFilter(FilterType.PROJECT_CODES, null);
        TeamFilter teamFilter4 = new TeamFilter(FilterType.NATIVE, "value");

        assertTrue(teamFilter1.isValid());
        assertFalse(teamFilter2.isValid());
        assertFalse(teamFilter3.isValid());
        assertTrue(teamFilter4.isValid());
    }
}
