package com.artezio.arttime.filter;

import static junitx.util.PrivateAccessor.setField;
import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.artezio.arttime.services.ProjectsSelectionCriteria;
import org.easymock.EasyMockRunner;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.artezio.arttime.datamodel.*;
import com.artezio.arttime.exceptions.NotAllowedNameException;
import com.artezio.arttime.services.repositories.EmployeeRepository;
import com.artezio.arttime.services.repositories.HourTypeRepository;
import com.artezio.arttime.services.repositories.ProjectRepository;

@RunWith(EasyMockRunner.class)
public class FilterServiceTest {
	private FilterService filterService;
	@Mock
	private FilterRepository filterRepository;
	@Mock
	private EmployeeRepository employeeRepository;
	@Mock
	private HourTypeRepository hourTypeRepository;
	@Mock
	private ProjectRepository projectRepository;
	
	@Before
	public void setUp() {
		filterService = new FilterService();
	}
	
	@Test
	public void testGetDefaultFilter_byEmployee() throws NoSuchFieldException {
		setField(filterService, "employeeRepository", employeeRepository);
		setField(filterService, "hourTypeRepository", hourTypeRepository);
		setField(filterService, "projectRepository", projectRepository);
		Employee employee = new Employee("user1");
        ProjectsSelectionCriteria ownProjectsCriteria = new ProjectsSelectionCriteria(employee);
        setField(filterService, "ownProjectsCriteria", ownProjectsCriteria);

		expect(employeeRepository.getDepartments()).andReturn(Collections.<String>emptyList());
		expect(hourTypeRepository.getAll()).andReturn(Collections.<HourType>emptyList());
		expect(projectRepository.getProjects(ownProjectsCriteria)).andReturn(Collections.<Project>emptyList());
		expect(employeeRepository.getAll()).andReturn(Collections.<Employee>emptyList());
		replay(employeeRepository, hourTypeRepository, projectRepository);
		
		Filter actual = filterService.getDefaultFilter(employee);
		
		verify(employeeRepository, hourTypeRepository, projectRepository);
		assertEquals(employee.getUserName(), actual.getOwner());
		assertEquals(FilterService.DEFAULT_FILTER_NAME, actual.getName());
	}
	

    @Test
    public void testSetOnlyOwnActiveProjects() throws NoSuchFieldException {
        setField(filterService, "projectRepository", projectRepository);
        Employee employee = new Employee("user1");
        ProjectsSelectionCriteria ownProjectsCriteria = new ProjectsSelectionCriteria(employee);
        setField(filterService, "ownProjectsCriteria", ownProjectsCriteria);
        ArrayList<Project> unexpected = new ArrayList<>();
        Filter filter = new Filter();
        filter.setProjects(unexpected);

        List<Project> expected = new ArrayList<>();
        expect(projectRepository.getProjects(ownProjectsCriteria)).andReturn(expected);
        replay(projectRepository);

        filterService.setOnlyOwnActiveProjects(filter);

        verify(projectRepository);
        assertSame(expected, filter.getProjects());
    }
}
