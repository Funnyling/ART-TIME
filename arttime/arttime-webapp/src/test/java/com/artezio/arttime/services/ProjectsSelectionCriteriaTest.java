package com.artezio.arttime.services;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.artezio.arttime.datamodel.Employee;

public class ProjectsSelectionCriteriaTest {
	
	@Test
	public void testEquals_EmptyCriteria() {
		ProjectsSelectionCriteria criteria = new ProjectsSelectionCriteria();
		criteria.setManager(null);
		criteria.setStatus(null);
		ProjectsSelectionCriteria criteria2 = new ProjectsSelectionCriteria();
		criteria2.setManager(null);
		criteria2.setStatus(null);
		
		boolean actual = criteria.equals(criteria2);
		
		assertTrue(actual);
	}
	
	@Test
	public void testEquals_ByManager() {
		ProjectsSelectionCriteria criteria = new ProjectsSelectionCriteria();
		criteria.setManager(new Employee("user"));
		criteria.setStatus(null);
		ProjectsSelectionCriteria criteria2 = new ProjectsSelectionCriteria();
		criteria2.setManager(new Employee("user"));
		criteria2.setStatus(null);
		
		boolean actual = criteria.equals(criteria2);
		
		assertTrue(actual);
	}
	
	@Test
	public void testEquals_ByStatus() {
		ProjectsSelectionCriteria criteria = new ProjectsSelectionCriteria();
		criteria.setManager(null);
		criteria.setStatus(ProjectsSelectionCriteria.Status.ACTIVE);
		ProjectsSelectionCriteria criteria2 = new ProjectsSelectionCriteria();
		criteria2.setManager(null);
		criteria2.setStatus(ProjectsSelectionCriteria.Status.ACTIVE);
		
		boolean actual = criteria.equals(criteria2);
		
		assertTrue(actual);
	}
	
	@Test
	public void testEquals_ByManagerAndStatus() {
		ProjectsSelectionCriteria criteria = new ProjectsSelectionCriteria();
		criteria.setManager(new Employee("user"));
		criteria.setStatus(ProjectsSelectionCriteria.Status.ACTIVE);
		ProjectsSelectionCriteria criteria2 = new ProjectsSelectionCriteria();
		criteria2.setManager(new Employee("user"));
		criteria2.setStatus(ProjectsSelectionCriteria.Status.ACTIVE);
		
		boolean actual = criteria.equals(criteria2);
		
		assertTrue(actual);
	}
	
}
