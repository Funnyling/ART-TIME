package com.artezio.arttime.web;

import static junitx.util.PrivateAccessor.setField;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;

import junitx.framework.ListAssert;

import org.junit.Test;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.HourType;
import com.artezio.arttime.datamodel.Hours;
import com.artezio.arttime.datamodel.Project;
import com.artezio.arttime.test.utils.CalendarUtils;

public class HoursIndexedBundleTest {
	private HoursIndexedBundle bundle;

	@Test
	public void testGetHours_ByDate_HoursNotExists() {
		bundle = new HoursIndexedBundle(new ArrayList<Hours>());
		Date expectedDate = new Date();

		List<Hours> actual = bundle.getHours(expectedDate);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByDate_ExpectedHours() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		Date expectedDate = new Date();
		Hours expectedHours1 = new Hours();
		expectedHours1.setProject(expectedProject);
		expectedHours1.setDate(expectedDate);
		Hours expectedHours2 = new Hours();
		expectedHours2.setProject(expectedProject);
		expectedHours2.setDate(expectedDate);
		setField(expectedHours1, "id", 1L);
		setField(expectedHours2, "id", 2L);
		bundle = new HoursIndexedBundle(Arrays.asList(expectedHours1,
				expectedHours2));

		List<Hours> actual = bundle.getHours(expectedDate);

		assertEquals(2, actual.size());
		assertTrue(actual.contains(expectedHours1));
		assertTrue(actual.contains(expectedHours2));
	}

	@Test
	public void testGetHours_ByDate_UnexpectedDate() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		Date expectedDate = new Date();
		Date unexpectedDate = CalendarUtils.getOffsetDate(2);
		Hours unexpectedHours = new Hours();
		unexpectedHours.setProject(expectedProject);
		unexpectedHours.setDate(unexpectedDate);
		setField(unexpectedHours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(unexpectedHours));

		List<Hours> actual = bundle.getHours(expectedDate);

		assertNull(actual);
	}
	
	@Test
	public void testGetHours_ByEmployee_ExpectedHours() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		Employee expectedEmployee = new Employee("employee");
		Hours expectedHours1 = new Hours();
		expectedHours1.setProject(expectedProject);
		expectedHours1.setEmployee(expectedEmployee);
		setField(expectedHours1, "id", 1L);
		Hours expectedHours2 = new Hours();
		expectedHours2.setProject(expectedProject);
		expectedHours2.setEmployee(expectedEmployee);
		setField(expectedHours2, "id", 2L);
		bundle = new HoursIndexedBundle(Arrays.asList(expectedHours1,
				expectedHours2));

		List<Hours> actual = bundle.getHours(expectedEmployee);

		assertEquals(2, actual.size());
		assertTrue(actual.contains(expectedHours1));
		assertTrue(actual.contains(expectedHours2));
	}

	@Test
	public void testGetHours_ByEmployee_UnxpectedEmployee() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		Employee expectedEmployee = new Employee("employee");
		Employee unexpectedEmployee = new Employee("unexpected employee");
		Hours expectedHours = new Hours();
		expectedHours.setProject(expectedProject);
		expectedHours.setEmployee(unexpectedEmployee);
		setField(expectedHours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(expectedHours));

		List<Hours> actual = bundle.getHours(expectedEmployee);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByHourType_ExpectedHours() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		Hours expectedHours1 = new Hours();
		expectedHours1.setProject(expectedProject);
		expectedHours1.setType(expectedType);
		setField(expectedHours1, "id", 1L);
		Hours expectedHours2 = new Hours();
		expectedHours2.setProject(expectedProject);
		expectedHours2.setType(expectedType);
		setField(expectedHours2, "id", 2L);
		bundle = new HoursIndexedBundle(Arrays.asList(expectedHours1,
				expectedHours2));

		List<Hours> actual = bundle.getHours(expectedType);

		assertEquals(2, actual.size());
		assertTrue(actual.contains(expectedHours1));
		assertTrue(actual.contains(expectedHours2));
	}

	@Test
	public void testGetHours_ByHourtype_UnxpectedHourtype() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		HourType unexpectedType = new HourType("unexpected type");
		Hours expectedHours = new Hours();
		expectedHours.setProject(expectedProject);
		expectedHours.setType(unexpectedType);
		setField(expectedHours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(expectedHours));

		List<Hours> actual = bundle.getHours(expectedType);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByHourtypeDate_ExpectedHours() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		Date expectedDate = new Date();
		Hours expectedHours1 = new Hours();
		expectedHours1.setProject(expectedProject);
		expectedHours1.setType(expectedType);
		expectedHours1.setDate(expectedDate);
		setField(expectedHours1, "id", 1L);
		Hours expectedHours2 = new Hours();
		expectedHours2.setProject(expectedProject);
		expectedHours2.setType(expectedType);
		expectedHours2.setDate(expectedDate);
		setField(expectedHours2, "id", 2L);
		bundle = new HoursIndexedBundle(Arrays.asList(expectedHours1,
				expectedHours2));

		List<Hours> actual = bundle.getHours(expectedType, expectedDate);

		assertEquals(2, actual.size());
		assertTrue(actual.contains(expectedHours1));
		assertTrue(actual.contains(expectedHours2));
	}

	@Test
	public void testGetHours_ByHourtypeDate_UnexpectedHourtype() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		HourType unexpectedType = new HourType("unexpected type");
		Date expectedDate = new Date();
		Hours unexpectedHours = new Hours();
		unexpectedHours.setProject(expectedProject);
		unexpectedHours.setType(unexpectedType);
		unexpectedHours.setDate(expectedDate);
		setField(unexpectedHours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(unexpectedHours));

		List<Hours> actual = bundle.getHours(expectedType, expectedDate);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByHourtypeDate_UnexpectedDate() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		Date expectedDate = new Date();
		Date unexpectedDate = CalendarUtils.getOffsetDate(2);
		Hours unexpectedHours = new Hours();
		unexpectedHours.setProject(expectedProject);
		unexpectedHours.setType(expectedType);
		unexpectedHours.setDate(unexpectedDate);
		setField(unexpectedHours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(unexpectedHours));

		List<Hours> actual = bundle.getHours(expectedType, expectedDate);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByHourtypeEmployee_ExpectedHours() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		Employee expectedEmployee = new Employee("employee");
		Hours expectedHours1 = new Hours();
		expectedHours1.setProject(expectedProject);
		expectedHours1.setType(expectedType);
		expectedHours1.setEmployee(expectedEmployee);
		setField(expectedHours1, "id", 1L);
		Hours expectedHours2 = new Hours();
		expectedHours2.setProject(expectedProject);
		expectedHours2.setType(expectedType);
		expectedHours2.setEmployee(expectedEmployee);
		setField(expectedHours2, "id", 2L);
		bundle = new HoursIndexedBundle(Arrays.asList(expectedHours1,
				expectedHours2));

		List<Hours> actual = bundle.getHours(expectedType, expectedEmployee);

		assertEquals(2, actual.size());
		assertTrue(actual.contains(expectedHours1));
		assertTrue(actual.contains(expectedHours2));
	}

	@Test
	public void testGetHours_ByHourtypeEmployee_UnexpectedHourtype() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		HourType unexpectedType = new HourType("unexpected type");
		Employee expectedEmployee = new Employee("employee");
		Hours unexpectedHours = new Hours();
		unexpectedHours.setProject(expectedProject);
		unexpectedHours.setType(unexpectedType);
		unexpectedHours.setEmployee(expectedEmployee);
		setField(unexpectedHours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(unexpectedHours));

		List<Hours> actual = bundle.getHours(expectedType, expectedEmployee);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByHourtypeEmployee_UnexpectedEmployee() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		Employee expectedEmployee = new Employee("employee");
		Employee unexpectedEmployee = new Employee("unexpected employee");
		Hours unexpectedHours = new Hours();
		unexpectedHours.setProject(expectedProject);
		unexpectedHours.setType(expectedType);
		unexpectedHours.setEmployee(unexpectedEmployee);
		setField(unexpectedHours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(unexpectedHours));

		List<Hours> actual = bundle.getHours(expectedType, expectedEmployee);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByHourtypeEmployeeDate_ExpectedHours() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		Employee expectedEmployee = new Employee("employee");
		Date expectedDate = new Date();
		Hours expectedHours1 = new Hours();
		expectedHours1.setProject(expectedProject);
		expectedHours1.setType(expectedType);
		expectedHours1.setEmployee(expectedEmployee);
		expectedHours1.setDate(expectedDate);
		setField(expectedHours1, "id", 1L);
		Hours expectedHours2 = new Hours();
		expectedHours2.setProject(expectedProject);
		expectedHours2.setType(expectedType);
		expectedHours2.setEmployee(expectedEmployee);
		expectedHours2.setDate(expectedDate);
		setField(expectedHours2, "id", 2L);
		bundle = new HoursIndexedBundle(Arrays.asList(expectedHours1,
				expectedHours2));

		List<Hours> actual = bundle.getHours(expectedType, expectedEmployee,
				expectedDate);

		assertEquals(2, actual.size());
		assertTrue(actual.contains(expectedHours1));
		assertTrue(actual.contains(expectedHours2));
	}

	@Test
	public void testGetHours_ByHourtypeEmployeeDate_UnexpectedHourtype() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		HourType unexpectedType = new HourType("unexpected type");
		Employee expectedEmployee = new Employee("employee");
		Date expectedDate = new Date();
		Hours unexpectedHours = new Hours();
		unexpectedHours.setProject(expectedProject);
		unexpectedHours.setType(unexpectedType);
		unexpectedHours.setEmployee(expectedEmployee);
		unexpectedHours.setDate(expectedDate);
		setField(unexpectedHours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(unexpectedHours));

		List<Hours> actual = bundle.getHours(expectedType, expectedEmployee,
				expectedDate);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByHourtypeEmployeeDate_UnexpectedEmployee() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		Employee expectedEmployee = new Employee("employee");
		Employee unexpectedEmployee = new Employee("unexpected employee");
		Date expectedDate = new Date();
		Hours unexpectedHours = new Hours();
		unexpectedHours.setProject(expectedProject);
		unexpectedHours.setType(expectedType);
		unexpectedHours.setEmployee(unexpectedEmployee);
		unexpectedHours.setDate(expectedDate);
		setField(unexpectedHours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(unexpectedHours));

		List<Hours> actual = bundle.getHours(expectedType, expectedEmployee,
				expectedDate);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByHourtypeEmployeeDate_UnexpectedDate() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		Employee expectedEmployee = new Employee("employee");
		Date expectedDate = new Date();
		Date unexpectedDate = CalendarUtils.getOffsetDate(2);
		Hours unexpectedHours = new Hours();
		unexpectedHours.setProject(expectedProject);
		unexpectedHours.setType(expectedType);
		unexpectedHours.setEmployee(expectedEmployee);
		unexpectedHours.setDate(unexpectedDate);
		setField(unexpectedHours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(unexpectedHours));

		List<Hours> actual = bundle.getHours(expectedType, expectedEmployee,
				expectedDate);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByProject_ExpectedHours() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		Hours expectedHours1 = new Hours();
		expectedHours1.setProject(expectedProject);
		setField(expectedHours1, "id", 1L);
		Hours expectedHours2 = new Hours();
		expectedHours2.setProject(expectedProject);
		setField(expectedHours2, "id", 2L);
		bundle = new HoursIndexedBundle(Arrays.asList(expectedHours1,
				expectedHours2));

		List<Hours> actual = bundle.getHours(expectedProject);

		assertEquals(2, actual.size());
		assertTrue(actual.contains(expectedHours1));
		assertTrue(actual.contains(expectedHours2));
	}

	@Test
	public void testGetHours_ByProject_UnexpectedProject() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		Project unexpectedProject = new Project();
		setField(expectedProject, "id", 2L);
		Hours unexpectedHours = new Hours();
		unexpectedHours.setProject(unexpectedProject);
		setField(unexpectedHours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(unexpectedHours));

		List<Hours> actual = bundle.getHours(expectedProject);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByProjectEmployee_ExpectedHours() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		Employee expectedEmployee = new Employee("employee");
		Hours expectedHours1 = new Hours();
		expectedHours1.setProject(expectedProject);
		expectedHours1.setEmployee(expectedEmployee);
		setField(expectedHours1, "id", 1L);
		Hours expectedHours2 = new Hours();
		expectedHours2.setProject(expectedProject);
		expectedHours2.setEmployee(expectedEmployee);
		setField(expectedHours2, "id", 2L);
		bundle = new HoursIndexedBundle(Arrays.asList(expectedHours1,
				expectedHours2));

		List<Hours> actual = bundle.getHours(expectedProject, expectedEmployee);

		assertEquals(2, actual.size());
		assertTrue(actual.contains(expectedHours1));
		assertTrue(actual.contains(expectedHours2));
	}

	@Test
	public void testGetHours_ByProjectEmployee_UnexpectedProject() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		Project unexpectedProject = new Project();
		setField(expectedProject, "id", 2L);
		Employee expectedEmployee = new Employee("employee");
		Hours unexpectedHours = new Hours();
		unexpectedHours.setProject(unexpectedProject);
		unexpectedHours.setEmployee(expectedEmployee);
		setField(unexpectedHours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(unexpectedHours));

		List<Hours> actual = bundle.getHours(expectedProject, expectedEmployee);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByProjectEmployee_UnexpectedEmployee() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		Employee expectedEmployee = new Employee("employee");
		Employee unexpectedEmployee = new Employee("unexpected employee");
		Hours unexpectedHours = new Hours();
		unexpectedHours.setProject(expectedProject);
		unexpectedHours.setEmployee(unexpectedEmployee);
		setField(unexpectedHours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(unexpectedHours));

		List<Hours> actual = bundle.getHours(expectedProject, expectedEmployee);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByProjectHourtype_ExpectedHours() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		Hours expectedHours1 = new Hours();
		expectedHours1.setProject(expectedProject);
		expectedHours1.setType(expectedType);
		setField(expectedHours1, "id", 1L);
		Hours expectedHours2 = new Hours();
		expectedHours2.setProject(expectedProject);
		expectedHours2.setType(expectedType);
		setField(expectedHours2, "id", 2L);
		bundle = new HoursIndexedBundle(Arrays.asList(expectedHours1,
				expectedHours2));

		List<Hours> actual = bundle.getHours(expectedProject, expectedType);

		assertEquals(2, actual.size());
		assertTrue(actual.contains(expectedHours1));
		assertTrue(actual.contains(expectedHours2));
	}

	@Test
	public void testGetHours_ByProjectHourtype_UnexpectedProject() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		Project unexpectedProject = new Project();
		setField(unexpectedProject, "id", 2L);
		HourType expectedType = new HourType("type");
		Hours unexpectedHours = new Hours();
		unexpectedHours.setProject(unexpectedProject);
		unexpectedHours.setType(expectedType);
		setField(unexpectedHours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(unexpectedHours));

		List<Hours> actual = bundle.getHours(expectedProject, expectedType);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByProjectHourtype_UnexpectedHourtype() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		HourType unexpectedType = new HourType("unexpected type");
		Hours unexpectedHours = new Hours();
		unexpectedHours.setProject(expectedProject);
		unexpectedHours.setType(unexpectedType);
		setField(unexpectedHours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(unexpectedHours));

		List<Hours> actual = bundle.getHours(expectedProject, expectedType);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByProjectHourtypeDate_ExpectedHours() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		Date expectedDate = new Date();
		Hours expectedHours1 = new Hours();
		expectedHours1.setProject(expectedProject);
		expectedHours1.setType(expectedType);
		expectedHours1.setDate(expectedDate);
		setField(expectedHours1, "id", 1L);
		Hours expectedHours2 = new Hours();
		expectedHours2.setProject(expectedProject);
		expectedHours2.setType(expectedType);
		expectedHours2.setDate(expectedDate);
		setField(expectedHours2, "id", 2L);
		bundle = new HoursIndexedBundle(Arrays.asList(expectedHours1,
				expectedHours2));

		List<Hours> actual = bundle.getHours(expectedProject, expectedType,
				expectedDate);

		assertEquals(2, actual.size());
		assertTrue(actual.contains(expectedHours1));
		assertTrue(actual.contains(expectedHours2));
	}

	@Test
	public void testGetHours_ByProjectHourtypeDate_UnexpectedProject() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		Project unexpectedProject = new Project();
		setField(expectedProject, "id", 2L);
		HourType expectedType = new HourType("type");
		Date expectedDate = new Date();
		Hours expectedHours1 = new Hours();
		expectedHours1.setProject(unexpectedProject);
		expectedHours1.setType(expectedType);
		expectedHours1.setDate(expectedDate);
		setField(expectedHours1, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(expectedHours1));

		List<Hours> actual = bundle.getHours(expectedProject, expectedType,
				expectedDate);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByProjectHourtypeDate_UnexpectedHourtype() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		HourType unexpectedType = new HourType("unexpected type");
		Date expectedDate = new Date();
		Hours expectedHours1 = new Hours();
		expectedHours1.setProject(expectedProject);
		expectedHours1.setType(unexpectedType);
		expectedHours1.setDate(expectedDate);
		setField(expectedHours1, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(expectedHours1));

		List<Hours> actual = bundle.getHours(expectedProject, expectedType,
				expectedDate);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByProjectHourtypeDate_UnexpectedDate() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		Date expectedDate = new Date();
		Date unexpectedDate = CalendarUtils.getOffsetDate(2);
		Hours expectedHours1 = new Hours();
		expectedHours1.setProject(expectedProject);
		expectedHours1.setType(expectedType);
		expectedHours1.setDate(unexpectedDate);
		setField(expectedHours1, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(expectedHours1));

		List<Hours> actual = bundle.getHours(expectedProject, expectedType,
				expectedDate);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByProjectHourtypeEmployee_ExpectedHours() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		Employee expectedEmployee = new Employee("employee");
		Hours expectedHours1 = new Hours();
		expectedHours1.setProject(expectedProject);
		expectedHours1.setType(expectedType);
		expectedHours1.setEmployee(expectedEmployee);
		setField(expectedHours1, "id", 1L);
		Hours expectedHours2 = new Hours();
		expectedHours2.setProject(expectedProject);
		expectedHours2.setType(expectedType);
		expectedHours2.setEmployee(expectedEmployee);
		setField(expectedHours2, "id", 2L);
		bundle = new HoursIndexedBundle(Arrays.asList(expectedHours1,
				expectedHours2));

		List<Hours> actual = bundle.getHours(expectedProject, expectedType,
				expectedEmployee);

		assertEquals(2, actual.size());
		assertTrue(actual.contains(expectedHours1));
		assertTrue(actual.contains(expectedHours2));
	}

	@Test
	public void testGetHours_ByProjectHourtypeEmployee_UnexpectedProject() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		Project unexpectedProject = new Project();
		setField(unexpectedProject, "id", 2L);
		HourType expectedType = new HourType("type");
		Employee expectedEmployee = new Employee("employee");
		Hours unexpectedHours = new Hours();
		unexpectedHours.setProject(unexpectedProject);
		unexpectedHours.setType(expectedType);
		unexpectedHours.setEmployee(expectedEmployee);
		setField(unexpectedHours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(unexpectedHours));

		List<Hours> actual = bundle.getHours(expectedProject, expectedType,
				expectedEmployee);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByProjectHourtypeEmployee_UnexpectedHourtype() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		HourType unexpectedType = new HourType("unexpected type");
		Employee expectedEmployee = new Employee("employee");
		Hours unexpectedHours = new Hours();
		unexpectedHours.setProject(expectedProject);
		unexpectedHours.setType(unexpectedType);
		unexpectedHours.setEmployee(expectedEmployee);
		setField(unexpectedHours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(unexpectedHours));

		List<Hours> actual = bundle.getHours(expectedProject, expectedType,
				expectedEmployee);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByProjectHourtypeEmployee_UnexpectedEmployee() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		Employee expectedEmployee = new Employee("employee");
		Employee unexpectedEmployee = new Employee("unexpected employee");
		Hours unexpectedHours = new Hours();
		unexpectedHours.setProject(expectedProject);
		unexpectedHours.setType(expectedType);
		unexpectedHours.setEmployee(unexpectedEmployee);
		setField(unexpectedHours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(unexpectedHours));

		List<Hours> actual = bundle.getHours(expectedProject, expectedType,
				expectedEmployee);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByProjectHourtypeEmployeeDate_ExpectedHours() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		Employee expectedEmployee = new Employee("employee");
		Date expectedDate = new Date();
		Hours expectedHours1 = new Hours();
		expectedHours1.setProject(expectedProject);
		expectedHours1.setType(expectedType);
		expectedHours1.setEmployee(expectedEmployee);
		expectedHours1.setDate(expectedDate);
		setField(expectedHours1, "id", 1L);
		Hours expectedHours2 = new Hours();
		expectedHours2.setProject(expectedProject);
		expectedHours2.setType(expectedType);
		expectedHours2.setEmployee(expectedEmployee);
		expectedHours2.setDate(expectedDate);
		setField(expectedHours2, "id", 2L);
		bundle = new HoursIndexedBundle(Arrays.asList(expectedHours1,
				expectedHours2));

		List<Hours> actual = bundle.getHours(expectedProject, expectedType,
				expectedEmployee, expectedDate);

		assertEquals(2, actual.size());
		assertTrue(actual.contains(expectedHours1));
		assertTrue(actual.contains(expectedHours2));
	}

	@Test
	public void testGetHours_ByProjectHourtypeEmployeeDate_UnexpectedProject() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		Project unexpectedProject = new Project();
		setField(unexpectedProject, "id", 2L);
		HourType expectedType = new HourType("type");
		Employee expectedEmployee = new Employee("employee");
		Date expectedDate = new Date();
		Hours unexpectedHours = new Hours();
		unexpectedHours.setProject(unexpectedProject);
		unexpectedHours.setType(expectedType);
		unexpectedHours.setEmployee(expectedEmployee);
		unexpectedHours.setDate(expectedDate);
		setField(unexpectedHours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(unexpectedHours));

		List<Hours> actual = bundle.getHours(expectedProject, expectedType,
				expectedEmployee, expectedDate);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByProjectHourtypeEmployeeDate_UnexpectedHourtype() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		HourType unexpectedType = new HourType("unexpected type");
		Employee expectedEmployee = new Employee("employee");
		Date expectedDate = new Date();
		Hours unexpectedHours = new Hours();
		unexpectedHours.setProject(expectedProject);
		unexpectedHours.setType(unexpectedType);
		unexpectedHours.setEmployee(expectedEmployee);
		unexpectedHours.setDate(expectedDate);
		setField(unexpectedHours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(unexpectedHours));

		List<Hours> actual = bundle.getHours(expectedProject, expectedType,
				expectedEmployee, expectedDate);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByProjectHourtypeEmployeeDate_UnexpectedEmployee() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		Employee expectedEmployee = new Employee("employee");
		Employee unexpectedEmployee = new Employee("unexpected employee");
		Date expectedDate = new Date();
		Hours unexpectedHours = new Hours();
		unexpectedHours.setProject(expectedProject);
		unexpectedHours.setType(expectedType);
		unexpectedHours.setEmployee(unexpectedEmployee);
		unexpectedHours.setDate(expectedDate);
		setField(unexpectedHours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(unexpectedHours));

		List<Hours> actual = bundle.getHours(expectedProject, expectedType,
				expectedEmployee, expectedDate);

		assertNull(actual);
	}

	@Test
	public void testGetHours_ByProjectHourtypeEmployeeDate_UnexpectedDate() throws NoSuchFieldException {
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedType = new HourType("type");
		Employee expectedEmployee = new Employee("employee");
		Date expectedDate = new Date();
		Date unexpectedDate = CalendarUtils.getOffsetDate(2);
		Hours unexpectedHours = new Hours();
		unexpectedHours.setProject(expectedProject);
		unexpectedHours.setType(expectedType);
		unexpectedHours.setEmployee(expectedEmployee);
		unexpectedHours.setDate(unexpectedDate);
		setField(unexpectedHours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(unexpectedHours));

		List<Hours> actual = bundle.getHours(expectedProject, expectedType,
				expectedEmployee, expectedDate);

		assertNull(actual);
	}

	
	@Test
	public void testGetUsedEmployee() throws NoSuchFieldException {
		Employee expectedEmployee1 = new Employee("expected employee 1");
		Employee expectedEmployee2 = new Employee("expected employee 2");
		Project project = new Project();
		Hours hours1 = new Hours();
		hours1.setProject(project);
		hours1.setEmployee(expectedEmployee1);
		Hours hours2 = new Hours();
		hours2.setProject(project);
		hours2.setEmployee(expectedEmployee2);
		setField(hours1, "id", 1L);
		setField(hours1, "id", 2L);
		bundle = new HoursIndexedBundle(Arrays.asList(hours1, hours2));

		Set<Employee> actual = bundle.getUsedEmployees();

		assertNotNull(actual);
		assertEquals(2, actual.size());
		assertTrue(actual.contains(expectedEmployee1));
		assertTrue(actual.contains(expectedEmployee2));
	}

	@Test
	public void testGetUsedEmployee_emptyBundle() {
		List<Hours> hours = new ArrayList<Hours>();
		bundle = new HoursIndexedBundle(hours);
		Set<Employee> actual = bundle.getUsedEmployees();
		assertNotNull(actual);
		assertTrue(actual.isEmpty());
	}

	@Test
	public void testGetUsedEmployee_byProject() throws NoSuchFieldException {
		Employee expectedEmployee1 = new Employee("expected employee 1");
		Employee expectedEmployee2 = new Employee("expected employee 2");
		Project project = new Project();
		Hours hours1 = new Hours();
		hours1.setProject(project);
		hours1.setEmployee(expectedEmployee1);
		Hours hours2 = new Hours();
		hours2.setProject(project);
		hours2.setEmployee(expectedEmployee2);
		setField(hours1, "id", 1L);
		setField(hours1, "id", 2L);
		bundle = new HoursIndexedBundle(Arrays.asList(hours1, hours2));
		Set<Employee> actual = bundle.getUsedEmployees(project);
		assertNotNull(actual);
		assertEquals(2, actual.size());
		assertTrue(actual.contains(expectedEmployee1));
		assertTrue(actual.contains(expectedEmployee2));
	}

	@Test
	public void testGetUsedEmployee_byProject_emptyBundle() {
		List<Hours> hours = new ArrayList<Hours>();
		Project project = new Project();
		bundle = new HoursIndexedBundle(hours);
		Set<Employee> actual = bundle.getUsedEmployees(project);
		assertNotNull(actual);
		assertTrue(actual.isEmpty());
	}

	@Test
	public void testGetUsedHourTypes_ByEmployee() throws NoSuchFieldException {
		Employee employee = new Employee("expected employee");
		Project project = new Project();
		HourType expectedHourType1 = new HourType("expected hourType 1");
		HourType expectedHourType2 = new HourType("expected hourType 2");
		Hours hours1 = new Hours();
		hours1.setProject(project);
		hours1.setEmployee(employee);
		hours1.setType(expectedHourType1);
		Hours hours2 = new Hours();
		hours2.setProject(project);
		hours2.setEmployee(employee);
		hours2.setType(expectedHourType2);
		setField(hours1, "id", 1L);
		setField(hours1, "id", 2L);
		bundle = new HoursIndexedBundle(Arrays.asList(hours1, hours2));

		Set<HourType> actual = bundle.getUsedHourTypes(employee);

		assertNotNull(actual);
		assertEquals(2, actual.size());
		assertTrue(actual.contains(expectedHourType1));
		assertTrue(actual.contains(expectedHourType2));
	}

	@Test
	public void testGetUsedHourTypes_ByEmployee_unexpectedEmployee() throws NoSuchFieldException {
		Employee unexpectedEmployee = new Employee("unexpected employee");
		Employee expectedEmployee = new Employee("expected employee");
		Project project = new Project();
		HourType expectedHourType = new HourType("expected hourType");
		Hours hours = new Hours();
		hours.setProject(project);
		hours.setEmployee(unexpectedEmployee);
		hours.setType(expectedHourType);
		setField(hours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(hours));

		Set<HourType> actual = bundle.getUsedHourTypes(expectedEmployee);

		assertNotNull(actual);
		assertTrue(actual.isEmpty());
	}

	@Test
	public void testGetUsedHourTypes_ByEmployee_bundleIsEmpty() {
		Employee expectedEmployee = new Employee("expected employee");
		bundle = new HoursIndexedBundle(new ArrayList<Hours>());

		Set<HourType> actual = bundle.getUsedHourTypes(expectedEmployee);

		assertNotNull(actual);
		assertTrue(actual.isEmpty());
	}

	@Test
	public void testGetUsedHourTypes_byProject() throws NoSuchFieldException {
		Project project = new Project();
		HourType expectedHourType1 = new HourType("expected hourType 1");
		HourType expectedHourType2 = new HourType("expected hourType 2");
		Hours hours1 = new Hours();
		hours1.setProject(project);
		hours1.setType(expectedHourType1);
		Hours hours2 = new Hours();
		hours2.setProject(project);
		hours2.setType(expectedHourType2);
		setField(hours1, "id", 1L);
		setField(hours1, "id", 2L);
		bundle = new HoursIndexedBundle(Arrays.asList(hours1, hours2));
		Set<HourType> actual = bundle.getUsedHourTypes(project);
		assertNotNull(actual);
		assertEquals(2, actual.size());
		assertTrue(actual.contains(expectedHourType1));
		assertTrue(actual.contains(expectedHourType2));
	}

	@Test
	public void testGetUsedProjects() throws NoSuchFieldException {
		Project expectedProject1 = new Project();
		Project expectedProject2 = new Project();
		setField(expectedProject1, "id", 1L);
		setField(expectedProject2, "id", 2L);
		Hours hours1 = new Hours();
		hours1.setProject(expectedProject1);
		Hours hours2 = new Hours();
		hours2.setProject(expectedProject2);
		setField(hours1, "id", 1L);
		setField(hours2, "id", 2L);
		bundle = new HoursIndexedBundle(Arrays.asList(hours1, hours2));
		Set<Project> actual = bundle.getUsedProjects();
		assertNotNull(actual);
		assertEquals(2, actual.size());
		assertTrue(actual.contains(expectedProject1));
		assertTrue(actual.contains(expectedProject2));
	}

	@Test
	public void testGetUsedProjects_ByEmployee() throws NoSuchFieldException {
		Employee expectedEmployee = new Employee("expected employee");
		Project expectedProject1 = new Project();
		Project expectedProject2 = new Project();
		setField(expectedProject1, "id", 1L);
		setField(expectedProject2, "id", 2L);
		Hours hours1 = new Hours();
		hours1.setEmployee(expectedEmployee);
		hours1.setProject(expectedProject1);
		Hours hours2 = new Hours();
		hours2.setEmployee(expectedEmployee);
		hours2.setProject(expectedProject2);
		setField(hours1, "id", 1L);
		setField(hours2, "id", 2L);
		bundle = new HoursIndexedBundle(Arrays.asList(hours1, hours2));

		Set<Project> actual = bundle.getUsedProjects(expectedEmployee);

		assertNotNull(actual);
		assertEquals(2, actual.size());
		assertTrue(actual.contains(expectedProject1));
		assertTrue(actual.contains(expectedProject2));
	}

	@Test
	public void testGetUsedProjects_ByEmployee_unexpectedEmployee() throws NoSuchFieldException {
		Employee expectedEmployee = new Employee("expected employee");
		Employee unexpectedEmployee = new Employee("unexpected employee");
		Project project = new Project();
		setField(project, "id", 1L);
		Hours hours1 = new Hours();
		hours1.setEmployee(unexpectedEmployee);
		hours1.setProject(project);
		setField(hours1, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(hours1));

		Set<Project> actual = bundle.getUsedProjects(expectedEmployee);

		assertTrue(actual.isEmpty());
	}

	@Test
	public void testGetUsedHourTypes_ByEmployeeProject() throws NoSuchFieldException {
		Employee expectedEmployee = new Employee("expected employee");
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType expectedHourType1 = new HourType("expected hourType 1");
		HourType expectedHourType2 = new HourType("expected hourType 2");
		Hours hours1 = new Hours();
		hours1.setProject(expectedProject);
		hours1.setEmployee(expectedEmployee);
		hours1.setType(expectedHourType1);
		Hours hours2 = new Hours();
		hours2.setProject(expectedProject);
		hours2.setEmployee(expectedEmployee);
		hours2.setType(expectedHourType2);
		setField(hours1, "id", 1L);
		setField(hours2, "id", 2L);
		bundle = new HoursIndexedBundle(Arrays.asList(hours1, hours2));

		Set<HourType> actual = bundle.getUsedHourTypes(expectedEmployee,
				expectedProject);

		assertEquals(2, actual.size());
		assertTrue(actual.contains(expectedHourType1));
		assertTrue(actual.contains(expectedHourType2));
	}

	@Test
	public void testGetUsedHourTypes_ByEmployeeProject_unexpectedEmployee() throws NoSuchFieldException {
		Employee expectedEmployee = new Employee("expected employee");
		Employee unexpectedEmployee = new Employee("unexpected employee");
		Project expectedProject = new Project();
		setField(expectedProject, "id", 1L);
		HourType hourType = new HourType("hourType");
		Hours hours = new Hours();
		hours.setProject(expectedProject);
		hours.setEmployee(unexpectedEmployee);
		hours.setType(hourType);
		setField(hours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(hours));

		Set<HourType> actual = bundle.getUsedHourTypes(expectedEmployee,
				expectedProject);

		assertTrue(actual.isEmpty());
	}

	@Test
	public void testGetUsedHourTypes_ByEmployeeProject_unexpectedProject() throws NoSuchFieldException {
		Employee expectedEmployee = new Employee("expected employee");
		Project expectedProject = new Project();
		Project unexpectedProject = new Project();
		setField(expectedProject, "id", 1L);
		setField(unexpectedProject, "id", 2L);
		HourType hourType = new HourType("hourType");
		Hours hours = new Hours();
		hours.setProject(unexpectedProject);
		hours.setEmployee(expectedEmployee);
		hours.setType(hourType);
		setField(hours, "id", 1L);
		bundle = new HoursIndexedBundle(Arrays.asList(hours));

		Set<HourType> actual = bundle.getUsedHourTypes(expectedEmployee,
				expectedProject);

		assertTrue(actual.isEmpty());
	}

	@Test
	public void testGetUsedHourTypes() throws NoSuchFieldException {
		Project project = new Project();
		HourType expectedType1 = new HourType("expected type 1");
		HourType expectedType2 = new HourType("expected type 2");
		Hours hours1 = new Hours();
		hours1.setProject(project);
		hours1.setType(expectedType1);
		Hours hours2 = new Hours();
		hours2.setProject(project);
		hours2.setType(expectedType2);
		setField(hours1, "id", 1L);
		setField(hours2, "id", 2L);
		bundle = new HoursIndexedBundle(Arrays.asList(hours1, hours2));

		Set<HourType> actual = bundle.getUsedHourTypes();

		assertEquals(2, actual.size());
		assertTrue(actual.contains(expectedType1));
		assertTrue(actual.contains(expectedType2));
	}
	
	@Test
	public void testAddHour() throws NoSuchFieldException {
		List<Hours> hours = new ArrayList<Hours>();
		bundle = new HoursIndexedBundle(hours);
		Project project = new Project();		
		Employee employee = new Employee();
		HourType hourType = new HourType();
		Date date = new Date();
		Hours hour = new Hours(project, date, employee, hourType);
		setField(hour, "id", 1L);
		List<Hours> expected = Arrays.asList(hour);
		
		bundle.add(hour);
		
		ListAssert.assertEquals(expected, bundle.getHours());
		ListAssert.assertEquals(expected, bundle.getHours(date));
		ListAssert.assertEquals(expected, bundle.getHours(employee));		
		ListAssert.assertEquals(expected, bundle.getHours(project));
		ListAssert.assertEquals(expected, bundle.getHours(hourType));
		ListAssert.assertEquals(expected, bundle.getHours(hourType, date));
		ListAssert.assertEquals(expected, bundle.getHours(hourType, employee));
		ListAssert.assertEquals(expected, bundle.getHours(project, employee));
		ListAssert.assertEquals(expected, bundle.getHours(project, hourType));
		ListAssert.assertEquals(expected, bundle.getHours(hourType, employee, date));
		ListAssert.assertEquals(expected, bundle.getHours(project, hourType, date));
		ListAssert.assertEquals(expected, bundle.getHours(project, hourType, employee));
		ListAssert.assertEquals(expected, bundle.getHours(project, hourType, employee, date));
	}
	
	@Test
	public void testGetSingleHours() {
		Project project = new Project();		
		Employee employee = new Employee();
		HourType hourType = new HourType();
		Date date = new Date();
		Hours hour = new Hours(project, date, employee, hourType);
		bundle = new HoursIndexedBundle(Arrays.asList(hour));
		
		Hours actual = bundle.getSingleHours(project, hourType, employee, date);
		
		assertSame(hour, actual);
	}
	
	@Test
	public void testGetSingleHours_ifNotExists() {
		Project project = new Project();		
		Employee employee = new Employee();
		HourType hourType = new HourType();
		Date date = new Date();
		bundle = new HoursIndexedBundle(new ArrayList<Hours>());
		
		Hours actual = bundle.getSingleHours(project, hourType, employee, date);
		
		assertNull(actual);
	}
	
	@Test
	public void testIsHoursExists() {
		Project project = new Project();		
		Employee employee = new Employee();
		HourType hourType = new HourType();
		Date date = new Date();
		Hours hour = new Hours(project, date, employee, hourType);
		bundle = new HoursIndexedBundle(Arrays.asList(hour));
		
		boolean actual = bundle.isHoursExists(project, hourType, employee, date);
		
		assertTrue(actual);
	}
	
	@Test
	public void testIsHoursExists_ifNotExists() {
		Project project = new Project();		
		Employee employee = new Employee();
		HourType hourType = new HourType();
		Date date = new Date();
		bundle = new HoursIndexedBundle(new ArrayList<Hours>());
		
		boolean actual = bundle.isHoursExists(project, hourType, employee, date);
		
		assertFalse(actual);
	}
}

