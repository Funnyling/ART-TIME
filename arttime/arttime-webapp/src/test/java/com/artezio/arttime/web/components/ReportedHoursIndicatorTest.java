package com.artezio.arttime.web.components;

import static junitx.util.PrivateAccessor.setField;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.createMockBuilder;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.*;

import java.math.BigDecimal;

import com.artezio.arttime.services.repositories.EmployeeRepository;
import com.artezio.arttime.services.repositories.HourTypeRepository;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;

import com.artezio.arttime.datamodel.Employee;
import com.artezio.arttime.datamodel.HourType;
import com.artezio.arttime.datamodel.Period;
import com.artezio.arttime.filter.Filter;
import com.artezio.arttime.services.WorkTimeService;
import com.artezio.arttime.services.repositories.HoursRepository;

public class ReportedHoursIndicatorTest {

    private ReportedHoursIndicator hoursIndicator;
    private HoursRepository hoursRepository;
    private WorkTimeService workTimeService;
    private EmployeeRepository employeeRepository;
    private Employee employee;
    private Period period;
    private Filter filter;
    private HourTypeRepository hourTypeRepository;


    @Before
    public void setUp() throws Exception {
        hoursIndicator = createMockBuilder(ReportedHoursIndicator.class).addMockedMethod("getFilter").createMock();
        employee = new Employee("employee");
        period = new Period();
        hoursRepository = createMock(HoursRepository.class);
        workTimeService = createMock(WorkTimeService.class);
        employeeRepository = createMock(EmployeeRepository.class);
        filter = createMock(Filter.class);
        hourTypeRepository = createMock(HourTypeRepository.class);

        hoursIndicator.setEmployee(employee);
        setField(hoursIndicator, "hoursRepository", hoursRepository);
        setField(hoursIndicator, "workTimeService", workTimeService);
        setField(hoursIndicator, "employeeRepository", employeeRepository);
        setField(hoursIndicator, "hourTypeRepository", hourTypeRepository);
    }

    @Test
    public void testGetRequiredTime() {

        BigDecimal expected = new BigDecimal("160");
        expect(hoursIndicator.getFilter()).andReturn(filter);
        expect(filter.getPeriod()).andReturn(period);
        expect(workTimeService.getRequiredWorkTime(employee, period)).andReturn(expected);
        replay(hoursIndicator, filter, workTimeService);

        BigDecimal actual = hoursIndicator.getRequiredTime();

        verify(hoursIndicator, filter, workTimeService);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetReportedTime() {
        BigDecimal expected = new BigDecimal("80");
        expect(hoursIndicator.getFilter()).andReturn(filter);
        expect(filter.getPeriod()).andReturn(period);
        expect(hoursRepository.getActualHoursSum(employee, period)).andReturn(expected);
        replay(hoursRepository, filter, hoursIndicator);

        BigDecimal actual = hoursIndicator.getReportedTime();

        verify(hoursRepository, filter, hoursIndicator);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetReportedTimePercents() {
        expect(hoursIndicator.getFilter()).andReturn(filter).times(2);
        expect(filter.getPeriod()).andReturn(period).times(2);
        expect(workTimeService.getRequiredWorkTime(employee, period)).andReturn(new BigDecimal("160"));
        expect(hoursRepository.getActualHoursSum(employee, period)).andReturn(new BigDecimal("100"));
        replay(hoursRepository, workTimeService, filter, hoursIndicator);

        int actual = hoursIndicator.getReportedTimePercents();

        assertEquals(62, actual);
        verify(hoursRepository, workTimeService, filter, hoursIndicator);
    }

    @Test
    public void testGetReportedTimePercents_RoundHalfDown() {
        expect(hoursIndicator.getFilter()).andReturn(filter).times(2);
        expect(filter.getPeriod()).andReturn(period).times(2);
        expect(workTimeService.getRequiredWorkTime(employee, period)).andReturn(new BigDecimal("160"));
        expect(hoursRepository.getActualHoursSum(employee, period)).andReturn(new BigDecimal("4"));
        replay(hoursRepository, workTimeService, filter, hoursIndicator);

        int actual = hoursIndicator.getReportedTimePercents();

        assertEquals(2, actual);
        verify(hoursRepository, workTimeService, filter, hoursIndicator);
    }

    @Test
    public void testGetOverTimePercents() {
        expect(hoursIndicator.getFilter()).andReturn(filter).times(2);
        expect(filter.getPeriod()).andReturn(period).times(2);
        expect(workTimeService.getRequiredWorkTime(employee, period)).andReturn(new BigDecimal("160"));
        expect(hoursRepository.getActualHoursSum(employee, period)).andReturn(new BigDecimal("180"));
        replay(hoursRepository, workTimeService, filter, hoursIndicator);

        int actual = hoursIndicator.getOverTimePercents();

        assertEquals(11, actual);
        verify(hoursRepository, workTimeService, filter, hoursIndicator);
    }

    @Test
    public void testIsTimeReportedIncorrectly_NotAllTimeReported() {
        expect(hoursIndicator.getFilter()).andReturn(filter).times(2);
        expect(filter.getPeriod()).andReturn(period).times(2);
        expect(workTimeService.getRequiredWorkTime(employee, period)).andReturn(new BigDecimal("160"));
        expect(hoursRepository.getActualHoursSum(employee, period)).andReturn(new BigDecimal("80"));
        replay(hoursRepository, workTimeService, filter, hoursIndicator);

        assertTrue(hoursIndicator.isTimeReportedIncorrectly());
        verify(hoursRepository, workTimeService, filter, hoursIndicator);
    }

    @Test
    public void testIsTimeReportedIncorrectly() {
        expect(hoursIndicator.getFilter()).andReturn(filter).times(2);
        expect(filter.getPeriod()).andReturn(period).times(2);
        expect(workTimeService.getRequiredWorkTime(employee, period)).andReturn(new BigDecimal("160"));
        expect(hoursRepository.getActualHoursSum(employee, period)).andReturn(new BigDecimal("160"));
        replay(hoursRepository, workTimeService, filter, hoursIndicator);

        assertFalse(hoursIndicator.isTimeReportedIncorrectly());
        verify(hoursRepository, workTimeService, filter, hoursIndicator);
    }

    @Test
    public void testIsTimeReportedIncorrectly_DifferentScale() {
        expect(hoursIndicator.getFilter()).andReturn(filter).times(2);
        expect(filter.getPeriod()).andReturn(period).times(2);
        expect(workTimeService.getRequiredWorkTime(employee, period)).andReturn(new BigDecimal("160"));
        expect(hoursRepository.getActualHoursSum(employee, period)).andReturn(new BigDecimal("160.00"));
        replay(hoursRepository, workTimeService, filter, hoursIndicator);

        assertFalse(hoursIndicator.isTimeReportedIncorrectly());
        verify(hoursRepository, workTimeService, filter, hoursIndicator);
    }

    @Test
    public void testRefreshEmployee() {
        Employee expected = new Employee("employee2");
        expect(employeeRepository.find(employee.getUserName())).andReturn(expected);
        replay(employeeRepository);

        hoursIndicator.refreshEmployee();

        verify(employeeRepository);
        assertSame(expected, hoursIndicator.getEmployee());
    }

    @Test
    public void testRefreshEmployee_IfEmployeeNotFound() {
        expect(employeeRepository.find(employee.getUserName())).andReturn(null);
        replay(employeeRepository);

        hoursIndicator.refreshEmployee();

        verify(employeeRepository);
        assertSame(employee, hoursIndicator.getEmployee());
    }
    
    @Test
    public void testGetWarningMessage_withoutActualTime(){
    	expect(hourTypeRepository.findActual()).andReturn(null);
    	replay(hourTypeRepository);
    	String message = hoursIndicator.getWarningMessage();
    	
    	verify(hourTypeRepository);
    	assertTrue(StringUtils.isEmpty(message));
    }
    
    @Test
    public void testGWarningMessage(){    	
    	expect(hourTypeRepository.findActual()).andReturn(new HourType());
    	replay(hourTypeRepository);
    	String message = hoursIndicator.getWarningMessage();
    	verify(hourTypeRepository);
    	assertTrue(!StringUtils.isEmpty(message));
    	
    }
}